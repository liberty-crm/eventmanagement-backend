using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using EventManagement.Domain;
using EventManagement.Domain.Entities;
using Microsoft.Extensions.Logging;

namespace EventManagement.Repository
{
    public class HistoryRepository : Repository<History, Guid>, IHistoryRepository
    {
        #region PRIVATE VARIABLE

        private readonly DbSet<History> _dbset;
        private readonly EventManagementContext _context;

        #endregion

        #region CONSTRUCTOR

        public HistoryRepository(EventManagementContext context, ILogger<HistoryRepository> logger) : base(context, logger)
        {
            _context = context;
            _dbset = context.Set<History>();
        }

        #endregion

        #region OVERRIDDEN IMPLEMENTATION

        public override string GetPrimaryKeyColumnName()
        {
            return "Id";
        }

        public override IQueryable<History> GetFilteredEntities(bool bIsAsTrackable = false)
        {
            return _dbset;
        }

        #endregion

    }
}
