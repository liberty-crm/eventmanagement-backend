using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using EventManagement.Domain;
using EventManagement.Domain.Entities;
using Microsoft.Extensions.Logging;

namespace EventManagement.Repository
{
    public class NotificationMailRepository : Repository<NotificationMail, Guid>, INotificationMailRepository
    {
        #region PRIVATE VARIABLE

        private readonly DbSet<NotificationMail> _dbset;
        private readonly EventManagementContext _context;

        #endregion

        #region CONSTRUCTOR

        public NotificationMailRepository(EventManagementContext context, ILogger<NotificationMailRepository> logger) : base(context, logger)
        {
            _context = context;
            _dbset = context.Set<NotificationMail>();
        }

        #endregion

        #region OVERRIDDEN IMPLEMENTATION

        public override string GetPrimaryKeyColumnName()
        {
            return "Id";
        }

        public override IQueryable<NotificationMail> GetFilteredEntities(bool bIsAsTrackable = false)
        {
            return _dbset;
        }

        #endregion

    }
}
