using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using EventManagement.Domain;
using EventManagement.Domain.Entities;
using Microsoft.Extensions.Logging;

namespace EventManagement.Repository
{
    public class AssetRepository : Repository<Asset, Guid>, IAssetRepository
    {
        #region PRIVATE VARIABLE

        private readonly DbSet<Asset> _dbset;
        private readonly EventManagementContext _context;

        #endregion

        #region CONSTRUCTOR

        public AssetRepository(EventManagementContext context, ILogger<AssetRepository> logger) : base(context, logger)
        {
            _context = context;
            _dbset = context.Set<Asset>();
        }

        #endregion

        #region OVERRIDDEN IMPLEMENTATION

        public override string GetPrimaryKeyColumnName()
        {
            return "Id";
        }

        public override IQueryable<Asset> GetFilteredEntities(bool bIsAsTrackable = false)
        {
            return _dbset;
        }

        #endregion

    }
}
