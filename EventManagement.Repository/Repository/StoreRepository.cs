using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using EventManagement.Domain;
using EventManagement.Domain.Entities;
using Microsoft.Extensions.Logging;

namespace EventManagement.Repository
{
    public class StoreRepository : Repository<Store, Guid>, IStoreRepository
    {
        #region PRIVATE VARIABLE

        private readonly DbSet<Store> _dbset;
        private readonly EventManagementContext _context;

        #endregion

        #region CONSTRUCTOR

        public StoreRepository(EventManagementContext context, ILogger<StoreRepository> logger) : base(context, logger)
        {
            _context = context;
            _dbset = context.Set<Store>();
        }

        #endregion

        #region OVERRIDDEN IMPLEMENTATION

        public override string GetPrimaryKeyColumnName()
        {
            return "Id";
        }

        public override IQueryable<Store> GetFilteredEntities(bool bIsAsTrackable = false)
        {
            return _dbset;
        }

        #endregion

    }
}
