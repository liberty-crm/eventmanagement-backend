using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using EventManagement.Domain;
using EventManagement.Domain.Entities;
using Microsoft.Extensions.Logging;

namespace EventManagement.Repository
{
    public class ServiceOfferedInvoiceRepository : Repository<ServiceOfferedInvoice, Guid>, IServiceOfferedInvoiceRepository
    {
        #region PRIVATE VARIABLE

        private readonly DbSet<ServiceOfferedInvoice> _dbset;
        private readonly EventManagementContext _context;

        #endregion

        #region CONSTRUCTOR

        public ServiceOfferedInvoiceRepository(EventManagementContext context, ILogger<ServiceOfferedInvoiceRepository> logger) : base(context, logger)
        {
            _context = context;
            _dbset = context.Set<ServiceOfferedInvoice>();
        }

        #endregion

        #region OVERRIDDEN IMPLEMENTATION

        public override string GetPrimaryKeyColumnName()
        {
            return "Id";
        }

        public override IQueryable<ServiceOfferedInvoice> GetFilteredEntities(bool bIsAsTrackable = false)
        {
            return _dbset;
        }

        #endregion

    }
}
