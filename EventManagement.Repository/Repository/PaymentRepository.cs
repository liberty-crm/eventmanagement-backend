using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using EventManagement.Domain;
using EventManagement.Domain.Entities;
using Microsoft.Extensions.Logging;

namespace EventManagement.Repository
{
    public class PaymentRepository : Repository<Payment, Guid>, IPaymentRepository
    {
        #region PRIVATE VARIABLE

        private readonly DbSet<Payment> _dbset;
        private readonly EventManagementContext _context;

        #endregion

        #region CONSTRUCTOR

        public PaymentRepository(EventManagementContext context, ILogger<PaymentRepository> logger) : base(context, logger)
        {
            _context = context;
            _dbset = context.Set<Payment>();
        }

        #endregion

        #region OVERRIDDEN IMPLEMENTATION

        public override string GetPrimaryKeyColumnName()
        {
            return "Id";
        }

        public override IQueryable<Payment> GetFilteredEntities(bool bIsAsTrackable = false)
        {
            return _dbset;
        }

        #endregion

    }
}
