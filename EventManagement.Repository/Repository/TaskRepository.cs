using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using EventManagement.Domain;
using EventManagement.Domain.Entities;
using Microsoft.Extensions.Logging;

namespace EventManagement.Repository
{
    public class TaskRepository : Repository<Task, Guid>, ITaskRepository
    {
        #region PRIVATE VARIABLE

        private readonly DbSet<Task> _dbset;
        private readonly EventManagementContext _context;

        #endregion

        #region CONSTRUCTOR

        public TaskRepository(EventManagementContext context, ILogger<TaskRepository> logger) : base(context, logger)
        {
            _context = context;
            _dbset = context.Set<Task>();
        }

        #endregion

        #region OVERRIDDEN IMPLEMENTATION

        public override string GetPrimaryKeyColumnName()
        {
            return "Id";
        }

        public override IQueryable<Task> GetFilteredEntities(bool bIsAsTrackable = false)
        {
            return _dbset;
        }

        #endregion

    }
}
