using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using EventManagement.Domain;
using EventManagement.Domain.Entities;
using Microsoft.Extensions.Logging;

namespace EventManagement.Repository
{
    public class LeadContactRepository : Repository<LeadContact, Guid>, ILeadContactRepository
    {
        #region PRIVATE VARIABLE

        private readonly DbSet<LeadContact> _dbset;
        private readonly EventManagementContext _context;

        #endregion

        #region CONSTRUCTOR

        public LeadContactRepository(EventManagementContext context, ILogger<LeadContactRepository> logger) : base(context, logger)
        {
            _context = context;
            _dbset = context.Set<LeadContact>();
        }

        #endregion

        #region OVERRIDDEN IMPLEMENTATION

        public override string GetPrimaryKeyColumnName()
        {
            return "Id";
        }

        public override IQueryable<LeadContact> GetFilteredEntities(bool bIsAsTrackable = false)
        {
            return _dbset;
        }

        #endregion

    }
}
