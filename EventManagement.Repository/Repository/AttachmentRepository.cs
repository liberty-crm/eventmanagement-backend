using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using EventManagement.Domain;
using EventManagement.Domain.Entities;
using Microsoft.Extensions.Logging;

namespace EventManagement.Repository
{
    public class AttachmentRepository : Repository<Attachment, Guid>, IAttachmentRepository
    {
        #region PRIVATE VARIABLE

        private readonly DbSet<Attachment> _dbset;
        private readonly EventManagementContext _context;

        #endregion

        #region CONSTRUCTOR

        public AttachmentRepository(EventManagementContext context, ILogger<AttachmentRepository> logger) : base(context, logger)
        {
            _context = context;
            _dbset = context.Set<Attachment>();
        }

        #endregion

        #region OVERRIDDEN IMPLEMENTATION

        public override string GetPrimaryKeyColumnName()
        {
            return "Id";
        }

        public override IQueryable<Attachment> GetFilteredEntities(bool bIsAsTrackable = false)
        {
            return _dbset;
        }

        #endregion

    }
}
