using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using EventManagement.Domain;
using EventManagement.Domain.Entities;
using Microsoft.Extensions.Logging;

namespace EventManagement.Repository
{
    public class FTCrmRepository : Repository<FTCrm, Guid>, IFTCrmRepository
    {
        #region PRIVATE VARIABLE

        private readonly DbSet<FTCrm> _dbset;
        private readonly EventManagementContext _context;

        #endregion

        #region CONSTRUCTOR

        public FTCrmRepository(EventManagementContext context, ILogger<FTCrmRepository> logger) : base(context, logger)
        {
            _context = context;
            _dbset = context.Set<FTCrm>();
        }

        #endregion

        #region OVERRIDDEN IMPLEMENTATION

        public override string GetPrimaryKeyColumnName()
        {
            return "Id";
        }

        public override IQueryable<FTCrm> GetFilteredEntities(bool bIsAsTrackable = false)
        {
            return _dbset;
        }

        #endregion

    }
}
