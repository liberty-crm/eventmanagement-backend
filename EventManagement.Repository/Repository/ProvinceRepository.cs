using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using EventManagement.Domain;
using EventManagement.Domain.Entities;
using Microsoft.Extensions.Logging;

namespace EventManagement.Repository
{
    public class ProvinceRepository : Repository<Province, Guid>, IProvinceRepository
    {
        #region PRIVATE VARIABLE

        private readonly DbSet<Province> _dbset;
        private readonly EventManagementContext _context;

        #endregion

        #region CONSTRUCTOR

        public ProvinceRepository(EventManagementContext context, ILogger<ProvinceRepository> logger) : base(context, logger)
        {
            _context = context;
            _dbset = context.Set<Province>();
        }

        #endregion

        #region OVERRIDDEN IMPLEMENTATION

        public override string GetPrimaryKeyColumnName()
        {
            return "Id";
        }

        public override IQueryable<Province> GetFilteredEntities(bool bIsAsTrackable = false)
        {
            return _dbset;
        }

        #endregion

    }
}
