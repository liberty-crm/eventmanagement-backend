using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using EventManagement.Domain;
using EventManagement.Domain.Entities;
using Microsoft.Extensions.Logging;

namespace EventManagement.Repository
{
    public class LeadRepository : Repository<Lead, Guid>, ILeadRepository
    {
        #region PRIVATE VARIABLE

        private readonly DbSet<Lead> _dbset;
        private readonly EventManagementContext _context;

        #endregion

        #region CONSTRUCTOR

        public LeadRepository(EventManagementContext context, ILogger<LeadRepository> logger) : base(context, logger)
        {
            _context = context;
            _dbset = context.Set<Lead>();
        }

        #endregion

        #region OVERRIDDEN IMPLEMENTATION

        public override string GetPrimaryKeyColumnName()
        {
            return "Id";
        }

        public override IQueryable<Lead> GetFilteredEntities(bool bIsAsTrackable = false)
        {
            return _dbset;
        }

        #endregion

    }
}
