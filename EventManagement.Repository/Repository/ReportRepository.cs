using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using EventManagement.Domain;
using EventManagement.Domain.Entities;
using Microsoft.Extensions.Logging;

namespace EventManagement.Repository
{
    public class ReportRepository : Repository<Report, Guid>, IReportRepository
    {
        #region PRIVATE VARIABLE

        private readonly DbSet<Report> _dbset;
        private readonly EventManagementContext _context;

        #endregion

        #region CONSTRUCTOR

        public ReportRepository(EventManagementContext context, ILogger<ReportRepository> logger) : base(context, logger)
        {
            _context = context;
            _dbset = context.Set<Report>();
        }

        #endregion

        #region OVERRIDDEN IMPLEMENTATION

        public override string GetPrimaryKeyColumnName()
        {
            return "Id";
        }

        public override IQueryable<Report> GetFilteredEntities(bool bIsAsTrackable = false)
        {
            return _dbset;
        }

        #endregion

    }
}
