using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using EventManagement.Domain;
using EventManagement.Domain.Entities;
using Microsoft.Extensions.Logging;

namespace EventManagement.Repository
{
    public class LeadServiceOfferedRepository : Repository<LeadServiceOffered, Guid>, ILeadServiceOfferedRepository
    {
        #region PRIVATE VARIABLE

        private readonly DbSet<LeadServiceOffered> _dbset;
        private readonly EventManagementContext _context;

        #endregion

        #region CONSTRUCTOR

        public LeadServiceOfferedRepository(EventManagementContext context, ILogger<LeadServiceOfferedRepository> logger) : base(context, logger)
        {
            _context = context;
            _dbset = context.Set<LeadServiceOffered>();
        }

        #endregion

        #region OVERRIDDEN IMPLEMENTATION

        public override string GetPrimaryKeyColumnName()
        {
            return "Id";
        }

        public override IQueryable<LeadServiceOffered> GetFilteredEntities(bool bIsAsTrackable = false)
        {
            return _dbset;
        }

        #endregion

    }
}
