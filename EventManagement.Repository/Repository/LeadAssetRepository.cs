using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using EventManagement.Domain;
using EventManagement.Domain.Entities;
using Microsoft.Extensions.Logging;

namespace EventManagement.Repository
{
    public class LeadAssetRepository : Repository<LeadAsset, Guid>, ILeadAssetRepository
    {
        #region PRIVATE VARIABLE

        private readonly DbSet<LeadAsset> _dbset;
        private readonly EventManagementContext _context;

        #endregion

        #region CONSTRUCTOR

        public LeadAssetRepository(EventManagementContext context, ILogger<LeadAssetRepository> logger) : base(context, logger)
        {
            _context = context;
            _dbset = context.Set<LeadAsset>();
        }

        #endregion

        #region OVERRIDDEN IMPLEMENTATION

        public override string GetPrimaryKeyColumnName()
        {
            return "Id";
        }

        public override IQueryable<LeadAsset> GetFilteredEntities(bool bIsAsTrackable = false)
        {
            return _dbset;
        }

        #endregion

    }
}
