using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using EventManagement.Domain;
using EventManagement.Domain.Entities;
using Microsoft.Extensions.Logging;

namespace EventManagement.Repository
{
    public class ServiceOfferedRepository : Repository<ServiceOffered, Guid>, IServiceOfferedRepository
    {
        #region PRIVATE VARIABLE

        private readonly DbSet<ServiceOffered> _dbset;
        private readonly EventManagementContext _context;

        #endregion

        #region CONSTRUCTOR

        public ServiceOfferedRepository(EventManagementContext context, ILogger<ServiceOfferedRepository> logger) : base(context, logger)
        {
            _context = context;
            _dbset = context.Set<ServiceOffered>();
        }

        #endregion

        #region OVERRIDDEN IMPLEMENTATION

        public override string GetPrimaryKeyColumnName()
        {
            return "Id";
        }

        public override IQueryable<ServiceOffered> GetFilteredEntities(bool bIsAsTrackable = false)
        {
            return _dbset;
        }

        #endregion

    }
}
