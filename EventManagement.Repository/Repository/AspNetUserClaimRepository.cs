using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using EventManagement.Domain;
using EventManagement.Domain.Entities;
using Microsoft.Extensions.Logging;
using FinanaceManagement.API.Models;

namespace EventManagement.Repository
{
    public class AspNetUserClaimRepository : Repository<AspNetUserClaim, int>, IAspNetUserClaimRepository
    {
        #region PRIVATE VARIABLE

        private readonly DbSet<AspNetUserClaim> _dbset;
        private readonly EventManagementContext _context;

        #endregion

        #region CONSTRUCTOR

        public AspNetUserClaimRepository(EventManagementContext context, ILogger<AspNetUserClaimRepository> logger) : base(context, logger)
        {
            _context = context;
            _dbset = context.Set<AspNetUserClaim>();
        }

        #endregion 

        #region OVERRIDDEN IMPLEMENTATION

        public override string GetPrimaryKeyColumnName()
        {
            return "Id";
        }

        public override IQueryable<AspNetUserClaim> GetFilteredEntities(bool bIsAsTrackable = false)
        {
            return _dbset;
        }

        #endregion

    }
}
