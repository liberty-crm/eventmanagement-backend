using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using EventManagement.Domain;
using EventManagement.Domain.Entities;
using Microsoft.Extensions.Logging;

namespace EventManagement.Repository
{
    public class ContactRepository : Repository<Contact, Guid>, IContactRepository
    {
        #region PRIVATE VARIABLE

        private readonly DbSet<Contact> _dbset;
        private readonly EventManagementContext _context;

        #endregion

        #region CONSTRUCTOR

        public ContactRepository(EventManagementContext context, ILogger<ContactRepository> logger) : base(context, logger)
        {
            _context = context;
            _dbset = context.Set<Contact>();
        }

        #endregion

        #region OVERRIDDEN IMPLEMENTATION

        public override string GetPrimaryKeyColumnName()
        {
            return "Id";
        }

        public override IQueryable<Contact> GetFilteredEntities(bool bIsAsTrackable = false)
        {
            return _dbset;
        }

        #endregion

    }
}
