using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
//using EventManagement.API.Helpers;
using EventManagement.Dto;
using EventManagement.Service;
using EventManagement.Domain.Entities;
using EventManagement.Utility;
using Microsoft.Extensions.Logging;
using IdentityServer4.AccessTokenValidation;

namespace EventManagement.API.Controllers
{
    /// <summary>
    /// LeadContact endpoint
    /// </summary>
    [Route("api/leadcontacts")]
    [Produces("application/json")]
    [ApiController]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]

    public class LeadContactController : Controller
    {

        #region PRIVATE MEMBERS

        private readonly ILeadContactService _leadcontactService;
        private ILogger<LeadContactController> _logger;
        private readonly IUrlHelper _urlHelper;

        #endregion


        #region CONSTRUCTOR

        public LeadContactController(ILeadContactService leadcontactService, ILogger<LeadContactController> logger, IUrlHelper urlHelper) 
        {
            _logger = logger;
            _leadcontactService = leadcontactService;
            _urlHelper = urlHelper;
        }

        #endregion


        #region HTTPGET

        [HttpGet(Name = "GetFilteredLeadContacts")]
        [Produces("application/vnd.tourmanagement.leadcontacts.hateoas+json", "application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<List<LeadContactDto>>> GetFilteredLeadContacts([FromQuery]FilterOptionsModel filterOptionsModel, [FromHeader(Name = "Accept")] string mediaType)
        {

            //if order by fields are not valid.
            if (!_leadcontactService.ValidMappingExists(filterOptionsModel.OrderBy))
            {
                //then return bad request.
                return BadRequest();
            }

            //if fields are not valid.
            if (!EventManagementUtils.TypeHasProperties<LeadContactDto>(filterOptionsModel.Fields))
            {
                //then return bad request.
                return BadRequest();
            }

            //get the paged/filtered show from db. 
            var leadcontactsFromRepo = await _leadcontactService.GetFilteredEntities(filterOptionsModel);

            //if HATEOAS links are required.
            if (mediaType == "application/vnd.tourmanagement.leadcontacts.hateoas+json")
            {
                //create HATEOAS links for each show.
                leadcontactsFromRepo.ForEach(leadcontact =>
                {
                    var entityLinks = CreateLinksForLeadContact(leadcontact.Id, filterOptionsModel.Fields);
                    leadcontact.links = entityLinks;
                });

                //prepare pagination metadata.
                var paginationMetadata = new
                {
                    totalCount = leadcontactsFromRepo.TotalCount,
                    pageSize = leadcontactsFromRepo.PageSize,
                    currentPage = leadcontactsFromRepo.CurrentPage,
                    totalPages = leadcontactsFromRepo.TotalPages,
                };

                //add pagination meta data to response header.
                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMetadata));

                //create links for shows.
                var links = CreateLinksForLeadContacts(filterOptionsModel, leadcontactsFromRepo.HasNext, leadcontactsFromRepo.HasPrevious);

                //prepare model with data and HATEOAS links.
                var linkedCollectionResource = new
                {
                    value = leadcontactsFromRepo,
                    links = links
                };

                //return the data with Ok response.
                return Ok(linkedCollectionResource);
            }
            else
            {
                var previousPageLink = leadcontactsFromRepo.HasPrevious ?
                    CreateLeadContactsResourceUri(filterOptionsModel, ResourceUriType.PreviousPage) : null;

                var nextPageLink = leadcontactsFromRepo.HasNext ?
                    CreateLeadContactsResourceUri(filterOptionsModel, ResourceUriType.NextPage) : null;

                //prepare the pagination metadata.
                var paginationMetadata = new
                {
                    previousPageLink = previousPageLink,
                    nextPageLink = nextPageLink,
                    totalCount = leadcontactsFromRepo.TotalCount,
                    pageSize = leadcontactsFromRepo.PageSize,
                    currentPage = leadcontactsFromRepo.CurrentPage,
                    totalPages = leadcontactsFromRepo.TotalPages
                };

                //add pagination meta data to response header.
                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMetadata));

                //return the data with Ok response.
                return Ok(leadcontactsFromRepo);
            }
        }


        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Produces("application/vnd.tourmanagement.leadcontacts.hateoas+json", "application/json")]
        [HttpGet("{id}", Name = "GetLeadContact")]
        public async Task<ActionResult<LeadContact>> GetLeadContact(Guid id, [FromQuery] string fields, [FromHeader(Name = "Accept")] string mediaType)
        {
            object leadcontactEntity = null;
            object linkedResourceToReturn = null;

            //if fields are not passed.
            if (string.IsNullOrEmpty(fields))
            {
                _logger.LogInformation("GetLeadContact called");

                //then get the whole entity and map it to the Dto.
                leadcontactEntity = Mapper.Map<LeadContactDto>(await _leadcontactService.GetEntityByIdAsync(id));
            }
            else
            {
                //if fields are passed then get the partial show.
                leadcontactEntity = await _leadcontactService.GetPartialEntityAsync(id, fields);
            }

            //if leadcontact not found.
            if (leadcontactEntity == null)
            {
                //then return not found response.
                return NotFound();
            }

            if (mediaType == "application/vnd.tourmanagement.leadcontacts.hateoas+json")
            {
                //create HATEOS links
                var links = CreateLinksForLeadContact(id, fields);

                //if fields are not passed.
                if (string.IsNullOrEmpty(fields))
                {
                    //convert the typed object to expando object.
                    linkedResourceToReturn = ((LeadContactDto)leadcontactEntity).ShapeData("") as IDictionary<string, object>;

                    //add the HATEOAS links to the model.
                    ((IDictionary<string, object>)linkedResourceToReturn).Add("links", links);
                }
                else
                {
                    linkedResourceToReturn = leadcontactEntity;

                    //add the HATEOAS links to the model.
                    ((dynamic)linkedResourceToReturn).links = links;

                }
            }
            else
            {
                linkedResourceToReturn = leadcontactEntity;
            }

            //return the Ok response.
            return Ok(linkedResourceToReturn);
        }

        #endregion


        #region HTTPPUT

        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPut("{id}", Name = "UpdateLeadContact")]
        public async Task<IActionResult> UpdateLeadContact(Guid id, [FromBody]LeadContactForUpdate LeadContactForUpdate)
        {

            //if show not found
            if (!await _leadcontactService.ExistAsync(x => x.Id == id))
            {
                //then return not found response.
                return NotFound();
            }

            //Update an entity.
            await _leadcontactService.UpdateEntityAsync(id, LeadContactForUpdate);

            //return the response.
            return NoContent();
        }

        #endregion


        #region HTTPPATCH

        [Consumes("application/json-patch+json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPatch("{id}")]
        public async Task<IActionResult> PartiallyUpdateLeadContact(Guid id, [FromBody] JsonPatchDocument<LeadContactForUpdate> jsonPatchDocument)
        {
            LeadContactForUpdate dto = new LeadContactForUpdate();
            LeadContact leadcontact = new LeadContact();

            //apply the patch changes to the dto. 
            jsonPatchDocument.ApplyTo(dto, ModelState);

            //if the jsonPatchDocument is not valid.
            if (!ModelState.IsValid)
            {
                //then return unprocessableEntity response.
                return new UnprocessableEntityObjectResult(ModelState);
            }

            //if the dto model is not valid after applying changes.
            if (!TryValidateModel(dto))
            {
                //then return unprocessableEntity response.
                return new UnprocessableEntityObjectResult(ModelState);
            }

            //map the chnages from dto to entity.
            Mapper.Map(dto, leadcontact);

            //set the Id for the show model.
            leadcontact.Id = id;

            //partially update the chnages to the db. 
            await _leadcontactService.UpdatePartialEntityAsync(leadcontact, jsonPatchDocument);

            //return the response.
            return NoContent();
        }

        #endregion


        #region HTTPPOST

        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPost(Name = "CreateLeadContact")]
        public async Task<ActionResult<LeadContactDto>> CreateLeadContact([FromBody]LeadContactForCreation leadcontact)
        {
            //create a show in db.
            var leadcontactToReturn = await _leadcontactService.CreateEntityAsync<LeadContactDto, LeadContactForCreation>(leadcontact);

            //return the show created response.
            return CreatedAtRoute("GetLeadContact", new { id = leadcontactToReturn.Id }, leadcontactToReturn);
        }

        #endregion


        #region HTTPDELETE
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}", Name = "DeleteLeadContactById")]
        public async Task<IActionResult> DeleteLeadContactById(Guid id)
        {
            //if the leadcontact exists
            if (await _leadcontactService.ExistAsync(x => x.Id == id))
            {
                //delete the leadcontact from the db.
                await _leadcontactService.DeleteEntityAsync(id);
            }
            else
            {
                //if leadcontact doesn't exists then returns not found.
                return NotFound();
            }

            //return the response.
            return NoContent();
        }

        #endregion


        #region PRIVATE METHODS

        private IEnumerable<LinkDto> CreateLinksForLeadContact(Guid id, string fields)
        {
            var links = new List<LinkDto>();

            if (string.IsNullOrWhiteSpace(fields))
            {
                links.Add(
                  new LinkDto(_urlHelper.Link("GetLeadContact", new { id = id }),
                  "self",
                  "GET"));
            }
            else
            {
                links.Add(
                  new LinkDto(_urlHelper.Link("GetLeadContact", new { id = id, fields = fields }),
                  "self",
                  "GET"));
            }

            links.Add(
              new LinkDto(_urlHelper.Link("DeleteLeadContactById", new { id = id }),
              "delete_leadcontact",
              "DELETE"));

            links.Add(
             new LinkDto(_urlHelper.Link("UpdateLeadContact", new { id = id }),
             "update_leadcontact",
             "PUT"));

            links.Add(
              new LinkDto(_urlHelper.Link("CreateLeadContact", new { }),
              "create_leadcontact",
              "POST"));

            //links.Add(
            //   new LinkDto(_urlHelper.Link("GetShowsForTour", new { }),
            //   "shows",
            //   "GET"));

            return links;
        }

        private IEnumerable<LinkDto> CreateLinksForLeadContacts(FilterOptionsModel filterOptionsModel, bool hasNext, bool hasPrevious)
        {
            var links = new List<LinkDto>();

            links.Add(
               new LinkDto(CreateLeadContactsResourceUri(filterOptionsModel, ResourceUriType.Current), "self", "GET"));

            if (hasNext)
            {
                links.Add(
                  new LinkDto(CreateLeadContactsResourceUri(filterOptionsModel, ResourceUriType.NextPage), "nextPage", "GET"));
            }

            if (hasPrevious)
            {
                links.Add(new LinkDto(CreateLeadContactsResourceUri(filterOptionsModel, ResourceUriType.PreviousPage), "previousPage", "GET"));
            }

            return links;
        }

        private string CreateLeadContactsResourceUri(FilterOptionsModel filterOptionsModel, ResourceUriType type)
        {
            switch (type)
            {
                case ResourceUriType.PreviousPage:
                    return _urlHelper.Link("GetFilteredLeadContacts",
                      new
                      {
                          fields = filterOptionsModel.Fields,
                          orderBy = filterOptionsModel.OrderBy,
                          searchQuery = filterOptionsModel.SearchQuery,
                          pageNumber = filterOptionsModel.PageNumber - 1,
                          pageSize = filterOptionsModel.PageSize
                      });
                case ResourceUriType.NextPage:
                    return _urlHelper.Link("GetFilteredLeadContacts",
                      new
                      {
                          fields = filterOptionsModel.Fields,
                          orderBy = filterOptionsModel.OrderBy,
                          searchQuery = filterOptionsModel.SearchQuery,
                          pageNumber = filterOptionsModel.PageNumber + 1,
                          pageSize = filterOptionsModel.PageSize
                      });

                default:
                    return _urlHelper.Link("GetFilteredLeadContacts",
                    new
                    {
                        fields = filterOptionsModel.Fields,
                        orderBy = filterOptionsModel.OrderBy,
                        searchQuery = filterOptionsModel.SearchQuery,
                        pageNumber = filterOptionsModel.PageNumber,
                        pageSize = filterOptionsModel.PageSize
                    });
            }
        }

        #endregion

    }
}
