using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
//using EventManagement.API.Helpers;
using EventManagement.Dto;
using EventManagement.Service;
using EventManagement.Domain.Entities;
using EventManagement.Utility;
using Microsoft.Extensions.Logging;
using System.Linq;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using Microsoft.Extensions.Configuration;
using RestSharp;
using EventManagement.Dto.Lead;
using IdentityServer4.AccessTokenValidation;
using SendGrid;
using SendGrid.Helpers.Mail;
using EventManagement.Utility.Enums;
using System.Text.RegularExpressions;
using EventManagement.Domain;

namespace EventManagement.API.Controllers
{
    /// <summary>
    /// Lead endpoint
    /// </summary>
    [Route("api/leads")]
    [Produces("application/json")]
    [ApiController]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]

    public class LeadController : Controller
    {

        #region PRIVATE MEMBERS

        private readonly ILeadService _leadService;
        private ILogger<LeadController> _logger;
        private readonly IUrlHelper _urlHelper;
        private readonly ILeadAssetService _leadAssetService;
        private readonly ILeadServiceOfferedService _leadServiceOfferedService;
        private readonly IContactService _contactService;
        private readonly ILeadContactService _leadContactService;
        private IConfiguration _configuration;
        private INotificationMailService _notificationMailService;
        private IAspNetUserClaimService _aspNetUserClaimService;
        private IAspUserService _aspUserService;
        private readonly IUserInfoService _userInfoService;
        private readonly ITaskService _taskService;
        private readonly IHistoryService _historyService;
        private readonly IStoreService _storeService;

        #endregion


        #region CONSTRUCTOR

        public LeadController(ILeadService leadService, ILogger<LeadController> logger, IUrlHelper urlHelper, 
            ILeadAssetService leadAssetService, ILeadServiceOfferedService leadServiceOfferedService
            , IContactService contactService, ILeadContactService leadContactService, IConfiguration configuration, INotificationMailService notificationMailService, IAspNetUserClaimService aspNetUserClaimService, IAspUserService aspUserService, IUserInfoService userInfoService, ITaskService taskService,
            IHistoryService historyService, IStoreService storeService) 
        {
            _logger = logger;
            _leadService = leadService;
            _urlHelper = urlHelper;
            _leadAssetService = leadAssetService;
            _leadServiceOfferedService = leadServiceOfferedService;
            _contactService = contactService;
            _leadContactService = leadContactService;
            _configuration = configuration;
            _notificationMailService = notificationMailService;
            _aspNetUserClaimService = aspNetUserClaimService;
            _aspUserService = aspUserService;
            _userInfoService = userInfoService;
            _taskService = taskService;
            _historyService = historyService;
            _storeService = storeService;
        }

        #endregion


        #region HTTPGET

        [HttpGet("GetFilteredLeadsDeatils", Name = "GetFilteredLeadsDeatils")]
        [Produces("application/vnd.tourmanagement.leads.hateoas+json", "application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<List<LeadDto>>> GetFilteredLeadsDeatils([FromQuery] FilterOptionsModel filterOptionsModel, [FromHeader(Name = "Accept")] string mediaType)
        {
            try
            {     
                //if order by fields are not valid.
                if (!_leadService.ValidMappingExists(filterOptionsModel.OrderBy))
                {
                    //then return bad request.
                    return BadRequest();
                }

                //if fields are not valid.
                if (!EventManagementUtils.TypeHasProperties<LeadDto>(filterOptionsModel.Fields))
                {
                    //then return bad request.
                    return BadRequest();
                }

                //get the paged/filtered show from db. 
                var leadsFromRepo = await _leadService.GetFilteredEntities(filterOptionsModel);                

                //if HATEOAS links are required.
                if (mediaType == "application/vnd.tourmanagement.leads.hateoas+json")
                {
                    //create HATEOAS links for each show.
                    leadsFromRepo.ForEach(lead =>
                    {
                        var entityLinks = CreateLinksForLead(lead.Id, filterOptionsModel.Fields);
                        lead.links = entityLinks;
                    });

                    //prepare pagination metadata.
                    var paginationMetadata = new
                    {
                        totalCount = leadsFromRepo.TotalCount,
                        pageSize = leadsFromRepo.PageSize,
                        currentPage = leadsFromRepo.CurrentPage,
                        totalPages = leadsFromRepo.TotalPages,
                    };

                    //add pagination meta data to response header.
                    Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMetadata));

                    //create links for shows.
                    var links = CreateLinksForLeads(filterOptionsModel, leadsFromRepo.HasNext, leadsFromRepo.HasPrevious);

                    //prepare model with data and HATEOAS links.
                    var linkedCollectionResource = new
                    {
                        value = leadsFromRepo,
                        links = links
                    };

                    //return the data with Ok response.
                    return Ok(linkedCollectionResource);
                }
                else
                {
                    var previousPageLink = leadsFromRepo.HasPrevious ?
                        CreateLeadsResourceUri(filterOptionsModel, ResourceUriType.PreviousPage) : null;

                    var nextPageLink = leadsFromRepo.HasNext ?
                        CreateLeadsResourceUri(filterOptionsModel, ResourceUriType.NextPage) : null;

                    //prepare the pagination metadata.
                    var paginationMetadata = new
                    {
                        previousPageLink = previousPageLink,
                        nextPageLink = nextPageLink,
                        totalCount = leadsFromRepo.TotalCount,
                        pageSize = leadsFromRepo.PageSize,
                        currentPage = leadsFromRepo.CurrentPage,
                        totalPages = leadsFromRepo.TotalPages
                    };

                    //add pagination meta data to response header.
                    Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMetadata));

                    //return the data with Ok response.
                    return Ok(leadsFromRepo);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpGet(Name = "GetFilteredLeads")]
        [Produces("application/vnd.tourmanagement.leads.hateoas+json", "application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<List<LeadDto>>> GetFilteredLeads([FromQuery]FilterOptionsModel filterOptionsModel, [FromHeader(Name = "Accept")] string mediaType)
        {
            try
            {
                var loggedinUserId = _userInfoService.UserId;
                var loggedinUserRoles = _userInfoService.Role;               

                //if order by fields are not valid.
               if (!_leadService.ValidMappingExists(filterOptionsModel.OrderBy))
                {
                    //then return bad request.
                    return BadRequest();
                }

                //if fields are not valid.
                if (!EventManagementUtils.TypeHasProperties<LeadDto>(filterOptionsModel.Fields))
                {
                    //then return bad request.
                    return BadRequest();
                }

                //get the paged/filtered show from db. 
                var leadsFromRepo = await _leadService.GetFilteredEntities(filterOptionsModel);
                var leadServiceFilter = new FilterOptionsModel();
                leadServiceFilter.PageSize = 20;
                leadServiceFilter.SearchQuery = "AssignedTo==\"" + loggedinUserId + "\"";
                var leadServices = _leadServiceOfferedService.GetFilteredEntities(leadServiceFilter).Result;
                foreach (var leadService in leadServices)
                {
                    var leadFilterOption = new FilterOptionsModel();
                    leadFilterOption.SearchQuery = "Id==\"" + leadService.Lead.Id + "\"";
                    var leads = _leadService.GetFilteredEntities(leadFilterOption).Result.FirstOrDefault();
                    if (!leadsFromRepo.Any(x => x.Id == leads.Id))
                    {
                        leadsFromRepo.Add(leads);
                    }
                }

                //if HATEOAS links are required.
                if (mediaType == "application/vnd.tourmanagement.leads.hateoas+json")
                {
                    //create HATEOAS links for each show.
                    leadsFromRepo.ForEach(lead =>
                    {
                        var entityLinks = CreateLinksForLead(lead.Id, filterOptionsModel.Fields);
                        lead.links = entityLinks;
                    });

                    //prepare pagination metadata.
                    var paginationMetadata = new
                    {
                        totalCount = leadsFromRepo.TotalCount,
                        pageSize = leadsFromRepo.PageSize,
                        currentPage = leadsFromRepo.CurrentPage,
                        totalPages = leadsFromRepo.TotalPages,
                    };

                    //add pagination meta data to response header.
                    Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMetadata));

                    //create links for shows.
                    var links = CreateLinksForLeads(filterOptionsModel, leadsFromRepo.HasNext, leadsFromRepo.HasPrevious);

                    //prepare model with data and HATEOAS links.
                    var linkedCollectionResource = new
                    {
                        value = leadsFromRepo,
                        links = links
                    };

                    //return the data with Ok response.
                    return Ok(linkedCollectionResource);
                }
                else
                {
                    var previousPageLink = leadsFromRepo.HasPrevious ?
                        CreateLeadsResourceUri(filterOptionsModel, ResourceUriType.PreviousPage) : null;

                    var nextPageLink = leadsFromRepo.HasNext ?
                        CreateLeadsResourceUri(filterOptionsModel, ResourceUriType.NextPage) : null;

                    //prepare the pagination metadata.
                    var paginationMetadata = new
                    {
                        previousPageLink = previousPageLink,
                        nextPageLink = nextPageLink,
                        totalCount = leadsFromRepo.TotalCount,
                        pageSize = leadsFromRepo.PageSize,
                        currentPage = leadsFromRepo.CurrentPage,
                        totalPages = leadsFromRepo.TotalPages
                    };

                    //add pagination meta data to response header.
                    Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMetadata));                   

                    //return the data with Ok response.
                    return Ok(leadsFromRepo);
                }
            }
            catch(Exception ex)
            {
                return BadRequest(ex);
            }            
        }


        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Produces("application/vnd.tourmanagement.leads.hateoas+json", "application/json")]
        [HttpGet("{id}", Name = "GetLead")]
        public async Task<ActionResult<LeadForUpdate>> GetLead(Guid id, [FromQuery] string fields, [FromHeader(Name = "Accept")] string mediaType)
        {
            try
            {
                LeadForUpdate leadEntity = new LeadForUpdate();
                object linkedResourceToReturn = null;

                //if fields are not passed.
                if (string.IsNullOrEmpty(fields))
                {
                    _logger.LogInformation("GetLead called");

                    //then get the whole entity and map it to the Dto.
                    var leadDto = Mapper.Map<LeadDto>(await _leadService.GetEntityByIdAsync(id));

                    leadEntity.Id = leadDto.Id;
                    leadEntity.LeadNumber = leadDto.LeadNumber;
                    leadEntity.CustomerInsight = leadDto.CustomerInsight;
                    leadEntity.AssignedTo = leadDto.AssignedTo;
                    leadEntity.AgentId = leadDto.AgentId;
                    leadEntity.Status = leadDto.Status;
                    leadEntity.AssignedStore = leadDto.AssignedStore;
                    if (leadEntity.AgentId != null || leadEntity.AgentId != string.Empty)
                    {
                        var referredEmployee = _aspUserService.GetEntityById(leadEntity.AgentId);                        
                        leadEntity.ReferredBy = referredEmployee != null?referredEmployee.FName + " " + referredEmployee.LName:string.Empty;                    
                    }

                    var assets = _leadAssetService.GetAllEntities().Where(x => x.LeadId.Equals(Guid.Parse(leadEntity.Id)))
                                 .Select(x => new AssetDto
                                 {
                                     Id = x.AssetId.ToString(),
                                     // Name = x.Asset.Name,
                                     Type = x.Type,
                                     Valuation = x.Valuation,
                                     Description = x.Description
                                 }).ToList();
                    var servicesOffered = _leadServiceOfferedService.GetAllEntities().Where(x => x.LeadId.Equals(Guid.Parse(leadEntity.Id)))
                                          .Select(x => new ServiceOfferedDto 
                                          {
                                              Id = x.ServiceOfferedId.ToString(),
                                              // Name = x.ServiceOffered.Name,
                                              Description = x.Description
                                          }).ToList();

                    leadEntity.SelectedAssetId = assets;
                    leadEntity.SelectedServiceId = servicesOffered;

                    var contact = _contactService.GetAllEntities().Where(x => x.RelatedLeadContactId.Equals(Guid.Parse(leadEntity.Id))).FirstOrDefault();
                    if (contact != null)
                    {
                        var contactDto = Mapper.Map<ContactDto>(contact);
                        leadEntity.FirstName = contact.FirstName;
                        leadEntity.MiddleName = contact.MiddleName;
                        leadEntity.LastName = contact.LastName;
                        leadEntity.Address = contact.Address;
                        leadEntity.City = contact.City;
                        leadEntity.Province = contact.Province;
                        leadEntity.Zipcode = contact.Zipcode;
                        leadEntity.Phone = contact.Phone;
                        leadEntity.Email = contact.Email;
                        leadEntity.Source = contact.Source;
                        leadEntity.ExistingClient = contact.ExistingClient;
                        leadEntity.Employment = contact.Employment;
                        leadEntity.Position = contact.Position;
                        leadEntity.EmploymentDate = contact.EmploymentDate;
                        leadEntity.CompanyName = contact.CompanyName;
                        leadEntity.Website = contact.Website;
                        leadEntity.SalaryFrequency = contact.SalaryFrequency;
                        leadEntity.NoOfChildren = contact.NoOfChildren;
                        leadEntity.ContactStatus = contact.Status;
                        //leadEntity.RelationWithLead = contact.RelationWithLead,
                        //leadEntity.RelatedLeadContactId = contact.RelatedLeadContactId,
                        leadEntity.AllowMarketingMessages = contact.AllowMarketingMessages;
                        leadEntity.AllowUseOfPersonalInfo = contact.AllowUseOfPersonalInfo;
                        leadEntity.MaritalStatus = contact.MaritalStatus;
                        leadEntity.Sin = contact.Sin;
                        leadEntity.SalaryAmount = contact.SalaryAmount!=null? (float)contact.SalaryAmount:0;
                    }
                }
                else
                {
                    //if fields are passed then get the partial show.
                    leadEntity = await _leadService.GetPartialEntityAsync(id, fields);
                }

                //if lead not found.
                if (leadEntity == null)
                {
                    //then return not found response.
                    return NotFound();
                }

                if (mediaType == "application/vnd.tourmanagement.leads.hateoas+json")
                {
                    //create HATEOS links
                    var links = CreateLinksForLead(id, fields);

                    //if fields are not passed.
                    if (string.IsNullOrEmpty(fields))
                    {
                        //convert the typed object to expando object.
                        linkedResourceToReturn = ((LeadForUpdate)leadEntity).ShapeData("") as IDictionary<string, object>;

                        //add the HATEOAS links to the model.
                        ((IDictionary<string, object>)linkedResourceToReturn).Add("links", links);
                    }
                    else
                    {
                        linkedResourceToReturn = leadEntity;

                        //add the HATEOAS links to the model.
                        ((dynamic)linkedResourceToReturn).links = links;

                    }
                }
                else
                {
                    linkedResourceToReturn = leadEntity;
                }

                //return the Ok response.
                return Ok(linkedResourceToReturn);
            }
            catch(Exception ex)
            {
                return BadRequest();
            }
            
        }

        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Produces("application/vnd.tourmanagement.leads.hateoas+json", "application/json")]
        [HttpGet("ValidateToken", Name = "IsSinTokenValid")]
        public async Task<ActionResult<SinTokenValidityDto>> IsSinTokenValid(string leadId, string token)
        {
            var result = new SinTokenValidityDto();
            var lead = await _leadService.GetEntityByIdAsync(Guid.Parse(leadId));           

            if (lead.IsEmailSent)
            {
                // Make Request
                var uri = _configuration.GetSection("IdentityServerUrl").Value;
                var client = new RestClient(uri + "/Account/");
                // make Request
                var requestGet = new RestRequest("VerifySinToken", Method.GET);

                // add Parameter                
                requestGet.AddParameter("userId", lead.AssignedTo);
                requestGet.AddParameter("token", token);
                var responseGet = client.Get(requestGet);
                var isValid = JsonConvert.DeserializeObject<bool>(responseGet.Content);

                if (!isValid)
                {
                    result.IsValid = false;
                    result.Message = "Link has expired.Please contact your Relationship Manager to get a new link";
                }
                else
                {
                    result.IsValid = true;
                    result.Message = string.Empty;
                }
            }  
            else
            {
                result.IsValid = false;
                result.Message = "You have already provided SIN details";
            }        
            
            return Ok(result);
           
        }

        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Produces("application/vnd.tourmanagement.leads.hateoas+json", "application/json")]
        [HttpGet("IsSinExist", Name = "IsSinExist")]
        public async Task<ActionResult<SinTokenValidityDto>> IsSinExist(string leadId, string sin)
        {
            return Ok(await _contactService.ExistAsync(x => x.RelatedLeadContactId != Guid.Parse(leadId) && x.Sin == sin));
        }


        #endregion


        #region HTTPPUT

        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPut("{id}", Name = "UpdateLead")]
        public async Task<IActionResult> UpdateLead(Guid id, [FromBody]LeadForUpdate leadForUpdate)
        {
            var isAssignedToChanged = false;
            //if show not found
            if (!await _leadService.ExistAsync(x => x.Id == id))
            {
                //then return not found response.
                return NotFound();
            }
            var oldLead = _leadService.GetEntityById(id);

            if (oldLead.AssignedTo != leadForUpdate.AssignedTo)
            {
                isAssignedToChanged = true;
            }
           
                //Update an entity.
                await _leadService.UpdateEntityAsync(id, leadForUpdate);
            
            var contact = _contactService.GetAllEntities().Where(x => x.RelatedLeadContactId.Equals(id)).FirstOrDefault();

            var contactForUpdate = new ContactForUpdate
            {
                Id = contact.Id.ToString(),
                FirstName = leadForUpdate.FirstName,
                MiddleName = leadForUpdate.MiddleName,
                LastName = leadForUpdate.LastName,
                Address = leadForUpdate.Address,
                City = leadForUpdate.City,
                Province = leadForUpdate.Province,
                Zipcode = leadForUpdate.Zipcode,
                Phone = leadForUpdate.Phone,
                Email = leadForUpdate.Email,
                Source = leadForUpdate.Source,
                ExistingClient = leadForUpdate.ExistingClient,
                Employment = leadForUpdate.Employment,
                Position = leadForUpdate.Position,
                EmploymentDate = leadForUpdate.EmploymentDate,
                CompanyName = leadForUpdate.CompanyName,
                Website = leadForUpdate.Website,
                SalaryFrequency = leadForUpdate.SalaryFrequency,
                NoOfChildren = leadForUpdate.NoOfChildren,
                Status = "Lead",
                RelationWithLead = "Self",
                RelatedLeadContactId = Guid.Parse(leadForUpdate.Id),
                AllowMarketingMessages = leadForUpdate.AllowMarketingMessages,
                AllowUseOfPersonalInfo = leadForUpdate.AllowUseOfPersonalInfo,
                MaritalStatus = leadForUpdate.MaritalStatus,
                SalaryAmount = leadForUpdate.SalaryAmount,
            };

            var leadAssets = new List<LeadAsset>();

            foreach (var asset in leadForUpdate.SelectedAssetId)
            {
                leadAssets.Add(new LeadAsset { LeadId = id, AssetId = Guid.Parse(asset.Id), Type = asset.Type, Valuation = asset.Valuation, Description = asset.Description, ContactId = contact.Id });
            }

            await _leadAssetService.DeleteBulkEntityAsync(x => x.LeadId.Equals(id));
            _leadAssetService.CreateBulkEntity(leadAssets);

            await _contactService.UpdateEntityAsync(contact.Id, contactForUpdate);

            if (isAssignedToChanged)
            {
                var leadNumber = _leadService.GetEntityById(id).LeadNumber;
                var assignedToUser = _aspUserService.GetEntityById(leadForUpdate.AssignedTo);

                if (assignedToUser != null)
                {
                    // Send notification mail to client
                    var htmlContent = _notificationMailService.GetAllEntities().Where(x => x.Name.Equals(NotificationMailEnum.LeadAssigned.ToString())).FirstOrDefault().MailContent;
                    htmlContent = Regex.Replace(htmlContent, "{%lead.assignedTo%}", assignedToUser.FName ?? "", RegexOptions.IgnoreCase);
                    htmlContent = Regex.Replace(htmlContent, "{%contact.FirstName%}", contact.FirstName ?? "", RegexOptions.IgnoreCase);
                    htmlContent = Regex.Replace(htmlContent, "{%contact.LastName%}", contact.LastName ?? "", RegexOptions.IgnoreCase);
                    htmlContent = Regex.Replace(htmlContent, "{%lead.Id%}", "Lead Id: " + leadNumber ?? "", RegexOptions.IgnoreCase);
                    htmlContent = Regex.Replace(htmlContent, "{%lead.Link%}", _configuration.GetSection("Frontend").Value + "lead/edit/" + id.ToString() ?? "", RegexOptions.IgnoreCase);
                    var subject = contact.FirstName + " " + contact.LastName + " - Lead Id: " + leadNumber + " has been assigned to you";
                    _notificationMailService.SendNotificationMail(assignedToUser.Email, htmlContent, subject);

                    //Create a task
                    var leadTask = new TaskForCreation()
                    {
                        LeadId = id.ToString(),
                        Title = "Lead of " + contact.FirstName + " " + contact.LastName + " has been assigned to you.",
                        Description = "Lead has been assigned to you.",
                        Date = DateTime.Now,
                        UserId = assignedToUser.Id,
                        IsCompleted = false,
                        IsImportant = true,
                        IsMarked = false
                    };
                    var taskToReturn = await _taskService.CreateEntityAsync<TaskDto, TaskForCreation>(leadTask);
                }
            }

            //return the response.
            return NoContent();
        }

        [AllowAnonymous]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPut("UpdateSensitiveLeadInfo", Name = "UpdateSensitiveLeadInfo")]
        public async Task<IActionResult> UpdateSensitiveLeadInfo([FromBody] SensitiveLeadInfoDto sensitiveInfo)
        {
            var lead = await _leadService.GetPartialEntityAsync(Guid.Parse(sensitiveInfo.LeadID), "AssignedTo");

            // Make Request
            var uri = _configuration.GetSection("IdentityServerUrl").Value;
            var client = new RestClient(uri + "/Account/");
            // make Request
            var requestGet = new RestRequest("VerifySinToken", Method.GET);

            // add Parameter                
            requestGet.AddParameter("userId", lead.AssignedTo);
            requestGet.AddParameter("token", sensitiveInfo.Token);
            var responseGet = client.Get(requestGet);
            var result = JsonConvert.DeserializeObject<bool>(responseGet.Content);

            if (result)
            {
                await _contactService.UpdateSinDetails(sensitiveInfo.LeadID, sensitiveInfo.Sin);
                await _leadService.MarkEmailSent(sensitiveInfo.LeadID, false);
                var filterOption = new FilterOptionsModel();
                filterOption.PageSize = 10000;
                filterOption.SearchQuery = "LeadId==\"" + sensitiveInfo.LeadID + "\"";
                filterOption.Fields = "ServiceOffered,Id";
                var leadServiceOffered = _leadServiceOfferedService.GetFilteredEntities(filterOption).Result;
                var fcrmForCreation = new FTCrmForCreation();
                foreach (var leadService in leadServiceOffered)
                {
                    if (leadService.ServiceOffered.Category.Equals("10 Year Tax Review"))
                    {
                        // Trigger FTCrm              
                        fcrmForCreation.LeadServiceId = leadService.Id;
                        var ftcrmToReturn = await _leadService.CreateFtCrmAndIntegration(fcrmForCreation);
                        break;
                    }
                }  

             
                //return the response.
                return NoContent();
            }
            else
            {
                return BadRequest();
            }
        }

        #endregion


        #region HTTPPATCH

        [Consumes("application/json-patch+json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPatch("{id}")]
        public async Task<IActionResult> PartiallyUpdateLead(Guid id, [FromBody] JsonPatchDocument<LeadForUpdate> jsonPatchDocument)
        {
            LeadForUpdate dto = new LeadForUpdate();
            Lead lead = new Lead();

            //apply the patch changes to the dto. 
            jsonPatchDocument.ApplyTo(dto, ModelState);

            //if the jsonPatchDocument is not valid.
            if (!ModelState.IsValid)
            {
                //then return unprocessableEntity response.
                return new UnprocessableEntityObjectResult(ModelState);
            }

            //if the dto model is not valid after applying changes.
            if (!TryValidateModel(dto))
            {
                //then return unprocessableEntity response.
                return new UnprocessableEntityObjectResult(ModelState);
            }

            //map the chnages from dto to entity.
            Mapper.Map(dto, lead);

            //set the Id for the show model.
            lead.Id = id;

            //partially update the chnages to the db. 
            await _leadService.UpdatePartialEntityAsync(lead, jsonPatchDocument);

            //return the response.
            return NoContent();
        }

        #endregion


        #region HTTPPOST

        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPost(Name = "CreateLead")]
        public async Task<ActionResult<LeadDto>> CreateLead([FromBody]LeadForCreation lead)
        {
            
            try
            {
                lead.Id = Guid.NewGuid().ToString();

                var agent = _aspUserService.GetEntityById(lead.AgentId);

                var leadAdminId = _aspNetUserClaimService.GetAllEntities().Where(x=>x.ClaimValue.Equals("Lead Admin")).FirstOrDefault().UserId;
                if (_userInfoService.Role.Equals("Relationship Manager"))
                {
                    lead.AssignedTo = _userInfoService.UserId;
                }
                else 
                {
                    lead.AssignedTo = leadAdminId;
                }                
                lead.Status = "New";
                var leadToReturn = await _leadService.CreateEntityAsync<LeadDto, LeadForCreation>(lead);
                
                //Create Contact and Save
                var contactForCreation = new ContactForCreation
                {
                    FirstName = lead.FirstName,
                    MiddleName = lead.MiddleName,
                    LastName = lead.LastName,
                    Address = lead.Address,
                    City = lead.City,
                    Province = lead.Province,
                    Zipcode = lead.Zipcode,
                    Phone = lead.Phone,
                    Email = lead.Email,
                    Source = lead.Source,
                    ExistingClient = lead.ExistingClient,
                    Employment = lead.Employment,
                    Position = lead.Position,
                    EmploymentDate = lead.EmploymentDate,
                    CompanyName = lead.CompanyName,
                    Website = lead.Website,
                    SalaryFrequency = lead.SalaryFrequency,
                    NoOfChildren = lead.NoOfChildren,
                    Status = "Lead",
                    RelationWithLead = "Self",
                    RelatedLeadContactId = Guid.Parse(lead.Id),
                    AllowMarketingMessages = lead.AllowMarketingMessages,
                    AllowUseOfPersonalInfo = lead.AllowUseOfPersonalInfo,
                    MaritalStatus = lead.MaritalStatus,
                    SalaryAmount = lead.SalaryAmount,
                };
                var contactToReturn = _contactService.CreateEntityAsync<ContactDto, ContactForCreation>(contactForCreation);                
                var leadContactForCreation = new LeadContactForCreation { LeadId = Guid.Parse(leadToReturn.Id), ContactId = Guid.Parse(contactToReturn.Result.Id) };
                var leadContact = await _leadContactService.CreateEntityAsync<LeadContactDto, LeadContactForCreation>(leadContactForCreation);
                
                //Create Lead Contact-Asset Record
                var leadAssets = new List<LeadAsset>();
                
                foreach (var asset in lead.SelectedAssetId)
                {
                    leadAssets.Add(new LeadAsset { LeadId = Guid.Parse(leadToReturn.Id), AssetId = Guid.Parse(asset.Id), Type = asset.Type, Valuation = asset.Valuation, Description = asset.Description, ContactId = Guid.Parse(contactToReturn.Result.Id) });
                }

                _leadAssetService.CreateBulkEntity(leadAssets);

                //Create Lead Contact-Service Record
                var leadServicesSelected = new List<LeadServiceOffered>();
                foreach (var service in lead.SelectedServiceId)
                {
                    leadServicesSelected.Add(new LeadServiceOffered { LeadId = Guid.Parse(leadToReturn.Id), ServiceOfferedId = Guid.Parse(service.Id), Description = service.Description, AssignedStatus = "Interested", ContactId = Guid.Parse(contactToReturn.Result.Id) });
                }

                _leadServiceOfferedService.CreateBulkEntity(leadServicesSelected);

                await _leadServiceOfferedService.SaveChangesAsync();

                var serviceHistory = new HistoryForCreation();
                foreach (var selectedService in leadServicesSelected)
                {
                    serviceHistory = new HistoryForCreation();
                    serviceHistory.LeadId = leadToReturn.Id;
                    serviceHistory.ServiceId = selectedService.Id.ToString();
                    serviceHistory.Activity = (int)HistoryEnum.ServiceAdded;
                    serviceHistory.UserId = _userInfoService.UserId;
                    
                    var historyToReturn = await _historyService.CreateEntityAsync<HistoryDto, HistoryForCreation>(serviceHistory);
                }

                // Send notification mail to client
                var htmlContent = _notificationMailService.GetAllEntities().Where(x => x.Name.Equals(NotificationMailEnum.LeadCreatedToClient.ToString())).FirstOrDefault().MailContent;
                htmlContent = Regex.Replace(htmlContent, "{%contact.FirstName%}", lead.FirstName ?? "", RegexOptions.IgnoreCase);
                htmlContent = Regex.Replace(htmlContent, "{%lead.Id%}", leadToReturn.LeadNumber.ToString() ?? "", RegexOptions.IgnoreCase);
                var subject = "Thank you for connecting with Liberty Edge!";
                _notificationMailService.SendNotificationMail(contactForCreation.Email, htmlContent , subject);

                // Send notification mail to agent
                var htmlContentAgent = _notificationMailService.GetAllEntities().Where(x => x.Name.Equals(NotificationMailEnum.LeadCreatedToEmployee.ToString())).FirstOrDefault().MailContent;
                htmlContentAgent = Regex.Replace(htmlContentAgent, "{%lead.agentId%}", agent.FName ?? "", RegexOptions.IgnoreCase);
                htmlContentAgent = Regex.Replace(htmlContentAgent, "{%contact.FirstName%}", lead.FirstName ?? "", RegexOptions.IgnoreCase);
                htmlContentAgent = Regex.Replace(htmlContentAgent, "{%contact.LastName%}", lead.LastName ?? "", RegexOptions.IgnoreCase);
                htmlContentAgent = Regex.Replace(htmlContentAgent, "{%lead.Id%}", leadToReturn.LeadNumber.ToString() , RegexOptions.IgnoreCase);
                var subjectAgent = lead.FirstName + " " + lead.LastName + "-" + leadToReturn.LeadNumber + "has submitted the lead form";
                _notificationMailService.SendNotificationMail(agent.Email, htmlContentAgent, subjectAgent);

                //return the show created response.
                return CreatedAtRoute("GetLead", new { id = leadToReturn.Id }, leadToReturn);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
            
        }        

        [AllowAnonymous]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPost("CreateWidgetLead", Name = "CreateWidgetLead")]
        public async Task<ActionResult<LeadDto>> CreateWidgetLead([FromBody] LeadForCreation lead)
        {
            try
            {
                return Ok(await _leadService.CreateWidgetLead(lead));
            }
            catch(Exception ex)
            {
                return BadRequest();
            }
        }

        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPost("{id}", Name = "SendEmailToGetSensitiveInfo")]
        public async Task<ActionResult<LeadDto>> SendEmailToGetSensitiveInfo(string id)
        {
            try
            {

                var lead = await _leadService.GetPartialEntityAsync(Guid.Parse(id), "AssignedTo");

                // Make Request
                var uri = _configuration.GetSection("IdentityServerUrl").Value;
                var client = new RestClient(uri + "/Account/");
                // make Request
                var requestGet = new RestRequest("GenerateSinToken", Method.GET);

                // add Parameter                
                requestGet.AddParameter("userId", lead.AssignedTo);
                var responseGet = client.Get(requestGet);
                var token =  JsonConvert.DeserializeObject<string>(responseGet.Content);

                await _leadService.SendEmailToGetLeadSensitiveInfo(id, token);
                return Ok("Email send successfully!");
            }
            catch (Exception ex)
            {
                return BadRequest();
            }

        }
        

        #endregion


        #region HTTPDELETE
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}", Name = "DeleteLeadById")]
        public async Task<IActionResult> DeleteLeadById(Guid id)
        {
            //if the lead exists
            if (await _leadService.ExistAsync(x => x.Id == id))
            {
                //delete the lead from the db.
                await _leadService.DeleteEntityAsync(id);
            }
            else
            {
                //if lead doesn't exists then returns not found.
                return NotFound();
            }

            //return the response.
            return NoContent();
        }

        #endregion


        #region PRIVATE METHODS

        private IEnumerable<LinkDto> CreateLinksForLead(Guid id, string fields)
        {
            var links = new List<LinkDto>();

            if (string.IsNullOrWhiteSpace(fields))
            {
                links.Add(
                  new LinkDto(_urlHelper.Link("GetLead", new { id = id }),
                  "self",
                  "GET"));
            }
            else
            {
                links.Add(
                  new LinkDto(_urlHelper.Link("GetLead", new { id = id, fields = fields }),
                  "self",
                  "GET"));
            }

            links.Add(
              new LinkDto(_urlHelper.Link("DeleteLeadById", new { id = id }),
              "delete_lead",
              "DELETE"));

            links.Add(
             new LinkDto(_urlHelper.Link("UpdateLead", new { id = id }),
             "update_lead",
             "PUT"));

            links.Add(
              new LinkDto(_urlHelper.Link("CreateLead", new { }),
              "create_lead",
              "POST"));

            //links.Add(
            //   new LinkDto(_urlHelper.Link("GetShowsForTour", new { }),
            //   "shows",
            //   "GET"));

            return links;
        }

        private IEnumerable<LinkDto> CreateLinksForLeads(FilterOptionsModel filterOptionsModel, bool hasNext, bool hasPrevious)
        {
            var links = new List<LinkDto>();

            links.Add(
               new LinkDto(CreateLeadsResourceUri(filterOptionsModel, ResourceUriType.Current), "self", "GET"));

            if (hasNext)
            {
                links.Add(
                  new LinkDto(CreateLeadsResourceUri(filterOptionsModel, ResourceUriType.NextPage), "nextPage", "GET"));
            }

            if (hasPrevious)
            {
                links.Add(new LinkDto(CreateLeadsResourceUri(filterOptionsModel, ResourceUriType.PreviousPage), "previousPage", "GET"));
            }

            return links;
        }

        private string CreateLeadsResourceUri(FilterOptionsModel filterOptionsModel, ResourceUriType type)
        {
            switch (type)
            {
                case ResourceUriType.PreviousPage:
                    return _urlHelper.Link("GetFilteredLeads",
                      new
                      {
                          fields = filterOptionsModel.Fields,
                          orderBy = filterOptionsModel.OrderBy,
                          searchQuery = filterOptionsModel.SearchQuery,
                          pageNumber = filterOptionsModel.PageNumber - 1,
                          pageSize = filterOptionsModel.PageSize
                      });
                case ResourceUriType.NextPage:
                    return _urlHelper.Link("GetFilteredLeads",
                      new
                      {
                          fields = filterOptionsModel.Fields,
                          orderBy = filterOptionsModel.OrderBy,
                          searchQuery = filterOptionsModel.SearchQuery,
                          pageNumber = filterOptionsModel.PageNumber + 1,
                          pageSize = filterOptionsModel.PageSize
                      });

                default:
                    return _urlHelper.Link("GetFilteredLeads",
                    new
                    {
                        fields = filterOptionsModel.Fields,
                        orderBy = filterOptionsModel.OrderBy,
                        searchQuery = filterOptionsModel.SearchQuery,
                        pageNumber = filterOptionsModel.PageNumber,
                        pageSize = filterOptionsModel.PageSize
                    });
            }
        }

        #endregion

    }
}
