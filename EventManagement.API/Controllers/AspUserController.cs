using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
//using FinanaceManagement.API.Helpers;
using Microsoft.Extensions.Logging;
using FinanaceManagement.API.Models;
using IdentityServer4.AccessTokenValidation;
using System.Linq;
using System.IO;
using Microsoft.Extensions.Configuration;
using System.Globalization;
using EventManagement.Dto;
using EventManagement.Utility;
using RestSharp;
using EventManagement.Domain;
using EventManagement.Service;
using EventManagement.Utility.Filters;
using EventManagement.Dto.AspUser;

namespace EventManagement.API.Controllers
{
    /// <summary>
    /// AspUser endpoint
    /// </summary>
    [Route("api/aspusers")]
    [Produces("application/json")]
    [ApiController]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class AspUserController : Controller
    {

        #region PRIVATE MEMBERS

        private readonly IAspUserService _aspuserService;
        private ILogger<AspUserController> _logger;
        private readonly IUrlHelper _urlHelper;
        private readonly IUserInfoService _userInfoService;
        private readonly IAspNetUserClaimService _aspNetUserClaimService;
        //Note:   These are the related tables where you want to add the user reference.

        //private readonly Iapp_UserService _app_UserService;
        //private readonly IEdge_UserAccountService _edge_UserAccountService;
        //private readonly IEdge_ClientUserService _edge_ClientUserService;
        //private readonly IEdge_ClientAccountService _edge_ClientAccountService;
        private IConfiguration _configuration;

        // Note: The Blob Service where you want to store the user image. 
        // private readonly IBlobService _blobService;

        #endregion


        #region CONSTRUCTOR

        public AspUserController(
            //IBlobService blobService, 
            IConfiguration configuration,
            //IEdge_ClientAccountService edge_ClientAccountService, 
            //IEdge_ClientUserService edge_ClientUserService, 
            //IEdge_UserAccountService edge_UserAccountService, 
            //Iapp_UserService app_UserService, 
            IAspUserService aspuserService, ILogger<AspUserController> logger, IUrlHelper urlHelper, IUserInfoService userinfoService, IAspNetUserClaimService aspNetUserClaimService)
        {
            _logger = logger;
            _aspuserService = aspuserService;
            _urlHelper = urlHelper;
            _configuration = configuration;
            _userInfoService = userinfoService;
            _aspNetUserClaimService = aspNetUserClaimService;

            //_app_UserService = app_UserService;
            //_edge_UserAccountService = edge_UserAccountService;
            //_edge_ClientUserService = edge_ClientUserService;
            //_edge_ClientAccountService = edge_ClientAccountService;
            //_blobService = blobService;
        }

        #endregion


        #region HTTPGET

        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("GetUserById/{id}", Name = "GetUserById")]
        public async Task<IActionResult> GetUserById(string id)
        {
            var user = _aspuserService.GetEntityById(id);

            var userData = Mapper.Map<AspUserDto>(user);

            return Ok(userData);
        }

        [HttpGet(Name = "GetFilteredAspUsers")]
        [Produces("application/vnd.tourmanagement.aspusers.hateoas+json", "application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<List<AspUserDto>>> GetFilteredAspUsers([FromQuery] FilterOptionsModel filterOptionsModel, [FromHeader(Name = "Accept")] string mediaType)
        {

            //if order by fields are not valid.
            if (!_aspuserService.ValidMappingExists(filterOptionsModel.OrderBy))
            {
                //then return bad request.
                return BadRequest();
            }

            //if fields are not valid.
            if (!EventManagementUtils.TypeHasProperties<AspUserDto>(filterOptionsModel.Fields))
            {
                //then return bad request.
                return BadRequest();
            }

            //get the paged/filtered show from db. 
            var aspusersFromRepo = await _aspuserService.GetFilteredEntities(filterOptionsModel);

            //if HATEOAS links are required.
            if (mediaType == "application/vnd.tourmanagement.aspusers.hateoas+json")
            {
                //create HATEOAS links for each show.
                aspusersFromRepo.ForEach(aspuser =>
                {
                    var entityLinks = CreateLinksForAspUser(aspuser.Id, filterOptionsModel.Fields);
                    aspuser.links = entityLinks;
                });

                //prepare pagination metadata.
                var paginationMetadata = new
                {
                    totalCount = aspusersFromRepo.TotalCount,
                    pageSize = aspusersFromRepo.PageSize,
                    currentPage = aspusersFromRepo.CurrentPage,
                    totalPages = aspusersFromRepo.TotalPages,
                };

                //add pagination meta data to response header.
                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMetadata));

                //create links for shows.
                var links = CreateLinksForAspUsers(filterOptionsModel, aspusersFromRepo.HasNext, aspusersFromRepo.HasPrevious);

                //prepare model with data and HATEOAS links.
                var linkedCollectionResource = new
                {
                    value = aspusersFromRepo,
                    links = links
                };

                //return the data with Ok response.
                return Ok(linkedCollectionResource);
            }
            else
            {
                var previousPageLink = aspusersFromRepo.HasPrevious ?
                    CreateAspUsersResourceUri(filterOptionsModel, ResourceUriType.PreviousPage) : null;

                var nextPageLink = aspusersFromRepo.HasNext ?
                    CreateAspUsersResourceUri(filterOptionsModel, ResourceUriType.NextPage) : null;

                //prepare the pagination metadata.
                var paginationMetadata = new
                {
                    previousPageLink = previousPageLink,
                    nextPageLink = nextPageLink,
                    totalCount = aspusersFromRepo.TotalCount,
                    pageSize = aspusersFromRepo.PageSize,
                    currentPage = aspusersFromRepo.CurrentPage,
                    totalPages = aspusersFromRepo.TotalPages
                };

                //add pagination meta data to response header.
                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMetadata));
                aspusersFromRepo.ForEach(aspuser =>
                {
                    var roles = _aspNetUserClaimService.GetAllRolesByUserId(aspuser.Id);
                    aspuser.role = string.Join(", ", roles);
                });

                //return the data with Ok response.
                return Ok(aspusersFromRepo);
            }
        }


        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Produces("application/vnd.tourmanagement.aspusers.hateoas+json", "application/json")]
        [HttpGet("{id}", Name = "GetAspUser")]
        public async Task<ActionResult<AspNetUsers>> GetAspUser(string id, [FromQuery] string fields, [FromHeader(Name = "Accept")] string mediaType)
        {
            AspUserDto aspuserEntity = null;
            object linkedResourceToReturn = null;

            //if fields are not passed.
            if (string.IsNullOrEmpty(fields))
            {
                _logger.LogInformation("GetAspUser called");

                //then get the whole entity and map it to the Dto.
                aspuserEntity = Mapper.Map<AspUserDto>(await _aspuserService.GetEntityByIdAsync(id));
            }
            else
            {
                //if fields are passed then get the partial show.
                aspuserEntity = await _aspuserService.GetPartialEntityAsync(id, fields);
            }
            var roles = _aspNetUserClaimService.GetAllRolesByUserId(aspuserEntity.Id);
            aspuserEntity.Role = string.Join(",", roles);

            //if aspuser not found.
            if (aspuserEntity == null)
            {
                //then return not found response.
                return NotFound();
            }

            if (mediaType == "application/vnd.tourmanagement.aspusers.hateoas+json")
            {
                //create HATEOS links
                var links = CreateLinksForAspUser(id, fields);

                //if fields are not passed.
                if (string.IsNullOrEmpty(fields))
                {
                    //convert the typed object to expando object.
                    linkedResourceToReturn = ((AspUserDto)aspuserEntity).ShapeData("") as IDictionary<string, object>;

                    //add the HATEOAS links to the model.
                    ((IDictionary<string, object>)linkedResourceToReturn).Add("links", links);
                }
                else
                {
                    linkedResourceToReturn = aspuserEntity;

                    //add the HATEOAS links to the model.
                    ((dynamic)linkedResourceToReturn).links = links;

                }
            }
            else
            {
                linkedResourceToReturn = aspuserEntity;
            }

            //return the Ok response.
            return Ok(linkedResourceToReturn);
        }



        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Produces("application/vnd.tourmanagement.aspusers.hateoas+json", "application/json")]
        [HttpGet("GetAspUserByRole/{Role}", Name = "GetAspUserByRole")]
        public async Task<ActionResult<AspNetUsers>> GetAspUserByRole(string role)
        {
            var filterOptionsModel = new FilterOptionsModel();

            var aspusersFromRepo = _aspNetUserClaimService.GetAllEntities().Where(x=>x.ClaimValue == role).ToList();
            var aspNetUsers = new List<AspNetUsers>();
            foreach (var user in aspusersFromRepo)
            {
                aspNetUsers.Add(_aspuserService.GetEntityById(user.UserId));
            }    
            

            //return the Ok response.
            return Ok(aspNetUsers);
        }

        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet("exists/{id}", Name = "Exists")]
        public async Task<ActionResult<bool>> Exists(string id)
        {
            //if fields are passed then get the partial show.
            bool userExists = await _aspuserService.ExistAsync(x => x.Id == id);
            //return the Ok response.
            return Ok(userExists);
        }

        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet("IsUserEmailExist", Name = "IsUserEmailExist")]
        public async Task<ActionResult<bool>> IsUserEmailExist(string email)
        {           
            return Ok(await _aspuserService.ExistAsync(x => x.Email == email));
        }

        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet("IsUserPhoneExist", Name = "IsUserPhoneExist")]
        public async Task<ActionResult<bool>> IsUserPhoneExist(string phone)
        {
            return Ok(await _aspuserService.ExistAsync(x => x.PhoneNumber == phone));
        }


        #endregion


        #region HTTPPUT

        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPut("{id}", Name = "UpdateAspUser")]
        public async Task<IActionResult> UpdateAspUser(string id, [FromBody]AspUserForUpdate AspUserForUpdate)
        {

            //if show not found
            if (!await _aspuserService.ExistAsync(x => x.Id == id.ToString()))
            {
                //then return not found response.
                return NotFound();
            }

            AspUserForUpdate.UserName = AspUserForUpdate.Email;
            AspUserForUpdate.NormalizedEmail = AspUserForUpdate.Email.ToUpper();
            AspUserForUpdate.NormalizedUserName = AspUserForUpdate.Email.ToUpper();

            //Update an entity.
            await _aspuserService.UpdateEntityAsync(id, AspUserForUpdate);
            //var claim = _aspNetUserClaimService.GetClaimByUserId(id);
            //var oldRoleClaim = claim.ClaimValue;

            var roles = _aspNetUserClaimService.GetAllRolesByUserId(id);
            var oldRoleClaim = string.Join(",", roles);

            var newRoleClaim = AspUserForUpdate.Role;
            var email = AspUserForUpdate.Email;
            if (oldRoleClaim != newRoleClaim)
            {
                var requestPost = new RestRequest("UpdateRole", Method.POST);
                requestPost.AddParameter("Email", email);
                requestPost.AddParameter("OldRole", oldRoleClaim);
                requestPost.AddParameter("NewRole", newRoleClaim);

                var uri = _configuration.GetSection("IdentityServerUrl").Value;
                var client = new RestClient(uri + "/Account/");
                var responsePost = client.Post(requestPost);
            }
            //return the response.
            return NoContent();
        }

        #endregion


        #region HTTPPATCH

        [Consumes("application/json-patch+json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPatch("{id}")]
        public async Task<IActionResult> PartiallyUpdateAspUser(string id, [FromBody] JsonPatchDocument<AspUserForUpdate> jsonPatchDocument)
        {
            AspUserForUpdate dto = new AspUserForUpdate();
            AspNetUsers aspuser = new AspNetUsers();

            //apply the patch changes to the dto. 
            jsonPatchDocument.ApplyTo(dto, ModelState);

            //if the jsonPatchDocument is not valid.
            if (!ModelState.IsValid)
            {
                //then return unprocessableEntity response.
                return new UnprocessableEntityObjectResult(ModelState);
            }

            //if the dto model is not valid after applying changes.
            if (!TryValidateModel(dto))
            {
                //then return unprocessableEntity response.
                return new UnprocessableEntityObjectResult(ModelState);
            }

            //map the chnages from dto to entity.
            Mapper.Map(dto, aspuser);

            //set the Id for the show model.
            aspuser.Id = id.ToString();

            //partially update the chnages to the db. 
            await _aspuserService.UpdatePartialEntityAsync(aspuser, jsonPatchDocument);

            //return the response.
            return NoContent();
        }
                
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPatch("ResetPassword", Name = "ResetPassword")]
        public async Task<IActionResult> ResetPassword([FromBody] ResetPasswordDto resetPasswordDto)
        {
            var uri = _configuration.GetSection("IdentityServerUrl").Value;
            var client = new RestClient(uri + "/Account/");
            // make Request
            var requestGet = new RestRequest("ChangePasswordFromApplication", Method.POST);

            // add Parameter                
            requestGet.AddParameter("userId", _userInfoService.UserId);
            requestGet.AddParameter("currentPassword", resetPasswordDto.CurrentPassword);
            requestGet.AddParameter("newPassword", resetPasswordDto.NewPassword);
            var responseGet = client.Post(requestGet);
            var res = JsonConvert.DeserializeObject(responseGet.Content);
            return Ok(res);
        }

        #endregion


        #region HTTPPOST

        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPost("UpdateAspUserwithImage", Name = "UpdateAspUserwithImage")]
        public ActionResult UpdateAspUserwithImage()
        {
            string subjectID = Request.Form["ID"];
            string oldFileName = Request.Form["ImageUrl"];
            string imageUri = "";
            AspUserDto user = new AspUserDto();

            try
            {
                // fetch file from Response
                if (Request.Form.Files.Count > 0)
                {
                    var file = Request.Form.Files[0];

                    // for add to TimeStamp in file Name
                    DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, 0).ToLocalTime();
                    TimeSpan span = (DateTime.UtcNow - epoch);
                    var timeStamp = span.TotalSeconds;

                    // set file credential for Upload to Azure Blob
                    Stream fileStream = file.OpenReadStream();
                    string contentType = file.ContentType;

                    //Users/95a54ae7-9853-42c2-aecc-51ab72ad7248_1573277141.78148.jpg
                    string fileName = "Users/" + subjectID + "_" + timeStamp + ".jpg";
                    string containerName = _configuration.GetSection("AzureBlob:AzureBlobContainer").Value;

                    if (!String.IsNullOrEmpty(oldFileName) && oldFileName != "undefined")
                    {
                        string rootDirectory = oldFileName.Substring(0, oldFileName.LastIndexOf('/') - 5);
                        oldFileName = oldFileName.Replace(rootDirectory, "");
                       // bool isDelete = _blobService.DeleteFromAzureBlob(oldFileName, containerName);
                    }

                    // call blobservice to Upload File and it will Return Image URL
                 //   imageUri = _blobService.UploadToAzureBlobFromStream(fileStream, contentType, fileName, containerName);
                }

                // get user data
                var userData = _aspuserService.GetEntityById(subjectID);

                string iDate = Request.Form["Birthday"];
                userData.Birthday = DateTime.ParseExact(iDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                if (Request.Form.Files.Count > 0)
                {
                    userData.ImageUrl = imageUri;
                }
                // fill the data
                if (!String.IsNullOrEmpty(Request.Form["FirstName"].ToString()))
                {
                    userData.FName = Request.Form["FirstName"];
                }
                if (!String.IsNullOrEmpty(Request.Form["LastName"].ToString()))
                {
                    userData.LName = Request.Form["LastName"];
                }
                if (!String.IsNullOrEmpty(Request.Form["Birthplace"].ToString()))
                {
                    userData.Birthplace = Request.Form["Birthplace"];
                }
                if (!String.IsNullOrEmpty(Request.Form["Gender"].ToString()))
                {
                    userData.Gender = Request.Form["Gender"];
                }
                if (!String.IsNullOrEmpty(Request.Form["Occupation"].ToString()))
                {
                    userData.Occupation = Request.Form["Occupation"];
                }
                if (!String.IsNullOrEmpty(Request.Form["PhoneNumber"].ToString()))
                {
                    userData.PhoneNumber = Request.Form["PhoneNumber"];
                }
                if (!String.IsNullOrEmpty(Request.Form["LivesIn"].ToString()))
                {
                    userData.LivesIn = Request.Form["LivesIn"];
                }

                _aspuserService.UpdateEntity(userData);
                _aspuserService.SaveChanges();

                user = Mapper.Map<AspUserDto>(userData);
            }
            catch (Exception ex)
            {
                return null;
            }

            return Ok(user);
        }

        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPost(Name = "CreateAspUser")]
        
        //[Authorize(Policy = Policy.CreateUsers)]
        public async Task<ActionResult<AspUserDto>> CreateAspUser([FromBody]AspUserForCreation aspuser)
        {
            var loggedInUser = _userInfoService;
            AspUserDto user = new AspUserDto();

            // Make Request
            var uri = _configuration.GetSection("IdentityServerUrl").Value;
            var client = new RestClient(uri + "/Account/");

            // make call for Update or Create User

            // make Request
            var requestPost = new RestRequest("InviteUserMail", Method.POST);

            //if user available or not.
            var userExists = _aspuserService.GetAllEntities().Where(x => x.Email == aspuser.Email).FirstOrDefault();

            if (userExists == null)
            {
                //create a show in db.
                aspuser.Id = Guid.NewGuid().ToString();
                aspuser.UserName = aspuser.Email;
                aspuser.NormalizedEmail = aspuser.Email.ToUpper();
                aspuser.NormalizedUserName = aspuser.Email.ToUpper();
                aspuser.SecurityStamp = Guid.NewGuid().ToString();
                aspuser.PasswordHash = "AO/JuU7z5uQGvK9I7k5RH//hk2L+1fO0MgVbzSHuk1R5rK6dUL84EQN0stu5vd5/FQ==";
                

                var aspuserToReturn = await _aspuserService.CreateEntityAsync<AspUserDto, AspUserForCreation>(aspuser);
                user = Mapper.Map<AspUserDto>(aspuserToReturn);
                
                // add Parameter
                requestPost.AddParameter("NewUser", true);
                requestPost.AddParameter("Email", user.Email);
                requestPost.AddParameter("AdminName", loggedInUser.FirstName + " " + loggedInUser.LastName);
                requestPost.AddParameter("Role", aspuserToReturn.Role);
                requestPost.AddParameter("UserId", aspuserToReturn.Id);
            }
            else
            {
                user = Mapper.Map<AspUserDto>(userExists);

                // add Parameter
                requestPost.AddParameter("NewUser", false);
                requestPost.AddParameter("Email", user.Email);
                requestPost.AddParameter("AdminName", loggedInUser.FirstName + " " + loggedInUser.LastName);
            }
            // call Marvin Endpoint
            var responsePost = client.Post(requestPost);

            #region Add to other related tables.

            //var isUserExist = await _edge_UserAccountService.ExistAsync(x => x.AccountId == new Guid(loggedInUser.SelectedAccountId) && x.UserId == user.Id);
            //if (!isUserExist)
            //{
            //    // create UserAccount Entity and stored in DB
            //    Edge_UserAccount accountEntity = new Edge_UserAccount();
            //    accountEntity.CreatedOn = DateTime.UtcNow;
            //    accountEntity.CreatedBy = "system";
            //    accountEntity.UpdatedOn = DateTime.UtcNow;
            //    accountEntity.CompanyName = loggedInUser.SelectedDatabase;
            //    accountEntity.Id = new Guid();
            //    accountEntity.AccountId = new Guid(loggedInUser.SelectedAccountId);
            //    accountEntity.UserId = user.Id;
            //    accountEntity.PrimaryUser = false;

            //    _edge_UserAccountService.CreateEntity(accountEntity);
            //    _edge_UserAccountService.SaveChanges();
            //}

            //// create ClientUser Entity and stored in DB
            //if (!String.IsNullOrEmpty(loggedInUser.SelectedDatabase))
            //{
            //    var clientData = _edge_ClientAccountService.GetAllEntities()
            //        .Where(c => c.DBName == loggedInUser.SelectedDatabase && c.AccountId.ToString() == loggedInUser.SelectedAccountId)
            //        .FirstOrDefault();

            //    Edge_ClientUser clientEntity = new Edge_ClientUser();
            //    clientEntity.CreatedOn = DateTime.UtcNow;
            //    clientEntity.CreatedBy = "system";
            //    clientEntity.UpdatedOn = DateTime.UtcNow;
            //    clientEntity.CompanyName = loggedInUser.SelectedDatabase;
            //    clientEntity.Id = new Guid();
            //    clientEntity.AccountId = new Guid(loggedInUser.SelectedAccountId);
            //    clientEntity.UserID = user.Id;
            //    clientEntity.ClientId = clientData.Id;
            //    clientEntity.Role = aspuser.Role;

            //    _edge_ClientUserService.CreateEntity(clientEntity);
            //    _edge_ClientUserService.SaveChanges();
            //}
            #endregion
            //return the show created response.
            return CreatedAtRoute("GetAspUser", new { id = user.Id }, user);
        }


        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPost("InviteUser", Name = "InviteUser")]
        
        //[Authorize(Policy = Policy.CreateUsers)]
        public async Task<ActionResult<AspUserDto>> InviteUser([FromBody]AspUserForCreation aspuser)
        {
            var loggedInUser = _userInfoService;
            AspUserDto user = new AspUserDto();

            // Make Request
            var uri = _configuration.GetSection("IdentityServerUrl").Value;
            var client = new RestClient(uri + "/Account/");

            // make call for Update or Create User

            // make Request
            var requestPost = new RestRequest("InviteUserMail", Method.POST);

            //if user available or not.
            var userExists = _aspuserService.GetAllEntities().Where(x => x.Email == aspuser.Email).FirstOrDefault();

            if (userExists == null)
            {
                //create a show in db.
                aspuser.Id = aspuser.Id != "" || aspuser.Id != null ? aspuser.Id : Guid.NewGuid().ToString();
                aspuser.UserName = aspuser.Email;
                aspuser.NormalizedEmail = aspuser.Email.ToUpper();
                aspuser.NormalizedUserName = aspuser.Email.ToUpper();
                aspuser.SecurityStamp = Guid.NewGuid().ToString();
                aspuser.PasswordHash = "AO/JuU7z5uQGvK9I7k5RH//hk2L+1fO0MgVbzSHuk1R5rK6dUL84EQN0stu5vd5/FQ==";
                //aspuser.TwoFactorEnabled = true;

                var aspuserToReturn = await _aspuserService.CreateEntityAsync<AspUserDto, AspUserForCreation>(aspuser);
                user = Mapper.Map<AspUserDto>(aspuserToReturn);

                // add Parameter
                requestPost.AddParameter("NewUser", true);
                requestPost.AddParameter("Email", user.Email);
                requestPost.AddParameter("AdminName", loggedInUser.FirstName + " " + loggedInUser.LastName);
                requestPost.AddParameter("Role", aspuser.Role);
                requestPost.AddParameter("FirstName", aspuser.FName);
                requestPost.AddParameter("LastName", aspuser.LName);
                requestPost.AddParameter("TwoFactorEnabled", aspuser.TwoFactorEnabled);
                requestPost.AddParameter("IsActive", aspuser.IsActive);
                requestPost.AddParameter("MiddleName", aspuser.MiddleName);
                requestPost.AddParameter("Address", aspuser.Address);
                requestPost.AddParameter("City", aspuser.City);
                requestPost.AddParameter("Province", aspuser.Province);
                requestPost.AddParameter("PostalCode", aspuser.PostalCode);
                requestPost.AddParameter("ReferencePercentage", aspuser.ReferencePercentage);
                requestPost.AddParameter("PhoneNumber", aspuser.PhoneNumber);

            }
            else
            {
                user = Mapper.Map<AspUserDto>(userExists);

                // add Parameter
                requestPost.AddParameter("NewUser", false);
                requestPost.AddParameter("Email", user.Email);
                requestPost.AddParameter("AdminName", loggedInUser.FirstName + " " + loggedInUser.LastName);
                requestPost.AddParameter("Role", aspuser.Role);
                requestPost.AddParameter("FirstName", aspuser.FName);
                requestPost.AddParameter("LastName", aspuser.LName);
                requestPost.AddParameter("TwoFactorEnabled", aspuser.TwoFactorEnabled);
                requestPost.AddParameter("IsActive", aspuser.IsActive);
                requestPost.AddParameter("MiddleName", aspuser.MiddleName);
                requestPost.AddParameter("Address", aspuser.Address);
                requestPost.AddParameter("City", aspuser.City);
                requestPost.AddParameter("Province", aspuser.Province);
                requestPost.AddParameter("PostalCode", aspuser.PostalCode);
                requestPost.AddParameter("ReferencePercentage", aspuser.ReferencePercentage);
                requestPost.AddParameter("PhoneNumber", aspuser.PhoneNumber);
            }
            // call Marvin Endpoint
            var responsePost = client.Post(requestPost);

            #region related entites handling

            //var isUserExist = await _edge_UserAccountService.ExistAsync(x => x.AccountId == new Guid(loggedInUser.SelectedAccountId) && x.UserId == user.Id);
            //if (!isUserExist)
            //{
            //    // create UserAccount Entity and stored in DB
            //    Edge_UserAccount accountEntity = new Edge_UserAccount();
            //    accountEntity.CreatedOn = DateTime.UtcNow;
            //    accountEntity.CreatedBy = "system";
            //    accountEntity.UpdatedOn = DateTime.UtcNow;
            //    accountEntity.CompanyName = loggedInUser.SelectedDatabase;
            //    accountEntity.Id = new Guid();
            //    accountEntity.AccountId = new Guid(loggedInUser.SelectedAccountId);
            //    accountEntity.UserId = user.Id;
            //    accountEntity.PrimaryUser = false;

            //    _edge_UserAccountService.CreateEntity(accountEntity);
            //    _edge_UserAccountService.SaveChanges();
            //}

            //if (!String.IsNullOrEmpty(clientId))
            //{
            //    Edge_ClientUser clientEntity = new Edge_ClientUser();
            //    clientEntity.CreatedOn = DateTime.UtcNow;
            //    clientEntity.CreatedBy = "system";
            //    clientEntity.UpdatedOn = DateTime.UtcNow;
            //    clientEntity.CompanyName = loggedInUser.SelectedDatabase;
            //    clientEntity.Id = new Guid();
            //    clientEntity.AccountId = new Guid(loggedInUser.SelectedAccountId);
            //    clientEntity.UserID = user.Id;
            //    clientEntity.ClientId = new Guid(clientId);
            //    clientEntity.Role = aspuser.Role;

            //    _edge_ClientUserService.CreateEntity(clientEntity);
            //    _edge_ClientUserService.SaveChanges();
            //}

            #endregion

            //return the show created response.
            return CreatedAtRoute("GetAspUser", new { id = user.Id }, user);
        }


        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPost("CreateWebsiteUser", Name = "CreateWebsiteUser")]
        [AllowAnonymous]
        [ApiKeyAuth]
        //[Authorize(Policy = Policy.CreateUsers)]
        public async Task<ActionResult<AspUserDto>> CreateWebsiteUser([FromBody] AspUserForCreation aspuser)
        {
            var loggedInUser = _userInfoService;
            AspUserDto user = new AspUserDto();

            // Make Request
            var uri = _configuration.GetSection("IdentityServerUrl").Value;
            var client = new RestClient(uri + "/Account/");

            // make call for Update or Create User

            // make Request
            var requestPost = new RestRequest("InviteUserMail", Method.POST);

            //if user available or not.
            var userExists = _aspuserService.GetAllEntities().Where(x => x.Email == aspuser.Email).FirstOrDefault();

            if (userExists == null)
            {
                //create a show in db.
                aspuser.Id = aspuser.Id != "" || aspuser.Id != null ? aspuser.Id : Guid.NewGuid().ToString();
                aspuser.UserName = aspuser.Email;
                aspuser.NormalizedEmail = aspuser.Email.ToUpper();
                aspuser.NormalizedUserName = aspuser.Email.ToUpper();
                aspuser.SecurityStamp = Guid.NewGuid().ToString();
                aspuser.PasswordHash = "AO/JuU7z5uQGvK9I7k5RH//hk2L+1fO0MgVbzSHuk1R5rK6dUL84EQN0stu5vd5/FQ==";
                aspuser.TwoFactorEnabled = true;

                var aspuserToReturn = await _aspuserService.CreateEntityAsync<AspUserDto, AspUserForCreation>(aspuser);
                user = Mapper.Map<AspUserDto>(aspuserToReturn);

                // add Parameter
                requestPost.AddParameter("NewUser", true);
                requestPost.AddParameter("Email", user.Email);
                requestPost.AddParameter("AdminName", loggedInUser.FirstName + " " + loggedInUser.LastName);
                requestPost.AddParameter("Role", aspuser.Role);
                requestPost.AddParameter("FirstName", aspuser.FName);
                requestPost.AddParameter("LastName", aspuser.LName);

            }
            else
            {
                user = Mapper.Map<AspUserDto>(userExists);

                // add Parameter
                requestPost.AddParameter("NewUser", false);
                requestPost.AddParameter("Email", user.Email);
                requestPost.AddParameter("AdminName", loggedInUser.FirstName + " " + loggedInUser.LastName);
                requestPost.AddParameter("Role", aspuser.Role);
                requestPost.AddParameter("FirstName", aspuser.FName);
                requestPost.AddParameter("LastName", aspuser.LName);
            }
            // call Marvin Endpoint
            var responsePost = client.Post(requestPost);

 
            //return the show created response.
            return CreatedAtRoute("GetAspUser", new { id = user.Id }, user);
        }
        #endregion


        #region HTTPDELETE
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}", Name = "DeleteAspUserById")]
        public async Task<IActionResult> DeleteAspUserById(string id)
        {
            //if the aspuser exists
            if (await _aspuserService.ExistAsync(x => x.Id == id.ToString()))
            {
                //delete the aspuser from the db.
                await _aspuserService.DeleteEntityAsync(id);
            }
            else
            {
                //if aspuser doesn't exists then returns not found.
                return NotFound();
            }

            //return the response.
            return NoContent();
        }

        #endregion


        #region PRIVATE METHODS

        private IEnumerable<LinkDto> CreateLinksForAspUser(string id, string fields)
        {
            var links = new List<LinkDto>();

            if (string.IsNullOrWhiteSpace(fields))
            {
                links.Add(
                  new LinkDto(_urlHelper.Link("GetAspUser", new { id = id }),
                  "self",
                  "GET"));
            }
            else
            {
                links.Add(
                  new LinkDto(_urlHelper.Link("GetAspUser", new { id = id, fields = fields }),
                  "self",
                  "GET"));
            }

            links.Add(
              new LinkDto(_urlHelper.Link("DeleteAspUserById", new { id = id }),
              "delete_aspuser",
              "DELETE"));

            links.Add(
             new LinkDto(_urlHelper.Link("UpdateAspUser", new { id = id }),
             "update_aspuser",
             "PUT"));

            links.Add(
              new LinkDto(_urlHelper.Link("CreateAspUser", new { }),
              "create_aspuser",
              "POST"));

            //links.Add(
            //   new LinkDto(_urlHelper.Link("GetShowsForTour", new { }),
            //   "shows",
            //   "GET"));

            return links;
        }

        private IEnumerable<LinkDto> CreateLinksForAspUsers(FilterOptionsModel filterOptionsModel, bool hasNext, bool hasPrevious)
        {
            var links = new List<LinkDto>();

            links.Add(
               new LinkDto(CreateAspUsersResourceUri(filterOptionsModel, ResourceUriType.Current), "self", "GET"));

            if (hasNext)
            {
                links.Add(
                  new LinkDto(CreateAspUsersResourceUri(filterOptionsModel, ResourceUriType.NextPage), "nextPage", "GET"));
            }

            if (hasPrevious)
            {
                links.Add(new LinkDto(CreateAspUsersResourceUri(filterOptionsModel, ResourceUriType.PreviousPage), "previousPage", "GET"));
            }

            return links;
        }

        private string CreateAspUsersResourceUri(FilterOptionsModel filterOptionsModel, ResourceUriType type)
        {
            switch (type)
            {
                case ResourceUriType.PreviousPage:
                    return _urlHelper.Link("GetFilteredAspUsers",
                      new
                      {
                          fields = filterOptionsModel.Fields,
                          orderBy = filterOptionsModel.OrderBy,
                          searchQuery = filterOptionsModel.SearchQuery,
                          pageNumber = filterOptionsModel.PageNumber - 1,
                          pageSize = filterOptionsModel.PageSize
                      });
                case ResourceUriType.NextPage:
                    return _urlHelper.Link("GetFilteredAspUsers",
                      new
                      {
                          fields = filterOptionsModel.Fields,
                          orderBy = filterOptionsModel.OrderBy,
                          searchQuery = filterOptionsModel.SearchQuery,
                          pageNumber = filterOptionsModel.PageNumber + 1,
                          pageSize = filterOptionsModel.PageSize
                      });

                default:
                    return _urlHelper.Link("GetFilteredAspUsers",
                    new
                    {
                        fields = filterOptionsModel.Fields,
                        orderBy = filterOptionsModel.OrderBy,
                        searchQuery = filterOptionsModel.SearchQuery,
                        pageNumber = filterOptionsModel.PageNumber,
                        pageSize = filterOptionsModel.PageSize
                    });
            }
        }

        #endregion

    }
}
