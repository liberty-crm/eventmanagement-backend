using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
//using EventManagement.API.Helpers;
using EventManagement.Dto;
using EventManagement.Service;
using EventManagement.Domain.Entities;
using EventManagement.Utility;
using Microsoft.Extensions.Logging;
using System.Linq;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;

namespace EventManagement.API.Controllers
{
    /// <summary>
    /// Contact endpoint
    /// </summary>
    [Route("api/contacts")]
    [Produces("application/json")]
    [ApiController]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]

    public class ContactController : Controller
    {

        #region PRIVATE MEMBERS

        private readonly IContactService _contactService;
        private ILogger<ContactController> _logger;
        private readonly IUrlHelper _urlHelper;

        #endregion


        #region CONSTRUCTOR

        public ContactController(IContactService contactService, ILogger<ContactController> logger, IUrlHelper urlHelper) 
        {
            _logger = logger;
            _contactService = contactService;
            _urlHelper = urlHelper;
        }

        #endregion


        #region HTTPGET

        [HttpGet(Name = "GetFilteredContacts")]
        [Produces("application/vnd.tourmanagement.contacts.hateoas+json", "application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<List<ContactDto>>> GetFilteredContacts([FromQuery]FilterOptionsModel filterOptionsModel, [FromHeader(Name = "Accept")] string mediaType)
        {

            //if order by fields are not valid.
            if (!_contactService.ValidMappingExists(filterOptionsModel.OrderBy))
            {
                //then return bad request.
                return BadRequest();
            }

            //if fields are not valid.
            if (!EventManagementUtils.TypeHasProperties<ContactDto>(filterOptionsModel.Fields))
            {
                //then return bad request.
                return BadRequest();
            }
            
            //get the paged/filtered show from db. 
            var contactsFromRepo = await _contactService.GetFilteredEntities(filterOptionsModel);            
            
            //if HATEOAS links are required.
            if (mediaType == "application/vnd.tourmanagement.contacts.hateoas+json")
            {
                //create HATEOAS links for each show.
                contactsFromRepo.ForEach(contact =>
                {
                    var entityLinks = CreateLinksForContact(contact.Id, filterOptionsModel.Fields);
                    contact.links = entityLinks;
                });

                //prepare pagination metadata.
                var paginationMetadata = new
                {
                    totalCount = contactsFromRepo.TotalCount,
                    pageSize = contactsFromRepo.PageSize,
                    currentPage = contactsFromRepo.CurrentPage,
                    totalPages = contactsFromRepo.TotalPages,
                };

                //add pagination meta data to response header.
                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMetadata));

                //create links for shows.
                var links = CreateLinksForContacts(filterOptionsModel, contactsFromRepo.HasNext, contactsFromRepo.HasPrevious);
                

                //prepare model with data and HATEOAS links.
                var linkedCollectionResource = new
                {
                    value = contactsFromRepo,
                    links = links
                };


                //return the data with Ok response.
                return Ok(linkedCollectionResource);
            }
            else
            {                

                var previousPageLink = contactsFromRepo.HasPrevious ?
                    CreateContactsResourceUri(filterOptionsModel, ResourceUriType.PreviousPage) : null;

                var nextPageLink = contactsFromRepo.HasNext ?
                    CreateContactsResourceUri(filterOptionsModel, ResourceUriType.NextPage) : null;

                //prepare the pagination metadata.
                var paginationMetadata = new
                {
                    previousPageLink = previousPageLink,
                    nextPageLink = nextPageLink,
                    totalCount = contactsFromRepo.TotalCount,
                    pageSize = contactsFromRepo.PageSize,
                    currentPage = contactsFromRepo.CurrentPage,
                    totalPages = contactsFromRepo.TotalPages
                };

                //add pagination meta data to response header.
                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMetadata));

                //return the data with Ok response.
                return Ok(contactsFromRepo);
            }
        }

        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Produces("application/vnd.tourmanagement.contacts.hateoas+json", "application/json")]
        [HttpGet("LeadId", Name = "GetContactByLeadId")]
        public async Task<ActionResult<List<ContactDto>>> GetFilteredContactsByLeadId([FromQuery] FilterOptionsModel filterOptionsModel, [FromHeader(Name = "Accept")] string mediaType, string LeadId)
        {
            //if order by fields are not valid.
            if (!_contactService.ValidMappingExists(filterOptionsModel.OrderBy))
            {
                //then return bad request.
                return BadRequest();
            }

            //if fields are not valid.
            if (!EventManagementUtils.TypeHasProperties<ContactDto>(filterOptionsModel.Fields))
            {
                //then return bad request.
                return BadRequest();
            }

            //get the paged/filtered show from db. 
            var contactsFromRepo = await _contactService.GetFilteredEntities(filterOptionsModel);

            var contactToReturn = contactsFromRepo.ToList().Where(x => x.RelatedLeadContactId != null && x.RelatedLeadContactId.Equals(Guid.Parse(LeadId))).ToList();
            //if HATEOAS links are required.
            if (mediaType == "application/vnd.tourmanagement.contacts.hateoas+json")
            {
                //create HATEOAS links for each show.
                contactsFromRepo.ForEach(contact =>
                {
                    var entityLinks = CreateLinksForContact(contact.Id, filterOptionsModel.Fields);
                    contact.links = entityLinks;
                });

                //prepare pagination metadata.
                var paginationMetadata = new
                {
                    totalCount = contactsFromRepo.TotalCount,
                    pageSize = contactsFromRepo.PageSize,
                    currentPage = contactsFromRepo.CurrentPage,
                    totalPages = contactsFromRepo.TotalPages,
                };

                //add pagination meta data to response header.
                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMetadata));

                //create links for shows.
                var links = CreateLinksForContacts(filterOptionsModel, contactsFromRepo.HasNext, contactsFromRepo.HasPrevious);

                var result = contactsFromRepo.Where(x => x.RelatedLeadContactId !=null && x.RelatedLeadContactId.Equals(Guid.Parse(LeadId)));

                //prepare model with data and HATEOAS links.
                var linkedCollectionResource = new
                {
                    value = result,
                    links = links
                };


                //return the data with Ok response.
                return Ok(linkedCollectionResource);
            }
            else
            {
                var result = contactsFromRepo.Where(x => x.RelatedLeadContactId != null && x.RelatedLeadContactId.Equals(Guid.Parse(LeadId)));

                var previousPageLink = contactsFromRepo.HasPrevious ?
                    CreateContactsResourceUri(filterOptionsModel, ResourceUriType.PreviousPage) : null;

                var nextPageLink = contactsFromRepo.HasNext ?
                    CreateContactsResourceUri(filterOptionsModel, ResourceUriType.NextPage) : null;

                //prepare the pagination metadata.
                var paginationMetadata = new
                {
                    previousPageLink = previousPageLink,
                    nextPageLink = nextPageLink,
                    totalCount = result.Count().ToString(),
                    pageSize = contactsFromRepo.PageSize,
                    currentPage = contactsFromRepo.CurrentPage,
                    totalPages = contactsFromRepo.TotalPages
                };

                //add pagination meta data to response header.
                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMetadata));

                //return the data with Ok response.
                return Ok(result);
            }
        }



        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Produces("application/vnd.tourmanagement.contacts.hateoas+json", "application/json")]
        [HttpGet("{id}", Name = "GetContact")]
        public async Task<ActionResult<Contact>> GetContact(Guid id, [FromQuery] string fields, [FromHeader(Name = "Accept")] string mediaType)
        {
            object contactEntity = null;
            object linkedResourceToReturn = null;

            //if fields are not passed.
            if (string.IsNullOrEmpty(fields))
            {
                _logger.LogInformation("GetContact called");

                //then get the whole entity and map it to the Dto.
                contactEntity = Mapper.Map<ContactDto>(await _contactService.GetEntityByIdAsync(id));
            }
            else
            {
                //if fields are passed then get the partial show.
                contactEntity = await _contactService.GetPartialEntityAsync(id, fields);
            }

            //if contact not found.
            if (contactEntity == null)
            {
                //then return not found response.
                return NotFound();
            }

            if (mediaType == "application/vnd.tourmanagement.contacts.hateoas+json")
            {
                //create HATEOS links
                var links = CreateLinksForContact(id, fields);

                //if fields are not passed.
                if (string.IsNullOrEmpty(fields))
                {
                    //convert the typed object to expando object.
                    linkedResourceToReturn = ((ContactDto)contactEntity).ShapeData("") as IDictionary<string, object>;

                    //add the HATEOAS links to the model.
                    ((IDictionary<string, object>)linkedResourceToReturn).Add("links", links);
                }
                else
                {
                    linkedResourceToReturn = contactEntity;

                    //add the HATEOAS links to the model.
                    ((dynamic)linkedResourceToReturn).links = links;

                }
            }
            else
            {
                linkedResourceToReturn = contactEntity;
            }

            //return the Ok response.
            return Ok(linkedResourceToReturn);
        }

        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Produces("application/vnd.tourmanagement.contacts.hateoas+json", "application/json")]
        [HttpGet("IsEmailExist", Name = "IsEmailExist")]
        public async Task<ActionResult<Contact>> IsEmailExist(string email)
        {
            return Ok(await _contactService.ExistAsync(x => x.Email == email));
        }

        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Produces("application/vnd.tourmanagement.contacts.hateoas+json", "application/json")]
        [HttpGet("IsPhoneNumberExist", Name = "IsPhoneNumberExist")]
        public async Task<ActionResult<Contact>> IsPhoneNumberExist(string phone)
        {
            return Ok(await _contactService.ExistAsync(x => x.Phone == phone));
        }

        #endregion


        #region HTTPPUT

        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPut("{id}", Name = "UpdateContact")]
        public async Task<IActionResult> UpdateContact(Guid id, [FromBody]ContactForUpdate ContactForUpdate)
        {
            try
            {

                //if show not found
                if (!await _contactService.ExistAsync(x => x.Id == id))
                {
                    //then return not found response.
                    return NotFound();
                }

                //Update an entity.
                await _contactService.UpdateEntityAsync(id, ContactForUpdate);

                //return the response.
                return NoContent();
            }
            catch(Exception ex)
            {
                return BadRequest();
            }
        }

        #endregion


        #region HTTPPATCH

        [Consumes("application/json-patch+json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPatch("{id}")]
        public async Task<IActionResult> PartiallyUpdateContact(Guid id, [FromBody] JsonPatchDocument<ContactForUpdate> jsonPatchDocument)
        {
            ContactForUpdate dto = new ContactForUpdate();
            Contact contact = new Contact();

            //apply the patch changes to the dto. 
            jsonPatchDocument.ApplyTo(dto, ModelState);

            //if the jsonPatchDocument is not valid.
            if (!ModelState.IsValid)
            {
                //then return unprocessableEntity response.
                return new UnprocessableEntityObjectResult(ModelState);
            }

            //if the dto model is not valid after applying changes.
            if (!TryValidateModel(dto))
            {
                //then return unprocessableEntity response.
                return new UnprocessableEntityObjectResult(ModelState);
            }

            //map the chnages from dto to entity.
            Mapper.Map(dto, contact);

            //set the Id for the show model.
            contact.Id = id;

            //partially update the chnages to the db. 
            await _contactService.UpdatePartialEntityAsync(contact, jsonPatchDocument);

            //return the response.
            return NoContent();
        }

        #endregion


        #region HTTPPOST

        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPost(Name = "CreateContact")]
        public async Task<ActionResult<ContactDto>> CreateContact([FromBody]ContactForCreation contact)
        {
            contact.Id = Guid.NewGuid().ToString();
            //create a show in db.
            var contactToReturn = await _contactService.CreateEntityAsync<ContactDto, ContactForCreation>(contact);

            //return the show created response.
            return CreatedAtRoute("GetContact", new { id = contactToReturn.Id }, contactToReturn);
        }

        #endregion


        #region HTTPDELETE
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}", Name = "DeleteContactById")]
        public async Task<IActionResult> DeleteContactById(Guid id)
        {
            //if the contact exists
            if (await _contactService.ExistAsync(x => x.Id == id))
            {
                //delete the contact from the db.
                await _contactService.DeleteEntityAsync(id);
            }
            else
            {
                //if contact doesn't exists then returns not found.
                return NotFound();
            }

            //return the response.
            return NoContent();
        }

        #endregion


        #region PRIVATE METHODS

        private IEnumerable<LinkDto> CreateLinksForContact(Guid id, string fields)
        {
            var links = new List<LinkDto>();

            if (string.IsNullOrWhiteSpace(fields))
            {
                links.Add(
                  new LinkDto(_urlHelper.Link("GetContact", new { id = id }),
                  "self",
                  "GET"));
            }
            else
            {
                links.Add(
                  new LinkDto(_urlHelper.Link("GetContact", new { id = id, fields = fields }),
                  "self",
                  "GET"));
            }

            links.Add(
              new LinkDto(_urlHelper.Link("DeleteContactById", new { id = id }),
              "delete_contact",
              "DELETE"));

            links.Add(
             new LinkDto(_urlHelper.Link("UpdateContact", new { id = id }),
             "update_contact",
             "PUT"));

            links.Add(
              new LinkDto(_urlHelper.Link("CreateContact", new { }),
              "create_contact",
              "POST"));

            //links.Add(
            //   new LinkDto(_urlHelper.Link("GetShowsForTour", new { }),
            //   "shows",
            //   "GET"));

            return links;
        }

        private IEnumerable<LinkDto> CreateLinksForContacts(FilterOptionsModel filterOptionsModel, bool hasNext, bool hasPrevious)
        {
            var links = new List<LinkDto>();

            links.Add(
               new LinkDto(CreateContactsResourceUri(filterOptionsModel, ResourceUriType.Current), "self", "GET"));

            if (hasNext)
            {
                links.Add(
                  new LinkDto(CreateContactsResourceUri(filterOptionsModel, ResourceUriType.NextPage), "nextPage", "GET"));
            }

            if (hasPrevious)
            {
                links.Add(new LinkDto(CreateContactsResourceUri(filterOptionsModel, ResourceUriType.PreviousPage), "previousPage", "GET"));
            }

            return links;
        }

        private string CreateContactsResourceUri(FilterOptionsModel filterOptionsModel, ResourceUriType type)
        {
            switch (type)
            {
                case ResourceUriType.PreviousPage:
                    return _urlHelper.Link("GetFilteredContacts",
                      new
                      {
                          fields = filterOptionsModel.Fields,
                          orderBy = filterOptionsModel.OrderBy,
                          searchQuery = filterOptionsModel.SearchQuery,
                          pageNumber = filterOptionsModel.PageNumber - 1,
                          pageSize = filterOptionsModel.PageSize
                      });
                case ResourceUriType.NextPage:
                    return _urlHelper.Link("GetFilteredContacts",
                      new
                      {
                          fields = filterOptionsModel.Fields,
                          orderBy = filterOptionsModel.OrderBy,
                          searchQuery = filterOptionsModel.SearchQuery,
                          pageNumber = filterOptionsModel.PageNumber + 1,
                          pageSize = filterOptionsModel.PageSize
                      });

                default:
                    return _urlHelper.Link("GetFilteredContacts",
                    new
                    {
                        fields = filterOptionsModel.Fields,
                        orderBy = filterOptionsModel.OrderBy,
                        searchQuery = filterOptionsModel.SearchQuery,
                        pageNumber = filterOptionsModel.PageNumber,
                        pageSize = filterOptionsModel.PageSize
                    });
            }
        }

        #endregion

    }
}
