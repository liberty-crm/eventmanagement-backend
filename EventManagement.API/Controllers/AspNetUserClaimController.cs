using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
//using EventManagement.API.Helpers;
using EventManagement.Dto;
using EventManagement.Service;
using EventManagement.Domain.Entities;
using EventManagement.Utility;
using Microsoft.Extensions.Logging;
using FinanaceManagement.API.Models;
using IdentityServer4.AccessTokenValidation;

namespace EventManagement.API.Controllers
{
    /// <summary>
    /// AspNetUserClaim endpoint
    /// </summary>
    [Route("api/aspnetuserclaims")]
    [Produces("application/json")]
    [ApiController]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]

    public class AspNetUserClaimController : Controller
    {

        #region PRIVATE MEMBERS

        private readonly IAspNetUserClaimService _aspnetuserclaimService;
        private ILogger<AspNetUserClaimController> _logger;
        private readonly IUrlHelper _urlHelper;

        #endregion


        #region CONSTRUCTOR

        public AspNetUserClaimController(IAspNetUserClaimService aspnetuserclaimService, ILogger<AspNetUserClaimController> logger, IUrlHelper urlHelper) 
        {
            _logger = logger;
            _aspnetuserclaimService = aspnetuserclaimService;
            _urlHelper = urlHelper;
        }

        #endregion


        #region HTTPGET

        [HttpGet(Name = "GetFilteredAspNetUserClaims")]
        [Produces("application/vnd.tourmanagement.aspnetuserclaims.hateoas+json", "application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<List<AspNetUserClaimDto>>> GetFilteredAspNetUserClaims([FromQuery]FilterOptionsModel filterOptionsModel, [FromHeader(Name = "Accept")] string mediaType)
        {

            //if order by fields are not valid.
            if (!_aspnetuserclaimService.ValidMappingExists(filterOptionsModel.OrderBy))
            {
                //then return bad request.
                return BadRequest();
            }

            //if fields are not valid.
            if (!EventManagementUtils.TypeHasProperties<AspNetUserClaimDto>(filterOptionsModel.Fields))
            {
                //then return bad request.
                return BadRequest();
            }

            //get the paged/filtered show from db. 
            var aspnetuserclaimsFromRepo = await _aspnetuserclaimService.GetFilteredEntities(filterOptionsModel);

            //if HATEOAS links are required.
            if (mediaType == "application/vnd.tourmanagement.aspnetuserclaims.hateoas+json")
            {
                //create HATEOAS links for each show.
                aspnetuserclaimsFromRepo.ForEach(aspnetuserclaim =>
                {
                    var entityLinks = CreateLinksForAspNetUserClaim(aspnetuserclaim.Id, filterOptionsModel.Fields);
                    aspnetuserclaim.links = entityLinks;
                });

                //prepare pagination metadata.
                var paginationMetadata = new
                {
                    totalCount = aspnetuserclaimsFromRepo.TotalCount,
                    pageSize = aspnetuserclaimsFromRepo.PageSize,
                    currentPage = aspnetuserclaimsFromRepo.CurrentPage,
                    totalPages = aspnetuserclaimsFromRepo.TotalPages,
                };

                //add pagination meta data to response header.
                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMetadata));

                //create links for shows.
                var links = CreateLinksForAspNetUserClaims(filterOptionsModel, aspnetuserclaimsFromRepo.HasNext, aspnetuserclaimsFromRepo.HasPrevious);

                //prepare model with data and HATEOAS links.
                var linkedCollectionResource = new
                {
                    value = aspnetuserclaimsFromRepo,
                    links = links
                };

                //return the data with Ok response.
                return Ok(linkedCollectionResource);
            }
            else
            {
                var previousPageLink = aspnetuserclaimsFromRepo.HasPrevious ?
                    CreateAspNetUserClaimsResourceUri(filterOptionsModel, ResourceUriType.PreviousPage) : null;

                var nextPageLink = aspnetuserclaimsFromRepo.HasNext ?
                    CreateAspNetUserClaimsResourceUri(filterOptionsModel, ResourceUriType.NextPage) : null;

                //prepare the pagination metadata.
                var paginationMetadata = new
                {
                    previousPageLink = previousPageLink,
                    nextPageLink = nextPageLink,
                    totalCount = aspnetuserclaimsFromRepo.TotalCount,
                    pageSize = aspnetuserclaimsFromRepo.PageSize,
                    currentPage = aspnetuserclaimsFromRepo.CurrentPage,
                    totalPages = aspnetuserclaimsFromRepo.TotalPages
                };

                //add pagination meta data to response header.
                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMetadata));

                //return the data with Ok response.
                return Ok(aspnetuserclaimsFromRepo);
            }
        }


        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Produces("application/vnd.tourmanagement.aspnetuserclaims.hateoas+json", "application/json")]
        [HttpGet("{id}", Name = "GetAspNetUserClaim")]
        public async Task<ActionResult<AspNetUserClaim>> GetAspNetUserClaim(int id, [FromQuery] string fields, [FromHeader(Name = "Accept")] string mediaType)
        {
            object aspnetuserclaimEntity = null;
            object linkedResourceToReturn = null;

            //if fields are not passed.
            if (string.IsNullOrEmpty(fields))
            {
                _logger.LogInformation("GetAspNetUserClaim called");

                //then get the whole entity and map it to the Dto.
                aspnetuserclaimEntity = Mapper.Map<AspNetUserClaimDto>(await _aspnetuserclaimService.GetEntityByIdAsync(id));
            }
            else
            {
                //if fields are passed then get the partial show.
                aspnetuserclaimEntity = await _aspnetuserclaimService.GetPartialEntityAsync(id, fields);
            }

            //if aspnetuserclaim not found.
            if (aspnetuserclaimEntity == null)
            {
                //then return not found response.
                return NotFound();
            }

            if (mediaType == "application/vnd.tourmanagement.aspnetuserclaims.hateoas+json")
            {
                //create HATEOS links
                var links = CreateLinksForAspNetUserClaim(id, fields);

                //if fields are not passed.
                if (string.IsNullOrEmpty(fields))
                {
                    //convert the typed object to expando object.
                    linkedResourceToReturn = ((AspNetUserClaimDto)aspnetuserclaimEntity).ShapeData("") as IDictionary<string, object>;

                    //add the HATEOAS links to the model.
                    ((IDictionary<string, object>)linkedResourceToReturn).Add("links", links);
                }
                else
                {
                    linkedResourceToReturn = aspnetuserclaimEntity;

                    //add the HATEOAS links to the model.
                    ((dynamic)linkedResourceToReturn).links = links;

                }
            }
            else
            {
                linkedResourceToReturn = aspnetuserclaimEntity;
            }

            //return the Ok response.
            return Ok(linkedResourceToReturn);
        }

        #endregion


        #region HTTPPUT

        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPut("{id}", Name = "UpdateAspNetUserClaim")]
        public async Task<IActionResult> UpdateAspNetUserClaim(int id, [FromBody]AspNetUserClaimForUpdate AspNetUserClaimForUpdate)
        {

            //if show not found
            if (!await _aspnetuserclaimService.ExistAsync(x => x.Id == id))
            {
                //then return not found response.
                return NotFound();
            }

            //Update an entity.
            await _aspnetuserclaimService.UpdateEntityAsync(id, AspNetUserClaimForUpdate);

            //return the response.
            return NoContent();
        }

        #endregion


        #region HTTPPATCH

        [Consumes("application/json-patch+json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPatch("{id}")]
        public async Task<IActionResult> PartiallyUpdateAspNetUserClaim(int id, [FromBody] JsonPatchDocument<AspNetUserClaimForUpdate> jsonPatchDocument)
        {
            AspNetUserClaimForUpdate dto = new AspNetUserClaimForUpdate();
            AspNetUserClaim aspnetuserclaim = new AspNetUserClaim();

            //apply the patch changes to the dto. 
            jsonPatchDocument.ApplyTo(dto, ModelState);

            //if the jsonPatchDocument is not valid.
            if (!ModelState.IsValid)
            {
                //then return unprocessableEntity response.
                return new UnprocessableEntityObjectResult(ModelState);
            }

            //if the dto model is not valid after applying changes.
            if (!TryValidateModel(dto))
            {
                //then return unprocessableEntity response.
                return new UnprocessableEntityObjectResult(ModelState);
            }

            //map the chnages from dto to entity.
            Mapper.Map(dto, aspnetuserclaim);

            //set the Id for the show model.
            aspnetuserclaim.Id = id;

            //partially update the chnages to the db. 
            await _aspnetuserclaimService.UpdatePartialEntityAsync(aspnetuserclaim, jsonPatchDocument);

            //return the response.
            return NoContent();
        }

        #endregion


        #region HTTPPOST

        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPost(Name = "CreateAspNetUserClaim")]
        public async Task<ActionResult<AspNetUserClaimDto>> CreateAspNetUserClaim([FromBody]AspNetUserClaimForCreation aspnetuserclaim)
        {
            //create a show in db.
            var aspnetuserclaimToReturn = await _aspnetuserclaimService.CreateEntityAsync<AspNetUserClaimDto, AspNetUserClaimForCreation>(aspnetuserclaim);

            //return the show created response.
            return CreatedAtRoute("GetAspNetUserClaim", new { id = aspnetuserclaimToReturn.Id }, aspnetuserclaimToReturn);
        }

        #endregion


        #region HTTPDELETE
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}", Name = "DeleteAspNetUserClaimById")]
        public async Task<IActionResult> DeleteAspNetUserClaimById(int id)
        {
            //if the aspnetuserclaim exists
            if (await _aspnetuserclaimService.ExistAsync(x => x.Id == id))
            {
                //delete the aspnetuserclaim from the db.
                await _aspnetuserclaimService.DeleteEntityAsync(id);
            }
            else
            {
                //if aspnetuserclaim doesn't exists then returns not found.
                return NotFound();
            }

            //return the response.
            return NoContent();
        }

        #endregion


        #region PRIVATE METHODS

        private IEnumerable<LinkDto> CreateLinksForAspNetUserClaim(int id, string fields)
        {
            var links = new List<LinkDto>();

            if (string.IsNullOrWhiteSpace(fields))
            {
                links.Add(
                  new LinkDto(_urlHelper.Link("GetAspNetUserClaim", new { id = id }),
                  "self",
                  "GET"));
            }
            else
            {
                links.Add(
                  new LinkDto(_urlHelper.Link("GetAspNetUserClaim", new { id = id, fields = fields }),
                  "self",
                  "GET"));
            }

            links.Add(
              new LinkDto(_urlHelper.Link("DeleteAspNetUserClaimById", new { id = id }),
              "delete_aspnetuserclaim",
              "DELETE"));

            links.Add(
             new LinkDto(_urlHelper.Link("UpdateAspNetUserClaim", new { id = id }),
             "update_aspnetuserclaim",
             "PUT"));

            links.Add(
              new LinkDto(_urlHelper.Link("CreateAspNetUserClaim", new { }),
              "create_aspnetuserclaim",
              "POST"));

            //links.Add(
            //   new LinkDto(_urlHelper.Link("GetShowsForTour", new { }),
            //   "shows",
            //   "GET"));

            return links;
        }

        private IEnumerable<LinkDto> CreateLinksForAspNetUserClaims(FilterOptionsModel filterOptionsModel, bool hasNext, bool hasPrevious)
        {
            var links = new List<LinkDto>();

            links.Add(
               new LinkDto(CreateAspNetUserClaimsResourceUri(filterOptionsModel, ResourceUriType.Current), "self", "GET"));

            if (hasNext)
            {
                links.Add(
                  new LinkDto(CreateAspNetUserClaimsResourceUri(filterOptionsModel, ResourceUriType.NextPage), "nextPage", "GET"));
            }

            if (hasPrevious)
            {
                links.Add(new LinkDto(CreateAspNetUserClaimsResourceUri(filterOptionsModel, ResourceUriType.PreviousPage), "previousPage", "GET"));
            }

            return links;
        }

        private string CreateAspNetUserClaimsResourceUri(FilterOptionsModel filterOptionsModel, ResourceUriType type)
        {
            switch (type)
            {
                case ResourceUriType.PreviousPage:
                    return _urlHelper.Link("GetFilteredAspNetUserClaims",
                      new
                      {
                          fields = filterOptionsModel.Fields,
                          orderBy = filterOptionsModel.OrderBy,
                          searchQuery = filterOptionsModel.SearchQuery,
                          pageNumber = filterOptionsModel.PageNumber - 1,
                          pageSize = filterOptionsModel.PageSize
                      });
                case ResourceUriType.NextPage:
                    return _urlHelper.Link("GetFilteredAspNetUserClaims",
                      new
                      {
                          fields = filterOptionsModel.Fields,
                          orderBy = filterOptionsModel.OrderBy,
                          searchQuery = filterOptionsModel.SearchQuery,
                          pageNumber = filterOptionsModel.PageNumber + 1,
                          pageSize = filterOptionsModel.PageSize
                      });

                default:
                    return _urlHelper.Link("GetFilteredAspNetUserClaims",
                    new
                    {
                        fields = filterOptionsModel.Fields,
                        orderBy = filterOptionsModel.OrderBy,
                        searchQuery = filterOptionsModel.SearchQuery,
                        pageNumber = filterOptionsModel.PageNumber,
                        pageSize = filterOptionsModel.PageSize
                    });
            }
        }

        #endregion

    }
}
