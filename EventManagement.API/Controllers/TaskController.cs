using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
//using EventManagement.API.Helpers;
using EventManagement.Dto;
using EventManagement.Service;
using EventManagement.Domain.Entities;
using EventManagement.Utility;
using Microsoft.Extensions.Logging;
using Task = EventManagement.Domain.Entities.Task;
using IdentityServer4.AccessTokenValidation;

namespace EventManagement.API.Controllers
{
    /// <summary>
    /// Task endpoint
    /// </summary>
    [Route("api/tasks")]
    [Produces("application/json")]
    [ApiController]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]

    public class TaskController : Controller
    {

        #region PRIVATE MEMBERS

        private readonly ITaskService _taskService;
        private ILogger<TaskController> _logger;
        private readonly IUrlHelper _urlHelper;

        #endregion


        #region CONSTRUCTOR

        public TaskController(ITaskService taskService, ILogger<TaskController> logger, IUrlHelper urlHelper) 
        {
            _logger = logger;
            _taskService = taskService;
            _urlHelper = urlHelper;
        }

        #endregion


        #region HTTPGET

        [HttpGet(Name = "GetFilteredTasks")]
        [Produces("application/vnd.tourmanagement.tasks.hateoas+json", "application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<List<TaskDto>>> GetFilteredTasks([FromQuery]FilterOptionsModel filterOptionsModel, [FromHeader(Name = "Accept")] string mediaType)
        {

            //if order by fields are not valid.
            if (!_taskService.ValidMappingExists(filterOptionsModel.OrderBy))
            {
                //then return bad request.
                return BadRequest();
            }

            //if fields are not valid.
            if (!EventManagementUtils.TypeHasProperties<TaskDto>(filterOptionsModel.Fields))
            {
                //then return bad request.
                return BadRequest();
            }

            //get the paged/filtered show from db. 
            var tasksFromRepo = await _taskService.GetFilteredEntities(filterOptionsModel);

            //if HATEOAS links are required.
            if (mediaType == "application/vnd.tourmanagement.tasks.hateoas+json")
            {
                //create HATEOAS links for each show.
                tasksFromRepo.ForEach(task =>
                {
                    var entityLinks = CreateLinksForTask(task.Id, filterOptionsModel.Fields);
                    task.links = entityLinks;
                });

                //prepare pagination metadata.
                var paginationMetadata = new
                {
                    totalCount = tasksFromRepo.TotalCount,
                    pageSize = tasksFromRepo.PageSize,
                    currentPage = tasksFromRepo.CurrentPage,
                    totalPages = tasksFromRepo.TotalPages,
                };

                //add pagination meta data to response header.
                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMetadata));

                //create links for shows.
                var links = CreateLinksForTasks(filterOptionsModel, tasksFromRepo.HasNext, tasksFromRepo.HasPrevious);

                //prepare model with data and HATEOAS links.
                var linkedCollectionResource = new
                {
                    value = tasksFromRepo,
                    links = links
                };

                //return the data with Ok response.
                return Ok(linkedCollectionResource);
            }
            else
            {
                var previousPageLink = tasksFromRepo.HasPrevious ?
                    CreateTasksResourceUri(filterOptionsModel, ResourceUriType.PreviousPage) : null;

                var nextPageLink = tasksFromRepo.HasNext ?
                    CreateTasksResourceUri(filterOptionsModel, ResourceUriType.NextPage) : null;

                //prepare the pagination metadata.
                var paginationMetadata = new
                {
                    previousPageLink = previousPageLink,
                    nextPageLink = nextPageLink,
                    totalCount = tasksFromRepo.TotalCount,
                    pageSize = tasksFromRepo.PageSize,
                    currentPage = tasksFromRepo.CurrentPage,
                    totalPages = tasksFromRepo.TotalPages
                };

                //add pagination meta data to response header.
                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMetadata));

                //return the data with Ok response.
                return Ok(tasksFromRepo);
            }
        }


        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Produces("application/vnd.tourmanagement.tasks.hateoas+json", "application/json")]
        [HttpGet("{id}", Name = "GetTask")]
        public async Task<ActionResult<Domain.Entities.Task>> GetTask(Guid id, [FromQuery] string fields, [FromHeader(Name = "Accept")] string mediaType)
        {
            object taskEntity = null;
            object linkedResourceToReturn = null;

            //if fields are not passed.
            if (string.IsNullOrEmpty(fields))
            {
                _logger.LogInformation("GetTask called");

                //then get the whole entity and map it to the Dto.
                taskEntity = Mapper.Map<TaskDto>(await _taskService.GetEntityByIdAsync(id));
            }
            else
            {
                //if fields are passed then get the partial show.
                taskEntity = await _taskService.GetPartialEntityAsync(id, fields);
            }

            //if task not found.
            if (taskEntity == null)
            {
                //then return not found response.
                return NotFound();
            }

            if (mediaType == "application/vnd.tourmanagement.tasks.hateoas+json")
            {
                //create HATEOS links
                var links = CreateLinksForTask(id, fields);

                //if fields are not passed.
                if (string.IsNullOrEmpty(fields))
                {
                    //convert the typed object to expando object.
                    linkedResourceToReturn = ((TaskDto)taskEntity).ShapeData("") as IDictionary<string, object>;

                    //add the HATEOAS links to the model.
                    ((IDictionary<string, object>)linkedResourceToReturn).Add("links", links);
                }
                else
                {
                    linkedResourceToReturn = taskEntity;

                    //add the HATEOAS links to the model.
                    ((dynamic)linkedResourceToReturn).links = links;

                }
            }
            else
            {
                linkedResourceToReturn = taskEntity;
            }

            //return the Ok response.
            return Ok(linkedResourceToReturn);
        }

        #endregion


        #region HTTPPUT

        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPut("{id}", Name = "UpdateTask")]
        public async Task<IActionResult> UpdateTask(Guid id, [FromBody]TaskForUpdate TaskForUpdate)
        {
            //if show not found
            if (!await _taskService.ExistAsync(x => x.Id == id))
            {
                //then return not found response.
                return NotFound();
            }

            TaskForUpdate.ServiceId = !string.IsNullOrEmpty(TaskForUpdate.ServiceId) ? TaskForUpdate.ServiceId : null;

            TaskForUpdate.LeadId = !string.IsNullOrEmpty(TaskForUpdate.LeadId) ? TaskForUpdate.LeadId : null;

            //Update an entity.
            await _taskService.UpdateEntityAsync(id, TaskForUpdate);

            //return the response.
            return NoContent();

        }

        #endregion


        #region HTTPPATCH

        [Consumes("application/json-patch+json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPatch("{id}")]
        public async Task<IActionResult> PartiallyUpdateTask(Guid id, [FromBody] JsonPatchDocument<TaskForUpdate> jsonPatchDocument)
        {
            TaskForUpdate dto = new TaskForUpdate();
            Task task = new Task();

            //apply the patch changes to the dto. 
            jsonPatchDocument.ApplyTo(dto, ModelState);

            //if the jsonPatchDocument is not valid.
            if (!ModelState.IsValid)
            {
                //then return unprocessableEntity response.
                return new UnprocessableEntityObjectResult(ModelState);
            }

            //if the dto model is not valid after applying changes.
            if (!TryValidateModel(dto))
            {
                //then return unprocessableEntity response.
                return new UnprocessableEntityObjectResult(ModelState);
            }

            //map the chnages from dto to entity.
            Mapper.Map(dto, task);

            //set the Id for the show model.
            task.Id = id;

            //partially update the chnages to the db. 
            await _taskService.UpdatePartialEntityAsync(task, jsonPatchDocument);

            //return the response.
            return NoContent();
        }

        #endregion


        #region HTTPPOST

        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPost(Name = "CreateTask")]
        public async Task<ActionResult<TaskDto>> CreateTask([FromBody]TaskForCreation task)
        {
            try
            {
                task.Id = Guid.NewGuid().ToString();

                task.ServiceId = !string.IsNullOrEmpty(task.ServiceId) ? task.ServiceId : null;

                task.LeadId = !string.IsNullOrEmpty(task.LeadId) ? task.LeadId : null;

                //create a show in db.
                var taskToReturn = await _taskService.CreateEntityAsync<TaskDto, TaskForCreation>(task);

                //return the show created response.
                return CreatedAtRoute("GetTask", new { id = taskToReturn.Id }, taskToReturn);
            }
            catch(Exception ex)
            {
                return BadRequest(ex);
            }
        }

        #endregion


        #region HTTPDELETE
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}", Name = "DeleteTaskById")]
        public async Task<IActionResult> DeleteTaskById(Guid id)
        {
            //if the task exists
            if (await _taskService.ExistAsync(x => x.Id == id))
            {
                //delete the task from the db.
                await _taskService.DeleteEntityAsync(id);
            }
            else
            {
                //if task doesn't exists then returns not found.
                return NotFound();
            }

            //return the response.
            return NoContent();
        }

        #endregion


        #region PRIVATE METHODS

        private IEnumerable<LinkDto> CreateLinksForTask(Guid id, string fields)
        {
            var links = new List<LinkDto>();

            if (string.IsNullOrWhiteSpace(fields))
            {
                links.Add(
                  new LinkDto(_urlHelper.Link("GetTask", new { id = id }),
                  "self",
                  "GET"));
            }
            else
            {
                links.Add(
                  new LinkDto(_urlHelper.Link("GetTask", new { id = id, fields = fields }),
                  "self",
                  "GET"));
            }

            links.Add(
              new LinkDto(_urlHelper.Link("DeleteTaskById", new { id = id }),
              "delete_task",
              "DELETE"));

            links.Add(
             new LinkDto(_urlHelper.Link("UpdateTask", new { id = id }),
             "update_task",
             "PUT"));

            links.Add(
              new LinkDto(_urlHelper.Link("CreateTask", new { }),
              "create_task",
              "POST"));

            //links.Add(
            //   new LinkDto(_urlHelper.Link("GetShowsForTour", new { }),
            //   "shows",
            //   "GET"));

            return links;
        }

        private IEnumerable<LinkDto> CreateLinksForTasks(FilterOptionsModel filterOptionsModel, bool hasNext, bool hasPrevious)
        {
            var links = new List<LinkDto>();

            links.Add(
               new LinkDto(CreateTasksResourceUri(filterOptionsModel, ResourceUriType.Current), "self", "GET"));

            if (hasNext)
            {
                links.Add(
                  new LinkDto(CreateTasksResourceUri(filterOptionsModel, ResourceUriType.NextPage), "nextPage", "GET"));
            }

            if (hasPrevious)
            {
                links.Add(new LinkDto(CreateTasksResourceUri(filterOptionsModel, ResourceUriType.PreviousPage), "previousPage", "GET"));
            }

            return links;
        }

        private string CreateTasksResourceUri(FilterOptionsModel filterOptionsModel, ResourceUriType type)
        {
            switch (type)
            {
                case ResourceUriType.PreviousPage:
                    return _urlHelper.Link("GetFilteredTasks",
                      new
                      {
                          fields = filterOptionsModel.Fields,
                          orderBy = filterOptionsModel.OrderBy,
                          searchQuery = filterOptionsModel.SearchQuery,
                          pageNumber = filterOptionsModel.PageNumber - 1,
                          pageSize = filterOptionsModel.PageSize
                      });
                case ResourceUriType.NextPage:
                    return _urlHelper.Link("GetFilteredTasks",
                      new
                      {
                          fields = filterOptionsModel.Fields,
                          orderBy = filterOptionsModel.OrderBy,
                          searchQuery = filterOptionsModel.SearchQuery,
                          pageNumber = filterOptionsModel.PageNumber + 1,
                          pageSize = filterOptionsModel.PageSize
                      });

                default:
                    return _urlHelper.Link("GetFilteredTasks",
                    new
                    {
                        fields = filterOptionsModel.Fields,
                        orderBy = filterOptionsModel.OrderBy,
                        searchQuery = filterOptionsModel.SearchQuery,
                        pageNumber = filterOptionsModel.PageNumber,
                        pageSize = filterOptionsModel.PageSize
                    });
            }
        }

        #endregion

    }
}
