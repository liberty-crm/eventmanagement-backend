using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
//using EventManagement.API.Helpers;
using EventManagement.Dto;
using EventManagement.Service;
using EventManagement.Domain.Entities;
using EventManagement.Utility;
using Microsoft.Extensions.Logging;
using System.Linq;
using IdentityServer4.AccessTokenValidation;
using EventManagement.Utility.Filters;
using EventManagement.Utility.Enums;
using System.Text.RegularExpressions;
using System.Net.Http;
using Polly;
using Microsoft.AspNetCore.JsonPatch.Operations;
using EventManagement.Dto.FTCrm;
using FinanaceManagement.API.Models;
using EventManagement.Domain;
using Microsoft.Extensions.Configuration;

namespace EventManagement.API.Controllers
{
    /// <summary>
    /// FTCrm endpoint
    /// </summary>
    [Route("api/ftcrms")]
    [Produces("application/json")]
    [ApiController]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]

    public class FTCrmController : Controller
    {

        #region PRIVATE MEMBERS

        private readonly IFTCrmService _ftcrmService;
        private ILogger<FTCrmController> _logger;
        private readonly IUrlHelper _urlHelper;
        private readonly ILeadServiceOfferedService _leadServiceOfferedService;
        private readonly IContactService _contactService;
        private readonly INotificationMailService _notificationMailService;
        private readonly IAspUserService _aspUserService;
        private readonly IServiceOfferedService _serviceOfferedService;
        private readonly ITaskService _taskService;
        private readonly ILeadServiceOfferedRepository _leadServiceOfferedRepository;
        private readonly IHistoryService _historyService;
        private readonly IConfiguration _configuration;
        private readonly IStoreService _storeService;

        #endregion


        #region CONSTRUCTOR

        public FTCrmController(IFTCrmService ftcrmService, ILogger<FTCrmController> logger, IUrlHelper urlHelper, ILeadServiceOfferedService leadServiceOfferedService, IContactService contactService, INotificationMailService notificationMailService, IAspUserService aspUserService, IServiceOfferedService serviceOfferedService, ITaskService taskService, ILeadServiceOfferedRepository leadServiceOfferedRepository, IHistoryService historyService, IConfiguration configuration, IStoreService storeService) 
        {
            _logger = logger;
            _ftcrmService = ftcrmService;
            _urlHelper = urlHelper;
            _leadServiceOfferedService = leadServiceOfferedService;
            _contactService = contactService;
            _notificationMailService = notificationMailService;
            _aspUserService = aspUserService;
            _serviceOfferedService = serviceOfferedService;
            _taskService = taskService;
            _leadServiceOfferedRepository = leadServiceOfferedRepository;
            _historyService = historyService;
            _configuration = configuration;
            _storeService = storeService;
        }

        #endregion


        #region HTTPGET

        [HttpGet(Name = "GetFilteredFTCrms")]
        [Produces("application/vnd.tourmanagement.ftcrms.hateoas+json", "application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<List<FTCrmDto>>> GetFilteredFTCrms([FromQuery]FilterOptionsModel filterOptionsModel, [FromHeader(Name = "Accept")] string mediaType)
        {

            //if order by fields are not valid.
            if (!_ftcrmService.ValidMappingExists(filterOptionsModel.OrderBy))
            {
                //then return bad request.
                return BadRequest();
            }

            //if fields are not valid.
            if (!EventManagementUtils.TypeHasProperties<FTCrmDto>(filterOptionsModel.Fields))
            {
                //then return bad request.
                return BadRequest();
            }

            //get the paged/filtered show from db. 
            var ftcrmsFromRepo = await _ftcrmService.GetFilteredEntities(filterOptionsModel);

            //if HATEOAS links are required.
            if (mediaType == "application/vnd.tourmanagement.ftcrms.hateoas+json")
            {
                //create HATEOAS links for each show.
                ftcrmsFromRepo.ForEach(ftcrm =>
                {
                    var entityLinks = CreateLinksForFTCrm(ftcrm.Id, filterOptionsModel.Fields);
                    ftcrm.links = entityLinks;
                });

                //prepare pagination metadata.
                var paginationMetadata = new
                {
                    totalCount = ftcrmsFromRepo.TotalCount,
                    pageSize = ftcrmsFromRepo.PageSize,
                    currentPage = ftcrmsFromRepo.CurrentPage,
                    totalPages = ftcrmsFromRepo.TotalPages,
                };

                //add pagination meta data to response header.
                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMetadata));

                //create links for shows.
                var links = CreateLinksForFTCrms(filterOptionsModel, ftcrmsFromRepo.HasNext, ftcrmsFromRepo.HasPrevious);

                //prepare model with data and HATEOAS links.
                var linkedCollectionResource = new
                {
                    value = ftcrmsFromRepo,
                    links = links
                };

                //return the data with Ok response.
                return Ok(linkedCollectionResource);
            }
            else
            {
                var previousPageLink = ftcrmsFromRepo.HasPrevious ?
                    CreateFTCrmsResourceUri(filterOptionsModel, ResourceUriType.PreviousPage) : null;

                var nextPageLink = ftcrmsFromRepo.HasNext ?
                    CreateFTCrmsResourceUri(filterOptionsModel, ResourceUriType.NextPage) : null;

                //prepare the pagination metadata.
                var paginationMetadata = new
                {
                    previousPageLink = previousPageLink,
                    nextPageLink = nextPageLink,
                    totalCount = ftcrmsFromRepo.TotalCount,
                    pageSize = ftcrmsFromRepo.PageSize,
                    currentPage = ftcrmsFromRepo.CurrentPage,
                    totalPages = ftcrmsFromRepo.TotalPages
                };

                //add pagination meta data to response header.
                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMetadata));

                //return the data with Ok response.
                return Ok(ftcrmsFromRepo);
            }
        }


        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Produces("application/vnd.tourmanagement.ftcrms.hateoas+json", "application/json")]
        [HttpGet("{id}", Name = "GetFTCrm")]
        public async Task<ActionResult<FTCrm>> GetFTCrm(Guid id, [FromQuery] string fields, [FromHeader(Name = "Accept")] string mediaType)
        {
            object ftcrmEntity = null;
            object linkedResourceToReturn = null;

            //if fields are not passed.
            if (string.IsNullOrEmpty(fields))
            {
                _logger.LogInformation("GetFTCrm called");

                //then get the whole entity and map it to the Dto.
                ftcrmEntity = Mapper.Map<FTCrmDto>(await _ftcrmService.GetEntityByIdAsync(id));
            }
            else
            {
                //if fields are passed then get the partial show.
                ftcrmEntity = await _ftcrmService.GetPartialEntityAsync(id, fields);
            }

            //if ftcrm not found.
            if (ftcrmEntity == null)
            {
                //then return not found response.
                return NotFound();
            }

            if (mediaType == "application/vnd.tourmanagement.ftcrms.hateoas+json")
            {
                //create HATEOS links
                var links = CreateLinksForFTCrm(id, fields);

                //if fields are not passed.
                if (string.IsNullOrEmpty(fields))
                {
                    //convert the typed object to expando object.
                    linkedResourceToReturn = ((FTCrmDto)ftcrmEntity).ShapeData("") as IDictionary<string, object>;

                    //add the HATEOAS links to the model.
                    ((IDictionary<string, object>)linkedResourceToReturn).Add("links", links);
                }
                else
                {
                    linkedResourceToReturn = ftcrmEntity;

                    //add the HATEOAS links to the model.
                    ((dynamic)linkedResourceToReturn).links = links;

                }
            }
            else
            {
                linkedResourceToReturn = ftcrmEntity;
            }

            //return the Ok response.
            return Ok(linkedResourceToReturn);
        }

        #endregion


        #region HTTPPUT

        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPut("{id}", Name = "UpdateFTCrm")]
        public async Task<IActionResult> UpdateFTCrm(Guid id, [FromBody]FTCrmForUpdate FTCrmForUpdate)
        {

            //if show not found
            if (!await _ftcrmService.ExistAsync(x => x.Id == id))
            {
                //then return not found response.
                return NotFound();
            }

            //Update an entity.
            await _ftcrmService.UpdateEntityAsync(id, FTCrmForUpdate);

            //return the response.
            return NoContent();
        }

        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPut("UpdateStatus", Name = "UpdateStatus")]
        [AllowAnonymous]
        [ApiKeyAuth]
        public async Task<IActionResult> UpdateStatus([FromBody]CrmData crmData)
        {
            //if show not found
            if (!await _ftcrmService.ExistAsync(x => x.FtCrmUniqueId.Equals(crmData.opp_id)))
            {
                //then return not found response.
                return NotFound();
            }
            var filterOption = new FilterOptionsModel();
            filterOption.PageSize = 10000;
            filterOption.SearchQuery = "FtCrmUniqueId==\"" + crmData.opp_id + "\"";
            var ftcrms = _ftcrmService.GetFilteredEntities(filterOption).Result;
            var entity = ftcrms.FirstOrDefault();
            if (entity != null)
            {
                var entityForUpdate = new FTCrmForUpdate();
                entityForUpdate.Id = entity.Id;
                entityForUpdate.LeadServiceId = entity.LeadServiceId;
                entityForUpdate.Status = crmData.stage_name;
                entityForUpdate.FtCrmUniqueId = entity.FtCrmUniqueId;
                entityForUpdate.Amount = crmData.refund_amount != null ? crmData.refund_amount : entity.Amount;
                entityForUpdate.Reason = entity.Reason;
                var filterOptionForServiceOffered = new FilterOptionsModel();
                filterOptionForServiceOffered.SearchQuery = "Id==\"" + entity.LeadServiceId + "\"";
                var servicesOffered = _leadServiceOfferedService.GetFilteredEntities(filterOptionForServiceOffered).Result;
                var service = servicesOffered.FirstOrDefault();
                var assignedEmployee = new AspNetUsers();
                if (service != null && service.AssignedTo != null)
                {
                    assignedEmployee = _aspUserService.GetEntityById(service.AssignedTo);
                }


                //Update an entity.
                await _ftcrmService.UpdateEntityAsync(entity.Id, entityForUpdate);

                // No Lead Id
                var htmlContent = _notificationMailService.GetAllEntities().Where(x => x.Name.Equals(NotificationMailEnum.FtCrmUpdateStatus.ToString())).FirstOrDefault().MailContent;
                htmlContent = Regex.Replace(htmlContent, "{%contact.FirstName%}", service.Contact.FirstName ?? "", RegexOptions.IgnoreCase);
                htmlContent = Regex.Replace(htmlContent, "{%contact.LastName%}", service.Contact.LastName ?? "", RegexOptions.IgnoreCase);
                htmlContent = Regex.Replace(htmlContent, "{%FtCrm.Id%}", entityForUpdate.FtCrmUniqueId ?? "", RegexOptions.IgnoreCase);
                htmlContent = Regex.Replace(htmlContent, "{%lead.Id%}", entity.LeadServiceOffered.Lead.LeadNumber.ToString() ?? "", RegexOptions.IgnoreCase);
                htmlContent = Regex.Replace(htmlContent, "{%FtCrm.OldStatus%}", entity.Status ?? "", RegexOptions.IgnoreCase);
                htmlContent = Regex.Replace(htmlContent, "{%FtCrm.NewStatus%}", entityForUpdate.Status ?? "", RegexOptions.IgnoreCase);
                htmlContent = Regex.Replace(htmlContent, "{%service.assignedTo%}", assignedEmployee.FName ?? "", RegexOptions.IgnoreCase);
                htmlContent = Regex.Replace(htmlContent, "{%service.serviceName%}", service.ServiceOffered.Name ?? "", RegexOptions.IgnoreCase);
                var subject = "Category updated to " + entityForUpdate.Status + " in Family Tax CRM for " + service.Contact.FirstName + " - Lead Id: "+ entity.LeadServiceOffered.Lead.LeadNumber.ToString();
                _notificationMailService.SendNotificationMail(assignedEmployee.Email, htmlContent, subject, entity.LeadServiceOffered.LeadId.ToString(), entity.LeadServiceId.ToString(), entityForUpdate.Status);
                               
                if (crmData.refund_amount != null && crmData.stage_name.Equals("Refund Approved"))
                {
                    await SendInvoiceToClient(service.Contact.Email, entityForUpdate.LeadServiceId);
                    SendProcessCompletionMail(service);
                }
            }
            //return the response.
            return NoContent();
        }

        #endregion


        #region HTTPPATCH

        [Consumes("application/json-patch+json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPatch("{id}")]
        public async Task<IActionResult> PartiallyUpdateFTCrm(Guid id, [FromBody] JsonPatchDocument<FTCrmForUpdate> jsonPatchDocument)
        {
            FTCrmForUpdate dto = new FTCrmForUpdate();
            FTCrm ftcrm = new FTCrm();

            //apply the patch changes to the dto. 
            jsonPatchDocument.ApplyTo(dto, ModelState);

            //if the jsonPatchDocument is not valid.
            if (!ModelState.IsValid)
            {
                //then return unprocessableEntity response.
                return new UnprocessableEntityObjectResult(ModelState);
            }

            //if the dto model is not valid after applying changes.
            if (!TryValidateModel(dto))
            {
                //then return unprocessableEntity response.
                return new UnprocessableEntityObjectResult(ModelState);
            }

            //map the chnages from dto to entity.
            Mapper.Map(dto, ftcrm);

            //set the Id for the show model.
            ftcrm.Id = id;

            //partially update the chnages to the db. 
            await _ftcrmService.UpdatePartialEntityAsync(ftcrm, jsonPatchDocument);

            //return the response.
            return NoContent();
        }

        #endregion


        #region HTTPPOST

        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPost(Name = "CreateFTCrm")]
        public async Task<ActionResult<FTCrmDto>> CreateFTCrm([FromBody]FTCrmForCreation ftcrm)
        {
            try
            {
                var existingFtCrm = _ftcrmService.GetAllEntities().Where(x => x.LeadServiceId == ftcrm.LeadServiceId).FirstOrDefault();
                FTCrmDto ftcrmToReturn;
                if (existingFtCrm == null)
                {
                    ftcrmToReturn = await _ftcrmService.CreateEntityAsync<FTCrmDto, FTCrmForCreation>(ftcrm);
                }
                else
                {
                    var ftcrmlocal = await _ftcrmService.GetEntityByIdAsync(existingFtCrm.Id);
                    ftcrmToReturn = new FTCrmDto()
                    {
                        Id = ftcrmlocal.Id,
                        Amount = ftcrmlocal.Amount,
                        FtCrmUniqueId = ftcrmlocal.FtCrmUniqueId,
                        LeadServiceId = ftcrmlocal.LeadServiceId,
                        Reason = ftcrmlocal.Reason,
                        Status = ftcrmlocal.Status,

                    };
                }

                //Code to trigger Family Tax CRM
                var filterOption = new FilterOptionsModel();
                filterOption.PageSize = 10000;
                filterOption.SearchQuery = "Id==\"" + ftcrm.LeadServiceId + "\"";

                var leadServiceOffered = _leadServiceOfferedService.GetFilteredEntities(filterOption).Result;
                var leadService = leadServiceOffered.Where(x => x.Id.Equals(ftcrm.LeadServiceId)).FirstOrDefault();

                var relationShipManager = _aspUserService.GetEntityById(leadService.Lead.AssignedTo);

                var salesRep = _aspUserService.GetEntityById(leadService.Lead.AgentId);


                var contact = _contactService.GetAllEntities().Where(x => x.RelatedLeadContactId.Equals(leadService.Lead.Id) && x.RelationWithLead.Equals("Self")).FirstOrDefault();

                leadService.AssignedTo = leadService.ServiceOffered.ServiceManagerId;

                var assignedToUser = _aspUserService.GetEntityById(leadService.AssignedTo);

                //FTCRM Api call with retry for 3 times
                var httpClient = new HttpClient();
                httpClient.DefaultRequestHeaders.Add("Auth-Key", "8D65A4647FED05E4C13DF30B5E8235DC");
                var formContent = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("first_name", contact.FirstName),
                    new KeyValuePair<string, string>("last_name", contact.LastName),
                    new KeyValuePair<string, string>("phone", contact.Phone),
                    new KeyValuePair<string, string>("email", contact.Email),
                    new KeyValuePair<string, string>("sin", contact.Sin),

                });
                var pauseBetweenFailures = TimeSpan.FromSeconds(2);

                var retryPolicy = Policy
                    .Handle<HttpRequestException>()
                    .WaitAndRetryAsync(3, retryAttempt =>
                        TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)));

                var ftcrmResponse = new FtCrmResponse();
                await retryPolicy.ExecuteAsync(async () =>
                {
                    var uri = "https://fintaxcrm.com/fapi/onboard";
                    var response = await httpClient
                      .PostAsync(uri, formContent);
                    var data = await response.Content.ReadAsStringAsync();
                    ftcrmResponse = JsonConvert.DeserializeObject<FtCrmResponse>(data);
                    response.EnsureSuccessStatusCode();
                });

                var ftcrmToUpdate = new FTCrmForUpdate();
                if (ftcrmResponse.status == 201 && ftcrmResponse.success.Equals("A New Opp has been created") && Int32.Parse(ftcrmResponse.data.opp_id) > 0)
                {
                    ftcrmToUpdate.Id = ftcrmToReturn.Id;
                    ftcrmToUpdate.FtCrmUniqueId = ftcrmResponse.data.opp_id;
                    ftcrmToUpdate.LeadServiceId = ftcrmToReturn.LeadServiceId;
                    ftcrmToUpdate.Status = "Successfully Intiated";
                    ftcrmToUpdate.Amount = 0;
                    ftcrmToUpdate.Reason = ftcrmResponse.success;

                    //Update LeadServiceOffered
                    var patchLeadServiceOffered = new JsonPatchDocument<LeadServiceOffered>();
                    patchLeadServiceOffered.Operations.Add(new Operation<LeadServiceOffered> { op = "replace", path = "/AssignedTo", value = leadService.ServiceOffered.ServiceManagerId });
                    patchLeadServiceOffered.Operations.Add(new Operation<LeadServiceOffered> { op = "replace", path = "/AssignedStatus", value = "Working" });
                    LeadServiceOffered _leadServiceOffered = new LeadServiceOffered();
                    patchLeadServiceOffered.ApplyTo(_leadServiceOffered);
                    _leadServiceOffered.Id = leadService.Id;
                    _leadServiceOfferedService.UpdatePartialEntityAsync(_leadServiceOffered, patchLeadServiceOffered);

                    //Add task for service
                    TaskForCreation serviceTask = CreateSericeTask(leadService);
                    var taskToReturn = await _taskService.CreateEntityAsync<TaskDto, TaskForCreation>(serviceTask);

                    // Save Ftcrm
                    await _ftcrmService.UpdateEntityAsync(ftcrmToReturn.Id, ftcrmToUpdate);

                    // Create Success History
                    var historyForCreation = new HistoryForCreation();
                    historyForCreation.Activity = 15;
                    historyForCreation.LeadId = leadService.Lead.Id.ToString();
                    historyForCreation.ServiceId = leadService.Id.ToString();
                    historyForCreation.TagData = JsonConvert.SerializeObject(new[] { new { key = ftcrmToUpdate.Reason, isNotification = true } }.ToList());
                    await _historyService.CreateEntityAsync<HistoryDto, HistoryForCreation>(historyForCreation);                   

                    //Send Notification Mails
                    SendEmailsOnSuccess(leadService, relationShipManager, assignedToUser, salesRep);
                }
                else
                {
                    ftcrmToUpdate.Id = ftcrmToReturn.Id;
                    ftcrmToUpdate.FtCrmUniqueId = null;
                    ftcrmToUpdate.LeadServiceId = ftcrmToReturn.LeadServiceId;
                    ftcrmToUpdate.Status = "Intialization Failed";
                    ftcrmToUpdate.Amount = 0;
                    ftcrmToUpdate.Reason = ftcrmResponse.error;
                    
                    // Save Ftcrm
                    await _ftcrmService.UpdateEntityAsync(ftcrmToReturn.Id, ftcrmToUpdate);
                    
                    //Create Faliure History
                    var historyForCreation = new HistoryForCreation();
                    historyForCreation.Activity = 16;
                    historyForCreation.LeadId = leadService.Lead.Id.ToString();
                    historyForCreation.ServiceId = leadService.Id.ToString();
                    historyForCreation.TagData = JsonConvert.SerializeObject(new[] { new { key = ftcrmToUpdate.Reason, isNotification = true } }.ToList());
                    await _historyService.CreateEntityAsync<HistoryDto, HistoryForCreation>(historyForCreation);
                    
                    // Send faliure mail
                    SendEmailsOnFaliure(leadService, relationShipManager, assignedToUser,ftcrmResponse.error);
                }               
                //return the show created response.
                return CreatedAtRoute("GetFTCrm", new { id = ftcrmToReturn.Id }, ftcrmToReturn);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }            
        }

        private static TaskForCreation CreateSericeTask(dynamic leadService)
        {
            return new TaskForCreation()
            {
                LeadId = leadService.Lead.Id,
                Title = "Service " + leadService.ServiceOffered.Name + " of Lead " + leadService.Contact.FirstName + " " + leadService.Contact.LastName + " has been assigned to you.",
                Description = "Service has been assigned to you.",
                Date = DateTime.Now,
                UserId = leadService.ServiceOffered.ServiceManagerId,
                IsCompleted = false,
                IsImportant = true,
                IsMarked = false,
                ServiceId = leadService.Id.ToString()
            };
        }

        private void SendEmailsOnFaliure(dynamic leadService, dynamic relationShipManager, dynamic assignedToUser, string reason)
        {
            var htmlContentServiceAssigned = _notificationMailService.GetAllEntities().Where(x => x.Name.Equals(NotificationMailEnum.IntegrationFaliureToServiceTeam.ToString())).FirstOrDefault().MailContent;
            htmlContentServiceAssigned = Regex.Replace(htmlContentServiceAssigned, "{%service.AssignedTo%}", relationShipManager.FName ?? "", RegexOptions.IgnoreCase);
            htmlContentServiceAssigned = Regex.Replace(htmlContentServiceAssigned, "{%contact.FirstName%}", leadService.Contact.FirstName ?? "", RegexOptions.IgnoreCase);
            htmlContentServiceAssigned = Regex.Replace(htmlContentServiceAssigned, "{%contact.LastName%}", leadService.Contact.LastName ?? "", RegexOptions.IgnoreCase);
            htmlContentServiceAssigned = Regex.Replace(htmlContentServiceAssigned, "{%contact.Email%}", leadService.Contact.Email ?? "", RegexOptions.IgnoreCase);
            htmlContentServiceAssigned = Regex.Replace(htmlContentServiceAssigned, "{%ftcrm.Reason%}", reason ?? "", RegexOptions.IgnoreCase);
            var subjectServiceAssigned = "10-year tax review update from Family Tax CRM failed for" + " - Lead Id: " + leadService.Lead.LeadNumber;
            _notificationMailService.SendNotificationMail(relationShipManager.Email, htmlContentServiceAssigned, subjectServiceAssigned, leadService.Lead.Id.ToString(), leadService.Id.ToString(), "10-year tax Integration Failed. Mail send to Relationship Manager");
        }

        private void SendProcessCompletionMail(dynamic service)
        {
            var htmlContentForRelationshipManager = _notificationMailService.GetAllEntities().Where(x => x.Name.Equals(NotificationMailEnum.ProcessCompleted.ToString())).FirstOrDefault().MailContent;
            var relationshipManager = _aspUserService.GetEntityById(service.Lead.AssignedTo);
            if (service.Lead.AssignedStore != null && service.Lead.AssignedStore != "")
            {
                var storeName = _storeService.GetEntityById(Guid.Parse(service.Lead.AssignedStore)).Name;

                htmlContentForRelationshipManager = Regex.Replace(htmlContentForRelationshipManager, "{%lead.StoreId%}", storeName ?? "", RegexOptions.IgnoreCase);
            }
            else
            {
                htmlContentForRelationshipManager = Regex.Replace(htmlContentForRelationshipManager, "{%lead.StoreId%}", "", RegexOptions.IgnoreCase);
            }

            htmlContentForRelationshipManager = Regex.Replace(htmlContentForRelationshipManager, "{%contact.FirstName%}", service.Contact.FirstName ?? "", RegexOptions.IgnoreCase);
            htmlContentForRelationshipManager = Regex.Replace(htmlContentForRelationshipManager, "{%contact.LastName%}", service.Contact.LastName ?? "", RegexOptions.IgnoreCase);
            htmlContentForRelationshipManager = Regex.Replace(htmlContentForRelationshipManager, "{%service.assignedTo%}", relationshipManager.FName ?? "", RegexOptions.IgnoreCase);
            htmlContentForRelationshipManager = Regex.Replace(htmlContentForRelationshipManager, "{%service.Name%}", service.ServiceOffered.Name ?? "", RegexOptions.IgnoreCase);
            htmlContentForRelationshipManager = Regex.Replace(htmlContentForRelationshipManager, "{%lead.Id%}", " Lead Id: " + service.Lead.LeadNumber ?? "", RegexOptions.IgnoreCase);
            htmlContentForRelationshipManager = Regex.Replace(htmlContentForRelationshipManager, "{%lead.Link%}", _configuration.GetSection("Frontend").Value + "lead/edit/" + service.Lead.Id.ToString() ?? "", RegexOptions.IgnoreCase);

            var subject = "Processing completed for " + service.ServiceOffered.Name + " request of " + service.Contact.FirstName + " " + service.Contact.LastName + " - Lead Id: " + service.Lead.LeadNumber;
            _notificationMailService.SendNotificationMail(relationshipManager.Email, htmlContentForRelationshipManager, subject, service.Lead.Id.ToString(), service.Id.ToString(), "Process Completed for Family Tax");
        }

        private async Task<bool> SendEmailsOnSuccess(dynamic leadService, dynamic relationShipManager, dynamic assignedToUser, dynamic salesRep)
        {

            var res = false;
            try
            {
                {
                    // Mail to Customer
                    var htmlContent = _notificationMailService.GetAllEntities().Where(x => x.Name.Equals(NotificationMailEnum.IntegrationTriggeredToClient.ToString())).FirstOrDefault().MailContent;
                    htmlContent = Regex.Replace(htmlContent, "{%contact.FirstName%}", leadService.Contact.FirstName ?? "", RegexOptions.IgnoreCase);
                    htmlContent = Regex.Replace(htmlContent, "{%lead.assignedTo%}", relationShipManager.FName ?? "", RegexOptions.IgnoreCase);
                    var subject = "Processing of your " + leadService.ServiceOffered.Name + " request with Liberty Financial Group";
                    var res1 = _notificationMailService.SendNotificationMail(leadService.Contact.Email, htmlContent, subject, leadService.Lead.Id.ToString(), leadService.Id.ToString(), "10-year tax Integration Succeeded. Mail sent to Customer");

                    // Mail to Product Manager of Assignment
                    var htmlContentServiceAssigned = (from mail in _notificationMailService.GetAllEntities()
                                                      where mail.Name.Equals(NotificationMailEnum.ServiceAssigned.ToString())
                                                      select mail.MailContent).FirstOrDefault();
                    htmlContentServiceAssigned = Regex.Replace(htmlContentServiceAssigned, "{%service.assignedTo%}", assignedToUser.FName ?? "", RegexOptions.IgnoreCase);
                    htmlContentServiceAssigned = Regex.Replace(htmlContentServiceAssigned, "{%service.Name%}", leadService.ServiceOffered.Name ?? "", RegexOptions.IgnoreCase);
                    htmlContentServiceAssigned = Regex.Replace(htmlContentServiceAssigned, "{%contact.FirstName%}", leadService.Contact.FirstName ?? "", RegexOptions.IgnoreCase);
                    htmlContentServiceAssigned = Regex.Replace(htmlContentServiceAssigned, "{%contact.LastName%}", leadService.Contact.LastName ?? "", RegexOptions.IgnoreCase);
                    htmlContentServiceAssigned = Regex.Replace(htmlContentServiceAssigned, "{%lead.Id%}", "Lead Id: " + Convert.ToString(leadService.Lead.LeadNumber) ?? "", RegexOptions.IgnoreCase);
                    htmlContentServiceAssigned = Regex.Replace(htmlContentServiceAssigned, "{%lead.Link%}", _configuration.GetSection("Frontend").Value + "lead/edit/" + leadService.Lead.Id.ToString() ?? "", RegexOptions.IgnoreCase);
                    var subjectServiceAssigned = "A new " + leadService.ServiceOffered.Name + " request for " + leadService.Contact.FirstName + " " + leadService.Contact.LastName + " - Lead Id: " + leadService.Lead.LeadNumber + " has been assigned to you";
                    var res2 = _notificationMailService.SendNotificationMail(assignedToUser.Email, htmlContentServiceAssigned, subjectServiceAssigned, leadService.Lead.Id.ToString(), leadService.Id.ToString(), "Service Assigned mail to Product Manager");


                    // Mail to Relationship Manager
                    var htmlContentIntegrationTriggeredToLeadEmployee = (from mail in _notificationMailService.GetAllEntities()
                                                                         where mail.Name.Equals(NotificationMailEnum.IntegrationTriggeredToEmployee.ToString())
                                                                         select mail.MailContent).FirstOrDefault();
                    var htmlContentIntegrationTriggeredToSalesRep = htmlContentIntegrationTriggeredToLeadEmployee;

                    htmlContentIntegrationTriggeredToLeadEmployee = Regex.Replace(htmlContentIntegrationTriggeredToLeadEmployee, "{%service.assignedTo%}", relationShipManager.FName ?? "", RegexOptions.IgnoreCase);
                    htmlContentIntegrationTriggeredToLeadEmployee = Regex.Replace(htmlContentIntegrationTriggeredToLeadEmployee, "{%service.Name%}", leadService.ServiceOffered.Name ?? "", RegexOptions.IgnoreCase);
                    htmlContentIntegrationTriggeredToLeadEmployee = Regex.Replace(htmlContentIntegrationTriggeredToLeadEmployee, "{%contact.FirstName%}", leadService.Contact.FirstName ?? "", RegexOptions.IgnoreCase);
                    htmlContentIntegrationTriggeredToLeadEmployee = Regex.Replace(htmlContentIntegrationTriggeredToLeadEmployee, "{%contact.LastName%}", leadService.Contact.LastName ?? "", RegexOptions.IgnoreCase);
                    htmlContentIntegrationTriggeredToLeadEmployee = Regex.Replace(htmlContentIntegrationTriggeredToLeadEmployee, "{%lead.Id%}", "Lead Id: " + Convert.ToString(leadService.Lead.LeadNumber) ?? "", RegexOptions.IgnoreCase);
                    var subjecthtmlContentIntegrationTriggeredToLeadEmployee = leadService.Contact.FirstName + " " + leadService.Contact.LastName + " - Lead Id: " + leadService.Lead.LeadNumber + " has submitted the additional details for their " + leadService.ServiceOffered.Name + " request";
                    var res3 = _notificationMailService.SendNotificationMail(relationShipManager.Email, htmlContentIntegrationTriggeredToLeadEmployee, subjecthtmlContentIntegrationTriggeredToLeadEmployee, leadService.Lead.Id.ToString(), leadService.Id.ToString(), "10-year tax Integration Succeeded. Mail to Relationship Manager");

                    //var htmlContentIntegrationTriggeredToSalesRep = _notificationMailService.GetAllEntities().Where(x => x.Name.Equals(NotificationMailEnum.IntegrationTriggeredToEmployee.ToString())).FirstOrDefault().MailContent;

                    if (salesRep != null)
                    {
                        // Mail to Ambassador
                        htmlContentIntegrationTriggeredToSalesRep = Regex.Replace(htmlContentIntegrationTriggeredToSalesRep, "{%service.assignedTo%}", salesRep.FName ?? "", RegexOptions.IgnoreCase);
                        htmlContentIntegrationTriggeredToSalesRep = Regex.Replace(htmlContentIntegrationTriggeredToSalesRep, "{%service.Name%}", leadService.ServiceOffered.Name ?? "", RegexOptions.IgnoreCase);
                        htmlContentIntegrationTriggeredToSalesRep = Regex.Replace(htmlContentIntegrationTriggeredToSalesRep, "{%contact.FirstName%}", leadService.Contact.FirstName ?? "", RegexOptions.IgnoreCase);
                        htmlContentIntegrationTriggeredToSalesRep = Regex.Replace(htmlContentIntegrationTriggeredToSalesRep, "{%contact.LastName%}", leadService.Contact.LastName ?? "", RegexOptions.IgnoreCase);
                        htmlContentIntegrationTriggeredToSalesRep = Regex.Replace(htmlContentIntegrationTriggeredToSalesRep, "{%lead.Id%}", "Lead Id: " + Convert.ToString(leadService.Lead.LeadNumber) ?? "", RegexOptions.IgnoreCase);
                        var subjecthtmlContentIntegrationTriggeredToSalesRep = leadService.Contact.FirstName + " " + leadService.Contact.LastName + " - Lead Id: " + leadService.Lead.LeadNumber + " has submitted the additional details for their " + leadService.ServiceOffered.Name + " request";
                        var res4 = _notificationMailService.SendNotificationMail(salesRep.Email, htmlContentIntegrationTriggeredToSalesRep, subjecthtmlContentIntegrationTriggeredToSalesRep, leadService.Lead.Id.ToString(), leadService.Id.ToString(), "10-year tax Integration Succeeded. Mail to Ambassador");
                    }
                    res = true;
                }
            }
            catch (Exception ex)
            {
                var me = ex.Message;
            }
            return res;
        }

        #endregion


        #region HTTPDELETE
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}", Name = "DeleteFTCrmById")]
        public async Task<IActionResult> DeleteFTCrmById(Guid id)
        {
            //if the ftcrm exists
            if (await _ftcrmService.ExistAsync(x => x.Id == id))
            {
                //delete the ftcrm from the db.
                await _ftcrmService.DeleteEntityAsync(id);
            }
            else
            {
                //if ftcrm doesn't exists then returns not found.
                return NotFound();
            }

            //return the response.
            return NoContent();
        }

        #endregion


        #region PRIVATE METHODS

        private async Task<bool> SendInvoiceToClient(string email, Guid LeadServiceId)
        {
            var retVal = false;
            var invoice = _leadServiceOfferedService.GetInvoice(LeadServiceId.ToString()).Result;
            try
            {

                var invoiceHtml = _notificationMailService.GetAllEntities().Where(x => x.Name.Equals(NotificationMailEnum.InvoiceToClient.ToString())).FirstOrDefault().MailContent;
                invoiceHtml = Regex.Replace(invoiceHtml, "{{invoice.from.title}}", invoice.From.Title ?? "", RegexOptions.IgnoreCase);
                invoiceHtml = Regex.Replace(invoiceHtml, "{{invoice.from.address}}", invoice.From.Address ?? "", RegexOptions.IgnoreCase);
                invoiceHtml = Regex.Replace(invoiceHtml, "{{invoice.from.phone}}", invoice.From.PhoneNumber ?? "", RegexOptions.IgnoreCase);
                invoiceHtml = Regex.Replace(invoiceHtml, "{{invoice.from.email}}", invoice.From.Email ?? "", RegexOptions.IgnoreCase);
                invoiceHtml = Regex.Replace(invoiceHtml, "{{invoice.from.website}}", invoice.From.Website ?? "", RegexOptions.IgnoreCase);
                
                invoiceHtml = Regex.Replace(invoiceHtml, "{{invoice.client.title}}", invoice.Client.Title ?? "", RegexOptions.IgnoreCase);
                invoiceHtml = Regex.Replace(invoiceHtml, "{{invoice.client.address}}", invoice.From.Address ?? "", RegexOptions.IgnoreCase);
                invoiceHtml = Regex.Replace(invoiceHtml, "{{invoice.client.phone}}", invoice.From.PhoneNumber ?? "", RegexOptions.IgnoreCase);
                invoiceHtml = Regex.Replace(invoiceHtml, "{{invoice.client.email}}", invoice.From.Email ?? "", RegexOptions.IgnoreCase);
                
                // invoiceHtml = Regex.Replace(invoiceHtml, "{{invoice.invoiceNumber}}", invoice.InvoiceNumber ?? "", RegexOptions.IgnoreCase);
                invoiceHtml = Regex.Replace(invoiceHtml, "{{invoice.issueDate}}", invoice.IssueDate.ToShortDateString() ?? "", RegexOptions.IgnoreCase);
                invoiceHtml = Regex.Replace(invoiceHtml, "{{invoice.dueDate}}", invoice.DueDate.ToShortDateString() ?? "", RegexOptions.IgnoreCase);
                invoiceHtml = Regex.Replace(invoiceHtml, "{{invoice.totalAmount}}", invoice.TotalAmount.ToString() ?? "", RegexOptions.IgnoreCase);

                invoiceHtml = Regex.Replace(invoiceHtml, "{{invoice.invoiceService.title}}", invoice.InvoiceService.Title ?? "", RegexOptions.IgnoreCase);
                invoiceHtml = Regex.Replace(invoiceHtml, "{{invoice.invoiceService.description}}", invoice.InvoiceService.Description ?? "", RegexOptions.IgnoreCase);
                invoiceHtml = Regex.Replace(invoiceHtml, "{{invoice.invoiceService.rate}}", invoice.InvoiceService.Rate.ToString() ?? "", RegexOptions.IgnoreCase);
                invoiceHtml = Regex.Replace(invoiceHtml, "{{invoice.client.provinceDetails.abbreviation}}", invoice.Client.ProvinceDetails.Abbreviation ?? "", RegexOptions.IgnoreCase);
                invoiceHtml = Regex.Replace(invoiceHtml, "{{invoice.invoiceService.total}}", invoice.InvoiceService.Total.ToString());

                invoiceHtml = Regex.Replace(invoiceHtml, "{{ invoice.client.provinceDetails.hstPercent }}", invoice.Client.ProvinceDetails.HSTPercent.ToString());
                invoiceHtml = Regex.Replace(invoiceHtml, "{{ invoice.hstAmount}}", invoice.HSTAmount.ToString());

                var htmlEmailContent = _notificationMailService.GetAllEntities().Where(x => x.Name.Equals(NotificationMailEnum.InvoiceEmailToClient.ToString())).FirstOrDefault().MailContent;
                htmlEmailContent = Regex.Replace(htmlEmailContent, "{%contact.FirstName%}", invoice.Client.Title ?? "", RegexOptions.IgnoreCase);
                htmlEmailContent = Regex.Replace(htmlEmailContent, "{%service.Name%}", invoice.InvoiceService.Title ?? "", RegexOptions.IgnoreCase);
                htmlEmailContent = Regex.Replace(htmlEmailContent, "{%lead.AssignedStore%}", "", RegexOptions.IgnoreCase);
                var subject = "Invoice for your " + invoice.InvoiceService.Title + " request with Liberty Edge";
                

                retVal = await _ftcrmService.SendInvoiceToClientAsync(invoiceHtml,email, LeadServiceId, invoice.TotalAmount,htmlEmailContent,subject);
                if (retVal)
                {
                    await UpdateLeadServiceOfferedStatus(LeadServiceId);
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                _notificationMailService.SendNotificationMail("ritwikmukhopadhyay@gmail.com", ex.Message, "Error for library");
            }

            return retVal;
        }

        private IEnumerable<LinkDto> CreateLinksForFTCrm(Guid id, string fields)
        {
            var links = new List<LinkDto>();

            if (string.IsNullOrWhiteSpace(fields))
            {
                links.Add(
                  new LinkDto(_urlHelper.Link("GetFTCrm", new { id = id }),
                  "self",
                  "GET"));
            }
            else
            {
                links.Add(
                  new LinkDto(_urlHelper.Link("GetFTCrm", new { id = id, fields = fields }),
                  "self",
                  "GET"));
            }

            links.Add(
              new LinkDto(_urlHelper.Link("DeleteFTCrmById", new { id = id }),
              "delete_ftcrm",
              "DELETE"));

            links.Add(
             new LinkDto(_urlHelper.Link("UpdateFTCrm", new { id = id }),
             "update_ftcrm",
             "PUT"));

            links.Add(
              new LinkDto(_urlHelper.Link("CreateFTCrm", new { }),
              "create_ftcrm",
              "POST"));

            //links.Add(
            //   new LinkDto(_urlHelper.Link("GetShowsForTour", new { }),
            //   "shows",
            //   "GET"));

            return links;
        }

        private IEnumerable<LinkDto> CreateLinksForFTCrms(FilterOptionsModel filterOptionsModel, bool hasNext, bool hasPrevious)
        {
            var links = new List<LinkDto>();

            links.Add(
               new LinkDto(CreateFTCrmsResourceUri(filterOptionsModel, ResourceUriType.Current), "self", "GET"));

            if (hasNext)
            {
                links.Add(
                  new LinkDto(CreateFTCrmsResourceUri(filterOptionsModel, ResourceUriType.NextPage), "nextPage", "GET"));
            }

            if (hasPrevious)
            {
                links.Add(new LinkDto(CreateFTCrmsResourceUri(filterOptionsModel, ResourceUriType.PreviousPage), "previousPage", "GET"));
            }

            return links;
        }

        private string CreateFTCrmsResourceUri(FilterOptionsModel filterOptionsModel, ResourceUriType type)
        {
            switch (type)
            {
                case ResourceUriType.PreviousPage:
                    return _urlHelper.Link("GetFilteredFTCrms",
                      new
                      {
                          fields = filterOptionsModel.Fields,
                          orderBy = filterOptionsModel.OrderBy,
                          searchQuery = filterOptionsModel.SearchQuery,
                          pageNumber = filterOptionsModel.PageNumber - 1,
                          pageSize = filterOptionsModel.PageSize
                      });
                case ResourceUriType.NextPage:
                    return _urlHelper.Link("GetFilteredFTCrms",
                      new
                      {
                          fields = filterOptionsModel.Fields,
                          orderBy = filterOptionsModel.OrderBy,
                          searchQuery = filterOptionsModel.SearchQuery,
                          pageNumber = filterOptionsModel.PageNumber + 1,
                          pageSize = filterOptionsModel.PageSize
                      });

                default:
                    return _urlHelper.Link("GetFilteredFTCrms",
                    new
                    {
                        fields = filterOptionsModel.Fields,
                        orderBy = filterOptionsModel.OrderBy,
                        searchQuery = filterOptionsModel.SearchQuery,
                        pageNumber = filterOptionsModel.PageNumber,
                        pageSize = filterOptionsModel.PageSize
                    });
            }
        }

        private async Task<int> UpdateLeadServiceOfferedStatus(Guid leadserviceId)
        {           
            return await _leadServiceOfferedRepository.UpdateEntityAsync(leadserviceId, x => new LeadServiceOffered { AssignedStatus = "Invoice" });
        }

        #endregion

    }
}
