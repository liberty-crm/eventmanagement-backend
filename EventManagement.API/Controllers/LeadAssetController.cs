using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
//using EventManagement.API.Helpers;
using EventManagement.Dto;
using EventManagement.Service;
using EventManagement.Domain.Entities;
using EventManagement.Utility;
using Microsoft.Extensions.Logging;
using IdentityServer4.AccessTokenValidation;

namespace EventManagement.API.Controllers
{
    /// <summary>
    /// LeadAsset endpoint
    /// </summary>
    [Route("api/leadassets")]
    [Produces("application/json")]
    [ApiController]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]

    public class LeadAssetController : Controller
    {

        #region PRIVATE MEMBERS

        private readonly ILeadAssetService _leadassetService;
        private ILogger<LeadAssetController> _logger;
        private readonly IUrlHelper _urlHelper;

        #endregion


        #region CONSTRUCTOR

        public LeadAssetController(ILeadAssetService leadassetService, ILogger<LeadAssetController> logger, IUrlHelper urlHelper) 
        {
            _logger = logger;
            _leadassetService = leadassetService;
            _urlHelper = urlHelper;
        }

        #endregion


        #region HTTPGET

        [HttpGet(Name = "GetFilteredLeadAssets")]
        [Produces("application/vnd.tourmanagement.leadassets.hateoas+json", "application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<List<LeadAssetDto>>> GetFilteredLeadAssets([FromQuery]FilterOptionsModel filterOptionsModel, [FromHeader(Name = "Accept")] string mediaType)
        {

            //if order by fields are not valid.
            if (!_leadassetService.ValidMappingExists(filterOptionsModel.OrderBy))
            {
                //then return bad request.
                return BadRequest();
            }

            //if fields are not valid.
            if (!EventManagementUtils.TypeHasProperties<LeadAssetDto>(filterOptionsModel.Fields))
            {
                //then return bad request.
                return BadRequest();
            }

            //get the paged/filtered show from db. 
            var leadassetsFromRepo = await _leadassetService.GetFilteredEntities(filterOptionsModel);

            //if HATEOAS links are required.
            if (mediaType == "application/vnd.tourmanagement.leadassets.hateoas+json")
            {
                //create HATEOAS links for each show.
                leadassetsFromRepo.ForEach(leadasset =>
                {
                    var entityLinks = CreateLinksForLeadAsset(leadasset.Id, filterOptionsModel.Fields);
                    leadasset.links = entityLinks;
                });

                //prepare pagination metadata.
                var paginationMetadata = new
                {
                    totalCount = leadassetsFromRepo.TotalCount,
                    pageSize = leadassetsFromRepo.PageSize,
                    currentPage = leadassetsFromRepo.CurrentPage,
                    totalPages = leadassetsFromRepo.TotalPages,
                };

                //add pagination meta data to response header.
                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMetadata));

                //create links for shows.
                var links = CreateLinksForLeadAssets(filterOptionsModel, leadassetsFromRepo.HasNext, leadassetsFromRepo.HasPrevious);

                //prepare model with data and HATEOAS links.
                var linkedCollectionResource = new
                {
                    value = leadassetsFromRepo,
                    links = links
                };

                //return the data with Ok response.
                return Ok(linkedCollectionResource);
            }
            else
            {
                var previousPageLink = leadassetsFromRepo.HasPrevious ?
                    CreateLeadAssetsResourceUri(filterOptionsModel, ResourceUriType.PreviousPage) : null;

                var nextPageLink = leadassetsFromRepo.HasNext ?
                    CreateLeadAssetsResourceUri(filterOptionsModel, ResourceUriType.NextPage) : null;

                //prepare the pagination metadata.
                var paginationMetadata = new
                {
                    previousPageLink = previousPageLink,
                    nextPageLink = nextPageLink,
                    totalCount = leadassetsFromRepo.TotalCount,
                    pageSize = leadassetsFromRepo.PageSize,
                    currentPage = leadassetsFromRepo.CurrentPage,
                    totalPages = leadassetsFromRepo.TotalPages
                };

                //add pagination meta data to response header.
                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMetadata));

                //return the data with Ok response.
                return Ok(leadassetsFromRepo);
            }
        }


        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Produces("application/vnd.tourmanagement.leadassets.hateoas+json", "application/json")]
        [HttpGet("{id}", Name = "GetLeadAsset")]
        public async Task<ActionResult<LeadAsset>> GetLeadAsset(Guid id, [FromQuery] string fields, [FromHeader(Name = "Accept")] string mediaType)
        {
            object leadassetEntity = null;
            object linkedResourceToReturn = null;

            //if fields are not passed.
            if (string.IsNullOrEmpty(fields))
            {
                _logger.LogInformation("GetLeadAsset called");

                //then get the whole entity and map it to the Dto.
                leadassetEntity = Mapper.Map<LeadAssetDto>(await _leadassetService.GetEntityByIdAsync(id));
            }
            else
            {
                //if fields are passed then get the partial show.
                leadassetEntity = await _leadassetService.GetPartialEntityAsync(id, fields);
            }

            //if leadasset not found.
            if (leadassetEntity == null)
            {
                //then return not found response.
                return NotFound();
            }

            if (mediaType == "application/vnd.tourmanagement.leadassets.hateoas+json")
            {
                //create HATEOS links
                var links = CreateLinksForLeadAsset(id, fields);

                //if fields are not passed.
                if (string.IsNullOrEmpty(fields))
                {
                    //convert the typed object to expando object.
                    linkedResourceToReturn = ((LeadAssetDto)leadassetEntity).ShapeData("") as IDictionary<string, object>;

                    //add the HATEOAS links to the model.
                    ((IDictionary<string, object>)linkedResourceToReturn).Add("links", links);
                }
                else
                {
                    linkedResourceToReturn = leadassetEntity;

                    //add the HATEOAS links to the model.
                    ((dynamic)linkedResourceToReturn).links = links;

                }
            }
            else
            {
                linkedResourceToReturn = leadassetEntity;
            }

            //return the Ok response.
            return Ok(linkedResourceToReturn);
        }

        #endregion


        #region HTTPPUT

        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPut("{id}", Name = "UpdateLeadAsset")]
        public async Task<IActionResult> UpdateLeadAsset(Guid id, [FromBody]LeadAssetForUpdate LeadAssetForUpdate)
        {

            //if show not found
            if (!await _leadassetService.ExistAsync(x => x.Id == id))
            {
                //then return not found response.
                return NotFound();
            }

            //Update an entity.
            await _leadassetService.UpdateEntityAsync(id, LeadAssetForUpdate);

            //return the response.
            return NoContent();
        }

        #endregion


        #region HTTPPATCH

        [Consumes("application/json-patch+json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPatch("{id}")]
        public async Task<IActionResult> PartiallyUpdateLeadAsset(Guid id, [FromBody] JsonPatchDocument<LeadAssetForUpdate> jsonPatchDocument)
        {
            LeadAssetForUpdate dto = new LeadAssetForUpdate();
            LeadAsset leadasset = new LeadAsset();

            //apply the patch changes to the dto. 
            jsonPatchDocument.ApplyTo(dto, ModelState);

            //if the jsonPatchDocument is not valid.
            if (!ModelState.IsValid)
            {
                //then return unprocessableEntity response.
                return new UnprocessableEntityObjectResult(ModelState);
            }

            //if the dto model is not valid after applying changes.
            if (!TryValidateModel(dto))
            {
                //then return unprocessableEntity response.
                return new UnprocessableEntityObjectResult(ModelState);
            }

            //map the chnages from dto to entity.
            Mapper.Map(dto, leadasset);

            //set the Id for the show model.
            leadasset.Id = id;

            //partially update the chnages to the db. 
            await _leadassetService.UpdatePartialEntityAsync(leadasset, jsonPatchDocument);

            //return the response.
            return NoContent();
        }

        #endregion


        #region HTTPPOST

        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPost(Name = "CreateLeadAsset")]
        public async Task<ActionResult<LeadAssetDto>> CreateLeadAsset([FromBody]LeadAssetForCreation leadasset)
        {
            //create a show in db.
            var leadassetToReturn = await _leadassetService.CreateEntityAsync<LeadAssetDto, LeadAssetForCreation>(leadasset);

            //return the show created response.
            return CreatedAtRoute("GetLeadAsset", new { id = leadassetToReturn.Id }, leadassetToReturn);
        }

        #endregion


        #region HTTPDELETE
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}", Name = "DeleteLeadAssetById")]
        public async Task<IActionResult> DeleteLeadAssetById(Guid id)
        {
            //if the leadasset exists
            if (await _leadassetService.ExistAsync(x => x.Id == id))
            {
                //delete the leadasset from the db.
                await _leadassetService.DeleteEntityAsync(id);
            }
            else
            {
                //if leadasset doesn't exists then returns not found.
                return NotFound();
            }

            //return the response.
            return NoContent();
        }

        #endregion


        #region PRIVATE METHODS

        private IEnumerable<LinkDto> CreateLinksForLeadAsset(Guid id, string fields)
        {
            var links = new List<LinkDto>();

            if (string.IsNullOrWhiteSpace(fields))
            {
                links.Add(
                  new LinkDto(_urlHelper.Link("GetLeadAsset", new { id = id }),
                  "self",
                  "GET"));
            }
            else
            {
                links.Add(
                  new LinkDto(_urlHelper.Link("GetLeadAsset", new { id = id, fields = fields }),
                  "self",
                  "GET"));
            }

            links.Add(
              new LinkDto(_urlHelper.Link("DeleteLeadAssetById", new { id = id }),
              "delete_leadasset",
              "DELETE"));

            links.Add(
             new LinkDto(_urlHelper.Link("UpdateLeadAsset", new { id = id }),
             "update_leadasset",
             "PUT"));

            links.Add(
              new LinkDto(_urlHelper.Link("CreateLeadAsset", new { }),
              "create_leadasset",
              "POST"));

            //links.Add(
            //   new LinkDto(_urlHelper.Link("GetShowsForTour", new { }),
            //   "shows",
            //   "GET"));

            return links;
        }

        private IEnumerable<LinkDto> CreateLinksForLeadAssets(FilterOptionsModel filterOptionsModel, bool hasNext, bool hasPrevious)
        {
            var links = new List<LinkDto>();

            links.Add(
               new LinkDto(CreateLeadAssetsResourceUri(filterOptionsModel, ResourceUriType.Current), "self", "GET"));

            if (hasNext)
            {
                links.Add(
                  new LinkDto(CreateLeadAssetsResourceUri(filterOptionsModel, ResourceUriType.NextPage), "nextPage", "GET"));
            }

            if (hasPrevious)
            {
                links.Add(new LinkDto(CreateLeadAssetsResourceUri(filterOptionsModel, ResourceUriType.PreviousPage), "previousPage", "GET"));
            }

            return links;
        }

        private string CreateLeadAssetsResourceUri(FilterOptionsModel filterOptionsModel, ResourceUriType type)
        {
            switch (type)
            {
                case ResourceUriType.PreviousPage:
                    return _urlHelper.Link("GetFilteredLeadAssets",
                      new
                      {
                          fields = filterOptionsModel.Fields,
                          orderBy = filterOptionsModel.OrderBy,
                          searchQuery = filterOptionsModel.SearchQuery,
                          pageNumber = filterOptionsModel.PageNumber - 1,
                          pageSize = filterOptionsModel.PageSize
                      });
                case ResourceUriType.NextPage:
                    return _urlHelper.Link("GetFilteredLeadAssets",
                      new
                      {
                          fields = filterOptionsModel.Fields,
                          orderBy = filterOptionsModel.OrderBy,
                          searchQuery = filterOptionsModel.SearchQuery,
                          pageNumber = filterOptionsModel.PageNumber + 1,
                          pageSize = filterOptionsModel.PageSize
                      });

                default:
                    return _urlHelper.Link("GetFilteredLeadAssets",
                    new
                    {
                        fields = filterOptionsModel.Fields,
                        orderBy = filterOptionsModel.OrderBy,
                        searchQuery = filterOptionsModel.SearchQuery,
                        pageNumber = filterOptionsModel.PageNumber,
                        pageSize = filterOptionsModel.PageSize
                    });
            }
        }

        #endregion

    }
}
