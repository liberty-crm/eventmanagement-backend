using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
//using EventManagement.API.Helpers;
using EventManagement.Dto;
using EventManagement.Service;
using EventManagement.Domain.Entities;
using EventManagement.Utility;
using Microsoft.Extensions.Logging;

namespace EventManagement.API.Controllers
{
    /// <summary>
    /// NotificationMail endpoint
    /// </summary>
    [Route("api/notificationmails")]
    [Produces("application/json")]
    [ApiController]
    public class NotificationMailController : Controller
    {

        #region PRIVATE MEMBERS

        private readonly INotificationMailService _notificationmailService;
        private ILogger<NotificationMailController> _logger;
        private readonly IUrlHelper _urlHelper;

        #endregion


        #region CONSTRUCTOR

        public NotificationMailController(INotificationMailService notificationmailService, ILogger<NotificationMailController> logger, IUrlHelper urlHelper) 
        {
            _logger = logger;
            _notificationmailService = notificationmailService;
            _urlHelper = urlHelper;
        }

        #endregion


        #region HTTPGET

        [HttpGet(Name = "GetFilteredNotificationMails")]
        [Produces("application/vnd.tourmanagement.notificationmails.hateoas+json", "application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<List<NotificationMailDto>>> GetFilteredNotificationMails([FromQuery]FilterOptionsModel filterOptionsModel, [FromHeader(Name = "Accept")] string mediaType)
        {

            //if order by fields are not valid.
            if (!_notificationmailService.ValidMappingExists(filterOptionsModel.OrderBy))
            {
                //then return bad request.
                return BadRequest();
            }

            //if fields are not valid.
            if (!EventManagementUtils.TypeHasProperties<NotificationMailDto>(filterOptionsModel.Fields))
            {
                //then return bad request.
                return BadRequest();
            }

            //get the paged/filtered show from db. 
            var notificationmailsFromRepo = await _notificationmailService.GetFilteredEntities(filterOptionsModel);

            //if HATEOAS links are required.
            if (mediaType == "application/vnd.tourmanagement.notificationmails.hateoas+json")
            {
                //create HATEOAS links for each show.
                notificationmailsFromRepo.ForEach(notificationmail =>
                {
                    var entityLinks = CreateLinksForNotificationMail(notificationmail.Id, filterOptionsModel.Fields);
                    notificationmail.links = entityLinks;
                });

                //prepare pagination metadata.
                var paginationMetadata = new
                {
                    totalCount = notificationmailsFromRepo.TotalCount,
                    pageSize = notificationmailsFromRepo.PageSize,
                    currentPage = notificationmailsFromRepo.CurrentPage,
                    totalPages = notificationmailsFromRepo.TotalPages,
                };

                //add pagination meta data to response header.
                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMetadata));

                //create links for shows.
                var links = CreateLinksForNotificationMails(filterOptionsModel, notificationmailsFromRepo.HasNext, notificationmailsFromRepo.HasPrevious);

                //prepare model with data and HATEOAS links.
                var linkedCollectionResource = new
                {
                    value = notificationmailsFromRepo,
                    links = links
                };

                //return the data with Ok response.
                return Ok(linkedCollectionResource);
            }
            else
            {
                var previousPageLink = notificationmailsFromRepo.HasPrevious ?
                    CreateNotificationMailsResourceUri(filterOptionsModel, ResourceUriType.PreviousPage) : null;

                var nextPageLink = notificationmailsFromRepo.HasNext ?
                    CreateNotificationMailsResourceUri(filterOptionsModel, ResourceUriType.NextPage) : null;

                //prepare the pagination metadata.
                var paginationMetadata = new
                {
                    previousPageLink = previousPageLink,
                    nextPageLink = nextPageLink,
                    totalCount = notificationmailsFromRepo.TotalCount,
                    pageSize = notificationmailsFromRepo.PageSize,
                    currentPage = notificationmailsFromRepo.CurrentPage,
                    totalPages = notificationmailsFromRepo.TotalPages
                };

                //add pagination meta data to response header.
                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMetadata));

                //return the data with Ok response.
                return Ok(notificationmailsFromRepo);
            }
        }


        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Produces("application/vnd.tourmanagement.notificationmails.hateoas+json", "application/json")]
        [HttpGet("{id}", Name = "GetNotificationMail")]
        public async Task<ActionResult<NotificationMail>> GetNotificationMail(Guid id, [FromQuery] string fields, [FromHeader(Name = "Accept")] string mediaType)
        {
            object notificationmailEntity = null;
            object linkedResourceToReturn = null;

            //if fields are not passed.
            if (string.IsNullOrEmpty(fields))
            {
                _logger.LogInformation("GetNotificationMail called");

                //then get the whole entity and map it to the Dto.
                notificationmailEntity = Mapper.Map<NotificationMailDto>(await _notificationmailService.GetEntityByIdAsync(id));
            }
            else
            {
                //if fields are passed then get the partial show.
                notificationmailEntity = await _notificationmailService.GetPartialEntityAsync(id, fields);
            }

            //if notificationmail not found.
            if (notificationmailEntity == null)
            {
                //then return not found response.
                return NotFound();
            }

            if (mediaType == "application/vnd.tourmanagement.notificationmails.hateoas+json")
            {
                //create HATEOS links
                var links = CreateLinksForNotificationMail(id, fields);

                //if fields are not passed.
                if (string.IsNullOrEmpty(fields))
                {
                    //convert the typed object to expando object.
                    linkedResourceToReturn = ((NotificationMailDto)notificationmailEntity).ShapeData("") as IDictionary<string, object>;

                    //add the HATEOAS links to the model.
                    ((IDictionary<string, object>)linkedResourceToReturn).Add("links", links);
                }
                else
                {
                    linkedResourceToReturn = notificationmailEntity;

                    //add the HATEOAS links to the model.
                    ((dynamic)linkedResourceToReturn).links = links;

                }
            }
            else
            {
                linkedResourceToReturn = notificationmailEntity;
            }

            //return the Ok response.
            return Ok(linkedResourceToReturn);
        }

        #endregion


        #region HTTPPUT

        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPut("{id}", Name = "UpdateNotificationMail")]
        public async Task<IActionResult> UpdateNotificationMail(Guid id, [FromBody]NotificationMailForUpdate NotificationMailForUpdate)
        {

            //if show not found
            if (!await _notificationmailService.ExistAsync(x => x.Id == id))
            {
                //then return not found response.
                return NotFound();
            }

            //Update an entity.
            await _notificationmailService.UpdateEntityAsync(id, NotificationMailForUpdate);

            //return the response.
            return NoContent();
        }

        #endregion


        #region HTTPPATCH

        [Consumes("application/json-patch+json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPatch("{id}")]
        public async Task<IActionResult> PartiallyUpdateNotificationMail(Guid id, [FromBody] JsonPatchDocument<NotificationMailForUpdate> jsonPatchDocument)
        {
            NotificationMailForUpdate dto = new NotificationMailForUpdate();
            NotificationMail notificationmail = new NotificationMail();

            //apply the patch changes to the dto. 
            jsonPatchDocument.ApplyTo(dto, ModelState);

            //if the jsonPatchDocument is not valid.
            if (!ModelState.IsValid)
            {
                //then return unprocessableEntity response.
                return new UnprocessableEntityObjectResult(ModelState);
            }

            //if the dto model is not valid after applying changes.
            if (!TryValidateModel(dto))
            {
                //then return unprocessableEntity response.
                return new UnprocessableEntityObjectResult(ModelState);
            }

            //map the chnages from dto to entity.
            Mapper.Map(dto, notificationmail);

            //set the Id for the show model.
            notificationmail.Id = id;

            //partially update the chnages to the db. 
            await _notificationmailService.UpdatePartialEntityAsync(notificationmail, jsonPatchDocument);

            //return the response.
            return NoContent();
        }

        #endregion


        #region HTTPPOST

        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPost(Name = "CreateNotificationMail")]
        public async Task<ActionResult<NotificationMailDto>> CreateNotificationMail([FromBody]NotificationMailForCreation notificationmail)
        {
            //create a show in db.
            var notificationmailToReturn = await _notificationmailService.CreateEntityAsync<NotificationMailDto, NotificationMailForCreation>(notificationmail);

            //return the show created response.
            return CreatedAtRoute("GetNotificationMail", new { id = notificationmailToReturn.Id }, notificationmailToReturn);
        }

        #endregion


        #region HTTPDELETE
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}", Name = "DeleteNotificationMailById")]
        public async Task<IActionResult> DeleteNotificationMailById(Guid id)
        {
            //if the notificationmail exists
            if (await _notificationmailService.ExistAsync(x => x.Id == id))
            {
                //delete the notificationmail from the db.
                await _notificationmailService.DeleteEntityAsync(id);
            }
            else
            {
                //if notificationmail doesn't exists then returns not found.
                return NotFound();
            }

            //return the response.
            return NoContent();
        }

        #endregion


        #region PRIVATE METHODS

        private IEnumerable<LinkDto> CreateLinksForNotificationMail(Guid id, string fields)
        {
            var links = new List<LinkDto>();

            if (string.IsNullOrWhiteSpace(fields))
            {
                links.Add(
                  new LinkDto(_urlHelper.Link("GetNotificationMail", new { id = id }),
                  "self",
                  "GET"));
            }
            else
            {
                links.Add(
                  new LinkDto(_urlHelper.Link("GetNotificationMail", new { id = id, fields = fields }),
                  "self",
                  "GET"));
            }

            links.Add(
              new LinkDto(_urlHelper.Link("DeleteNotificationMailById", new { id = id }),
              "delete_notificationmail",
              "DELETE"));

            links.Add(
             new LinkDto(_urlHelper.Link("UpdateNotificationMail", new { id = id }),
             "update_notificationmail",
             "PUT"));

            links.Add(
              new LinkDto(_urlHelper.Link("CreateNotificationMail", new { }),
              "create_notificationmail",
              "POST"));

            //links.Add(
            //   new LinkDto(_urlHelper.Link("GetShowsForTour", new { }),
            //   "shows",
            //   "GET"));

            return links;
        }

        private IEnumerable<LinkDto> CreateLinksForNotificationMails(FilterOptionsModel filterOptionsModel, bool hasNext, bool hasPrevious)
        {
            var links = new List<LinkDto>();

            links.Add(
               new LinkDto(CreateNotificationMailsResourceUri(filterOptionsModel, ResourceUriType.Current), "self", "GET"));

            if (hasNext)
            {
                links.Add(
                  new LinkDto(CreateNotificationMailsResourceUri(filterOptionsModel, ResourceUriType.NextPage), "nextPage", "GET"));
            }

            if (hasPrevious)
            {
                links.Add(new LinkDto(CreateNotificationMailsResourceUri(filterOptionsModel, ResourceUriType.PreviousPage), "previousPage", "GET"));
            }

            return links;
        }

        private string CreateNotificationMailsResourceUri(FilterOptionsModel filterOptionsModel, ResourceUriType type)
        {
            switch (type)
            {
                case ResourceUriType.PreviousPage:
                    return _urlHelper.Link("GetFilteredNotificationMails",
                      new
                      {
                          fields = filterOptionsModel.Fields,
                          orderBy = filterOptionsModel.OrderBy,
                          searchQuery = filterOptionsModel.SearchQuery,
                          pageNumber = filterOptionsModel.PageNumber - 1,
                          pageSize = filterOptionsModel.PageSize
                      });
                case ResourceUriType.NextPage:
                    return _urlHelper.Link("GetFilteredNotificationMails",
                      new
                      {
                          fields = filterOptionsModel.Fields,
                          orderBy = filterOptionsModel.OrderBy,
                          searchQuery = filterOptionsModel.SearchQuery,
                          pageNumber = filterOptionsModel.PageNumber + 1,
                          pageSize = filterOptionsModel.PageSize
                      });

                default:
                    return _urlHelper.Link("GetFilteredNotificationMails",
                    new
                    {
                        fields = filterOptionsModel.Fields,
                        orderBy = filterOptionsModel.OrderBy,
                        searchQuery = filterOptionsModel.SearchQuery,
                        pageNumber = filterOptionsModel.PageNumber,
                        pageSize = filterOptionsModel.PageSize
                    });
            }
        }

        #endregion

    }
}
