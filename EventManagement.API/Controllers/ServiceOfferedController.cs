using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
//using EventManagement.API.Helpers;
using EventManagement.Dto;
using EventManagement.Service;
using EventManagement.Domain.Entities;
using EventManagement.Utility;
using Microsoft.Extensions.Logging;
using IdentityServer4.AccessTokenValidation;

namespace EventManagement.API.Controllers
{
    /// <summary>
    /// ServiceOffered endpoint
    /// </summary>
    [Route("api/serviceoffereds")]
    [Produces("application/json")]
    [ApiController]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]

    public class ServiceOfferedController : Controller
    {

        #region PRIVATE MEMBERS

        private readonly IServiceOfferedService _serviceofferedService;
        private ILogger<ServiceOfferedController> _logger;
        private readonly IUrlHelper _urlHelper;

        #endregion


        #region CONSTRUCTOR

        public ServiceOfferedController(IServiceOfferedService serviceofferedService, ILogger<ServiceOfferedController> logger, IUrlHelper urlHelper) 
        {
            _logger = logger;
            _serviceofferedService = serviceofferedService;
            _urlHelper = urlHelper;
        }

        #endregion


        #region HTTPGET

        [AllowAnonymous]
        [HttpGet(Name = "GetFilteredServiceOffereds")]
        [Produces("application/vnd.tourmanagement.serviceoffereds.hateoas+json", "application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<List<ServiceOfferedDto>>> GetFilteredServiceOffereds([FromQuery]FilterOptionsModel filterOptionsModel, [FromHeader(Name = "Accept")] string mediaType)
        {
            try
            {
                //if order by fields are not valid.
                if (!_serviceofferedService.ValidMappingExists(filterOptionsModel.OrderBy))
                {
                    //then return bad request.
                    return BadRequest();
                }

                //if fields are not valid.
                if (!EventManagementUtils.TypeHasProperties<ServiceOfferedDto>(filterOptionsModel.Fields))
                {
                    //then return bad request.
                    return BadRequest();
                }

                //get the paged/filtered show from db. 
                var serviceofferedsFromRepo = await _serviceofferedService.GetFilteredEntities(filterOptionsModel);

                //if HATEOAS links are required.
                if (mediaType == "application/vnd.tourmanagement.serviceoffereds.hateoas+json")
                {
                    //create HATEOAS links for each show.
                    serviceofferedsFromRepo.ForEach(serviceoffered =>
                    {
                        var entityLinks = CreateLinksForServiceOffered(serviceoffered.Id, filterOptionsModel.Fields);
                        serviceoffered.links = entityLinks;
                    });

                    //prepare pagination metadata.
                    var paginationMetadata = new
                    {
                        totalCount = serviceofferedsFromRepo.TotalCount,
                        pageSize = serviceofferedsFromRepo.PageSize,
                        currentPage = serviceofferedsFromRepo.CurrentPage,
                        totalPages = serviceofferedsFromRepo.TotalPages,
                    };

                    //add pagination meta data to response header.
                    Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMetadata));

                    //create links for shows.
                    var links = CreateLinksForServiceOffereds(filterOptionsModel, serviceofferedsFromRepo.HasNext, serviceofferedsFromRepo.HasPrevious);

                    //prepare model with data and HATEOAS links.
                    var linkedCollectionResource = new
                    {
                        value = serviceofferedsFromRepo,
                        links = links
                    };

                    //return the data with Ok response.
                    return Ok(linkedCollectionResource);
                }
                else
                {
                    var previousPageLink = serviceofferedsFromRepo.HasPrevious ?
                        CreateServiceOfferedsResourceUri(filterOptionsModel, ResourceUriType.PreviousPage) : null;

                    var nextPageLink = serviceofferedsFromRepo.HasNext ?
                        CreateServiceOfferedsResourceUri(filterOptionsModel, ResourceUriType.NextPage) : null;

                    //prepare the pagination metadata.
                    var paginationMetadata = new
                    {
                        previousPageLink = previousPageLink,
                        nextPageLink = nextPageLink,
                        totalCount = serviceofferedsFromRepo.TotalCount,
                        pageSize = serviceofferedsFromRepo.PageSize,
                        currentPage = serviceofferedsFromRepo.CurrentPage,
                        totalPages = serviceofferedsFromRepo.TotalPages
                    };

                    //add pagination meta data to response header.
                    Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMetadata));

                    //return the data with Ok response.
                    return Ok(serviceofferedsFromRepo);
                }
            }
            catch(Exception ex)
            {
                return BadRequest();
            }            
        }


        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Produces("application/vnd.tourmanagement.serviceoffereds.hateoas+json", "application/json")]
        [HttpGet("{id}", Name = "GetServiceOffered")]
        public async Task<ActionResult<ServiceOffered>> GetServiceOffered(Guid id, [FromQuery] string fields, [FromHeader(Name = "Accept")] string mediaType)
        {
            object serviceofferedEntity = null;
            object linkedResourceToReturn = null;

            //if fields are not passed.
            if (string.IsNullOrEmpty(fields))
            {
                _logger.LogInformation("GetServiceOffered called");

                //then get the whole entity and map it to the Dto.
                serviceofferedEntity = Mapper.Map<ServiceOfferedDto>(await _serviceofferedService.GetEntityByIdAsync(id));
            }
            else
            {
                //if fields are passed then get the partial show.
                serviceofferedEntity = await _serviceofferedService.GetPartialEntityAsync(id, fields);
            }

            //if serviceoffered not found.
            if (serviceofferedEntity == null)
            {
                //then return not found response.
                return NotFound();
            }

            if (mediaType == "application/vnd.tourmanagement.serviceoffereds.hateoas+json")
            {
                //create HATEOS links
                var links = CreateLinksForServiceOffered(id, fields);

                //if fields are not passed.
                if (string.IsNullOrEmpty(fields))
                {
                    //convert the typed object to expando object.
                    linkedResourceToReturn = ((ServiceOfferedDto)serviceofferedEntity).ShapeData("") as IDictionary<string, object>;

                    //add the HATEOAS links to the model.
                    ((IDictionary<string, object>)linkedResourceToReturn).Add("links", links);
                }
                else
                {
                    linkedResourceToReturn = serviceofferedEntity;

                    //add the HATEOAS links to the model.
                    ((dynamic)linkedResourceToReturn).links = links;

                }
            }
            else
            {
                linkedResourceToReturn = serviceofferedEntity;
            }

            //return the Ok response.
            return Ok(linkedResourceToReturn);
        }

        #endregion


        #region HTTPPUT

        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPut("{id}", Name = "UpdateServiceOffered")]
        public async Task<IActionResult> UpdateServiceOffered(Guid id, [FromBody]ServiceOfferedForUpdate ServiceOfferedForUpdate)
        {

            //if show not found
            if (!await _serviceofferedService.ExistAsync(x => x.Id == id))
            {
                //then return not found response.
                return NotFound();
            }

            //Update an entity.
            await _serviceofferedService.UpdateEntityAsync(id, ServiceOfferedForUpdate);

            //return the response.
            return NoContent();
        }

        #endregion


        #region HTTPPATCH

        [Consumes("application/json-patch+json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPatch("{id}")]
        public async Task<IActionResult> PartiallyUpdateServiceOffered(Guid id, [FromBody] JsonPatchDocument<ServiceOfferedForUpdate> jsonPatchDocument)
        {
            ServiceOfferedForUpdate dto = new ServiceOfferedForUpdate();
            ServiceOffered serviceoffered = new ServiceOffered();

            //apply the patch changes to the dto. 
            jsonPatchDocument.ApplyTo(dto, ModelState);

            //if the jsonPatchDocument is not valid.
            if (!ModelState.IsValid)
            {
                //then return unprocessableEntity response.
                return new UnprocessableEntityObjectResult(ModelState);
            }

            //if the dto model is not valid after applying changes.
            if (!TryValidateModel(dto))
            {
                //then return unprocessableEntity response.
                return new UnprocessableEntityObjectResult(ModelState);
            }

            //map the chnages from dto to entity.
            Mapper.Map(dto, serviceoffered);

            //set the Id for the show model.
            serviceoffered.Id = id;

            //partially update the chnages to the db. 
            await _serviceofferedService.UpdatePartialEntityAsync(serviceoffered, jsonPatchDocument);

            //return the response.
            return NoContent();
        }

        #endregion


        #region HTTPPOST

        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPost(Name = "CreateServiceOffered")]
        public async Task<ActionResult<ServiceOfferedDto>> CreateServiceOffered([FromBody]ServiceOfferedForCreation serviceoffered)
        {
            serviceoffered.Id = Guid.NewGuid().ToString();
            //create a show in db.
            var serviceofferedToReturn = await _serviceofferedService.CreateEntityAsync<ServiceOfferedDto, ServiceOfferedForCreation>(serviceoffered);

            //return the show created response.
            return CreatedAtRoute("GetServiceOffered", new { id = serviceofferedToReturn.Id }, serviceofferedToReturn);
        }

        #endregion


        #region HTTPDELETE
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}", Name = "DeleteServiceOfferedById")]
        public async Task<IActionResult> DeleteServiceOfferedById(Guid id)
        {
            //if the serviceoffered exists
            if (await _serviceofferedService.ExistAsync(x => x.Id == id))
            {
                //delete the serviceoffered from the db.
                await _serviceofferedService.DeleteEntityAsync(id);
            }
            else
            {
                //if serviceoffered doesn't exists then returns not found.
                return NotFound();
            }

            //return the response.
            return NoContent();
        }

        #endregion


        #region PRIVATE METHODS

        private IEnumerable<LinkDto> CreateLinksForServiceOffered(Guid id, string fields)
        {
            var links = new List<LinkDto>();

            if (string.IsNullOrWhiteSpace(fields))
            {
                links.Add(
                  new LinkDto(_urlHelper.Link("GetServiceOffered", new { id = id }),
                  "self",
                  "GET"));
            }
            else
            {
                links.Add(
                  new LinkDto(_urlHelper.Link("GetServiceOffered", new { id = id, fields = fields }),
                  "self",
                  "GET"));
            }

            links.Add(
              new LinkDto(_urlHelper.Link("DeleteServiceOfferedById", new { id = id }),
              "delete_serviceoffered",
              "DELETE"));

            links.Add(
             new LinkDto(_urlHelper.Link("UpdateServiceOffered", new { id = id }),
             "update_serviceoffered",
             "PUT"));

            links.Add(
              new LinkDto(_urlHelper.Link("CreateServiceOffered", new { }),
              "create_serviceoffered",
              "POST"));

            //links.Add(
            //   new LinkDto(_urlHelper.Link("GetShowsForTour", new { }),
            //   "shows",
            //   "GET"));

            return links;
        }

        private IEnumerable<LinkDto> CreateLinksForServiceOffereds(FilterOptionsModel filterOptionsModel, bool hasNext, bool hasPrevious)
        {
            var links = new List<LinkDto>();

            links.Add(
               new LinkDto(CreateServiceOfferedsResourceUri(filterOptionsModel, ResourceUriType.Current), "self", "GET"));

            if (hasNext)
            {
                links.Add(
                  new LinkDto(CreateServiceOfferedsResourceUri(filterOptionsModel, ResourceUriType.NextPage), "nextPage", "GET"));
            }

            if (hasPrevious)
            {
                links.Add(new LinkDto(CreateServiceOfferedsResourceUri(filterOptionsModel, ResourceUriType.PreviousPage), "previousPage", "GET"));
            }

            return links;
        }

        private string CreateServiceOfferedsResourceUri(FilterOptionsModel filterOptionsModel, ResourceUriType type)
        {
            switch (type)
            {
                case ResourceUriType.PreviousPage:
                    return _urlHelper.Link("GetFilteredServiceOffereds",
                      new
                      {
                          fields = filterOptionsModel.Fields,
                          orderBy = filterOptionsModel.OrderBy,
                          searchQuery = filterOptionsModel.SearchQuery,
                          pageNumber = filterOptionsModel.PageNumber - 1,
                          pageSize = filterOptionsModel.PageSize
                      });
                case ResourceUriType.NextPage:
                    return _urlHelper.Link("GetFilteredServiceOffereds",
                      new
                      {
                          fields = filterOptionsModel.Fields,
                          orderBy = filterOptionsModel.OrderBy,
                          searchQuery = filterOptionsModel.SearchQuery,
                          pageNumber = filterOptionsModel.PageNumber + 1,
                          pageSize = filterOptionsModel.PageSize
                      });

                default:
                    return _urlHelper.Link("GetFilteredServiceOffereds",
                    new
                    {
                        fields = filterOptionsModel.Fields,
                        orderBy = filterOptionsModel.OrderBy,
                        searchQuery = filterOptionsModel.SearchQuery,
                        pageNumber = filterOptionsModel.PageNumber,
                        pageSize = filterOptionsModel.PageSize
                    });
            }
        }

        #endregion

    }
}
