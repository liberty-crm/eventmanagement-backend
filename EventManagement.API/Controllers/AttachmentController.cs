using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
//using EventManagement.API.Helpers;
using EventManagement.Dto;
using EventManagement.Service;
using EventManagement.Domain.Entities;
using EventManagement.Utility;
using Microsoft.Extensions.Logging;
using System.IO;
using System.Net.Http.Headers;
using IdentityServer4.AccessTokenValidation;

namespace EventManagement.API.Controllers
{
    /// <summary>
    /// Attachment endpoint
    /// </summary>
    [Route("api/attachments")]
    [Produces("application/json")]
    [ApiController]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]

    public class AttachmentController : Controller
    {

        #region PRIVATE MEMBERS

        private readonly IAttachmentService _attachmentService;
        private ILogger<AttachmentController> _logger;
        private readonly IUrlHelper _urlHelper;

        #endregion


        #region CONSTRUCTOR

        public AttachmentController(IAttachmentService attachmentService, ILogger<AttachmentController> logger, IUrlHelper urlHelper) 
        {
            _logger = logger;
            _attachmentService = attachmentService;
            _urlHelper = urlHelper;
        }

        #endregion


        #region HTTPGET

        [HttpGet(Name = "GetFilteredAttachments")]
        [Produces("application/vnd.tourmanagement.attachments.hateoas+json", "application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<List<AttachmentDto>>> GetFilteredAttachments([FromQuery]FilterOptionsModel filterOptionsModel, [FromHeader(Name = "Accept")] string mediaType)
        {

            //if order by fields are not valid.
            if (!_attachmentService.ValidMappingExists(filterOptionsModel.OrderBy))
            {
                //then return bad request.
                return BadRequest();
            }

            //if fields are not valid.
            if (!EventManagementUtils.TypeHasProperties<AttachmentDto>(filterOptionsModel.Fields))
            {
                //then return bad request.
                return BadRequest();
            }

            //get the paged/filtered show from db. 
            var attachmentsFromRepo = await _attachmentService.GetFilteredEntities(filterOptionsModel);

            //if HATEOAS links are required.
            if (mediaType == "application/vnd.tourmanagement.attachments.hateoas+json")
            {
                //create HATEOAS links for each show.
                attachmentsFromRepo.ForEach(attachment =>
                {
                    var entityLinks = CreateLinksForAttachment(attachment.Id, filterOptionsModel.Fields);
                    attachment.links = entityLinks;
                });

                //prepare pagination metadata.
                var paginationMetadata = new
                {
                    totalCount = attachmentsFromRepo.TotalCount,
                    pageSize = attachmentsFromRepo.PageSize,
                    currentPage = attachmentsFromRepo.CurrentPage,
                    totalPages = attachmentsFromRepo.TotalPages,
                };

                //add pagination meta data to response header.
                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMetadata));

                //create links for shows.
                var links = CreateLinksForAttachments(filterOptionsModel, attachmentsFromRepo.HasNext, attachmentsFromRepo.HasPrevious);

                //prepare model with data and HATEOAS links.
                var linkedCollectionResource = new
                {
                    value = attachmentsFromRepo,
                    links = links
                };

                //return the data with Ok response.
                return Ok(linkedCollectionResource);
            }
            else
            {
                var previousPageLink = attachmentsFromRepo.HasPrevious ?
                    CreateAttachmentsResourceUri(filterOptionsModel, ResourceUriType.PreviousPage) : null;

                var nextPageLink = attachmentsFromRepo.HasNext ?
                    CreateAttachmentsResourceUri(filterOptionsModel, ResourceUriType.NextPage) : null;

                //prepare the pagination metadata.
                var paginationMetadata = new
                {
                    previousPageLink = previousPageLink,
                    nextPageLink = nextPageLink,
                    totalCount = attachmentsFromRepo.TotalCount,
                    pageSize = attachmentsFromRepo.PageSize,
                    currentPage = attachmentsFromRepo.CurrentPage,
                    totalPages = attachmentsFromRepo.TotalPages
                };

                //add pagination meta data to response header.
                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMetadata));

                //return the data with Ok response.
                return Ok(attachmentsFromRepo);
            }
        }


        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Produces("application/vnd.tourmanagement.attachments.hateoas+json", "application/json")]
        [HttpGet("{id}", Name = "GetAttachment")]
        public async Task<ActionResult<Attachment>> GetAttachment(Guid id, [FromQuery] string fields, [FromHeader(Name = "Accept")] string mediaType)
        {
            object attachmentEntity = null;
            object linkedResourceToReturn = null;

            //if fields are not passed.
            if (string.IsNullOrEmpty(fields))
            {
                _logger.LogInformation("GetAttachment called");

                //then get the whole entity and map it to the Dto.
                attachmentEntity = Mapper.Map<AttachmentDto>(await _attachmentService.GetEntityByIdAsync(id));
            }
            else
            {
                //if fields are passed then get the partial show.
                attachmentEntity = await _attachmentService.GetPartialEntityAsync(id, fields);
            }

            //if attachment not found.
            if (attachmentEntity == null)
            {
                //then return not found response.
                return NotFound();
            }

            if (mediaType == "application/vnd.tourmanagement.attachments.hateoas+json")
            {
                //create HATEOS links
                var links = CreateLinksForAttachment(id, fields);

                //if fields are not passed.
                if (string.IsNullOrEmpty(fields))
                {
                    //convert the typed object to expando object.
                    linkedResourceToReturn = ((AttachmentDto)attachmentEntity).ShapeData("") as IDictionary<string, object>;

                    //add the HATEOAS links to the model.
                    ((IDictionary<string, object>)linkedResourceToReturn).Add("links", links);
                }
                else
                {
                    linkedResourceToReturn = attachmentEntity;

                    //add the HATEOAS links to the model.
                    ((dynamic)linkedResourceToReturn).links = links;

                }
            }
            else
            {
                linkedResourceToReturn = attachmentEntity;
            }

            //return the Ok response.
            return Ok(linkedResourceToReturn);
        }

        #endregion


        #region HTTPPUT

        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPut("{id}", Name = "UpdateAttachment")]
        public async Task<IActionResult> UpdateAttachment(Guid id, [FromBody]AttachmentForUpdate AttachmentForUpdate)
        {

            //if show not found
            if (!await _attachmentService.ExistAsync(x => x.Id == id))
            {
                //then return not found response.
                return NotFound();
            }

            //Update an entity.
            await _attachmentService.UpdateEntityAsync(id, AttachmentForUpdate);

            //return the response.
            return NoContent();
        }

        #endregion


        #region HTTPPATCH

        [Consumes("application/json-patch+json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPatch("{id}")]
        public async Task<IActionResult> PartiallyUpdateAttachment(Guid id, [FromBody] JsonPatchDocument<AttachmentForUpdate> jsonPatchDocument)
        {
            AttachmentForUpdate dto = new AttachmentForUpdate();
            Attachment attachment = new Attachment();

            //apply the patch changes to the dto. 
            jsonPatchDocument.ApplyTo(dto, ModelState);

            //if the jsonPatchDocument is not valid.
            if (!ModelState.IsValid)
            {
                //then return unprocessableEntity response.
                return new UnprocessableEntityObjectResult(ModelState);
            }

            //if the dto model is not valid after applying changes.
            if (!TryValidateModel(dto))
            {
                //then return unprocessableEntity response.
                return new UnprocessableEntityObjectResult(ModelState);
            }

            //map the chnages from dto to entity.
            Mapper.Map(dto, attachment);

            //set the Id for the show model.
            attachment.Id = id;

            //partially update the chnages to the db. 
            await _attachmentService.UpdatePartialEntityAsync(attachment, jsonPatchDocument);

            //return the response.
            return NoContent();
        }

        #endregion


        #region HTTPPOST

        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPost(Name = "CreateAttachment")]
        public async Task<ActionResult<AttachmentDto>> CreateAttachment([FromBody]AttachmentForCreation attachment)
        {
            //create a show in db.
            var attachmentToReturn = await _attachmentService.CreateEntityAsync<AttachmentDto, AttachmentForCreation>(attachment);

            //return the show created response.
            return CreatedAtRoute("GetAttachment", new { id = attachmentToReturn.Id }, attachmentToReturn);
        }


        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPost("UploadAttachment", Name = "UploadAttachment")]
        public async Task<ActionResult> UploadAttachment(string attachmentType, string leadId, string serviceId, string userId)
        {
            AttachmentForCreation attachment = new AttachmentForCreation();
            attachment.LeadId = string.IsNullOrEmpty(leadId) ? null : leadId;
            attachment.ServiceId = string.IsNullOrEmpty(serviceId) ? null : serviceId;
            attachment.UserId = userId;
            attachment.Type = attachmentType;

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!_attachmentService.IsMultipartContentType(Request.ContentType))
            {
                BadRequest(StatusCodes.Status415UnsupportedMediaType);
            }


            var attachmentList = new List<dynamic>();
            for (int i = 0; i < Request.Form.Files.Count; i++)
            {                
                string filename = Request.Form.Files[i].FileName;
                byte[] fileBytes;
                using (var ms = new MemoryStream())
                {
                    Request.Form.Files[i].CopyTo(ms);
                    fileBytes = ms.ToArray();
                    // act on the Base64 data
                }

                if (EventManagementUtils.IsValidFileExtension(filename))
                {
                    var fileName = ContentDispositionHeaderValue.Parse(Request.Form.Files[i].ContentDisposition).FileName;
                    var originalFileName = JsonConvert.DeserializeObject(fileName).ToString();
                    var attachments = (await _attachmentService.UploadAttachment(originalFileName, attachment, fileBytes));
                    attachmentList.Add(attachments);
                }

            }

            return CreatedAtRoute("GetFilteredAttachments", attachmentList);
        }

        #endregion


        #region HTTPDELETE
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}", Name = "DeleteAttachmentById")]
        public async Task<IActionResult> DeleteAttachmentById(Guid id)
        {
            //if the attachment exists
            if (await _attachmentService.ExistAsync(x => x.Id == id))
            {
                //delete the attachment from the db.
                await _attachmentService.DeleteEntityAsync(id);
            }
            else
            {
                //if attachment doesn't exists then returns not found.
                return NotFound();
            }

            //return the response.
            return NoContent();
        }

        #endregion


        #region PRIVATE METHODS

        private IEnumerable<LinkDto> CreateLinksForAttachment(Guid id, string fields)
        {
            var links = new List<LinkDto>();

            if (string.IsNullOrWhiteSpace(fields))
            {
                links.Add(
                  new LinkDto(_urlHelper.Link("GetAttachment", new { id = id }),
                  "self",
                  "GET"));
            }
            else
            {
                links.Add(
                  new LinkDto(_urlHelper.Link("GetAttachment", new { id = id, fields = fields }),
                  "self",
                  "GET"));
            }

            links.Add(
              new LinkDto(_urlHelper.Link("DeleteAttachmentById", new { id = id }),
              "delete_attachment",
              "DELETE"));

            links.Add(
             new LinkDto(_urlHelper.Link("UpdateAttachment", new { id = id }),
             "update_attachment",
             "PUT"));

            links.Add(
              new LinkDto(_urlHelper.Link("CreateAttachment", new { }),
              "create_attachment",
              "POST"));

            //links.Add(
            //   new LinkDto(_urlHelper.Link("GetShowsForTour", new { }),
            //   "shows",
            //   "GET"));

            return links;
        }

        private IEnumerable<LinkDto> CreateLinksForAttachments(FilterOptionsModel filterOptionsModel, bool hasNext, bool hasPrevious)
        {
            var links = new List<LinkDto>();

            links.Add(
               new LinkDto(CreateAttachmentsResourceUri(filterOptionsModel, ResourceUriType.Current), "self", "GET"));

            if (hasNext)
            {
                links.Add(
                  new LinkDto(CreateAttachmentsResourceUri(filterOptionsModel, ResourceUriType.NextPage), "nextPage", "GET"));
            }

            if (hasPrevious)
            {
                links.Add(new LinkDto(CreateAttachmentsResourceUri(filterOptionsModel, ResourceUriType.PreviousPage), "previousPage", "GET"));
            }

            return links;
        }

        private string CreateAttachmentsResourceUri(FilterOptionsModel filterOptionsModel, ResourceUriType type)
        {
            switch (type)
            {
                case ResourceUriType.PreviousPage:
                    return _urlHelper.Link("GetFilteredAttachments",
                      new
                      {
                          fields = filterOptionsModel.Fields,
                          orderBy = filterOptionsModel.OrderBy,
                          searchQuery = filterOptionsModel.SearchQuery,
                          pageNumber = filterOptionsModel.PageNumber - 1,
                          pageSize = filterOptionsModel.PageSize
                      });
                case ResourceUriType.NextPage:
                    return _urlHelper.Link("GetFilteredAttachments",
                      new
                      {
                          fields = filterOptionsModel.Fields,
                          orderBy = filterOptionsModel.OrderBy,
                          searchQuery = filterOptionsModel.SearchQuery,
                          pageNumber = filterOptionsModel.PageNumber + 1,
                          pageSize = filterOptionsModel.PageSize
                      });

                default:
                    return _urlHelper.Link("GetFilteredAttachments",
                    new
                    {
                        fields = filterOptionsModel.Fields,
                        orderBy = filterOptionsModel.OrderBy,
                        searchQuery = filterOptionsModel.SearchQuery,
                        pageNumber = filterOptionsModel.PageNumber,
                        pageSize = filterOptionsModel.PageSize
                    });
            }
        }

        #endregion

    }
}
