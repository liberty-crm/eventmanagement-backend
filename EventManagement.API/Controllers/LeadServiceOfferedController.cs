using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
//using EventManagement.API.Helpers;
using EventManagement.Dto;
using EventManagement.Service;
using EventManagement.Domain.Entities;
using EventManagement.Utility;
using Microsoft.Extensions.Logging;
using System.Linq;
using IdentityServer4.AccessTokenValidation;
using EventManagement.Utility.Enums;
using System.Text.RegularExpressions;
using Microsoft.Extensions.Configuration;
using EventManagement.Domain;

namespace EventManagement.API.Controllers
{
    /// <summary>
    /// LeadServiceOffered endpoint
    /// </summary>
    [Route("api/leadserviceoffereds")]
    [Produces("application/json")]
    [ApiController]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]

    public class LeadServiceOfferedController : Controller
    {

        #region PRIVATE MEMBERS

        private readonly ILeadServiceOfferedService _leadserviceofferedService;
        private ILogger<LeadServiceOfferedController> _logger;
        private readonly IUrlHelper _urlHelper;
        private readonly IContactService _contactService;
        private readonly IAspUserService _aspUserService;
        private readonly INotificationMailService _notificationMailService;
        private readonly IServiceOfferedService _serviceOfferedService;
        private readonly IFTCrmService _fTCrmService;
        private readonly IConfiguration _configuration;
        private readonly ITaskService _taskService;
        private readonly IHistoryService _historyService;
        private readonly IUserInfoService _userInfoService;
        private readonly ILeadService _leadService;


        #endregion


        #region CONSTRUCTOR

        public LeadServiceOfferedController(ILeadServiceOfferedService leadserviceofferedService, 
                ILogger<LeadServiceOfferedController> logger, IUrlHelper urlHelper,
                IContactService contactService,IAspUserService aspUserService, INotificationMailService notificationMailService
            , IServiceOfferedService serviceOfferedService, IFTCrmService fTCrmService, IConfiguration configuration, ITaskService taskService, IHistoryService historyService ,IUserInfoService userInfoService, ILeadService leadService) 
        {
            _logger = logger;
            _leadserviceofferedService = leadserviceofferedService;
            _urlHelper = urlHelper;
            _contactService = contactService;
            _aspUserService = aspUserService;
            _notificationMailService = notificationMailService;
            _serviceOfferedService = serviceOfferedService;
            _fTCrmService = fTCrmService;
            _configuration = configuration;
            _taskService = taskService;
            _historyService = historyService;
            _userInfoService = userInfoService;
            _leadService = leadService;
        }

        #endregion


        #region HTTPGET

        [HttpGet(Name = "GetFilteredLeadServiceOffereds")]
        [Produces("application/vnd.tourmanagement.leadserviceoffereds.hateoas+json", "application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<List<LeadServiceOfferedDto>>> GetFilteredLeadServiceOffereds([FromQuery]FilterOptionsModel filterOptionsModel, [FromHeader(Name = "Accept")] string mediaType)
        {

            //if order by fields are not valid.
            if (!_leadserviceofferedService.ValidMappingExists(filterOptionsModel.OrderBy))
            {
                //then return bad request.
                return BadRequest();
            }

            //if fields are not valid.
            if (!EventManagementUtils.TypeHasProperties<LeadServiceOfferedDto>(filterOptionsModel.Fields))
            {
                //then return bad request.
                return BadRequest();
            }

            //get the paged/filtered show from db. 
            var leadserviceofferedsFromRepo = await _leadserviceofferedService.GetFilteredEntities(filterOptionsModel);

            //if HATEOAS links are required.
            if (mediaType == "application/vnd.tourmanagement.leadserviceoffereds.hateoas+json")
            {
                //create HATEOAS links for each show.
                leadserviceofferedsFromRepo.ForEach(leadserviceoffered =>
                {
                    var entityLinks = CreateLinksForLeadServiceOffered(leadserviceoffered.Id, filterOptionsModel.Fields);
                    leadserviceoffered.links = entityLinks;
                });

                //prepare pagination metadata.
                var paginationMetadata = new
                {
                    totalCount = leadserviceofferedsFromRepo.TotalCount,
                    pageSize = leadserviceofferedsFromRepo.PageSize,
                    currentPage = leadserviceofferedsFromRepo.CurrentPage,
                    totalPages = leadserviceofferedsFromRepo.TotalPages,
                };

                //add pagination meta data to response header.
                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMetadata));

                //create links for shows.
                var links = CreateLinksForLeadServiceOffereds(filterOptionsModel, leadserviceofferedsFromRepo.HasNext, leadserviceofferedsFromRepo.HasPrevious);

                //prepare model with data and HATEOAS links.
                var linkedCollectionResource = new
                {
                    value = leadserviceofferedsFromRepo,
                    links = links
                };

                //return the data with Ok response.
                return Ok(linkedCollectionResource);
            }
            else
            {
                var previousPageLink = leadserviceofferedsFromRepo.HasPrevious ?
                    CreateLeadServiceOfferedsResourceUri(filterOptionsModel, ResourceUriType.PreviousPage) : null;

                var nextPageLink = leadserviceofferedsFromRepo.HasNext ?
                    CreateLeadServiceOfferedsResourceUri(filterOptionsModel, ResourceUriType.NextPage) : null;

                //prepare the pagination metadata.
                var paginationMetadata = new
                {
                    previousPageLink = previousPageLink,
                    nextPageLink = nextPageLink,
                    totalCount = leadserviceofferedsFromRepo.TotalCount,
                    pageSize = leadserviceofferedsFromRepo.PageSize,
                    currentPage = leadserviceofferedsFromRepo.CurrentPage,
                    totalPages = leadserviceofferedsFromRepo.TotalPages
                };

                //add pagination meta data to response header.
                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMetadata));
                leadserviceofferedsFromRepo.ForEach(leadserviceoffered =>
                {                    
                    var ftcrm = _fTCrmService.GetAllEntities().Where(x=>x.LeadServiceId.Equals(leadserviceoffered.Id)).FirstOrDefault();
                    if(ftcrm != null)
                    {
                        leadserviceoffered.ftcrm = ftcrm;
                        
                        if (ftcrm.FtCrmUniqueId!=null && Int32.Parse(ftcrm.FtCrmUniqueId) > 0)
                        { 
                            leadserviceoffered.disableTrigger = true; 
                        }
                        else
                        { 
                            leadserviceoffered.disableTrigger = false; 
                        }
                    }
                    
                });
                //return the data with Ok response.
                return Ok(leadserviceofferedsFromRepo);
            }
        }


        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Produces("application/vnd.tourmanagement.leadserviceoffereds.hateoas+json", "application/json")]
        [HttpGet("{id}", Name = "GetLeadServiceOffered")]
        public async Task<ActionResult<LeadServiceOffered>> GetLeadServiceOffered(Guid id, [FromQuery] string fields, [FromHeader(Name = "Accept")] string mediaType)
        {
            object leadserviceofferedEntity = null;
            object linkedResourceToReturn = null;

            //if fields are not passed.
            if (string.IsNullOrEmpty(fields))
            {
                _logger.LogInformation("GetLeadServiceOffered called");

                //then get the whole entity and map it to the Dto.
                leadserviceofferedEntity = Mapper.Map<LeadServiceOfferedDto>(await _leadserviceofferedService.GetEntityByIdAsync(id));
            }
            else
            {
                //if fields are passed then get the partial show.
                leadserviceofferedEntity = await _leadserviceofferedService.GetPartialEntityAsync(id, fields);
            }

            //if leadserviceoffered not found.
            if (leadserviceofferedEntity == null)
            {
                //then return not found response.
                return NotFound();
            }

            if (mediaType == "application/vnd.tourmanagement.leadserviceoffereds.hateoas+json")
            {
                //create HATEOS links
                var links = CreateLinksForLeadServiceOffered(id, fields);

                //if fields are not passed.
                if (string.IsNullOrEmpty(fields))
                {
                    //convert the typed object to expando object.
                    linkedResourceToReturn = ((LeadServiceOfferedDto)leadserviceofferedEntity).ShapeData("") as IDictionary<string, object>;

                    //add the HATEOAS links to the model.
                    ((IDictionary<string, object>)linkedResourceToReturn).Add("links", links);
                }
                else
                {
                    linkedResourceToReturn = leadserviceofferedEntity;

                    //add the HATEOAS links to the model.
                    ((dynamic)linkedResourceToReturn).links = links;

                }
            }
            else
            {
                linkedResourceToReturn = leadserviceofferedEntity;
            }

            //return the Ok response.
            return Ok(linkedResourceToReturn);
        }


        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Produces("application/vnd.tourmanagement.leadserviceoffereds.hateoas+json", "application/json")]
        [HttpGet("GetInvoice", Name = "GetInvoice")]
        public async Task<ActionResult<LeadServiceOffered>> GetInvoice(string leadServiceOfferedId)
        {
            try
            {
                return Ok(await _leadserviceofferedService.GetInvoice(leadServiceOfferedId));
            }
            catch(Exception ex)
            {
                return BadRequest(ex);
            }
        }

        #endregion


        #region HTTPPUT

        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPut("{id}", Name = "UpdateLeadServiceOffered")]
        public async Task<IActionResult> UpdateLeadServiceOffered(Guid id, [FromBody]LeadServiceOfferedForUpdate LeadServiceOfferedForUpdate)
        {

            //if show not found
            if (!await _leadserviceofferedService.ExistAsync(x => x.Id == id))
            {
                //then return not found response.
                return NotFound();
            }
            var oldLeadService = _leadserviceofferedService.GetEntityById(id);

            //Update an entity.
            await _leadserviceofferedService.UpdateEntityAsync(id, LeadServiceOfferedForUpdate);

            if (oldLeadService.AssignedTo != LeadServiceOfferedForUpdate.AssignedTo)
            {
                var assignedToUser = _aspUserService.GetEntityById(LeadServiceOfferedForUpdate.AssignedTo);
                var service = _serviceOfferedService.GetEntityById(oldLeadService.ServiceOfferedId);
                var contact = _contactService.GetEntityById(oldLeadService.ContactId);
                var leadNumber = await _leadService.GetPartialEntityAsync(oldLeadService.LeadId, "LeadNumber");
                var htmlContent = _notificationMailService.GetAllEntities().Where(x => x.Name.Equals(NotificationMailEnum.ServiceAssigned.ToString())).FirstOrDefault().MailContent;
                htmlContent = Regex.Replace(htmlContent, "{%service.assignedTo%}", assignedToUser.FName ?? "", RegexOptions.IgnoreCase);
                htmlContent = Regex.Replace(htmlContent, "{%service.Name%}", service.Name ?? "", RegexOptions.IgnoreCase);
                htmlContent = Regex.Replace(htmlContent, "{%contact.FirstName%}", contact.FirstName ?? "", RegexOptions.IgnoreCase);
                htmlContent = Regex.Replace(htmlContent, "{%contact.LastName%}", contact.LastName ?? "", RegexOptions.IgnoreCase);
                htmlContent = Regex.Replace(htmlContent, "{%lead.Id%}", "Lead Id:" + leadNumber.LeadNumber.ToString() ?? "", RegexOptions.IgnoreCase);
                htmlContent = Regex.Replace(htmlContent, "{%lead.Link%}", _configuration.GetSection("Frontend").Value + "lead/edit/" + id.ToString() ?? "", RegexOptions.IgnoreCase);
                var subject = "A new " + service.Name + " request for "+ contact.FirstName +" "+ contact.LastName +" - Lead Id: " + leadNumber.LeadNumber.ToString() + " has been assigned to you";
                _notificationMailService.SendNotificationMail(assignedToUser.Email, htmlContent, subject);


                //Create Task
                var leadTask = new TaskForCreation()
                {
                    LeadId = LeadServiceOfferedForUpdate.LeadId,
                    Title = "Service " + service.Name +" of Lead "+ contact.FirstName + " " + contact.LastName + " has been assigned to you.",
                    Description = "Service has been assigned to you.",
                    Date = DateTime.Now,
                    UserId = assignedToUser.Id,
                    IsCompleted = false,
                    IsImportant = true,
                    IsMarked = false,
                    ServiceId = id.ToString()                    
                };
                var taskToReturn = await _taskService.CreateEntityAsync<TaskDto, TaskForCreation>(leadTask);
         
            }
            var historyForCreation = new HistoryForCreation()
            {
                LeadId = LeadServiceOfferedForUpdate.LeadId,
                ServiceId = id.ToString(),
                Activity = 4,
                UserId = _userInfoService.UserId

            };

            var history = await _historyService.CreateEntityAsync<HistoryDto, HistoryForCreation>(historyForCreation);

            //return the response.
            return NoContent();
        }

        #endregion


        #region HTTPPATCH

        [Consumes("application/json-patch+json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPatch("{id}")]
        public async Task<IActionResult> PartiallyUpdateLeadServiceOffered(Guid id, [FromBody] JsonPatchDocument<LeadServiceOfferedForUpdate> jsonPatchDocument)
        {
            LeadServiceOfferedForUpdate dto = new LeadServiceOfferedForUpdate();
            LeadServiceOffered leadserviceoffered = new LeadServiceOffered();
            var fields = "AssignedTo,ServiceOfferedId,ContactId,LeadId";
            var oldLeadService = await _leadserviceofferedService.GetPartialEntityAsync(id, fields);
            //apply the patch changes to the dto. 
            jsonPatchDocument.ApplyTo(dto, ModelState);

            //if the jsonPatchDocument is not valid.
            if (!ModelState.IsValid)
            {
                //then return unprocessableEntity response.
                return new UnprocessableEntityObjectResult(ModelState);
            }

            //if the dto model is not valid after applying changes.
            if (!TryValidateModel(dto))
            {
                //then return unprocessableEntity response.
                return new UnprocessableEntityObjectResult(ModelState);
            }

            //map the chnages from dto to entity.
            Mapper.Map(dto, leadserviceoffered);

            //set the Id for the show model.
            leadserviceoffered.Id = id;

            //partially update the chnages to the db. 
            try
            {
                var result =await _leadserviceofferedService.UpdatePartialEntityAsync(leadserviceoffered, jsonPatchDocument);
            }
            catch (Exception ex)
            {
                var error = ex.Message;
            }
            if (oldLeadService.AssignedTo != dto.AssignedTo)
            {
                var assignedToUser = _aspUserService.GetEntityById(dto.AssignedTo);
                var service = _serviceOfferedService.GetEntityById(oldLeadService.ServiceOfferedId);
                var contact = _contactService.GetEntityById(oldLeadService.ContactId);                
                var leadNumber = await _leadService.GetPartialEntityAsync(oldLeadService.LeadId, "LeadNumber");
                var htmlContent = _notificationMailService.GetAllEntities().Where(x => x.Name.Equals(NotificationMailEnum.ServiceAssigned.ToString())).FirstOrDefault().MailContent;
                htmlContent = Regex.Replace(htmlContent, "{%service.assignedTo%}", assignedToUser.FName ?? "", RegexOptions.IgnoreCase);
                htmlContent = Regex.Replace(htmlContent, "{%service.Name%}", service.Name ?? "", RegexOptions.IgnoreCase);
                htmlContent = Regex.Replace(htmlContent, "{%contact.FirstName%}", contact.FirstName ?? "", RegexOptions.IgnoreCase);
                htmlContent = Regex.Replace(htmlContent, "{%contact.LastName%}", contact.LastName ?? "", RegexOptions.IgnoreCase);
                htmlContent = Regex.Replace(htmlContent, "{%lead.Id%}", "Lead Id:" + leadNumber.LeadNumber.ToString() ?? "", RegexOptions.IgnoreCase);
                htmlContent = Regex.Replace(htmlContent, "{%lead.Link%}", _configuration.GetSection("Frontend").Value + "lead/edit/" + oldLeadService.LeadId.ToString() ?? "", RegexOptions.IgnoreCase);
                var subject = "A new " + service.Name + " request for " + contact.FirstName + " " + contact.LastName + " - Lead Id: " + leadNumber.LeadNumber + " has been assigned to you";
                _notificationMailService.SendNotificationMail(assignedToUser.Email, htmlContent, subject);


                var serviceTask = new TaskForCreation()
                {
                    Id = Guid.NewGuid().ToString(),
                    LeadId = oldLeadService.LeadId.ToString(),
                    Title = "Service " + service.Name + " of Lead " + contact.FirstName + " " + contact.LastName + " has been assigned to you.",
                    Description = "Service has been assigned to you.",
                    Date = DateTime.Now,
                    UserId = assignedToUser.Id,
                    IsCompleted = false,
                    IsImportant = true,
                    IsMarked = false,
                    ServiceId = leadserviceoffered.Id.ToString()
                };
                var result = await _taskService.CreateEntityAsync<TaskDto, TaskForCreation>(serviceTask);
            }            

            //return the response.
            return NoContent();
        }

        #endregion


        #region HTTPPOST

        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPost(Name = "CreateLeadServiceOffered")]
        public async Task<ActionResult<LeadServiceOfferedDto>> CreateLeadServiceOffered([FromBody]LeadServiceOfferedForCreation leadserviceoffered)
        {
            try
            {
                leadserviceoffered.Id = Guid.NewGuid().ToString();

                leadserviceoffered.ContactId = this._contactService.GetAllEntities().Where(x => x.RelatedLeadContactId == Guid.Parse(leadserviceoffered.LeadId)
                                                                            && x.RelationWithLead.ToLower() == "self")
                                                                            .Select(x => x.Id).FirstOrDefault().ToString();

                //create a show in db.
                var leadserviceofferedToReturn = await _leadserviceofferedService.CreateEntityAsync<LeadServiceOfferedDto, LeadServiceOfferedForCreation>(leadserviceoffered);

                var historyForCreation = new HistoryForCreation()
                {
                    LeadId = leadserviceofferedToReturn.LeadId.ToString(),
                    ServiceId = leadserviceofferedToReturn.Id.ToString(),
                    Activity = 3,
                    UserId = _userInfoService.UserId ?? _userInfoService.UserId,
                };

                var history = await _historyService.CreateEntityAsync<HistoryDto, HistoryForCreation>(historyForCreation);

                //return the show created response.
                return CreatedAtRoute("GetLeadServiceOffered", new { id = leadserviceofferedToReturn.Id }, leadserviceofferedToReturn);
            }
            catch(Exception ex)
            {
                return BadRequest(ex);
            }
            

        }

        #endregion


        #region HTTPDELETE
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}", Name = "DeleteLeadServiceOfferedById")]
        public async Task<IActionResult> DeleteLeadServiceOfferedById(string id)
        {
            //if the leadserviceoffered exists
            if (await _leadserviceofferedService.ExistAsync(x => x.Id == Guid.Parse(id)))
            {
                //delete the leadserviceoffered from the db.
                await _leadserviceofferedService.DeleteEntityAsync(Guid.Parse(id));
            }
            else
            {
                //if leadserviceoffered doesn't exists then returns not found.
                return NotFound();
            }

            //return the response.
            return NoContent();
        }

        #endregion


        #region PRIVATE METHODS

        private IEnumerable<LinkDto> CreateLinksForLeadServiceOffered(Guid id, string fields)
        {
            var links = new List<LinkDto>();

            if (string.IsNullOrWhiteSpace(fields))
            {
                links.Add(
                  new LinkDto(_urlHelper.Link("GetLeadServiceOffered", new { id = id }),
                  "self",
                  "GET"));
            }
            else
            {
                links.Add(
                  new LinkDto(_urlHelper.Link("GetLeadServiceOffered", new { id = id, fields = fields }),
                  "self",
                  "GET"));
            }

            links.Add(
              new LinkDto(_urlHelper.Link("DeleteLeadServiceOfferedById", new { id = id }),
              "delete_leadserviceoffered",
              "DELETE"));

            links.Add(
             new LinkDto(_urlHelper.Link("UpdateLeadServiceOffered", new { id = id }),
             "update_leadserviceoffered",
             "PUT"));

            links.Add(
              new LinkDto(_urlHelper.Link("CreateLeadServiceOffered", new { }),
              "create_leadserviceoffered",
              "POST"));

            //links.Add(
            //   new LinkDto(_urlHelper.Link("GetShowsForTour", new { }),
            //   "shows",
            //   "GET"));

            return links;
        }

        private IEnumerable<LinkDto> CreateLinksForLeadServiceOffereds(FilterOptionsModel filterOptionsModel, bool hasNext, bool hasPrevious)
        {
            var links = new List<LinkDto>();

            links.Add(
               new LinkDto(CreateLeadServiceOfferedsResourceUri(filterOptionsModel, ResourceUriType.Current), "self", "GET"));

            if (hasNext)
            {
                links.Add(
                  new LinkDto(CreateLeadServiceOfferedsResourceUri(filterOptionsModel, ResourceUriType.NextPage), "nextPage", "GET"));
            }

            if (hasPrevious)
            {
                links.Add(new LinkDto(CreateLeadServiceOfferedsResourceUri(filterOptionsModel, ResourceUriType.PreviousPage), "previousPage", "GET"));
            }

            return links;
        }

        private string CreateLeadServiceOfferedsResourceUri(FilterOptionsModel filterOptionsModel, ResourceUriType type)
        {
            switch (type)
            {
                case ResourceUriType.PreviousPage:
                    return _urlHelper.Link("GetFilteredLeadServiceOffereds",
                      new
                      {
                          fields = filterOptionsModel.Fields,
                          orderBy = filterOptionsModel.OrderBy,
                          searchQuery = filterOptionsModel.SearchQuery,
                          pageNumber = filterOptionsModel.PageNumber - 1,
                          pageSize = filterOptionsModel.PageSize
                      });
                case ResourceUriType.NextPage:
                    return _urlHelper.Link("GetFilteredLeadServiceOffereds",
                      new
                      {
                          fields = filterOptionsModel.Fields,
                          orderBy = filterOptionsModel.OrderBy,
                          searchQuery = filterOptionsModel.SearchQuery,
                          pageNumber = filterOptionsModel.PageNumber + 1,
                          pageSize = filterOptionsModel.PageSize
                      });

                default:
                    return _urlHelper.Link("GetFilteredLeadServiceOffereds",
                    new
                    {
                        fields = filterOptionsModel.Fields,
                        orderBy = filterOptionsModel.OrderBy,
                        searchQuery = filterOptionsModel.SearchQuery,
                        pageNumber = filterOptionsModel.PageNumber,
                        pageSize = filterOptionsModel.PageSize
                    });
            }
        }

        #endregion

    }
}
