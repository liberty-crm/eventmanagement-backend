using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
//using EventManagement.API.Helpers;
using EventManagement.Dto;
using EventManagement.Service;
using EventManagement.Domain.Entities;
using EventManagement.Utility;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.JsonPatch.Operations;
using System.Linq;
using EventManagement.Domain;
using System.Security.Cryptography.X509Certificates;
using EventManagement.Utility.Enums;
using System.Text.RegularExpressions;
using System.Runtime.CompilerServices;
using System.Net.Http;
using Polly;
using EventManagement.Dto.FTCrm;

namespace EventManagement.API.Controllers
{
    /// <summary>
    /// Payment endpoint
    /// </summary>
    [Route("api/payments")]
    [Produces("application/json")]
    [ApiController]
    public class PaymentController : Controller
    {

        #region PRIVATE MEMBERS

        private readonly IPaymentService _paymentService;
        private ILogger<PaymentController> _logger;
        private readonly IUrlHelper _urlHelper;
        private ILeadServiceOfferedRepository _leadServiceOfferedRepository;
        private IFTCrmService _fTCrmService;
        private INotificationMailService _notificationMailService;
        private ILeadServiceOfferedService _leadServiceOfferedService;
        private IAspUserService _aspUserService;
        private IHistoryService _historyService;

        #endregion


        #region CONSTRUCTOR

        public PaymentController(IPaymentService paymentService, ILogger<PaymentController> logger, IUrlHelper urlHelper, ILeadServiceOfferedRepository leadServiceOfferedRepository, IFTCrmService fTCrmService, INotificationMailService notificationMailService, ILeadServiceOfferedService leadServiceOfferedService, IAspUserService aspUserService, IHistoryService historyService) 
        {
            _logger = logger;
            _paymentService = paymentService;
            _urlHelper = urlHelper;
            _leadServiceOfferedRepository = leadServiceOfferedRepository;
            _fTCrmService = fTCrmService;
            _notificationMailService = notificationMailService;
            _leadServiceOfferedService = leadServiceOfferedService;
            _aspUserService = aspUserService;
            _historyService = historyService;
        }

        #endregion


        #region HTTPGET

        [HttpGet(Name = "GetFilteredPayments")]
        [Produces("application/vnd.tourmanagement.payments.hateoas+json", "application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<List<PaymentDto>>> GetFilteredPayments([FromQuery]FilterOptionsModel filterOptionsModel, [FromHeader(Name = "Accept")] string mediaType)
        {

            //if order by fields are not valid.
            if (!_paymentService.ValidMappingExists(filterOptionsModel.OrderBy))
            {
                //then return bad request.
                return BadRequest();
            }

            //if fields are not valid.
            if (!EventManagementUtils.TypeHasProperties<PaymentDto>(filterOptionsModel.Fields))
            {
                //then return bad request.
                return BadRequest();
            }
                //get the paged/filtered show from db. 
                var paymentsFromRepo = await _paymentService.GetFilteredEntities(filterOptionsModel);

                //if HATEOAS links are required.
                if (mediaType == "application/vnd.tourmanagement.payments.hateoas+json")
                {
                    //create HATEOAS links for each show.
                    paymentsFromRepo.ForEach(payment =>
                    {
                        var entityLinks = CreateLinksForPayment(payment.Id, filterOptionsModel.Fields);
                        payment.links = entityLinks;
                    });

                    //prepare pagination metadata.
                    var paginationMetadata = new
                    {
                        totalCount = paymentsFromRepo.TotalCount,
                        pageSize = paymentsFromRepo.PageSize,
                        currentPage = paymentsFromRepo.CurrentPage,
                        totalPages = paymentsFromRepo.TotalPages,
                    };

                    //add pagination meta data to response header.
                    Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMetadata));

                    //create links for shows.
                    var links = CreateLinksForPayments(filterOptionsModel, paymentsFromRepo.HasNext, paymentsFromRepo.HasPrevious);

                    //prepare model with data and HATEOAS links.
                    var linkedCollectionResource = new
                    {
                        value = paymentsFromRepo,
                        links = links
                    };

                    //return the data with Ok response.
                    return Ok(linkedCollectionResource);
                }
                else
                {
                    var previousPageLink = paymentsFromRepo.HasPrevious ?
                        CreatePaymentsResourceUri(filterOptionsModel, ResourceUriType.PreviousPage) : null;

                    var nextPageLink = paymentsFromRepo.HasNext ?
                        CreatePaymentsResourceUri(filterOptionsModel, ResourceUriType.NextPage) : null;

                    //prepare the pagination metadata.
                    var paginationMetadata = new
                    {
                        previousPageLink = previousPageLink,
                        nextPageLink = nextPageLink,
                        totalCount = paymentsFromRepo.TotalCount,
                        pageSize = paymentsFromRepo.PageSize,
                        currentPage = paymentsFromRepo.CurrentPage,
                        totalPages = paymentsFromRepo.TotalPages
                    };

                    //add pagination meta data to response header.
                    Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMetadata));

                    //return the data with Ok response.
                    return Ok(paymentsFromRepo);
                }
            
        }


        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Produces("application/vnd.tourmanagement.payments.hateoas+json", "application/json")]
        [HttpGet("{id}", Name = "GetPayment")]
        public async Task<ActionResult<Payment>> GetPayment(Guid id, [FromQuery] string fields, [FromHeader(Name = "Accept")] string mediaType)
        {
            object paymentEntity = null;
            object linkedResourceToReturn = null;

            //if fields are not passed.
            if (string.IsNullOrEmpty(fields))
            {
                _logger.LogInformation("GetPayment called");

                //then get the whole entity and map it to the Dto.
                paymentEntity = Mapper.Map<PaymentDto>(await _paymentService.GetEntityByIdAsync(id));
            }
            else
            {
                //if fields are passed then get the partial show.
                paymentEntity = await _paymentService.GetPartialEntityAsync(id, fields);
            }

            //if payment not found.
            if (paymentEntity == null)
            {
                //then return not found response.
                return NotFound();
            }

            if (mediaType == "application/vnd.tourmanagement.payments.hateoas+json")
            {
                //create HATEOS links
                var links = CreateLinksForPayment(id, fields);

                //if fields are not passed.
                if (string.IsNullOrEmpty(fields))
                {
                    //convert the typed object to expando object.
                    linkedResourceToReturn = ((PaymentDto)paymentEntity).ShapeData("") as IDictionary<string, object>;

                    //add the HATEOAS links to the model.
                    ((IDictionary<string, object>)linkedResourceToReturn).Add("links", links);
                }
                else
                {
                    linkedResourceToReturn = paymentEntity;

                    //add the HATEOAS links to the model.
                    ((dynamic)linkedResourceToReturn).links = links;

                }
            }
            else
            {
                linkedResourceToReturn = paymentEntity;
            }

            //return the Ok response.
            return Ok(linkedResourceToReturn);
        }

        #endregion


        #region HTTPPUT

        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPut("{id}", Name = "UpdatePayment")]
        public async Task<IActionResult> UpdatePayment(Guid id, [FromBody]PaymentForUpdate PaymentForUpdate)
        {

            try
            {
                //if show not found
                if (!await _paymentService.ExistAsync(x => x.Id == id))
                {
                    //then return not found response.
                    return NotFound();
                }

                //Update an entity.
                await _paymentService.UpdateEntityAsync(id, PaymentForUpdate);

                //return the response.
                return NoContent();
            }
            catch(Exception ex)
            {
                return BadRequest(ex);
            }
            
        }

        #endregion


        #region HTTPPATCH

        [Consumes("application/json-patch+json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPatch("{id}")]
        public async Task<IActionResult> PartiallyUpdatePayment(Guid id, [FromBody] JsonPatchDocument<PaymentForUpdate> jsonPatchDocument)
        {
            PaymentForUpdate dto = new PaymentForUpdate();
            Payment payment = new Payment();

            //apply the patch changes to the dto. 
            jsonPatchDocument.ApplyTo(dto, ModelState);

            //if the jsonPatchDocument is not valid.
            if (!ModelState.IsValid)
            {
                //then return unprocessableEntity response.
                return new UnprocessableEntityObjectResult(ModelState);
            }

            //if the dto model is not valid after applying changes.
            if (!TryValidateModel(dto))
            {
                //then return unprocessableEntity response.
                return new UnprocessableEntityObjectResult(ModelState);
            }

            //map the chnages from dto to entity.
            Mapper.Map(dto, payment);

            //set the Id for the show model.
            payment.Id = id;

            //partially update the chnages to the db. 
            await _paymentService.UpdatePartialEntityAsync(payment, jsonPatchDocument);

            //return the response.
            return NoContent();
        }

        #endregion


        #region HTTPPOST

        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPost(Name = "CreatePayment")]
        public async Task<ActionResult<PaymentDto>> CreatePayment([FromBody] PaymentForCreation payment)
        {
            //create a show in db.
            var paymentToReturn = await _paymentService.CreateEntityAsync<PaymentDto, PaymentForCreation>(payment);

            SendNotificationMail(Guid.Parse(payment.LeadServiceId));

            var payments = _paymentService.GetAllEntities().Where(x => x.LeadServiceId == Guid.Parse(payment.LeadServiceId)).Select(x => x.Amount);

            decimal total = payments.Sum();

            var ftcrm = _fTCrmService.GetAllEntities().Where(x => x.LeadServiceId == Guid.Parse(payment.LeadServiceId)).FirstOrDefault();

            if (total >= (decimal)(ftcrm.Amount * 0.33))

            {
                TriggerFtCrmApiAsync(ftcrm.FtCrmUniqueId, Guid.Parse(payment.LeadServiceId));
                UpdateLeadServiceOfferedStatus(ftcrm.FtCrmUniqueId,Guid.Parse(payment.LeadServiceId));
                
            }
            
            //return the show created response.
            return CreatedAtRoute("GetPayment", new { id = paymentToReturn.Id }, paymentToReturn);
        }

        #endregion


        #region HTTPDELETE
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}", Name = "DeletePaymentById")]
        public async Task<IActionResult> DeletePaymentById(Guid id)
        {
            //if the payment exists
            if (await _paymentService.ExistAsync(x => x.Id == id))
            {
                //delete the payment from the db.
                await _paymentService.DeleteEntityAsync(id);
            }
            else
            {
                //if payment doesn't exists then returns not found.
                return NotFound();
            }

            //return the response.
            return NoContent();
        }

        #endregion


        #region PRIVATE METHODS

        private IEnumerable<LinkDto> CreateLinksForPayment(Guid id, string fields)
        {
            var links = new List<LinkDto>();

            if (string.IsNullOrWhiteSpace(fields))
            {
                links.Add(
                  new LinkDto(_urlHelper.Link("GetPayment", new { id = id }),
                  "self",
                  "GET"));
            }
            else
            {
                links.Add(
                  new LinkDto(_urlHelper.Link("GetPayment", new { id = id, fields = fields }),
                  "self",
                  "GET"));
            }

            links.Add(
              new LinkDto(_urlHelper.Link("DeletePaymentById", new { id = id }),
              "delete_payment",
              "DELETE"));

            links.Add(
             new LinkDto(_urlHelper.Link("UpdatePayment", new { id = id }),
             "update_payment",
             "PUT"));

            links.Add(
              new LinkDto(_urlHelper.Link("CreatePayment", new { }),
              "create_payment",
              "POST"));

            //links.Add(
            //   new LinkDto(_urlHelper.Link("GetShowsForTour", new { }),
            //   "shows",
            //   "GET"));

            return links;
        }

        private IEnumerable<LinkDto> CreateLinksForPayments(FilterOptionsModel filterOptionsModel, bool hasNext, bool hasPrevious)
        {
            var links = new List<LinkDto>();

            links.Add(
               new LinkDto(CreatePaymentsResourceUri(filterOptionsModel, ResourceUriType.Current), "self", "GET"));

            if (hasNext)
            {
                links.Add(
                  new LinkDto(CreatePaymentsResourceUri(filterOptionsModel, ResourceUriType.NextPage), "nextPage", "GET"));
            }

            if (hasPrevious)
            {
                links.Add(new LinkDto(CreatePaymentsResourceUri(filterOptionsModel, ResourceUriType.PreviousPage), "previousPage", "GET"));
            }

            return links;
        }

        private string CreatePaymentsResourceUri(FilterOptionsModel filterOptionsModel, ResourceUriType type)
        {
            switch (type)
            {
                case ResourceUriType.PreviousPage:
                    return _urlHelper.Link("GetFilteredPayments",
                      new
                      {
                          fields = filterOptionsModel.Fields,
                          orderBy = filterOptionsModel.OrderBy,
                          searchQuery = filterOptionsModel.SearchQuery,
                          pageNumber = filterOptionsModel.PageNumber - 1,
                          pageSize = filterOptionsModel.PageSize
                      });
                case ResourceUriType.NextPage:
                    return _urlHelper.Link("GetFilteredPayments",
                      new
                      {
                          fields = filterOptionsModel.Fields,
                          orderBy = filterOptionsModel.OrderBy,
                          searchQuery = filterOptionsModel.SearchQuery,
                          pageNumber = filterOptionsModel.PageNumber + 1,
                          pageSize = filterOptionsModel.PageSize
                      });

                default:
                    return _urlHelper.Link("GetFilteredPayments",
                    new
                    {
                        fields = filterOptionsModel.Fields,
                        orderBy = filterOptionsModel.OrderBy,
                        searchQuery = filterOptionsModel.SearchQuery,
                        pageNumber = filterOptionsModel.PageNumber,
                        pageSize = filterOptionsModel.PageSize
                    });
            }
        }

        private async Task<int> UpdateLeadServiceOfferedStatus(string ftCrmUniqueId,Guid leadserviceId)
        {
            var result = 0;
            try
            {
                var leadServiceOffered = _leadServiceOfferedRepository.GetAllEntities().Where(x => x.Id == leadserviceId).FirstOrDefault();
                var patchLeadService = new JsonPatchDocument<LeadServiceOffered>();
                patchLeadService.Operations.Add(new Operation<LeadServiceOffered> { op = "replace", path = "/AssignedStatus", value = "Payment Complete" });
                patchLeadService.ApplyTo(leadServiceOffered);

                _leadServiceOfferedRepository.UpdatePartialEntityAsync(leadServiceOffered, patchLeadService);                
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return result;
        }

        private void SendNotificationMail(Guid leadServiceId)
        {
            var filterModelOption = new FilterOptionsModel();
            filterModelOption.SearchQuery = "Id==\"" + leadServiceId + "\"";           
            var leadServiceOffered = _leadServiceOfferedService.GetFilteredEntities(filterModelOption).Result.FirstOrDefault();

            var relationshipManager = _aspUserService.GetEntityById(leadServiceOffered.Lead.AssignedTo);

            var htmlContent = _notificationMailService.GetAllEntities().Where(x => x.Name.Equals(NotificationMailEnum.PaymentReceivedConfirmation.ToString())).FirstOrDefault().MailContent;
            htmlContent = Regex.Replace(htmlContent, "{%contact.FirstName%}", leadServiceOffered.Contact.FirstName ?? "", RegexOptions.IgnoreCase);
            htmlContent = Regex.Replace(htmlContent, "{%service.Name%}", leadServiceOffered.ServiceOffered.Name ?? "", RegexOptions.IgnoreCase);
            htmlContent = Regex.Replace(htmlContent, "{%lead.assignedTo%}", relationshipManager.FName ?? "", RegexOptions.IgnoreCase);            
            var subject = "Payment Received";
            _notificationMailService.SendNotificationMail(leadServiceOffered.Contact.Email, htmlContent, subject, leadServiceOffered.Lead.Id.ToString(), leadServiceOffered.Id.ToString(),"Payment received notification sent to customer and sent update to Family Tax system.");
            
        }

        private async System.Threading.Tasks.Task TriggerFtCrmApiAsync(string ftCrmUniqueId, Guid leadServiceId)
        {
            var filterModelOption = new FilterOptionsModel();
            filterModelOption.SearchQuery = "Id==\"" + leadServiceId + "\"";
            var leadServiceOffered = _leadServiceOfferedService.GetFilteredEntities(filterModelOption).Result.FirstOrDefault();            
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Add("Auth-Key", "8D65A4647FED05E4C13DF30B5E8235DC");
            var formContent = new FormUrlEncodedContent(new[]
            {
                    new KeyValuePair<string, string>("opp_id", ftCrmUniqueId)

                });
            var pauseBetweenFailures = TimeSpan.FromSeconds(2);

            var retryPolicy = Policy
                .Handle<HttpRequestException>()
                .WaitAndRetryAsync(3, retryAttempt =>
                    TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)));

            var ftcrmResponse = new FtCrmResponse();
            await retryPolicy.ExecuteAsync(async () =>
            {
                var uri = "https://fintaxcrm.com/fapi/movetopaid";
                var response = await httpClient
                  .PostAsync(uri, formContent);
                var data = await response.Content.ReadAsStringAsync();
                ftcrmResponse = JsonConvert.DeserializeObject<FtCrmResponse>(data);
                response.EnsureSuccessStatusCode();
            });            
        }

        #endregion

    }
}
