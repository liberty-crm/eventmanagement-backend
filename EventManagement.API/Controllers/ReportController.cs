using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
//using EventManagement.API.Helpers;
using EventManagement.Dto;
using EventManagement.Service;
using EventManagement.Domain.Entities;
using EventManagement.Utility;
using Microsoft.Extensions.Logging;
using IdentityServer4.AccessTokenValidation;
using System.Linq;
using EventManagement.Dto.Report;
using EventManagement.Domain;

namespace EventManagement.API.Controllers
{
    /// <summary>
    /// Report endpoint
    /// </summary>
    [Route("api/reports")]
    [Produces("application/json")]
    [ApiController]
    //[Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]

    public class ReportController : Controller
    {

        #region PRIVATE MEMBERS

        private readonly IReportService _reportService;
        private ILogger<ReportController> _logger;
        private readonly IUrlHelper _urlHelper;
        private readonly IFTCrmService _fTCrmService;
        private readonly IServiceOfferedInvoiceService _serviceOfferedInvoiceService;
        private readonly ILeadServiceOfferedService _leadServiceOfferedService;
        private readonly IProvinceService _provinceService;
        private readonly IAspUserService _aspUserService;
        private readonly IUserInfoService _userInfoService;
        private readonly ILeadService _leadService;
        private readonly ILeadServiceOfferedService _leadserviceofferedService;

        #endregion


        #region CONSTRUCTOR

        public ReportController(IReportService reportService, ILogger<ReportController> logger, IUrlHelper urlHelper, IFTCrmService fTCrmService, IServiceOfferedInvoiceService serviceOfferedInvoiceService,ILeadServiceOfferedService leadServiceOfferedService, IProvinceService provinceService, IAspUserService aspUserService, IUserInfoService userInfoService, ILeadService leadService, ILeadServiceOfferedService leadserviceofferedService) 
        {
            _logger = logger;
            _reportService = reportService;
            _urlHelper = urlHelper;
            _fTCrmService = fTCrmService;
            _serviceOfferedInvoiceService = serviceOfferedInvoiceService;
            _leadServiceOfferedService = leadServiceOfferedService;
            _provinceService = provinceService;
            _aspUserService = aspUserService;
            _userInfoService = userInfoService;
            _leadService = leadService;
            _leadserviceofferedService = leadserviceofferedService;
        }

        #endregion


        #region HTTPGET

        //Lead
        [HttpGet("Lead", Name = "GetFilteredLead")]
        [Produces("application/vnd.tourmanagement.leads.hateoas+json", "application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<List<LeadDto>>> GetFilteredLead([FromQuery] FilterOptionsModel filterOptionsModel, [FromHeader(Name = "Accept")] string mediaType)
        {
            try
            {
                var loggedinUserId = _userInfoService.UserId;
                var loggedinUserRoles = _userInfoService.Role;

                //if order by fields are not valid.
                if (!_leadService.ValidMappingExists(filterOptionsModel.OrderBy))
                {
                    //then return bad request.
                    return BadRequest();
                }

                //if fields are not valid.
                if (!EventManagementUtils.TypeHasProperties<LeadDto>(filterOptionsModel.Fields))
                {
                    //then return bad request.
                    return BadRequest();
                }

                //get the paged/filtered show from db. 
                var leadsFromRepo = await _leadService.GetFilteredEntities(filterOptionsModel);
                var leadServiceFilter = new FilterOptionsModel();
                leadServiceFilter.PageSize = 20;
                leadServiceFilter.SearchQuery = "AssignedTo==\"" + loggedinUserId + "\"";
                var leadServices = _leadServiceOfferedService.GetFilteredEntities(leadServiceFilter).Result;
                foreach (var leadService in leadServices)
                {
                    var leadFilterOption = new FilterOptionsModel();
                    leadFilterOption.SearchQuery = "Id==\"" + leadService.Lead.Id + "\"";
                    var leads = _leadService.GetFilteredEntities(leadFilterOption).Result.FirstOrDefault();
                    if (!leadsFromRepo.Any(x => x.Id == leads.Id))
                    {
                        leadsFromRepo.Add(leads);
                    }
                }

                //if HATEOAS links are required.
                if (mediaType == "application/vnd.tourmanagement.leads.hateoas+json")
                {
                    //create HATEOAS links for each show.
                    leadsFromRepo.ForEach(lead =>
                    {
                        var entityLinks = CreateLinksForReport(lead.Id, filterOptionsModel.Fields);
                        lead.links = entityLinks;
                    });

                    //prepare pagination metadata.
                    var paginationMetadata = new
                    {
                        totalCount = leadsFromRepo.TotalCount,
                        pageSize = leadsFromRepo.PageSize,
                        currentPage = leadsFromRepo.CurrentPage,
                        totalPages = leadsFromRepo.TotalPages,
                    };

                    //add pagination meta data to response header.
                    Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMetadata));

                    //create links for shows.
                    var links = CreateLinksForReports(filterOptionsModel, leadsFromRepo.HasNext, leadsFromRepo.HasPrevious);

                    //prepare model with data and HATEOAS links.
                    var linkedCollectionResource = new
                    {
                        value = leadsFromRepo,
                        links = links
                    };

                    //return the data with Ok response.
                    return Ok(linkedCollectionResource);
                }
                else
                {
                    var previousPageLink = leadsFromRepo.HasPrevious ?
                        CreateLeadsResourceUri(filterOptionsModel, ResourceUriType.PreviousPage) : null;

                    var nextPageLink = leadsFromRepo.HasNext ?
                        CreateLeadsResourceUri(filterOptionsModel, ResourceUriType.NextPage) : null;

                    //prepare the pagination metadata.
                    var paginationMetadata = new
                    {
                        previousPageLink = previousPageLink,
                        nextPageLink = nextPageLink,
                        totalCount = leadsFromRepo.TotalCount,
                        pageSize = leadsFromRepo.PageSize,
                        currentPage = leadsFromRepo.CurrentPage,
                        totalPages = leadsFromRepo.TotalPages
                    };

                    //add pagination meta data to response header.
                    Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMetadata));

                    //return the data with Ok response.
                    return Ok(leadsFromRepo);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        //Lead Serviceoffered
        [HttpGet("LeadServiceOffered", Name = "GetFilteredLeadServiceOffered")]
        [Produces("application/vnd.tourmanagement.leadserviceoffereds.hateoas+json", "application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<List<LeadServiceOfferedDto>>> GetFilteredLeadServiceOffered([FromQuery] FilterOptionsModel filterOptionsModel, [FromHeader(Name = "Accept")] string mediaType)
        {

            //if order by fields are not valid.
            if (!_leadserviceofferedService.ValidMappingExists(filterOptionsModel.OrderBy))
            {
                //then return bad request.
                return BadRequest();
            }

            //if fields are not valid.
            if (!EventManagementUtils.TypeHasProperties<LeadServiceOfferedDto>(filterOptionsModel.Fields))
            {
                //then return bad request.
                return BadRequest();
            }

            //get the paged/filtered show from db. 
            var leadserviceofferedsFromRepo = await _leadserviceofferedService.GetFilteredEntities(filterOptionsModel);

            //if HATEOAS links are required.
            if (mediaType == "application/vnd.tourmanagement.leadserviceoffereds.hateoas+json")
            {
                //create HATEOAS links for each show.
                leadserviceofferedsFromRepo.ForEach(leadserviceoffered =>
                {
                    var entityLinks = CreateLinksForReport(leadserviceoffered.Id, filterOptionsModel.Fields);
                    leadserviceoffered.links = entityLinks;
                });

                //prepare pagination metadata.
                var paginationMetadata = new
                {
                    totalCount = leadserviceofferedsFromRepo.TotalCount,
                    pageSize = leadserviceofferedsFromRepo.PageSize,
                    currentPage = leadserviceofferedsFromRepo.CurrentPage,
                    totalPages = leadserviceofferedsFromRepo.TotalPages,
                };

                //add pagination meta data to response header.
                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMetadata));

                //create links for shows.
                var links = CreateLinksForReports(filterOptionsModel, leadserviceofferedsFromRepo.HasNext, leadserviceofferedsFromRepo.HasPrevious);

                //prepare model with data and HATEOAS links.
                var linkedCollectionResource = new
                {
                    value = leadserviceofferedsFromRepo,
                    links = links
                };

                //return the data with Ok response.
                return Ok(linkedCollectionResource);
            }
            else
            {
                var previousPageLink = leadserviceofferedsFromRepo.HasPrevious ?
                    CreateLeadServiceOfferedsResourceUri(filterOptionsModel, ResourceUriType.PreviousPage) : null;

                var nextPageLink = leadserviceofferedsFromRepo.HasNext ?
                    CreateLeadServiceOfferedsResourceUri(filterOptionsModel, ResourceUriType.NextPage) : null;

                //prepare the pagination metadata.
                var paginationMetadata = new
                {
                    previousPageLink = previousPageLink,
                    nextPageLink = nextPageLink,
                    totalCount = leadserviceofferedsFromRepo.TotalCount,
                    pageSize = leadserviceofferedsFromRepo.PageSize,
                    currentPage = leadserviceofferedsFromRepo.CurrentPage,
                    totalPages = leadserviceofferedsFromRepo.TotalPages
                };

                //add pagination meta data to response header.
                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMetadata));
                leadserviceofferedsFromRepo.ForEach(leadserviceoffered =>
                {
                    var ftcrm = _fTCrmService.GetAllEntities().Where(x => x.LeadServiceId.Equals(leadserviceoffered.Id)).FirstOrDefault();
                    if (ftcrm != null)
                    {
                        leadserviceoffered.ftcrm = ftcrm;

                        if (ftcrm.FtCrmUniqueId != null && Int32.Parse(ftcrm.FtCrmUniqueId) > 0)
                        {
                            leadserviceoffered.disableTrigger = true;
                        }
                        else
                        {
                            leadserviceoffered.disableTrigger = false;
                        }
                    }

                });
                //return the data with Ok response.
                return Ok(leadserviceofferedsFromRepo);
            }
        }



        [HttpGet(Name = "GetFilteredReports")]
        [Produces("application/vnd.tourmanagement.reports.hateoas+json", "application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<List<ReportDto>>> GetFilteredReports([FromQuery] FilterOptionsModel filterOptionsModel, [FromHeader(Name = "Accept")] string mediaType)
        {
            try
            {
                //if order by fields are not valid.
                if (!_reportService.ValidMappingExists(filterOptionsModel.OrderBy))
                {
                    //then return bad request.
                    return BadRequest();
                }

                //if fields are not valid.
                if (!EventManagementUtils.TypeHasProperties<ReportDto>(filterOptionsModel.Fields))
                {
                    //then return bad request.
                    return BadRequest();
                }

                //get the paged/filtered show from db. 
                var reportsFromRepo = await _reportService.GetFilteredEntities(filterOptionsModel);

                var previousPageLink = reportsFromRepo.HasPrevious ?
                    CreateReportsResourceUri(filterOptionsModel, ResourceUriType.PreviousPage) : null;

                var nextPageLink = reportsFromRepo.HasNext ?
                    CreateReportsResourceUri(filterOptionsModel, ResourceUriType.NextPage) : null;

                //prepare the pagination metadata.
                var paginationMetadata = new
                {
                    previousPageLink = previousPageLink,
                    nextPageLink = nextPageLink,
                    totalCount = reportsFromRepo.TotalCount,
                    pageSize = reportsFromRepo.PageSize,
                    currentPage = reportsFromRepo.CurrentPage,
                    totalPages = reportsFromRepo.TotalPages
                };

                //add pagination meta data to response header.
                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMetadata));

                //return the data with Ok response.
                return Ok(reportsFromRepo);
            }
            catch(Exception ex)
            {
                var exxx = ex;
            }
            return BadRequest();            

        }

        [HttpGet("OperationalReport", Name = "GetOperationalReport")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<List<ReportDto>>> GetOperationalReport([FromQuery] FilterOptionsModel filterOptionsModel, [FromHeader(Name = "Accept")] string mediaType)
        {
            var returnList = new List<OperationReport>();
            //if order by fields are not valid.
            if (!_reportService.ValidMappingExists(filterOptionsModel.OrderBy))
            {
                //then return bad request.
                return BadRequest();
            }

            //if fields are not valid.
            if (!EventManagementUtils.TypeHasProperties<ReportDto>(filterOptionsModel.Fields))
            {
                //then return bad request.
                return BadRequest();
            }

            //var invoiceFilterOption = new FilterOptionsModel();
            //invoiceFilterOption.PageSize = 10000;
            var invoices = await _serviceOfferedInvoiceService.GetFilteredEntities(filterOptionsModel);

            foreach (var invoice in invoices)
            {
                var filterOption = new FilterOptionsModel();
                filterOption.SearchQuery = "Id==\"" + invoice.LeadServiceOfferedId + "\"";
                filterOption.PageSize = 10000;
                var leadServices = await _leadServiceOfferedService.GetFilteredEntities(filterOption);
                foreach(var leadService in leadServices )
                {
                    var province = await _provinceService.GetEntityByIdAsync(Guid.Parse(leadService.Contact.Province));
                    var HstPercent = province.HSTPercent;
                    var salesRep = _aspUserService.GetAllEntities().Where(x => x.Id == leadService.Lead.AgentId).FirstOrDefault();
                    var salesRepPercent = salesRep !=null && salesRep.ReferencePercentage != null ? Int32.Parse(salesRep.ReferencePercentage) : 0;
                    var ftcrm = _fTCrmService.GetAllEntities().Where(x => x.LeadServiceId == leadService.Id).FirstOrDefault();
                    double refundAmount = ftcrm.Amount;
                    double invoicedAmount = Math.Round((double)(refundAmount * 0.33),2);
                    double _hst = Math.Round((double)((invoicedAmount * HstPercent) / 100),2);
                    double _familyTaxChargers = Math.Round((double)(invoicedAmount * 0.30),2);
                    double _stanFinal = Math.Round((double)(_hst * 0.30),2);
                    double _lfgAmount = Math.Round((double)(invoicedAmount - _familyTaxChargers),2);
                    double _lfgHst = Math.Round((double)(_hst - _stanFinal),2);
                    double _repAmount = Math.Round(_lfgAmount * ((double)salesRepPercent / 100),2);
                    double _repHst = Math.Round(_repAmount * ((double)HstPercent / 100),2);
                    double _lfgFinalAmount = Math.Round((double)(_lfgAmount - _repAmount),2);
                    double _lfgFinalHst = Math.Round((double)(_lfgHst - _repHst),2);

                    var report = new OperationReport()
                    {
                        Name = leadService.Contact.FirstName,
                        Number = leadService.Contact.Phone,
                        Email = leadService.Contact.Email,
                        RefundAmount = refundAmount,
                        InvoiceAmount = Math.Round(invoicedAmount, 2),
                        HST = Math.Round(_hst, 2),
                        FamilyTaxCharge = Math.Round(_familyTaxChargers, 2),
                        StanFinal = Math.Round(_stanFinal, 2),
                        LFGAmount = Math.Round(_lfgAmount, 2),
                        LFGHST = Math.Round(_lfgHst, 2),
                        Rep = salesRep != null ? salesRep.FName + " " + salesRep.LName : "",
                        RepPercentage = salesRepPercent,
                        RepAmount = Math.Round(_repAmount, 2),
                        RepHST = Math.Round(_repHst, 2),
                        LFGFinalAmount = Math.Round(_lfgFinalAmount, 2),
                        LFGFinalHST = Math.Round(_lfgFinalHst, 2),
                        InvoiceDate = invoice.CreatedOn,

                    };
                    returnList.Add(report);
                }

            }

            //get the paged/filtered show from db. 

            var previousPageLink = invoices.HasPrevious ?
                CreateReportsResourceUri(filterOptionsModel, ResourceUriType.PreviousPage) : null;

            var nextPageLink = invoices.HasNext ?
                CreateReportsResourceUri(filterOptionsModel, ResourceUriType.NextPage) : null;

            //prepare the pagination metadata.
            var paginationMetadata = new
            {
                previousPageLink = previousPageLink,
                nextPageLink = nextPageLink,
                totalCount = invoices.TotalCount,
                pageSize = invoices.PageSize,
                currentPage = invoices.CurrentPage,
                totalPages = invoices.TotalPages
            };

            //add pagination meta data to response header.
            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMetadata));

            //return the data with Ok response.
            return Ok(returnList);

        }

        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Produces("application/vnd.tourmanagement.reports.hateoas+json", "application/json")]
        [HttpGet("{id}", Name = "GetReport")]
        public async Task<ActionResult<Report>> GetReport(Guid id, [FromQuery] string fields, [FromHeader(Name = "Accept")] string mediaType)
        {
            object reportEntity = null;
            object linkedResourceToReturn = null;

            //if fields are not passed.
            if (string.IsNullOrEmpty(fields))
            {
                _logger.LogInformation("GetReport called");

                //then get the whole entity and map it to the Dto.
                reportEntity = Mapper.Map<ReportDto>(await _reportService.GetEntityByIdAsync(id));
            }
            else
            {
                //if fields are passed then get the partial show.
                reportEntity = await _reportService.GetPartialEntityAsync(id, fields);
            }

            //if report not found.
            if (reportEntity == null)
            {
                //then return not found response.
                return NotFound();
            }

            if (mediaType == "application/vnd.tourmanagement.reports.hateoas+json")
            {
                //create HATEOS links
                var links = CreateLinksForReport(id, fields);

                //if fields are not passed.
                if (string.IsNullOrEmpty(fields))
                {
                    //convert the typed object to expando object.
                    linkedResourceToReturn = ((ReportDto)reportEntity).ShapeData("") as IDictionary<string, object>;

                    //add the HATEOAS links to the model.
                    ((IDictionary<string, object>)linkedResourceToReturn).Add("links", links);
                }
                else
                {
                    linkedResourceToReturn = reportEntity;

                    //add the HATEOAS links to the model.
                    ((dynamic)linkedResourceToReturn).links = links;

                }
            }
            else
            {
                linkedResourceToReturn = reportEntity;
            }

            //return the Ok response.
            return Ok(linkedResourceToReturn);
        }

        #endregion


        #region HTTPPUT

        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPut("{id}", Name = "UpdateReport")]
        public async Task<IActionResult> UpdateReport(Guid id, [FromBody]ReportForUpdate ReportForUpdate)
        {

            //if show not found
            if (!await _reportService.ExistAsync(x => x.Id == id))
            {
                //then return not found response.
                return NotFound();
            }

            //Update an entity.
            await _reportService.UpdateEntityAsync(id, ReportForUpdate);

            //return the response.
            return NoContent();
        }

        #endregion


        #region HTTPPATCH

        [Consumes("application/json-patch+json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPatch("{id}")]
        public async Task<IActionResult> PartiallyUpdateReport(Guid id, [FromBody] JsonPatchDocument<ReportForUpdate> jsonPatchDocument)
        {
            ReportForUpdate dto = new ReportForUpdate();
            Report report = new Report();

            //apply the patch changes to the dto. 
            jsonPatchDocument.ApplyTo(dto, ModelState);

            //if the jsonPatchDocument is not valid.
            if (!ModelState.IsValid)
            {
                //then return unprocessableEntity response.
                return new UnprocessableEntityObjectResult(ModelState);
            }

            //if the dto model is not valid after applying changes.
            if (!TryValidateModel(dto))
            {
                //then return unprocessableEntity response.
                return new UnprocessableEntityObjectResult(ModelState);
            }

            //map the chnages from dto to entity.
            Mapper.Map(dto, report);

            //set the Id for the show model.
            report.Id = id;

            //partially update the chnages to the db. 
            await _reportService.UpdatePartialEntityAsync(report, jsonPatchDocument);

            //return the response.
            return NoContent();
        }

        #endregion


        #region HTTPPOST

        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPost(Name = "CreateReport")]
        public async Task<ActionResult<ReportDto>> CreateReport([FromBody]ReportForCreation report)
        {
            //create a show in db.
            var reportToReturn = await _reportService.CreateEntityAsync<ReportDto, ReportForCreation>(report);

            //return the show created response.
            return CreatedAtRoute("GetReport", new { id = reportToReturn.Id }, reportToReturn);
        }

        #endregion


        #region HTTPDELETE
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}", Name = "DeleteReportById")]
        public async Task<IActionResult> DeleteReportById(Guid id)
        {
            //if the report exists
            if (await _reportService.ExistAsync(x => x.Id == id))
            {
                //delete the report from the db.
                await _reportService.DeleteEntityAsync(id);
            }
            else
            {
                //if report doesn't exists then returns not found.
                return NotFound();
            }

            //return the response.
            return NoContent();
        }

        #endregion


        #region PRIVATE METHODS

        private IEnumerable<LinkDto> CreateLinksForReport(Guid id, string fields)
        {
            var links = new List<LinkDto>();

            if (string.IsNullOrWhiteSpace(fields))
            {
                links.Add(
                  new LinkDto(_urlHelper.Link("GetReport", new { id = id }),
                  "self",
                  "GET"));

                //Lead
                links.Add(
                 new LinkDto(_urlHelper.Link("GetLead", new { id = id }),
                 "self",
                 "GET"));

                //Lead ServiceOffered
                links.Add(
                   new LinkDto(_urlHelper.Link("GetLeadServiceOffered", new { id = id }),
                   "self",
                   "GET"));
            }
            else
            {
                links.Add(
                  new LinkDto(_urlHelper.Link("GetReport", new { id = id, fields = fields }),
                  "self",
                  "GET"));

                //lead
                links.Add(
                  new LinkDto(_urlHelper.Link("GetLead", new { id = id, fields = fields }),
                  "self",
                  "GET"));

                //Lead ServiceOffered
                links.Add(
                 new LinkDto(_urlHelper.Link("GetLeadServiceOffered", new { id = id, fields = fields }),
                 "self",
                 "GET"));
            }

            links.Add(
              new LinkDto(_urlHelper.Link("DeleteReportById", new { id = id }),
              "delete_report",
              "DELETE"));

            links.Add(
             new LinkDto(_urlHelper.Link("UpdateReport", new { id = id }),
             "update_report",
             "PUT"));

            links.Add(
              new LinkDto(_urlHelper.Link("CreateReport", new { }),
              "create_report",
              "POST"));

            //Lead
            links.Add(
             new LinkDto(_urlHelper.Link("DeleteLeadById", new { id = id }),
             "delete_lead",
             "DELETE"));

            links.Add(
             new LinkDto(_urlHelper.Link("UpdateLead", new { id = id }),
             "update_lead",
             "PUT"));

            links.Add(
              new LinkDto(_urlHelper.Link("CreateLead", new { }),
              "create_lead",
              "POST"));

            //Lead ServiceOffered
            links.Add(
             new LinkDto(_urlHelper.Link("DeleteLeadServiceOfferedById", new { id = id }),
             "delete_leadserviceoffered",
             "DELETE"));

            links.Add(
             new LinkDto(_urlHelper.Link("UpdateLeadServiceOffered", new { id = id }),
             "update_leadserviceoffered",
             "PUT"));

            links.Add(
              new LinkDto(_urlHelper.Link("CreateLeadServiceOffered", new { }),
              "create_leadserviceoffered",
              "POST"));

            //links.Add(
            //   new LinkDto(_urlHelper.Link("GetShowsForTour", new { }),
            //   "shows",
            //   "GET"));

            return links;
        }

        private IEnumerable<LinkDto> CreateLinksForReports(FilterOptionsModel filterOptionsModel, bool hasNext, bool hasPrevious)
        {
            var links = new List<LinkDto>();

            links.Add(
               new LinkDto(CreateReportsResourceUri(filterOptionsModel, ResourceUriType.Current), "self", "GET"));
            
            //Lead
            links.Add(
               new LinkDto(CreateLeadsResourceUri(filterOptionsModel, ResourceUriType.Current), "self", "GET"));

            //Lead ServiceOffered
            links.Add(
               new LinkDto(CreateLeadServiceOfferedsResourceUri(filterOptionsModel, ResourceUriType.Current), "self", "GET"));

            if (hasNext)
            {
                links.Add(
                  new LinkDto(CreateReportsResourceUri(filterOptionsModel, ResourceUriType.NextPage), "nextPage", "GET"));

                //Lead
                links.Add(
                   new LinkDto(CreateLeadsResourceUri(filterOptionsModel, ResourceUriType.NextPage), "nextPage", "GET"));

                //Lead ServiceOffered
                links.Add(
                 new LinkDto(CreateLeadServiceOfferedsResourceUri(filterOptionsModel, ResourceUriType.NextPage), "nextPage", "GET"));

            }

            if (hasPrevious)
            {
                links.Add(new LinkDto(CreateReportsResourceUri(filterOptionsModel, ResourceUriType.PreviousPage), "previousPage", "GET"));

                //Lead
                links.Add(new LinkDto(CreateLeadsResourceUri(filterOptionsModel, ResourceUriType.PreviousPage), "previousPage", "GET"));

                //Lead ServiceOffered
                links.Add(new LinkDto(CreateLeadServiceOfferedsResourceUri(filterOptionsModel, ResourceUriType.PreviousPage), "previousPage", "GET"));

            }

            return links;
        }

        private string CreateReportsResourceUri(FilterOptionsModel filterOptionsModel, ResourceUriType type)
        {
            switch (type)
            {
                case ResourceUriType.PreviousPage:
                    return _urlHelper.Link("GetFilteredReports",
                      new
                      {
                          fields = filterOptionsModel.Fields,
                          orderBy = filterOptionsModel.OrderBy,
                          searchQuery = filterOptionsModel.SearchQuery,
                          pageNumber = filterOptionsModel.PageNumber - 1,
                          pageSize = filterOptionsModel.PageSize
                      });
                case ResourceUriType.NextPage:
                    return _urlHelper.Link("GetFilteredReports",
                      new
                      {
                          fields = filterOptionsModel.Fields,
                          orderBy = filterOptionsModel.OrderBy,
                          searchQuery = filterOptionsModel.SearchQuery,
                          pageNumber = filterOptionsModel.PageNumber + 1,
                          pageSize = filterOptionsModel.PageSize
                      });

                default:
                    return _urlHelper.Link("GetFilteredReports",
                    new
                    {
                        fields = filterOptionsModel.Fields,
                        orderBy = filterOptionsModel.OrderBy,
                        searchQuery = filterOptionsModel.SearchQuery,
                        pageNumber = filterOptionsModel.PageNumber,
                        pageSize = filterOptionsModel.PageSize
                    });
            }
        }

        //Lead
        private string CreateLeadsResourceUri(FilterOptionsModel filterOptionsModel, ResourceUriType type)
        {
            switch (type)
            {
                case ResourceUriType.PreviousPage:
                    return _urlHelper.Link("GetFilteredLeads",
                      new
                      {
                          fields = filterOptionsModel.Fields,
                          orderBy = filterOptionsModel.OrderBy,
                          searchQuery = filterOptionsModel.SearchQuery,
                          pageNumber = filterOptionsModel.PageNumber - 1,
                          pageSize = filterOptionsModel.PageSize
                      });
                case ResourceUriType.NextPage:
                    return _urlHelper.Link("GetFilteredLeads",
                      new
                      {
                          fields = filterOptionsModel.Fields,
                          orderBy = filterOptionsModel.OrderBy,
                          searchQuery = filterOptionsModel.SearchQuery,
                          pageNumber = filterOptionsModel.PageNumber + 1,
                          pageSize = filterOptionsModel.PageSize
                      });

                default:
                    return _urlHelper.Link("GetFilteredLeads",
                    new
                    {
                        fields = filterOptionsModel.Fields,
                        orderBy = filterOptionsModel.OrderBy,
                        searchQuery = filterOptionsModel.SearchQuery,
                        pageNumber = filterOptionsModel.PageNumber,
                        pageSize = filterOptionsModel.PageSize
                    });
            }
        }

        //Lead ServiceOffered
        private string CreateLeadServiceOfferedsResourceUri(FilterOptionsModel filterOptionsModel, ResourceUriType type)
        {
            switch (type)
            {
                case ResourceUriType.PreviousPage:
                    return _urlHelper.Link("GetFilteredLeadServiceOffereds",
                      new
                      {
                          fields = filterOptionsModel.Fields,
                          orderBy = filterOptionsModel.OrderBy,
                          searchQuery = filterOptionsModel.SearchQuery,
                          pageNumber = filterOptionsModel.PageNumber - 1,
                          pageSize = filterOptionsModel.PageSize
                      });
                case ResourceUriType.NextPage:
                    return _urlHelper.Link("GetFilteredLeadServiceOffereds",
                      new
                      {
                          fields = filterOptionsModel.Fields,
                          orderBy = filterOptionsModel.OrderBy,
                          searchQuery = filterOptionsModel.SearchQuery,
                          pageNumber = filterOptionsModel.PageNumber + 1,
                          pageSize = filterOptionsModel.PageSize
                      });

                default:
                    return _urlHelper.Link("GetFilteredLeadServiceOffereds",
                    new
                    {
                        fields = filterOptionsModel.Fields,
                        orderBy = filterOptionsModel.OrderBy,
                        searchQuery = filterOptionsModel.SearchQuery,
                        pageNumber = filterOptionsModel.PageNumber,
                        pageSize = filterOptionsModel.PageSize
                    });
            }
        }
        #endregion

    }
}
