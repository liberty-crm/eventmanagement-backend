using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
//using EventManagement.API.Helpers;
using EventManagement.Dto;
using EventManagement.Service;
using EventManagement.Domain.Entities;
using EventManagement.Utility;
using Microsoft.Extensions.Logging;

namespace EventManagement.API.Controllers
{
    /// <summary>
    /// Province endpoint
    /// </summary>
    [Route("api/provinces")]
    [Produces("application/json")]
    [ApiController]
    public class ProvinceController : Controller
    {

        #region PRIVATE MEMBERS

        private readonly IProvinceService _provinceService;
        private ILogger<ProvinceController> _logger;
        private readonly IUrlHelper _urlHelper;

        #endregion


        #region CONSTRUCTOR

        public ProvinceController(IProvinceService provinceService, ILogger<ProvinceController> logger, IUrlHelper urlHelper) 
        {
            _logger = logger;
            _provinceService = provinceService;
            _urlHelper = urlHelper;
        }

        #endregion


        #region HTTPGET
        [AllowAnonymous]
        [HttpGet(Name = "GetFilteredProvinces")]
        [Produces("application/vnd.tourmanagement.provinces.hateoas+json", "application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<List<ProvinceDto>>> GetFilteredProvinces([FromQuery]FilterOptionsModel filterOptionsModel, [FromHeader(Name = "Accept")] string mediaType)
        {

            //if order by fields are not valid.
            if (!_provinceService.ValidMappingExists(filterOptionsModel.OrderBy))
            {
                //then return bad request.
                return BadRequest();
            }

            //if fields are not valid.
            if (!EventManagementUtils.TypeHasProperties<ProvinceDto>(filterOptionsModel.Fields))
            {
                //then return bad request.
                return BadRequest();
            }
            filterOptionsModel.PageSize = 20;
            //get the paged/filtered show from db. 
            var provincesFromRepo = await _provinceService.GetFilteredEntities(filterOptionsModel);

            //if HATEOAS links are required.
            if (mediaType == "application/vnd.tourmanagement.provinces.hateoas+json")
            {
                //create HATEOAS links for each show.
                provincesFromRepo.ForEach(province =>
                {
                    var entityLinks = CreateLinksForProvince(province.Id, filterOptionsModel.Fields);
                    province.links = entityLinks;
                });

                //prepare pagination metadata.
                var paginationMetadata = new
                {
                    totalCount = provincesFromRepo.TotalCount,
                    pageSize = provincesFromRepo.PageSize,
                    currentPage = provincesFromRepo.CurrentPage,
                    totalPages = provincesFromRepo.TotalPages,
                };

                //add pagination meta data to response header.
                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMetadata));

                //create links for shows.
                var links = CreateLinksForProvinces(filterOptionsModel, provincesFromRepo.HasNext, provincesFromRepo.HasPrevious);

                //prepare model with data and HATEOAS links.
                var linkedCollectionResource = new
                {
                    value = provincesFromRepo,
                    links = links
                };

                //return the data with Ok response.
                return Ok(linkedCollectionResource);
            }
            else
            {
                var previousPageLink = provincesFromRepo.HasPrevious ?
                    CreateProvincesResourceUri(filterOptionsModel, ResourceUriType.PreviousPage) : null;

                var nextPageLink = provincesFromRepo.HasNext ?
                    CreateProvincesResourceUri(filterOptionsModel, ResourceUriType.NextPage) : null;

                //prepare the pagination metadata.
                var paginationMetadata = new
                {
                    previousPageLink = previousPageLink,
                    nextPageLink = nextPageLink,
                    totalCount = provincesFromRepo.TotalCount,
                    pageSize = provincesFromRepo.PageSize,
                    currentPage = provincesFromRepo.CurrentPage,
                    totalPages = provincesFromRepo.TotalPages
                };

                //add pagination meta data to response header.
                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMetadata));

                //return the data with Ok response.
                return Ok(provincesFromRepo);
            }
        }


        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Produces("application/vnd.tourmanagement.provinces.hateoas+json", "application/json")]
        [HttpGet("{id}", Name = "GetProvince")]
        public async Task<ActionResult<Province>> GetProvince(Guid id, [FromQuery] string fields, [FromHeader(Name = "Accept")] string mediaType)
        {
            object provinceEntity = null;
            object linkedResourceToReturn = null;

            //if fields are not passed.
            if (string.IsNullOrEmpty(fields))
            {
                _logger.LogInformation("GetProvince called");

                //then get the whole entity and map it to the Dto.
                provinceEntity = Mapper.Map<ProvinceDto>(await _provinceService.GetEntityByIdAsync(id));
            }
            else
            {
                //if fields are passed then get the partial show.
                provinceEntity = await _provinceService.GetPartialEntityAsync(id, fields);
            }

            //if province not found.
            if (provinceEntity == null)
            {
                //then return not found response.
                return NotFound();
            }

            if (mediaType == "application/vnd.tourmanagement.provinces.hateoas+json")
            {
                //create HATEOS links
                var links = CreateLinksForProvince(id, fields);

                //if fields are not passed.
                if (string.IsNullOrEmpty(fields))
                {
                    //convert the typed object to expando object.
                    linkedResourceToReturn = ((ProvinceDto)provinceEntity).ShapeData("") as IDictionary<string, object>;

                    //add the HATEOAS links to the model.
                    ((IDictionary<string, object>)linkedResourceToReturn).Add("links", links);
                }
                else
                {
                    linkedResourceToReturn = provinceEntity;

                    //add the HATEOAS links to the model.
                    ((dynamic)linkedResourceToReturn).links = links;

                }
            }
            else
            {
                linkedResourceToReturn = provinceEntity;
            }

            //return the Ok response.
            return Ok(linkedResourceToReturn);
        }

        #endregion


        #region HTTPPUT

        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPut("{id}", Name = "UpdateProvince")]
        public async Task<IActionResult> UpdateProvince(Guid id, [FromBody]ProvinceForUpdate ProvinceForUpdate)
        {

            //if show not found
            if (!await _provinceService.ExistAsync(x => x.Id == id))
            {
                //then return not found response.
                return NotFound();
            }

            //Update an entity.
            await _provinceService.UpdateEntityAsync(id, ProvinceForUpdate);

            //return the response.
            return NoContent();
        }

        #endregion


        #region HTTPPATCH

        [Consumes("application/json-patch+json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPatch("{id}")]
        public async Task<IActionResult> PartiallyUpdateProvince(Guid id, [FromBody] JsonPatchDocument<ProvinceForUpdate> jsonPatchDocument)
        {
            ProvinceForUpdate dto = new ProvinceForUpdate();
            Province province = new Province();

            //apply the patch changes to the dto. 
            jsonPatchDocument.ApplyTo(dto, ModelState);

            //if the jsonPatchDocument is not valid.
            if (!ModelState.IsValid)
            {
                //then return unprocessableEntity response.
                return new UnprocessableEntityObjectResult(ModelState);
            }

            //if the dto model is not valid after applying changes.
            if (!TryValidateModel(dto))
            {
                //then return unprocessableEntity response.
                return new UnprocessableEntityObjectResult(ModelState);
            }

            //map the chnages from dto to entity.
            Mapper.Map(dto, province);

            //set the Id for the show model.
            province.Id = id;

            //partially update the chnages to the db. 
            await _provinceService.UpdatePartialEntityAsync(province, jsonPatchDocument);

            //return the response.
            return NoContent();
        }

        #endregion


        #region HTTPPOST

        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPost(Name = "CreateProvince")]
        public async Task<ActionResult<ProvinceDto>> CreateProvince([FromBody]ProvinceForCreation province)
        {
            //create a show in db.
            var provinceToReturn = await _provinceService.CreateEntityAsync<ProvinceDto, ProvinceForCreation>(province);

            //return the show created response.
            return CreatedAtRoute("GetProvince", new { id = provinceToReturn.Id }, provinceToReturn);
        }

        #endregion


        #region HTTPDELETE
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}", Name = "DeleteProvinceById")]
        public async Task<IActionResult> DeleteProvinceById(Guid id)
        {
            //if the province exists
            if (await _provinceService.ExistAsync(x => x.Id == id))
            {
                //delete the province from the db.
                await _provinceService.DeleteEntityAsync(id);
            }
            else
            {
                //if province doesn't exists then returns not found.
                return NotFound();
            }

            //return the response.
            return NoContent();
        }

        #endregion


        #region PRIVATE METHODS

        private IEnumerable<LinkDto> CreateLinksForProvince(Guid id, string fields)
        {
            var links = new List<LinkDto>();

            if (string.IsNullOrWhiteSpace(fields))
            {
                links.Add(
                  new LinkDto(_urlHelper.Link("GetProvince", new { id = id }),
                  "self",
                  "GET"));
            }
            else
            {
                links.Add(
                  new LinkDto(_urlHelper.Link("GetProvince", new { id = id, fields = fields }),
                  "self",
                  "GET"));
            }

            links.Add(
              new LinkDto(_urlHelper.Link("DeleteProvinceById", new { id = id }),
              "delete_province",
              "DELETE"));

            links.Add(
             new LinkDto(_urlHelper.Link("UpdateProvince", new { id = id }),
             "update_province",
             "PUT"));

            links.Add(
              new LinkDto(_urlHelper.Link("CreateProvince", new { }),
              "create_province",
              "POST"));

            //links.Add(
            //   new LinkDto(_urlHelper.Link("GetShowsForTour", new { }),
            //   "shows",
            //   "GET"));

            return links;
        }

        private IEnumerable<LinkDto> CreateLinksForProvinces(FilterOptionsModel filterOptionsModel, bool hasNext, bool hasPrevious)
        {
            var links = new List<LinkDto>();

            links.Add(
               new LinkDto(CreateProvincesResourceUri(filterOptionsModel, ResourceUriType.Current), "self", "GET"));

            if (hasNext)
            {
                links.Add(
                  new LinkDto(CreateProvincesResourceUri(filterOptionsModel, ResourceUriType.NextPage), "nextPage", "GET"));
            }

            if (hasPrevious)
            {
                links.Add(new LinkDto(CreateProvincesResourceUri(filterOptionsModel, ResourceUriType.PreviousPage), "previousPage", "GET"));
            }

            return links;
        }

        private string CreateProvincesResourceUri(FilterOptionsModel filterOptionsModel, ResourceUriType type)
        {
            switch (type)
            {
                case ResourceUriType.PreviousPage:
                    return _urlHelper.Link("GetFilteredProvinces",
                      new
                      {
                          fields = filterOptionsModel.Fields,
                          orderBy = filterOptionsModel.OrderBy,
                          searchQuery = filterOptionsModel.SearchQuery,
                          pageNumber = filterOptionsModel.PageNumber - 1,
                          pageSize = filterOptionsModel.PageSize
                      });
                case ResourceUriType.NextPage:
                    return _urlHelper.Link("GetFilteredProvinces",
                      new
                      {
                          fields = filterOptionsModel.Fields,
                          orderBy = filterOptionsModel.OrderBy,
                          searchQuery = filterOptionsModel.SearchQuery,
                          pageNumber = filterOptionsModel.PageNumber + 1,
                          pageSize = filterOptionsModel.PageSize
                      });

                default:
                    return _urlHelper.Link("GetFilteredProvinces",
                    new
                    {
                        fields = filterOptionsModel.Fields,
                        orderBy = filterOptionsModel.OrderBy,
                        searchQuery = filterOptionsModel.SearchQuery,
                        pageNumber = filterOptionsModel.PageNumber,
                        pageSize = filterOptionsModel.PageSize
                    });
            }
        }

        #endregion

    }
}
