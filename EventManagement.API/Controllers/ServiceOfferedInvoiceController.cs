using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
//using EventManagement.API.Helpers;
using EventManagement.Dto;
using EventManagement.Service;
using EventManagement.Domain.Entities;
using EventManagement.Utility;
using Microsoft.Extensions.Logging;

namespace EventManagement.API.Controllers
{
    /// <summary>
    /// ServiceOfferedInvoice endpoint
    /// </summary>
    [Route("api/serviceofferedinvoices")]
    [Produces("application/json")]
    [ApiController]
    public class ServiceOfferedInvoiceController : Controller
    {

        #region PRIVATE MEMBERS

        private readonly IServiceOfferedInvoiceService _serviceofferedinvoiceService;
        private ILogger<ServiceOfferedInvoiceController> _logger;
        private readonly IUrlHelper _urlHelper;

        #endregion


        #region CONSTRUCTOR

        public ServiceOfferedInvoiceController(IServiceOfferedInvoiceService serviceofferedinvoiceService, ILogger<ServiceOfferedInvoiceController> logger, IUrlHelper urlHelper) 
        {
            _logger = logger;
            _serviceofferedinvoiceService = serviceofferedinvoiceService;
            _urlHelper = urlHelper;
        }

        #endregion


        #region HTTPGET

        [HttpGet(Name = "GetFilteredServiceOfferedInvoices")]
        [Produces("application/vnd.tourmanagement.serviceofferedinvoices.hateoas+json", "application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<List<ServiceOfferedInvoiceDto>>> GetFilteredServiceOfferedInvoices([FromQuery]FilterOptionsModel filterOptionsModel, [FromHeader(Name = "Accept")] string mediaType)
        {

            //if order by fields are not valid.
            if (!_serviceofferedinvoiceService.ValidMappingExists(filterOptionsModel.OrderBy))
            {
                //then return bad request.
                return BadRequest();
            }

            //if fields are not valid.
            if (!EventManagementUtils.TypeHasProperties<ServiceOfferedInvoiceDto>(filterOptionsModel.Fields))
            {
                //then return bad request.
                return BadRequest();
            }

            //get the paged/filtered show from db. 
            var serviceofferedinvoicesFromRepo = await _serviceofferedinvoiceService.GetFilteredEntities(filterOptionsModel);

            //if HATEOAS links are required.
            if (mediaType == "application/vnd.tourmanagement.serviceofferedinvoices.hateoas+json")
            {
                //create HATEOAS links for each show.
                serviceofferedinvoicesFromRepo.ForEach(serviceofferedinvoice =>
                {
                    var entityLinks = CreateLinksForServiceOfferedInvoice(serviceofferedinvoice.Id, filterOptionsModel.Fields);
                    serviceofferedinvoice.links = entityLinks;
                });

                //prepare pagination metadata.
                var paginationMetadata = new
                {
                    totalCount = serviceofferedinvoicesFromRepo.TotalCount,
                    pageSize = serviceofferedinvoicesFromRepo.PageSize,
                    currentPage = serviceofferedinvoicesFromRepo.CurrentPage,
                    totalPages = serviceofferedinvoicesFromRepo.TotalPages,
                };

                //add pagination meta data to response header.
                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMetadata));

                //create links for shows.
                var links = CreateLinksForServiceOfferedInvoices(filterOptionsModel, serviceofferedinvoicesFromRepo.HasNext, serviceofferedinvoicesFromRepo.HasPrevious);

                //prepare model with data and HATEOAS links.
                var linkedCollectionResource = new
                {
                    value = serviceofferedinvoicesFromRepo,
                    links = links
                };

                //return the data with Ok response.
                return Ok(linkedCollectionResource);
            }
            else
            {
                var previousPageLink = serviceofferedinvoicesFromRepo.HasPrevious ?
                    CreateServiceOfferedInvoicesResourceUri(filterOptionsModel, ResourceUriType.PreviousPage) : null;

                var nextPageLink = serviceofferedinvoicesFromRepo.HasNext ?
                    CreateServiceOfferedInvoicesResourceUri(filterOptionsModel, ResourceUriType.NextPage) : null;

                //prepare the pagination metadata.
                var paginationMetadata = new
                {
                    previousPageLink = previousPageLink,
                    nextPageLink = nextPageLink,
                    totalCount = serviceofferedinvoicesFromRepo.TotalCount,
                    pageSize = serviceofferedinvoicesFromRepo.PageSize,
                    currentPage = serviceofferedinvoicesFromRepo.CurrentPage,
                    totalPages = serviceofferedinvoicesFromRepo.TotalPages
                };

                //add pagination meta data to response header.
                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMetadata));

                //return the data with Ok response.
                return Ok(serviceofferedinvoicesFromRepo);
            }
        }


        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Produces("application/vnd.tourmanagement.serviceofferedinvoices.hateoas+json", "application/json")]
        [HttpGet("{id}", Name = "GetServiceOfferedInvoice")]
        public async Task<ActionResult<ServiceOfferedInvoice>> GetServiceOfferedInvoice(Guid id, [FromQuery] string fields, [FromHeader(Name = "Accept")] string mediaType)
        {
            object serviceofferedinvoiceEntity = null;
            object linkedResourceToReturn = null;

            //if fields are not passed.
            if (string.IsNullOrEmpty(fields))
            {
                _logger.LogInformation("GetServiceOfferedInvoice called");

                //then get the whole entity and map it to the Dto.
                serviceofferedinvoiceEntity = Mapper.Map<ServiceOfferedInvoiceDto>(await _serviceofferedinvoiceService.GetEntityByIdAsync(id));
            }
            else
            {
                //if fields are passed then get the partial show.
                serviceofferedinvoiceEntity = await _serviceofferedinvoiceService.GetPartialEntityAsync(id, fields);
            }

            //if serviceofferedinvoice not found.
            if (serviceofferedinvoiceEntity == null)
            {
                //then return not found response.
                return NotFound();
            }

            if (mediaType == "application/vnd.tourmanagement.serviceofferedinvoices.hateoas+json")
            {
                //create HATEOS links
                var links = CreateLinksForServiceOfferedInvoice(id, fields);

                //if fields are not passed.
                if (string.IsNullOrEmpty(fields))
                {
                    //convert the typed object to expando object.
                    linkedResourceToReturn = ((ServiceOfferedInvoiceDto)serviceofferedinvoiceEntity).ShapeData("") as IDictionary<string, object>;

                    //add the HATEOAS links to the model.
                    ((IDictionary<string, object>)linkedResourceToReturn).Add("links", links);
                }
                else
                {
                    linkedResourceToReturn = serviceofferedinvoiceEntity;

                    //add the HATEOAS links to the model.
                    ((dynamic)linkedResourceToReturn).links = links;

                }
            }
            else
            {
                linkedResourceToReturn = serviceofferedinvoiceEntity;
            }

            //return the Ok response.
            return Ok(linkedResourceToReturn);
        }

        #endregion


        #region HTTPPUT

        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPut("{id}", Name = "UpdateServiceOfferedInvoice")]
        public async Task<IActionResult> UpdateServiceOfferedInvoice(Guid id, [FromBody]ServiceOfferedInvoiceForUpdate ServiceOfferedInvoiceForUpdate)
        {

            //if show not found
            if (!await _serviceofferedinvoiceService.ExistAsync(x => x.Id == id))
            {
                //then return not found response.
                return NotFound();
            }

            //Update an entity.
            await _serviceofferedinvoiceService.UpdateEntityAsync(id, ServiceOfferedInvoiceForUpdate);

            //return the response.
            return NoContent();
        }

        #endregion


        #region HTTPPATCH

        [Consumes("application/json-patch+json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPatch("{id}")]
        public async Task<IActionResult> PartiallyUpdateServiceOfferedInvoice(Guid id, [FromBody] JsonPatchDocument<ServiceOfferedInvoiceForUpdate> jsonPatchDocument)
        {
            ServiceOfferedInvoiceForUpdate dto = new ServiceOfferedInvoiceForUpdate();
            ServiceOfferedInvoice serviceofferedinvoice = new ServiceOfferedInvoice();

            //apply the patch changes to the dto. 
            jsonPatchDocument.ApplyTo(dto, ModelState);

            //if the jsonPatchDocument is not valid.
            if (!ModelState.IsValid)
            {
                //then return unprocessableEntity response.
                return new UnprocessableEntityObjectResult(ModelState);
            }

            //if the dto model is not valid after applying changes.
            if (!TryValidateModel(dto))
            {
                //then return unprocessableEntity response.
                return new UnprocessableEntityObjectResult(ModelState);
            }

            //map the chnages from dto to entity.
            Mapper.Map(dto, serviceofferedinvoice);

            //set the Id for the show model.
            serviceofferedinvoice.Id = id;

            //partially update the chnages to the db. 
            await _serviceofferedinvoiceService.UpdatePartialEntityAsync(serviceofferedinvoice, jsonPatchDocument);

            //return the response.
            return NoContent();
        }

        #endregion


        #region HTTPPOST

        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPost(Name = "CreateServiceOfferedInvoice")]
        public async Task<ActionResult<ServiceOfferedInvoiceDto>> CreateServiceOfferedInvoice([FromBody]ServiceOfferedInvoiceForCreation serviceofferedinvoice)
        {
            //create a show in db.
            var serviceofferedinvoiceToReturn = await _serviceofferedinvoiceService.CreateEntityAsync<ServiceOfferedInvoiceDto, ServiceOfferedInvoiceForCreation>(serviceofferedinvoice);

            //return the show created response.
            return CreatedAtRoute("GetServiceOfferedInvoice", new { id = serviceofferedinvoiceToReturn.Id }, serviceofferedinvoiceToReturn);
        }

        #endregion


        #region HTTPDELETE
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}", Name = "DeleteServiceOfferedInvoiceById")]
        public async Task<IActionResult> DeleteServiceOfferedInvoiceById(Guid id)
        {
            //if the serviceofferedinvoice exists
            if (await _serviceofferedinvoiceService.ExistAsync(x => x.Id == id))
            {
                //delete the serviceofferedinvoice from the db.
                await _serviceofferedinvoiceService.DeleteEntityAsync(id);
            }
            else
            {
                //if serviceofferedinvoice doesn't exists then returns not found.
                return NotFound();
            }

            //return the response.
            return NoContent();
        }

        #endregion


        #region PRIVATE METHODS

        private IEnumerable<LinkDto> CreateLinksForServiceOfferedInvoice(Guid id, string fields)
        {
            var links = new List<LinkDto>();

            if (string.IsNullOrWhiteSpace(fields))
            {
                links.Add(
                  new LinkDto(_urlHelper.Link("GetServiceOfferedInvoice", new { id = id }),
                  "self",
                  "GET"));
            }
            else
            {
                links.Add(
                  new LinkDto(_urlHelper.Link("GetServiceOfferedInvoice", new { id = id, fields = fields }),
                  "self",
                  "GET"));
            }

            links.Add(
              new LinkDto(_urlHelper.Link("DeleteServiceOfferedInvoiceById", new { id = id }),
              "delete_serviceofferedinvoice",
              "DELETE"));

            links.Add(
             new LinkDto(_urlHelper.Link("UpdateServiceOfferedInvoice", new { id = id }),
             "update_serviceofferedinvoice",
             "PUT"));

            links.Add(
              new LinkDto(_urlHelper.Link("CreateServiceOfferedInvoice", new { }),
              "create_serviceofferedinvoice",
              "POST"));

            //links.Add(
            //   new LinkDto(_urlHelper.Link("GetShowsForTour", new { }),
            //   "shows",
            //   "GET"));

            return links;
        }

        private IEnumerable<LinkDto> CreateLinksForServiceOfferedInvoices(FilterOptionsModel filterOptionsModel, bool hasNext, bool hasPrevious)
        {
            var links = new List<LinkDto>();

            links.Add(
               new LinkDto(CreateServiceOfferedInvoicesResourceUri(filterOptionsModel, ResourceUriType.Current), "self", "GET"));

            if (hasNext)
            {
                links.Add(
                  new LinkDto(CreateServiceOfferedInvoicesResourceUri(filterOptionsModel, ResourceUriType.NextPage), "nextPage", "GET"));
            }

            if (hasPrevious)
            {
                links.Add(new LinkDto(CreateServiceOfferedInvoicesResourceUri(filterOptionsModel, ResourceUriType.PreviousPage), "previousPage", "GET"));
            }

            return links;
        }

        private string CreateServiceOfferedInvoicesResourceUri(FilterOptionsModel filterOptionsModel, ResourceUriType type)
        {
            switch (type)
            {
                case ResourceUriType.PreviousPage:
                    return _urlHelper.Link("GetFilteredServiceOfferedInvoices",
                      new
                      {
                          fields = filterOptionsModel.Fields,
                          orderBy = filterOptionsModel.OrderBy,
                          searchQuery = filterOptionsModel.SearchQuery,
                          pageNumber = filterOptionsModel.PageNumber - 1,
                          pageSize = filterOptionsModel.PageSize
                      });
                case ResourceUriType.NextPage:
                    return _urlHelper.Link("GetFilteredServiceOfferedInvoices",
                      new
                      {
                          fields = filterOptionsModel.Fields,
                          orderBy = filterOptionsModel.OrderBy,
                          searchQuery = filterOptionsModel.SearchQuery,
                          pageNumber = filterOptionsModel.PageNumber + 1,
                          pageSize = filterOptionsModel.PageSize
                      });

                default:
                    return _urlHelper.Link("GetFilteredServiceOfferedInvoices",
                    new
                    {
                        fields = filterOptionsModel.Fields,
                        orderBy = filterOptionsModel.OrderBy,
                        searchQuery = filterOptionsModel.SearchQuery,
                        pageNumber = filterOptionsModel.PageNumber,
                        pageSize = filterOptionsModel.PageSize
                    });
            }
        }

        #endregion

    }
}
