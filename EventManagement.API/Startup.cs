using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.IO;
using EventManagement.Domain;
using EventManagement.Domain.Entities;
using EventManagement.Dto;
using EventManagement.Repository;
using EventManagement.Service;
using System;
using System.Reflection;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Formatters;
using System.Collections.Generic;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerUI;
using IdentityServer4.AccessTokenValidation;
using IdentityModel.AspNetCore.OAuth2Introspection;


namespace EventManagement.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(setupAction => 
            {
                setupAction.ReturnHttpNotAcceptable = true;

                var jsonOutputFormatter = setupAction.OutputFormatters
               .OfType<JsonOutputFormatter>().FirstOrDefault();

                if (jsonOutputFormatter != null)
                {
                   jsonOutputFormatter.SupportedMediaTypes.Add("application/vnd.EventManagement.Reports.hateoas + json");
                   jsonOutputFormatter.SupportedMediaTypes.Add("application/vnd.EventManagement.ServiceOfferedInvoices.hateoas + json");
                   jsonOutputFormatter.SupportedMediaTypes.Add("application/vnd.EventManagement.Payments.hateoas + json");
                   jsonOutputFormatter.SupportedMediaTypes.Add("application/vnd.EventManagement.Provinces.hateoas + json");
                   jsonOutputFormatter.SupportedMediaTypes.Add("application/vnd.EventManagement.NotificationMails.hateoas + json");
                   jsonOutputFormatter.SupportedMediaTypes.Add("application/vnd.EventManagement.FTCrms.hateoas + json");
                   jsonOutputFormatter.SupportedMediaTypes.Add("application/vnd.EventManagement.Historys.hateoas + json");
                   jsonOutputFormatter.SupportedMediaTypes.Add("application/vnd.EventManagement.Attachments.hateoas + json");
                   jsonOutputFormatter.SupportedMediaTypes.Add("application/vnd.EventManagement.Tasks.hateoas + json");
                   jsonOutputFormatter.SupportedMediaTypes.Add("application/vnd.EventManagement.Products.hateoas + json");
                   jsonOutputFormatter.SupportedMediaTypes.Add("application/vnd.EventManagement.Stores.hateoas + json");
                   jsonOutputFormatter.SupportedMediaTypes.Add("application/vnd.EventManagement.Contacts.hateoas + json");
                   jsonOutputFormatter.SupportedMediaTypes.Add("application/vnd.EventManagement.LeadContacts.hateoas + json");
                   jsonOutputFormatter.SupportedMediaTypes.Add("application/vnd.EventManagement.LeadAssets.hateoas + json");
                   jsonOutputFormatter.SupportedMediaTypes.Add("application/vnd.EventManagement.LeadServiceOffereds.hateoas + json");
                   jsonOutputFormatter.SupportedMediaTypes.Add("application/vnd.EventManagement.Assets.hateoas + json");
                   jsonOutputFormatter.SupportedMediaTypes.Add("application/vnd.EventManagement.ServiceOffereds.hateoas + json");
                   jsonOutputFormatter.SupportedMediaTypes.Add("application/vnd.EventManagement.Leads.hateoas + json");
                   jsonOutputFormatter.SupportedMediaTypes.Add("application/vnd.EventManagement.AspNetUserClaims.hateoas + json");
                    jsonOutputFormatter.SupportedMediaTypes.Add("application/vnd.tourmanagement.events.hateoas+json");
                }

            }).SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
             .AddJsonOptions(options =>
             {
                 options.SerializerSettings.DateParseHandling = DateParseHandling.DateTimeOffset;
                 options.SerializerSettings.ContractResolver =
                     new CamelCasePropertyNamesContractResolver();
             });          


            #region CORS POLICY

            // Configure CORS so the API allows requests from JavaScript.  
            // For demo purposes, all origins/headers/methods are allowed.  
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAllOriginsHeadersAndMethods",
                    builder => builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod().WithExposedHeaders("X-Pagination"));
            });

            #endregion

            #region REGISTER DB-CONTEXT

            var connectionString = Configuration["ConnectionStrings:EventManagementDB"];
            services.AddDbContext<EventManagementContext>(o => o.UseSqlServer(connectionString));

            #endregion

            // register the repository
           services.AddScoped<IAspNetUserClaimService, AspNetUserClaimService>();
           services.AddScoped<IAspNetUserClaimRepository,AspNetUserClaimRepository>();
           services.AddScoped<ILeadService, LeadService>();
           services.AddScoped<ILeadRepository,LeadRepository>();
           services.AddScoped<IServiceOfferedService, ServiceOfferedService>();
           services.AddScoped<IServiceOfferedRepository,ServiceOfferedRepository>();
           services.AddScoped<IAssetService, AssetService>();
           services.AddScoped<IAssetRepository,AssetRepository>();
           services.AddScoped<ILeadServiceOfferedService, LeadServiceOfferedService>();
           services.AddScoped<ILeadServiceOfferedRepository,LeadServiceOfferedRepository>();
           services.AddScoped<ILeadAssetService, LeadAssetService>();
           services.AddScoped<ILeadAssetRepository,LeadAssetRepository>();
           services.AddScoped<ILeadContactService, LeadContactService>();
           services.AddScoped<ILeadContactRepository,LeadContactRepository>();
           services.AddScoped<IContactService, ContactService>();
           services.AddScoped<IContactRepository,ContactRepository>();
           services.AddScoped<IStoreService, StoreService>();
           services.AddScoped<IStoreRepository,StoreRepository>();
           services.AddScoped<IProductService, ProductService>();
           services.AddScoped<IProductRepository,ProductRepository>();
           services.AddScoped<ITaskService, TaskService>();
           services.AddScoped<ITaskRepository,TaskRepository>();
           services.AddScoped<IAttachmentService, AttachmentService>();
           services.AddScoped<IAttachmentRepository,AttachmentRepository>();
           services.AddScoped<IHistoryService, HistoryService>();
           services.AddScoped<IHistoryRepository,HistoryRepository>();
           services.AddScoped<IFTCrmService, FTCrmService>();
           services.AddScoped<IFTCrmRepository,FTCrmRepository>();
           services.AddScoped<INotificationMailService, NotificationMailService>();
           services.AddScoped<INotificationMailRepository,NotificationMailRepository>();
           services.AddScoped<IProvinceService, ProvinceService>();
           services.AddScoped<IProvinceRepository,ProvinceRepository>();
           services.AddScoped<IPaymentService, PaymentService>();
           services.AddScoped<IPaymentRepository,PaymentRepository>();
           services.AddScoped<IServiceOfferedInvoiceService, ServiceOfferedInvoiceService>();
           services.AddScoped<IServiceOfferedInvoiceRepository,ServiceOfferedInvoiceRepository>();
           services.AddScoped<IReportService, ReportService>();
           services.AddScoped<IReportRepository,ReportRepository>();
            services.AddScoped(typeof(IRepository<,>), typeof(Repository<,>));

            // register the repository
            services.AddScoped<IEventRepository,EventRepository>();
            services.AddScoped<IAspUserRepository, AspUserRepository>();

            // register an IHttpContextAccessor so we can access the current
            // HttpContext in services by injecting it
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            // register the user info service
            services.AddScoped<IUserInfoService, UserInfoService>();

            // register the user info service
            services.AddScoped<IEventService, EventService>();
            services.AddScoped<IAspUserService, AspUserService>();

            services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();

            services.AddScoped<IUrlHelper, UrlHelper>(implementationFactory =>
            {
                var actionContext = implementationFactory.GetService<IActionContextAccessor>().ActionContext;
                return new UrlHelper(actionContext);
            });
            var identityServerUrl = Configuration["IdentityServerUrl"];

            services.AddAuthentication(IdentityServerAuthenticationDefaults.AuthenticationScheme)
                .AddIdentityServerAuthentication(options =>
                {
                    options.Authority = identityServerUrl;
                    //  options.RequireHttpsMetadata = false; // only for development
                    options.ApiName = "tourmanagementapi";
                    options.ApiSecret = "secret";
                    options.EnableCaching = true;
                    options.CacheDuration = TimeSpan.FromMinutes(1); // that's the default
                    options.TokenRetriever = new Func<HttpRequest, string>(req =>
                    {
                        var fromHeader = TokenRetrieval.FromAuthorizationHeader();
                        var fromQuery = TokenRetrieval.FromQueryString();
                        return fromHeader(req) ?? fromQuery(req);
                    });

                });

            services.AddSwaggerGen(setupAction =>
            {
                setupAction.SwaggerDoc("LibertyCRMAPISpecification", new Swashbuckle.AspNetCore.Swagger.Info
                {
                    Title = "Liberty CRM API",
                    Version = "1",
                    Description = "Through this api you can access CRM related entities.",

                });

            
                var xmlCommentFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlCommentDtoFile = "EventManagement.Dto.xml";
                var xmlCommentUtilFile = "EventManagement.Utility.xml";
                var xmlCommentFullPath = Path.Combine(AppContext.BaseDirectory, xmlCommentFile);
                var xmlCommentDtoFullPath = Path.Combine(AppContext.BaseDirectory, xmlCommentDtoFile);
                var xmlCommentUtilFullPath = Path.Combine(AppContext.BaseDirectory, xmlCommentUtilFile);
                setupAction.IncludeXmlComments(xmlCommentFullPath);
                setupAction.IncludeXmlComments(xmlCommentDtoFullPath);
                setupAction.IncludeXmlComments(xmlCommentUtilFullPath);
                setupAction.AddSecurityDefinition("oauth2", new OAuth2Scheme
                {
                    Description = "Standard Authorization header using the Bearer scheme. Example: \"bearer {token}\"",
                    Type = "oauth2",
                    Flow = "implicit",
                    AuthorizationUrl = identityServerUrl + "/connect/authorize",
                    TokenUrl = identityServerUrl + "/connect/token",
                    Scopes = new Dictionary<string, string> {
                        { "tourmanagementapi", "Tour Management API" }
                    }
                });
                setupAction.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>>
                {
                    { "oauth2", new[] { "tourmanagementapi" } }
                });
            });


            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.InvalidModelStateResponseFactory = actionContext =>
                {
                    var actionExecutingContext =
                        actionContext as Microsoft.AspNetCore.Mvc.Filters.ActionExecutingContext;

                    // if there are modelstate errors & all keys were correctly
                    // found/parsed we're dealing with validation errors
                    if (actionContext.ModelState.ErrorCount > 0
                        && actionExecutingContext?.ActionArguments.Count == actionContext.ActionDescriptor.Parameters.Count)
                    {
                        return new UnprocessableEntityObjectResult(actionContext.ModelState);
                    }

                    // if one of the keys wasn't correctly found / couldn't be parsed
                    // we're dealing with null/unparsable input
                    return new BadRequestObjectResult(actionContext.ModelState);
                };
            });
            services.AddApplicationInsightsTelemetry();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, EventManagementContext context)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();                
            }
            else
            {
                //TODO::
                app.UseExceptionHandler(appBuilder =>
                {
                    appBuilder.Run(async (ctx) =>
                    {
                        var exceptionHandlerFeature = ctx.Features.Get<IExceptionHandlerFeature>();
                        if (exceptionHandlerFeature != null)
                        {
                            var logger = loggerFactory.CreateLogger("Global exception logger");
                            logger.LogError(500,
                                exceptionHandlerFeature.Error,
                                exceptionHandlerFeature.Error.Message);
                        }

                        ctx.Response.StatusCode = 500;
                        await ctx.Response.WriteAsync("An unexpected fault happened. Try again later.");
                    });
                });

                app.UseHsts();
            }

            AutoMapper.Mapper.Initialize(config =>
            {
               config.CreateMap<ReportForUpdate, Report>();
               config.CreateMap<ReportForCreation, Report>();
               config.CreateMap<Report, ReportDto>();
               config.CreateMap<ServiceOfferedInvoiceForUpdate, ServiceOfferedInvoice>();
               config.CreateMap<ServiceOfferedInvoiceForCreation, ServiceOfferedInvoice>();
               config.CreateMap<ServiceOfferedInvoice, ServiceOfferedInvoiceDto>();
               config.CreateMap<PaymentForUpdate, Payment>();
               config.CreateMap<PaymentForCreation, Payment>();
               config.CreateMap<Payment, PaymentDto>();
               config.CreateMap<ProvinceForUpdate, Province>();
               config.CreateMap<ProvinceForCreation, Province>();
               config.CreateMap<Province, ProvinceDto>();
               config.CreateMap<NotificationMailForUpdate, NotificationMail>();
               config.CreateMap<NotificationMailForCreation, NotificationMail>();
               config.CreateMap<NotificationMail, NotificationMailDto>();
               config.CreateMap<FTCrmForUpdate, FTCrm>();
               config.CreateMap<FTCrmForCreation, FTCrm>();
               config.CreateMap<FTCrm, FTCrmDto>();
               config.CreateMap<HistoryForUpdate, History>();
               config.CreateMap<HistoryForCreation, History>();
               config.CreateMap<History, HistoryDto>();
               config.CreateMap<AttachmentForUpdate, Attachment>();
               config.CreateMap<AttachmentForCreation, Attachment>();
               config.CreateMap<Attachment, AttachmentDto>();
               config.CreateMap<TaskForUpdate, Task>();
               config.CreateMap<TaskForCreation, Task>();
               config.CreateMap<Task, TaskDto>();
               config.CreateMap<ProductForUpdate, Product>();
               config.CreateMap<ProductForCreation, Product>();
               config.CreateMap<Product, ProductDto>();
               config.CreateMap<StoreForUpdate, Store>();
               config.CreateMap<StoreForCreation, Store>();
               config.CreateMap<Store, StoreDto>();
               config.CreateMap<ContactForUpdate, Domain.Entities.Contact>();
               config.CreateMap<ContactForCreation, Domain.Entities.Contact>();
               config.CreateMap<Domain.Entities.Contact, ContactDto>();
               config.CreateMap<LeadContactForUpdate, LeadContact>();
               config.CreateMap<LeadContactForCreation, LeadContact>();
               config.CreateMap<LeadContact, LeadContactDto>();
               config.CreateMap<LeadAssetForUpdate, LeadAsset>();
               config.CreateMap<LeadAssetForCreation, LeadAsset>();
               config.CreateMap<LeadAsset, LeadAssetDto>();
               config.CreateMap<LeadServiceOfferedForUpdate, LeadServiceOffered>();
               config.CreateMap<LeadServiceOfferedForCreation, LeadServiceOffered>();
               config.CreateMap<LeadServiceOffered, LeadServiceOfferedDto>();
               config.CreateMap<AssetForUpdate, Asset>();
               config.CreateMap<AssetForCreation, Asset>();
               config.CreateMap<Asset, AssetDto>();
               config.CreateMap<ServiceOfferedForUpdate, ServiceOffered>();
               config.CreateMap<ServiceOfferedForCreation, ServiceOffered>();
               config.CreateMap<ServiceOffered, ServiceOfferedDto>();
               config.CreateMap<LeadForUpdate, Lead>();
               config.CreateMap<LeadForCreation, Lead>();
               config.CreateMap<Lead, LeadDto>();
               config.CreateMap<AspNetUserClaimForUpdate, AspNetUserClaim>();
               config.CreateMap<AspNetUserClaimForCreation, AspNetUserClaim>();
               config.CreateMap<AspNetUserClaim, AspNetUserClaimDto>();
                config.CreateMap<FinanaceManagement.API.Models.AspNetUsers, AspUserDto>();
                config.CreateMap<AspUserForCreation, FinanaceManagement.API.Models.AspNetUsers>();
                config.CreateMap<AspUserForUpdate, FinanaceManagement.API.Models.AspNetUsers>();

                config.CreateMap<Event, EventDto>();
                config.CreateMap<EventForCreation, Event>();
                config.CreateMap<EventForUpdate, Event>();

            });

            // Enable CORS
            app.UseCors("AllowAllOriginsHeadersAndMethods");

            app.UseHttpsRedirection();

            app.UseSwagger();
            var hostedURL = Configuration["HostedUrl"];
            app.UseSwaggerUI(setupAction =>
            {
                setupAction.SwaggerEndpoint("/swagger/LibertyCRMAPISpecification/swagger.json", "Liberty CRM API");
                setupAction.RoutePrefix = "";
                setupAction.OAuthClientId("swaggerui");
                setupAction.OAuth2RedirectUrl(hostedURL + "oauth2-redirect.html");
                setupAction.DocExpansion(DocExpansion.None);
                setupAction.EnableDeepLinking();
                setupAction.DisplayOperationId();
            });            

            app.UseMvc();
            

            //Add the seed data to the database.
            context.EnsureSeedDataForContext();

        }
    }
}
