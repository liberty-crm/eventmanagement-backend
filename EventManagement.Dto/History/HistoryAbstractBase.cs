using System;

namespace EventManagement.Dto
{
    public abstract class HistoryAbstractBase
    {
        /// <summary>
        /// History Id.
        /// </summary>
        public string Id { get; set; }

        public string LeadId { get; set; }

        public string ServiceId { get; set; }

        public string UserId { get; set; }

        public int? Activity { get; set; }

        public string TagData { get; set; }

    }
}
