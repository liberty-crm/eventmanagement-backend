﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EventManagement.Dto.AspUser
{
    public class ResetPasswordDto
    {
        public string NewPassword { get; set; }
        public string CurrentPassword { get; set; }
    }
}
