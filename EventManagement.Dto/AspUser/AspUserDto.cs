using System;

namespace EventManagement.Dto
{
    /// <summary>
    /// AspUser Model
    /// </summary>
    public class AspUserDto : AspUserAbstractBase
    {
        public int AccessFailedCount { get; set; }
        public string ConcurrencyStamp { get; set; }
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }
        public bool LockoutEnabled { get; set; }
        public DateTimeOffset? LockoutEnd { get; set; }
        public string NormalizedEmail { get; set; }
        public string NormalizedUserName { get; set; }
        public string PasswordHash { get; set; }
        public string PhoneNumber { get; set; }
        public bool PhoneNumberConfirmed { get; set; }
        public string SecurityStamp { get; set; }
        public bool TwoFactorEnabled { get; set; }
        public string UserName { get; set; }
        public string Database { get; set; }
        public bool IsActive { get; set; }
        public DateTime Birthday { get; set; }
        public string Birthplace { get; set; }
        public string Gender { get; set; }
        public string LivesIn { get; set; }
        public string Occupation { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public string ImageUrl { get; set; }
        public string Role { get; set; }
        public string EmployeeNumber { get; set; }

        public string MiddleName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Province { get; set; }
        public string PostalCode { get; set; }
        public string ReferencePercentage { get; set; }

        public AspUserDto()
        {
            AccessFailedCount = 0;
            EmailConfirmed = false;
            LockoutEnabled = true;
            PhoneNumberConfirmed = false;
            TwoFactorEnabled = false;
            Birthday = new DateTime(1971, 1, 1);
        }
    }

}
 