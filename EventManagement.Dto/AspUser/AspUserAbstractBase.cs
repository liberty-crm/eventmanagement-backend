using System;

namespace EventManagement.Dto
{
    public abstract class AspUserAbstractBase
    {
        /// <summary>
        /// AspUser Id.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// AspUser Name.
        /// </summary>
        public string Name { get; set; }

    }
}
