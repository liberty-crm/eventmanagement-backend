using System;

namespace EventManagement.Dto
{
    public abstract class ProvinceAbstractBase
    {
        /// <summary>
        /// Province Id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Province Name.
        /// </summary>
        public string Name { get; set; }

    }
}
