using System;

namespace EventManagement.Dto
{
    public abstract class AssetAbstractBase
    {
        /// <summary>
        /// Asset Id.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Asset Name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// asset/liability
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// asset valuation
        /// </summary>
        public string Valuation { get; set; }

        /// <summary>
        /// asset description
        /// </summary>
        public string Description { get; set; }

    }
}
