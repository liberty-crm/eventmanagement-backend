using System;
using System.ComponentModel.DataAnnotations;

namespace EventManagement.Dto
{
    /// <summary>
    /// ServiceOffered Update Model.
    /// </summary>
    public class ServiceOfferedForUpdate : ServiceOfferedAbstractBase
    {

    }
}
