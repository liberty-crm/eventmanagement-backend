using FinanaceManagement.API.Models;
using System;

namespace EventManagement.Dto
{
    public abstract class ServiceOfferedAbstractBase
    {
        /// <summary>
        /// ServiceOffered Id.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// ServiceOffered Name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// asset description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// ServiceOffered Code.
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// ServiceOffered Category.
        /// </summary>
        public string Category { get; set; }

        /// <summary>
        /// ServiceOffered ServiceManager.
        /// </summary>
        public string ServiceManagerId { get; set; }

        /// <summary>
        /// ServiceOffered Invoice Generation.
        /// </summary>
        public string InvoiceGeneration { get; set; }

        /// <summary>
        /// ServiceOffered Invoice Amount Calculation.
        /// </summary>
        public string InvoiceAmountCalculation { get; set; }

        /// <summary>
        /// ServiceOffered Value.
        /// </summary>
        public string Value { get; set; }

        public AspNetUsers User { get; set; }


    }
}
