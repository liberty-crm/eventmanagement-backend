using System;

namespace EventManagement.Dto
{
    public abstract class PaymentAbstractBase
    {
        /// <summary>
        /// Payment Id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Payment Date.
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Payment Mode.
        /// </summary>
        public string Mode { get; set; }

        /// <summary>
        /// Payment Location.
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        /// Payment amount
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Payment ReferenceDetails.
        /// </summary>
        public string ReferenceDetails { get; set; }


        /// <summary>
        /// Payment LeadServiceId.
        /// </summary>
        public string LeadServiceId { get; set; }

    }
}
