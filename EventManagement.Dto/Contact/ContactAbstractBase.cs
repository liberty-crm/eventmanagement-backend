using System;

namespace EventManagement.Dto
{
    public abstract class ContactAbstractBase
    {
        /// <summary>
        /// Contact Id.
        /// </summary>
        public string Id { get; set; }

        public string FirstName { get; set; }
        
        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public string Address { get; set; }

        public string City { get; set; }

        public string Province { get; set; }

        public string Zipcode { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public string Source { get; set; }

        public bool ExistingClient { get; set; }

        public string Employment { get; set; }

        public string Position { get; set; }

        public string CompanyName { get; set; }

        public string Website { get; set; }

        public DateTime? EmploymentDate { get; set; }

        public string SalaryFrequency { get; set; }

        public int NoOfChildren { get; set; }

        public string RelationWithLead { get; set; }

        public Guid? RelatedLeadContactId { get; set; }

        public string Status { get; set; } //Lead or Customer

        public bool? AllowMarketingMessages { get; set; }

        public bool? AllowUseOfPersonalInfo { get; set; }

        public string MaritalStatus { get; set; }

        public string Sin { get; set; }

        public float? SalaryAmount { get; set; }
    }
}
