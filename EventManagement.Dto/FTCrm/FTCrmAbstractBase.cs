using System;

namespace EventManagement.Dto
{
    public abstract class FTCrmAbstractBase
    {
        /// <summary>
        /// FTCrm Id.
        /// </summary>
        public Guid Id { get; set; }

        public string FtCrmUniqueId { get; set; }

        public Guid LeadServiceId { get; set; }        

        public string Status { get; set; }

        public float Amount { get; set; }

        public string Reason { get; set; }

    }
}
