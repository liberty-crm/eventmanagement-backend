﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EventManagement.Dto.FTCrm
{
    public class CrmData
    {
        public string opp_id { get; set; }

        public string stage_name { get; set; }

        public float? refund_amount { get; set; }
    }
}
