﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EventManagement.Dto.FTCrm
{
    public class FtCrmResponse
    {
        public int status { get; set; }
        
        public string message { get; set; }

        public string success { get; set; }

        public string error { get; set; }

        public Data data { get; set; }
    }
    public class Data
    {
        public string opp_id { get; set; }
    }
}
