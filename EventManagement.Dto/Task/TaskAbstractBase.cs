using System;

namespace EventManagement.Dto
{
    public abstract class TaskAbstractBase
    {
        /// <summary>
        ///  Task Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Task Title
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Task Date
        /// </summary>
        public DateTime? Date { get; set; }

        /// <summary>
        /// Task Compeleted/Not Completed
        /// </summary>
        public bool IsCompleted { get; set; }

        /// <summary>
        /// Task Description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Task Important/Not Important
        /// </summary>
        public bool IsImportant { get; set; }

        /// <summary>
        /// Task Marked/Not Marked
        /// </summary>
        public bool IsMarked { get; set; }

        /// <summary>
        /// Lead Id
        /// </summary>
        public string LeadId { get; set; }

        /// <summary>
        /// Service Id
        /// </summary>
        public string ServiceId { get; set; }

        /// <summary>
        /// User Id
        /// </summary>
        public string UserId { get; set; }

    }
}
