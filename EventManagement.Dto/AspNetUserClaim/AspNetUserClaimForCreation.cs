using System;
using System.ComponentModel.DataAnnotations;

namespace EventManagement.Dto
{
    /// <summary>
    ///     AspNetUserClaim For Creation
    /// </summary>
    public class AspNetUserClaimForCreation : AspNetUserClaimAbstractBase
    {
    }
}
