using System;

namespace EventManagement.Dto
{
    /// <summary>
    ///     AspNetUserClaim Abstract Base
    /// </summary>
    public abstract class AspNetUserClaimAbstractBase
    {
        /// <summary>
        /// AspNetUserClaim Id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        ///  AspNetUserClaim Claim Type
        /// </summary>
        public string ClaimType { get; set; }

        /// <summary>
        ///  AspNetUserClaim Claim Value
        /// </summary>
        public string ClaimValue { get; set; }

        /// <summary>
        ///  AspNetUserClaim User Id
        /// </summary>
        public Guid UserId { get; set; }


    }
}
