using System;

namespace EventManagement.Dto
{
    public abstract class ProductAbstractBase
    {
        /// <summary>
        /// Product Id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Product Name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Product Code.
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Product Category.
        /// </summary>
        public string Category { get; set; }

    }
}
