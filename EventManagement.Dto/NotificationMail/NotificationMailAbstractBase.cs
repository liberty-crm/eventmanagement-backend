using System;

namespace EventManagement.Dto
{
    public abstract class NotificationMailAbstractBase
    {
        /// <summary>
        /// NotificationMail Id.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// NotificationMail Name.
        /// </summary>
        public string Name { get; set; }

        public string MailContent { get; set; }

    }
}
