using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EventManagement.Dto
{
    /// <summary>
    /// Lead Update Model.
    /// </summary>
    public class LeadForUpdate : LeadAbstractBase
    {  

        /// <summary>
        /// Lead First Name
        /// </summary>       
        public string FirstName { get; set; }

        /// <summary>
        /// Lead Middle Name
        /// </summary>
        public string MiddleName { get; set; }

        /// <summary>
        /// Lead Last Name
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Lead Address
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Lead City
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Lead Province
        /// </summary>
        public string Province { get; set; }

        /// <summary>
        /// Zipcode
        /// </summary>
        public string Zipcode { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Phone
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Source
        /// </summary>
        public string Source { get; set; }

        /// <summary>
        /// ExistingClient
        /// </summary>
        public bool ExistingClient { get; set; }

        /// <summary>
        /// Employment
        /// </summary>
        public string Employment { get; set; }

        /// <summary>
        /// Position
        /// </summary>
        public string Position { get; set; }

        /// <summary>
        /// CompanyName
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// Website
        /// </summary>
        public string Website { get; set; }

        /// <summary>
        /// EmploymentDate
        /// </summary>
        public DateTime? EmploymentDate { get; set; }

        /// <summary>
        /// SalaryFrequency
        /// </summary>
        public string SalaryFrequency { get; set; }

        /// <summary>
        /// NoOfChildren
        /// </summary>
        public int NoOfChildren { get; set; }        

        /// <summary>
        /// CustomerInsight
        /// </summary>
        public string CustomerInsight { get; set; }

        /// <summary>
        /// AllowMarketingMessages
        /// </summary>
        public bool? AllowMarketingMessages { get; set; }

        public bool? AllowUseOfPersonalInfo { get; set; }

        public string Status { get; set; }

        public string ContactStatus { get; set; }

        public string AssignedTo { get; set; }

        public string AgentId { get; set; }

        public string AssignedStore { get; set; }

        /// <summary>
        /// SelectedAssetId
        /// </summary>
        public List<AssetDto> SelectedAssetId { get; set; }

        /// <summary>
        /// SelectedServiceId
        /// </summary>
        public List<ServiceOfferedDto> SelectedServiceId { get; set; }

        /// <summary>
        /// MaritalStatus
        /// </summary>
        public string MaritalStatus { get; set; }

        public string Sin { get; set; }

        /// <summary>
        /// SalaryAmount
        /// </summary>
        public float SalaryAmount { get; set; }

        public string ReferredBy { get; set; }

        public string LeadNumber { get; set; }

    }
}
