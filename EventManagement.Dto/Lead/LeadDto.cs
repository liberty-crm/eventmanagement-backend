using EventManagement.Domain.Entities;
using System;

namespace EventManagement.Dto
{
    /// <summary>
    /// Lead Model
    /// </summary>
    public class LeadDto : LeadAbstractBase
    {
        public string Status { get; set; }

        public string AssignedTo { get; set; }

        public string AgentId { get; set; }

        public string CustomerInsight { get; set; }

        public ContactDto ContactDto { get; set; }

        public string AssignedStore { get; set; }

        public string LeadNumber { get; set; }

    }

}
 