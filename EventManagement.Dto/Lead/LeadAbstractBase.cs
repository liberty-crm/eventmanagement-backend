using System;
using System.Collections.Generic;

namespace EventManagement.Dto
{
    /// <summary>
    /// Lead Abstract Base
    /// </summary>
    public abstract class LeadAbstractBase
    {
        public string Id { get; set; }
    }
}
