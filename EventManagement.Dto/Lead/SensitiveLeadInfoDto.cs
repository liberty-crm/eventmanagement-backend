﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EventManagement.Dto.Lead
{
    public class SensitiveLeadInfoDto
    {
        public string LeadID { get; set; }

        public string Token { get; set; }

        public string Sin { get; set; }
    }
}
