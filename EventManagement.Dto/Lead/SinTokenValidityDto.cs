﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EventManagement.Dto.Lead
{
    public class SinTokenValidityDto
    {
        public bool IsValid { get; set; }

        public string Message { get; set; }
    }
}
