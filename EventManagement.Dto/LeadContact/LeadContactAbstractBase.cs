using System;

namespace EventManagement.Dto
{
    public abstract class LeadContactAbstractBase
    {
        /// <summary>
        /// LeadContact Id.
        /// </summary>
        
        public Guid Id { get; set; }

        public Guid ContactId { get; set; }

        public Guid LeadId { get; set; }

    }
}
