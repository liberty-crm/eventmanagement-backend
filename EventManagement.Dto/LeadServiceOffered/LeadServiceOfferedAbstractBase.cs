using System;

namespace EventManagement.Dto
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class LeadServiceOfferedAbstractBase
    {
        /// <summary>
        /// LeadServiceOffered Id.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// LeadServiceOffered Description 
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// ServiceOffered Id.
        /// </summary>
        public string ServiceOfferedId { get; set; }

        /// <summary>
        /// LeadServiceOffered Name.
        /// </summary>
        public string LeadId { get; set; }

        /// <summary>
        /// Contact Id
        /// </summary>
        public string ContactId { get; set; }

        /// <summary>
        /// LeadServiceOffered Assigned To. 
        /// </summary>
        public string AssignedTo { get; set; }

        /// <summary>
        /// LeadServiceOffered Assigned Status. 
        /// </summary>
        public string AssignedStatus { get; set; }

    }
}
