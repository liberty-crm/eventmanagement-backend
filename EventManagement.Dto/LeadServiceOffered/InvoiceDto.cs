﻿using EventManagement.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace EventManagement.Dto.LeadServiceOffered
{
    public class InvoiceDto
    {
        public string InvoiceNumber { get; set; }
        public DateTime IssueDate { get; set; }
        public DateTime DueDate { get; set; }

        public InvoiceFrom From { get; set; }

        public InvoiceClient Client { get; set; }

        public InvoiceService InvoiceService { get; set; }

        public float HSTAmount { get; set; }

        public float TotalAmount { get; set; }

    }

    public class InvoiceFrom
    {
        public string Title { get; set; }

        public string Address { get; set; }

        public string PhoneNumber { get; set; }

        public string Email { get; set; }

        public string Website { get; set; }
    }

    public class InvoiceClient
    {
        public string Title { get; set; }

        public string Address { get; set; }

        public string PhoneNumber { get; set; }

        public string Email { get; set; }

        public Province ProvinceDetails { get; set; }
    }

    public class InvoiceService
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public int Qty { get; set; }

        public float Rate { get; set; }

        public float Total { get; set; }
    }

}
