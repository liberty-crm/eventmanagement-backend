using System;
using System.ComponentModel.DataAnnotations;

namespace EventManagement.Dto
{
    /// <summary>
    /// LeadServiceOffered Update Model.
    /// </summary>
    public class LeadServiceOfferedForUpdate : LeadServiceOfferedAbstractBase
    {

    }
}
