﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EventManagement.Dto.Report
{
    public class OperationReport
    {
        public string Name { get; set; }
        public string Number { get; set; }
        public double RefundAmount { get; set; }
        public double HST { get; set; }
        public double InvoiceAmount { get; set; }
        public string Paid { get; set; }
        public double FamilyTaxCharge { get; set; }
        public double StanFinal { get; set; }
        public double LFGAmount { get; set; }
        public double LFGHST { get; set; }
        public string Rep { get; set; }
        public double RepPercentage { get; set; }
        public double RepAmount { get; set; }
        public double RepHST { get; set; }
        public double LFGFinalAmount { get; set; }
        public double LFGFinalHST { get; set; }
        public DateTime InvoiceDate { get; set; }

        public string Email { get; set; }

    }
}
