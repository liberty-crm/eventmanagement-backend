using System;

namespace EventManagement.Dto
{
    public abstract class ReportAbstractBase
    {
        /// <summary>
        /// Report Id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Report Name.
        /// </summary>
        public string Name { get; set; }

    }
}
