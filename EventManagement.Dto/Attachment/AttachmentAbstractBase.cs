using System;

namespace EventManagement.Dto
{
    public abstract class AttachmentAbstractBase
    {
        /// <summary>
        /// Attachment Id.
        /// </summary>
        public string Id { get; set; }

        public string LeadId { get; set; }

        public string ServiceId { get; set; }

        public string UserId { get; set; }

        public string Type { get; set; }

        public string OriginalDocumentName { get; set; }

        public string UploadedDocumentName { get; set; }

        public string UploadPath { get; set; }

    }
}
