using System;

namespace EventManagement.Dto
{
    public abstract class ServiceOfferedInvoiceAbstractBase
    {
        /// <summary>
        /// ServiceOfferedInvoice Id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Inovice number
        /// </summary>
        public int InvoiceNumber { get; set; }

        public Guid LeadServiceOfferedId { get; set; }

        public float Amount { get; set; }

        public DateTime? SentDate { get; set; }

        public string AttachmentPath { get; set; }       

    }
}
