using System;

namespace EventManagement.Dto
{
    /// <summary>
    /// LeadAsset Abstract Base
    /// </summary>
    public abstract class LeadAssetAbstractBase
    {
        /// <summary>
        /// LeadAsset Id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Lead Id
        /// </summary>
        public Guid LeadId { get; set; }

        /// <summary>
        /// Asset Id.
        /// </summary>
        public Guid AssetId { get; set; }

    }
}
