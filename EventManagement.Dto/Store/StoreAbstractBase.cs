using System;

namespace EventManagement.Dto
{
    public abstract class StoreAbstractBase
    {
        /// <summary>
        /// Store Id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Store Name.
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Store Name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Store Name.
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Store Name.
        /// </summary>
        public string Zipcode { get; set; }

        /// <summary>
        /// Store Name.
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Store Name.
        /// </summary>
        public string Province { get; set; }

        /// <summary>
        /// Store Name.
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// Store Name.
        /// </summary>
        public string Parentdistrict { get; set; }

    }
}
