﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Marvin.IDP.Model
{
    public class ShowRecoveryCodesViewModel
    {
        public List<string> RecoveryCodes { get; set; }
    }
}
