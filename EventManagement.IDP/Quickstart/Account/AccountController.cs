using IdentityModel;
using IdentityServer4.Events;
using IdentityServer4.Extensions;
using IdentityServer4.Models;
using IdentityServer4.Services;
using IdentityServer4.Stores;
using Marvin.IDP;
using Marvin.IDP.Entities;
using Marvin.IDP.Model;
using Marvin.IDP.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;

namespace IdentityServer4.Quickstart.UI
{
    [SecurityHeaders]
    [AllowAnonymous]
    public class AccountController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IIdentityServerInteractionService _interaction;
        private readonly IClientStore _clientStore;
        private readonly IAuthenticationSchemeProvider _schemeProvider;
        private readonly IEventService _events;
        private readonly UrlEncoder _urlEncoder;
        private readonly ILogger _logger;
        private const string AuthenticatorUriFormat = "otpauth://totp/{0}:{1}?secret={2}&issuer={0}&digits=6";
        private const string RecoveryCodesKey = nameof(RecoveryCodesKey);
        //private readonly IMarvinUserRepository _marvinUserRepository;
        private readonly IPersistedGrantStore _persistedGrantStore;

        private IApplicationAccountRepository _applicationAccountRepository;
        /// <summary>
        /// The claims factory.
        /// </summary>
        protected readonly IUserClaimsPrincipalFactory<User> _claimsFactory;
        private IConfiguration _configuration;

        public AccountController(
            IApplicationAccountRepository applicationAccountRepository,
            UrlEncoder urlEncoder,
            IPersistedGrantStore persistedGrantStore,
            UserManager<User> userManager,
            SignInManager<User> signInManager,
            IIdentityServerInteractionService interaction,
            IClientStore clientStore,
            IAuthenticationSchemeProvider schemeProvider,
            IUserClaimsPrincipalFactory<User> userClaimsPrincipalFactory,
            IEventService events,
            ILogger<AccountController> logger,
            IConfiguration configuration)
        {
            _applicationAccountRepository = applicationAccountRepository;
            _urlEncoder = urlEncoder;
            _userManager = userManager;
            //_userManager.PasswordHasher = new MyPasswordHasher();
            _signInManager = signInManager;
            // if the TestUserStore is not in DI, then we'll just use the global users collection
            // this is where you would plug in your own custom identity management library (e.g. ASP.NET Identity)
            //  _marvinUserRepository = marvinUserRepository;
            _persistedGrantStore = persistedGrantStore;
            _interaction = interaction;
            _clientStore = clientStore;
            _schemeProvider = schemeProvider;
            _claimsFactory = userClaimsPrincipalFactory;
            _events = events;
            _logger = logger;
            _configuration = configuration;
        }

        [HttpGet]
        public async Task<IActionResult> GetAccount(string returnUrl)
        {
            var accounts = _applicationAccountRepository.GetAccounts().ToList();

            return Ok(accounts);
        }



        /// <summary>
        /// Entry point into the login workflow
        /// </summary>
        [HttpGet]
        public async Task<IActionResult> Login(string returnUrl)
        {
            // build a model so we know what to show on the login page
            var vm = await BuildLoginViewModelAsync(returnUrl);

            if (vm.IsExternalLoginOnly)
            {
                // we only have one option for logging in and it's an external provider
                return RedirectToAction("Challenge", "External", new { provider = vm.ExternalLoginScheme, returnUrl });
            }

            return View(vm);
        }

        /// <summary>
        /// Handle postback from username/password login
        /// </summary>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginInputModel model, string button)
        {
            // check if we are in the context of an authorization request
            var context = await _interaction.GetAuthorizationContextAsync(model.ReturnUrl);

            // the user clicked the "cancel" button
            if (button != "login")
            {
                if (context != null)
                {
                    // if the user cancels, send a result back into IdentityServer as if they 
                    // denied the consent (even if this client does not require consent).
                    // this will send back an access denied OIDC error response to the client.
                    await _interaction.GrantConsentAsync(context, ConsentResponse.Denied);

                    // we can trust model.ReturnUrl since GetAuthorizationContextAsync returned non-null
                    if (await _clientStore.IsPkceClientAsync(context.ClientId))
                    {
                        // if the client is PKCE then we assume it's native, so this change in how to
                        // return the response is for better UX for the end user.
                        return View("Redirect", new RedirectViewModel { RedirectUrl = model.ReturnUrl });
                    }

                    return Redirect(model.ReturnUrl);
                }
                else
                {
                    // since we don't have a valid context, then we just go back to the home page
                    return Redirect("~/");
                }
            }

            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByNameAsync(model.Username);
                if (user != null && !user.IsActive)
                {
                     ModelState.AddModelError("", "User is disabled");
                     return View();
                }
                if (user != null && !await _userManager.IsLockedOutAsync(user))
                {
                    if (await _userManager.CheckPasswordAsync(user, model.Password))
                    {
                        if (!await _userManager.IsEmailConfirmedAsync(user))
                        {
                            ModelState.AddModelError("", "Email is not confirmed");
                            return View();
                        }

                        await _userManager.ResetAccessFailedCountAsync(user);

                        var principal = await _claimsFactory.CreateAsync(user);

                        var result = await _signInManager.PasswordSignInAsync(model.Username, model.Password, model.RememberLogin, lockoutOnFailure: false);
                        if (result.RequiresTwoFactor)
                        {
                            TempData["UserName"] = model.Username;
                            TempData["RedirectUrl"] = model.ReturnUrl;
                            return RedirectToAction(nameof(AuthenticatorType));
                        }

                        await HttpContext.SignInAsync(IdentityConstants.ApplicationScheme, principal);

                        return Redirect(model.ReturnUrl);
                    }

                    await _userManager.AccessFailedAsync(user);

                    if (await _userManager.IsLockedOutAsync(user))
                    {
                        // email user, notifying them of lockout
                    }
                }

                ModelState.AddModelError("", "Invalid UserName or Password");
            }

            // something went wrong, show form with error
            var vm = await BuildLoginViewModelAsync(model);
            return View(vm);
        }

        [HttpGet]
        public async Task<IActionResult> AuthenticatorType()
        {
            return View();
        }

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> AuthenticatorType(string authenticatorType)
        //{
        //    if (authenticatorType == "1")
        //    {
        //        return View("TwoFactor");
        //    }
        //    else if (authenticatorType == "2")
        //    {
        //        return View("EnableAuthenticator");
        //    }
        //    else
        //    {
        //        return View();
        //    }

        //}

        [HttpGet]
        public async Task<IActionResult> TwoFactor()
        {
            //var user = await _userManager.GetUserAsync(User);GetTwoFactorAuthenticationUserAsync
            var user = await _userManager.FindByNameAsync(TempData["UserName"].ToString());
            TempData.Keep();
            var token = await _userManager.GenerateTwoFactorTokenAsync(user, "Email");
            // System.IO.File.WriteAllText("email2sv.txt", token);

            var htmlContent = "<table width =\"350\" align=\"center\">" +
                        "<tbody><tr><td style =\" text-align: center;\">" +
                        "<img src=\"https://lfgcrmstorage.blob.core.windows.net/lfgcrmcontainer/Logo.png \" width=\"150\" height=\"68\"/></td></tr> " +
                        "<tr><td style=\"text-align: left;\">" +
                        "<p style=\"padding-left: 30px;\"><strong>Dear " + user.FName + " " + user.LName + ",</strong></p>" +
                        "<p style=\"padding-left: 30px;\">You have logged into Liberty Financial Group CRM. In order to proceed further, you will need to enter a one-time passcode to confirm that its really you. This passcode will expire in 5 minutes.</p>" +
                        "<p style=\"padding-left: 30px;\"><strong>Your One-Time Passcode is <br><code style=\"color:red; font-size:20px;\">" + token + "</code></strong></p>" +
                        "<p style=\"padding-left: 30px; color:red\">Please enter the code into the from for which you have requested access. Thank you for utilizing our service.</p>" +
                        "<p style=\"padding-left: 30px;\">This is a notification-only email, please don't reply to this message. </p>" +
                "<p style=\"padding-left: 30px;\"> If you have any questions, please feel free to contact by clicking here <span style=\"color: #004284;\">help center</span>.</p>" +
                "<p style=\"padding-left: 30px;\">Sincerely,</p>" +
                "<p style=\"padding-left: 30px;\"><strong>- The LFG Team  </strong></p> " +
                        "</td>" +
                        "</tr>" +
                        "</tbody>" +
                        "</table>";


            var client = new SendGridClient(_configuration.GetSection("SendgridApi").Value);
            var msg = MailHelper.CreateSingleEmail(new EmailAddress("support@libertyfinancialgroup.ca"), new EmailAddress(user.Email),
                "2 Factor Authentication", token, htmlContent);
            var response = client.SendEmailAsync(msg);
            await HttpContext.SignInAsync(IdentityConstants.TwoFactorUserIdScheme, Store2FA(user.Id, "Email"));

            return View();
        }
        [HttpPost]
        public async Task<IActionResult> TwoFactor(TwoFactorModel model)
        {
            var result = await HttpContext.AuthenticateAsync(IdentityConstants.TwoFactorUserIdScheme);
            if (!result.Succeeded)
            {
                ModelState.AddModelError("", "You login request has expired, please start over");
                return View("Login");
            }

            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByIdAsync(result.Principal.FindFirstValue("sub"));

                if (user != null)
                {
                    var isValid = await _userManager.VerifyTwoFactorTokenAsync(user,
                        result.Principal.FindFirstValue("amr"), model.Token);

                    if (isValid)
                    {
                        await HttpContext.SignOutAsync(IdentityConstants.TwoFactorUserIdScheme);

                        var claimsPrincipal = await _claimsFactory.CreateAsync(user);
                        await HttpContext.SignInAsync(IdentityConstants.ApplicationScheme, claimsPrincipal);


                        return Redirect(TempData["RedirectUrl"].ToString());
                        TempData.Keep();
                        // return RedirectToAction("Index");
                    }

                    ModelState.AddModelError("", "Invalid token");
                    return View();
                }

                ModelState.AddModelError("", "Invalid Request");
            }

            return View();
        }

        [HttpGet]
        public async Task<IActionResult> EnableAuthenticator(bool isValidatorCheck = true, string returnUrl = "", string email = "")
        {
            User user;
            //var user = await _userManager.GetUserAsync(User);
            if (!String.IsNullOrEmpty(email))
            {
                user = await _userManager.FindByNameAsync(email);
                //await _signInManager.RefreshSignInAsync(user);
            }
            else
            {
                user = await _userManager.FindByNameAsync(TempData["UserName"].ToString());
                TempData.Keep();
                //await _signInManager.RefreshSignInAsync(user);
            }

            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }
            if (isValidatorCheck)
            {
                var validProviders = await _userManager.GetValidTwoFactorProvidersAsync(user);

                if (validProviders.Contains(_userManager.Options.Tokens.AuthenticatorTokenProvider))
                {
                    return RedirectToAction(nameof(LoginWith2fa));
                }
                else
                {
                    var model = new EnableAuthenticatorViewModel();
                    await LoadSharedKeyAndQrCodeUriAsync(user, model);
                    return View(model);
                }
            }
            else
            {
                TempData["RedirectUrl"] = returnUrl;
                TempData["UserName"] = email;
                await _userManager.ResetAuthenticatorKeyAsync(user);
                var model = new EnableAuthenticatorViewModel();
                await LoadSharedKeyAndQrCodeUriAsync(user, model);
                return View(model);
            }
        }

        [HttpPost]
        public async Task<IActionResult> EnableAuthenticator(EnableAuthenticatorViewModel model)
        {

            User user;

            user = await _userManager.FindByNameAsync(TempData["UserName"].ToString());
            TempData.Keep();
            //var user = await _signInManager.GetTwoFactorAuthenticationUserAsync();
            TempData.Keep();
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            if (!ModelState.IsValid)
            {
                await LoadSharedKeyAndQrCodeUriAsync(user, model);
                return View(model);
            }

            // Strip spaces and hypens
            var verificationCode = model.Code.Replace(" ", string.Empty).Replace("-", string.Empty);

            var is2faTokenValid = await _userManager.VerifyTwoFactorTokenAsync(
                user, _userManager.Options.Tokens.AuthenticatorTokenProvider, verificationCode);

            if (!is2faTokenValid)
            {
                ModelState.AddModelError("Code", "Invalid verification code.");
                await LoadSharedKeyAndQrCodeUriAsync(user, model);
                return View(model);
            }

            await _userManager.SetTwoFactorEnabledAsync(user, true);
            _logger.LogInformation("User with ID {UserId} has enabled 2FA with an authenticator app.", user.Id);
            var recoveryCodes = await _userManager.GenerateNewTwoFactorRecoveryCodesAsync(user, 5);
            TempData[RecoveryCodesKey] = recoveryCodes.ToArray();
            var codes = new ShowRecoveryCodesViewModel();
            codes.RecoveryCodes = recoveryCodes.ToList();
            return View("ShowRecoveryCodes", codes);
        }

        [HttpPost]
        public async Task<IActionResult> Disable2FA(bool value, string email)
        {
            var user = await _userManager.FindByNameAsync(email);

            if (!value)
            {
                await _userManager.ResetAuthenticatorKeyAsync(user);
                //await _userManager.RemoveAuthenticationTokenAsync(user, "[AspNetUserStore]", _userManager.Options.Tokens.AuthenticatorTokenProvider);
            }

            var result = await _userManager.SetTwoFactorEnabledAsync(user, value);

            //return new ResultVM
            //{
            //    Status = result.Succeeded ? Status.Success : Status.Error,
            //    Message = result.Succeeded ? "2FA has been successfully disabled" : $"Failed to disable 2FA {result.Errors.FirstOrDefault()?.Description}"
            //};
            return Ok();
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> LoginWith2fa()
        {
            var model = new LoginWith2faViewModel();
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LoginWith2fa(LoginWith2faViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = await _userManager.FindByNameAsync(TempData["UserName"].ToString());
            TempData.Keep();
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            var authenticatorCode = model.TwoFactorCode.Replace(" ", string.Empty).Replace("-", string.Empty);

            var is2faTokenValid = await _userManager.VerifyTwoFactorTokenAsync(
                user, _userManager.Options.Tokens.AuthenticatorTokenProvider, authenticatorCode);

            if (is2faTokenValid)
            {
                _logger.LogInformation("User with ID {UserId} logged in with 2fa.", user.Id);

                await HttpContext.SignOutAsync(IdentityConstants.TwoFactorUserIdScheme);
                var claimsPrincipal = await _claimsFactory.CreateAsync(user);
                await HttpContext.SignInAsync(IdentityConstants.ApplicationScheme, claimsPrincipal);

                var returnURL = TempData["RedirectUrl"].ToString();
                TempData.Keep();

                return Redirect(returnURL);
            }
            else
            {
                _logger.LogWarning("Invalid authenticator code entered for user with ID {UserId}.", user.Id);
                ModelState.AddModelError("Code", "Invalid authenticator code.");
                return View();
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> LoginWithRecoveryCode()
        {
            // Ensure the user has gone through the username & password screen first
            var user = await _userManager.FindByNameAsync(TempData["UserName"].ToString());
            TempData.Keep();
            if (user == null)
            {
                throw new ApplicationException($"Unable to load two-factor authentication user.");
            }
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LoginWithRecoveryCode(LoginWithRecoveryCodeViewModel model, string returnUrl = null)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = await _userManager.FindByNameAsync(TempData["UserName"].ToString());
            TempData.Keep();
            if (user == null)
            {
                throw new ApplicationException($"Unable to load two-factor authentication user.");
            }

            var recoveryCode = model.RecoveryCode.Replace(" ", string.Empty);

            var result = await _signInManager.TwoFactorRecoveryCodeSignInAsync(recoveryCode);

            if (result.Succeeded)
            {
                _logger.LogInformation("User with ID {UserId} logged in with a recovery code.", user.Id);
                return Redirect("GenerateRecoveryCodesWarning");
            }
            else
            {
                _logger.LogWarning("Invalid recovery code entered for user with ID {UserId}", user.Id);
                ModelState.AddModelError(string.Empty, "Invalid recovery code entered.");
                return View();
            }
        }

        [HttpGet]
        public async Task<IActionResult> grants()
        {
            var user = await _userManager.FindByNameAsync(TempData["UserName"].ToString());
            await HttpContext.SignOutAsync(IdentityConstants.TwoFactorUserIdScheme);
            var claimsPrincipal = await _claimsFactory.CreateAsync(user);
            await HttpContext.SignInAsync(IdentityConstants.ApplicationScheme, claimsPrincipal);

            var returnURL = TempData["RedirectUrl"].ToString();
            TempData.Keep();
            return Redirect(returnURL);
        }
        private async Task LoadSharedKeyAndQrCodeUriAsync(User user, EnableAuthenticatorViewModel model)
        {
            var unformattedKey = await _userManager.GetAuthenticatorKeyAsync(user);
            if (string.IsNullOrEmpty(unformattedKey))
            {
                await _userManager.ResetAuthenticatorKeyAsync(user);
                unformattedKey = await _userManager.GetAuthenticatorKeyAsync(user);
            }

            model.SharedKey = FormatKey(unformattedKey);
            model.AuthenticatorUri = GenerateQrCodeUri(user.Email, unformattedKey);
        }

        private string FormatKey(string unformattedKey)
        {
            var result = new StringBuilder();
            int currentPosition = 0;
            while (currentPosition + 4 < unformattedKey.Length)
            {
                result.Append(unformattedKey.Substring(currentPosition, 4)).Append(" ");
                currentPosition += 4;
            }
            if (currentPosition < unformattedKey.Length)
            {
                result.Append(unformattedKey.Substring(currentPosition));
            }

            return result.ToString().ToLowerInvariant();
        }

        private string GenerateQrCodeUri(string email, string unformattedKey)
        {
            return string.Format(
                AuthenticatorUriFormat,
                _urlEncoder.Encode("VNC"),
                _urlEncoder.Encode(email),
                unformattedKey);
        }
        private ClaimsPrincipal Store2FA(string userId, string provider)
        {
            var identity = new ClaimsIdentity(new List<Claim>
            {
                new Claim("sub", userId),
                new Claim("amr", provider)
            }, IdentityConstants.TwoFactorUserIdScheme);

            return new ClaimsPrincipal(identity);
        }

        [HttpGet]
        public async Task<IActionResult> GenerateRecoveryCodesWarning()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> GenerateRecoveryCodes()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            if (!user.TwoFactorEnabled)
            {
                throw new ApplicationException($"Cannot generate recovery codes for user with ID '{user.Id}' as they do not have 2FA enabled.");
            }

            var recoveryCodes = await _userManager.GenerateNewTwoFactorRecoveryCodesAsync(user, 5);
            TempData[RecoveryCodesKey] = recoveryCodes.ToArray();
            var codes = new ShowRecoveryCodesViewModel();
            codes.RecoveryCodes = recoveryCodes.ToList();
            return View("ShowRecoveryCodes", codes);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SendEmail()
        {
            //var user = await _userManager.GetUserAsync(User);GetTwoFactorAuthenticationUserAsync
            var user = await _userManager.FindByNameAsync(TempData["UserName"].ToString());
            TempData.Keep();

            string[] codesArray = (string[])TempData[RecoveryCodesKey];
            string codes = "<table width =\"350\" align=\"center\">" +
                        "<tbody><tr><td style =\" text-align: center;\">" +
                        "<img src=\"https://lfgcrmstorage.blob.core.windows.net/lfgcrmcontainer/Logo.png \" width=\"150\" height=\"68\"/></td></tr> " +
                        "<tr><td style=\"text-align: left;\">" +
                        "<p style=\"padding-left: 30px;\">Please keep your recovery codes safe.</p>" +
                        "<p style=\"padding-left: 30px;\">Each can be used only once.</p>" +
                        "<p style=\"padding-left: 30px;\">";

            string codesHTML = "<table width =\"350\" align=\"center\">" +
                        "<tbody><tr><td style =\" text-align: center;\">" +
                        "<img src=\"https://lfgcrmstorage.blob.core.windows.net/lfgcrmcontainer/Logo.png \" width=\"150\" height=\"68\"/></td></tr> " +
                        "<tr><td style=\"text-align: left;\">" +
                        "<p style=\"padding-left: 30px;\">Please keep your recovery codes safe.</p>" +
                        "<p style=\"padding-left: 30px;\">Each can be used only once.</p>" +
                        "<p style=\"padding-left: 30px;\">";


            foreach (var code in codesArray)
            {
                codes = codes + code + " , ";

                codesHTML = codesHTML + "<br /><code style=\"color:red; padding-left: 30px;\">" + code + "</code>";

            }
            codes = codes +
                       "<br /></p>" +
                       "<p style=\"padding-left: 30px;\">We love hearing from you!</p>" +
                       "<p style=\"padding-left: 30px;\">Have any question? Please checkout our <span style=\"color: #004284;\">help center</span>.</p>" +
                       "<p style=\"padding-left: 30px;\">Thanks</p>" +
                       "<p style=\"padding-left: 30px;\"><strong>Liberty Financial Group </strong></p> " +
                       "</td>" +
                       "</tr>" +
                       "</tbody>" +
                       "</table>";

            codesHTML = codesHTML +
                        "<br /></p>" +
                        "<p style=\"padding-left: 30px;\">We love hearing from you!</p>" +
                        "<p style=\"padding-left: 30px;\">Have any question? Please checkout our <span style=\"color: #004284;\">help center</span>.</p>" +
                        "<p style=\"padding-left: 30px;\">Thanks</p>" +
                        "<p style=\"padding-left: 30px;\"><strong>Liberty Financial Group </strong></p> " +
                        "</td>" +
                        "</tr>" +
                        "</tbody>" +
                        "</table>";

            var client = new SendGridClient(_configuration.GetSection("SendgridApi").Value);
            var msg = MailHelper.CreateSingleEmail(new EmailAddress("support@libertyfinancialgroup.ca"), new EmailAddress(user.UserName),
                "Recovery Codes", codes, codesHTML);
            var response = await client.SendEmailAsync(msg);

            var recoverycodes = new ShowRecoveryCodesViewModel();
            recoverycodes.RecoveryCodes = codesArray.ToList();
            return View("ShowRecoveryCodes", recoverycodes);
        }

        [HttpPost]
        public async Task<IActionResult> Create(CreateUserModel modelUser)
        {
            try
            {
                User user;
                if (String.IsNullOrEmpty(modelUser.ID))
                {
                    var isUserExist = await _userManager.FindByNameAsync(modelUser.Email);
                    if (isUserExist != null)
                    {

                        if (isUserExist.Database != null || isUserExist.Database != "" && isUserExist.Database != modelUser.Database)
                        {
                            isUserExist.Database = isUserExist.Database + "|" + modelUser.Database;
                        }
                        else
                        {
                            isUserExist.Database = modelUser.Database;
                        }

                        await _userManager.UpdateAsync(isUserExist);

                        var code = await _userManager.GenerateEmailConfirmationTokenAsync(isUserExist);
                        var callbackUrl = _configuration.GetSection("FrontendUrl").Value;

                        var confirmButton = "<table width =\"350\" align=\"center\">" +
                        "<tbody><tr><td style =\" text-align: center;\">" +
                        "<img src=\"https://lfgcrmstorage.blob.core.windows.net/lfgcrmcontainer/Logo.png \" width=\"150\" height=\"68\"/></td></tr> " +
                        "<tr><td style=\"text-align: left;\">" +
                        "<p style=\"padding-left: 30px;\"><strong> " + modelUser.AdminName + " </strong> is inviting you to access the<strong> Liberty Financial Group Portal.</strong></p> " +
                        "<p style=\"padding-left: 30px;\">And start looking at business transformation reports.</p>" +
                        "<p style=\"padding-left: 30px;\">Please accept your invite by clicking below.</p>" +
                        "<br />" +
                        "<table class=\"buttonwrapper\" style=\"background-color:#004284;\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\">" +
                        "<tbody>" +
                        "<tr>" +
                        "<td class=\"button\" style=\"text-align: center; font-size: 18px; font-family: sans-serif; font-weight: bold; padding: 0 30px 0 30px;\" height=\"45\">" +
                        "<a style=\"color: #fff; text-decoration: none;\" href ='" + callbackUrl + "'>Accept Invitation</a></td>" +
                        "</tr>" +
                        "</tbody>" +
                        "</table>" +
                        "<br />" +
                        "<p style=\"padding-left: 30px;\">We love hearing from you!</p>" +
                        "<p style=\"padding-left: 30px;\">Have any question? Please checkout our <span style=\"color: #004284;\">help center</span>.</p>" +
                        "<p style=\"padding-left: 30px;\">Thanks</p>" +
                        "<p style=\"padding-left: 30px;\"><strong>Liberty Financial Group </strong></p> " +
                        "</td>" +
                        "</tr>" +
                        "</tbody>" +
                        "</table>";

                        var client = new SendGridClient(_configuration.GetSection("SendgridApi").Value);
                        var msg = MailHelper.CreateSingleEmail(new EmailAddress("support@libertyfinancialgroup.ca"), new EmailAddress(isUserExist.Email),
                            "Confirm Email", callbackUrl, confirmButton);
                        var response = client.SendEmailAsync(msg);

                        return Ok(isUserExist.Id);
                    }
                    else
                    {
                        //Create
                        user = new Marvin.IDP.Entities.User();
                        //user.PasswordHash = "Vadera@2019";
                        user.UserName = modelUser.Email;
                        user.Email = modelUser.Email;
                        user.NormalizedEmail = modelUser.Email;
                        user.NormalizedUserName = modelUser.Email;
                        user.IsActive = modelUser.IsActive;
                        user.Database = modelUser.Database;
                        //List<UserClaim> userClaims = new List<UserClaim>();

                        ////Add Basic claims
                        //userClaims.Add(new UserClaim("given_name", modelUser.FirstName));
                        //userClaims.Add(new UserClaim("given_lname", modelUser.LastName));
                        //userClaims.Add(new UserClaim(JwtClaimTypes.Email, modelUser.Email));

                        //// Add the claims to the user object
                        //user.Claims = userClaims;

                        //await _userManager.CreateAsync(user, "Vadera@2019");
                        var result = await _userManager.CreateAsync(user, "Edge@2020");

                        // Adding User Claims

                        Claim firstNameClaim;
                        firstNameClaim = new Claim("given_name", modelUser.FirstName);
                        await _userManager.AddClaimAsync(user, firstNameClaim);

                        Claim lastNameClaim;
                        lastNameClaim = new Claim("given_lname", modelUser.LastName);
                        await _userManager.AddClaimAsync(user, lastNameClaim);

                        if (result.Succeeded)
                        {
                            var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                            var callbackUrl = Url.Action(
                               "ConfirmEmail", "Account",
                               new { userId = user.Id, code = code, returnUrl = modelUser.ReturnURL },
                               protocol: Request.Scheme);

                            var confirmButton = "<table width =\"350\" align=\"center\">" +
                         "<tbody><tr><td style =\" text-align: center;\">" +
                         "<img src=\"https://lfgcrmstorage.blob.core.windows.net/lfgcrmcontainer/Logo.png \" width=\"150\" height=\"68\"/></td></tr> " +
                         "<tr><td style=\"text-align: left;\">" +
                         "<p style=\"padding-left: 30px;\"><strong> " + modelUser.AdminName + " </strong> is inviting you to access the<strong> Liberty CRM Portal </strong></p> " +
                         "<p style=\"padding-left: 30px;\">And start looking at business transformation reports.</p>" +
                         "<p style=\"padding-left: 30px;\">your password => <code>Edge@2020</code></p>" +
                         "<p style=\"padding-left: 30px;\">Please accept your invite by clicking below.</p>" +
                         "<br />" +
                         "<table class=\"buttonwrapper\" style=\"background-color:#004284;\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\">" +
                         "<tbody>" +
                         "<tr>" +
                         "<td class=\"button\" style=\"text-align: center; font-size: 18px; font-family: sans-serif; font-weight: bold; padding: 0 30px 0 30px;\" height=\"45\">" +
                         "<a style=\"color: #fff; text-decoration: none;\" href ='" + callbackUrl + "'>Confirm My Email</a></td>" +
                         "</tr>" +
                         "</tbody>" +
                         "</table>" +
                         "<br />" +
                         "<p style=\"padding-left: 30px;\">We love hearing from you!</p>" +
                         "<p style=\"padding-left: 30px;\">Have any question? Please checkout our <span style=\"color: #004284;\">help center</span>.</p>" +
                         "<p style=\"padding-left: 30px;\">Thanks</p>" +
                         "<p style=\"padding-left: 30px;\"><strong>Liberty Financial Group </strong></p> " +
                         "</td>" +
                         "</tr>" +
                         "</tbody>" +
                         "</table>";

                            var client = new SendGridClient(_configuration.GetSection("SendgridApi").Value);
                            var msg = MailHelper.CreateSingleEmail(new EmailAddress("support@libertyfinancialgroup.ca"), new EmailAddress(user.UserName),
                                "Confirm Email", callbackUrl, confirmButton);
                            var response = client.SendEmailAsync(msg);

                            return Ok(user.Id);
                        }
                    }
                }
                else
                {
                    // Update
                    user = await _userManager.FindByIdAsync(modelUser.ID);
                }

                // Only database will be updated for Create and Update scenarios
                if (!String.IsNullOrEmpty(modelUser.Database))
                {
                    user.Database = modelUser.Database;
                }
                // Only Birthday will be updated for Create and Update scenarios
                if (!String.IsNullOrEmpty(modelUser.Birthday))
                {
                    string iDate = modelUser.Birthday;
                    user.Birthday = DateTime.ParseExact(iDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                }

                // Only Birthplace will be updated for Create and Update scenarios
                if (!String.IsNullOrEmpty(modelUser.Birthplace))
                {
                    user.Birthplace = modelUser.Birthplace;
                }

                // Only Gender will be updated for Create and Update scenarios
                if (!String.IsNullOrEmpty(modelUser.Gender))
                {
                    user.Gender = modelUser.Gender;
                }

                // Only Occupation will be updated for Create and Update scenarios
                if (!String.IsNullOrEmpty(modelUser.Occupation))
                {
                    user.Occupation = modelUser.Occupation;
                }

                // Only PhoneNumber will be updated for Create and Update scenarios
                if (!String.IsNullOrEmpty(modelUser.PhoneNumber))
                {
                    user.PhoneNumber = modelUser.PhoneNumber;
                }

                // Only LivesIn will be updated for Create and Update scenarios
                if (!String.IsNullOrEmpty(modelUser.LivesIn))
                {
                    user.LivesIn = modelUser.LivesIn;
                }
                //// Only Password will be updated for Create and Update scenarios
                //if (!String.IsNullOrEmpty(modelUser.Password))
                //{
                //    user.Password = modelUser.Password;
                //}

                #region Update Claims
                //Add Basic claims
                var claims = await _claimsFactory.CreateAsync(user);
                if (!String.IsNullOrEmpty(modelUser.FirstName))
                {
                    var claimName = claims.Claims.FirstOrDefault(x => x.Type == "given_name");
                    if (claimName != null)
                    {
                        Claim newClaim = new Claim("given_name", modelUser.FirstName);

                        await _userManager.ReplaceClaimAsync(user, claimName, newClaim);
                    }
                    else
                    {
                        Claim firstNameClaim;
                        firstNameClaim = new Claim("given_name", modelUser.FirstName);
                        await _userManager.AddClaimAsync(user, firstNameClaim);
                    }
                }
                if (!String.IsNullOrEmpty(modelUser.LastName))
                {
                    var claimLName = claims.Claims.FirstOrDefault(x => x.Type == "given_lname");
                    if (claimLName != null)
                    {
                        Claim newClaim = new Claim("given_lname", modelUser.LastName);

                        await _userManager.ReplaceClaimAsync(user, claimLName, newClaim);
                    }
                    else
                    {
                        Claim lastNameClaim;
                        lastNameClaim = new Claim("given_lname", modelUser.LastName);
                        await _userManager.AddClaimAsync(user, lastNameClaim);
                    }
                }
                if (!String.IsNullOrEmpty(modelUser.ImageUrl))
                {
                    var imageClaim = claims.Claims.FirstOrDefault(x => x.Type == "image_url");
                    if (imageClaim != null)
                    {
                        Claim newClaim = new Claim("image_url", modelUser.ImageUrl);

                        await _userManager.ReplaceClaimAsync(user, imageClaim, newClaim);
                    }
                    else
                    {
                        Claim ImageUrlClaim;
                        ImageUrlClaim = new Claim("image_url", modelUser.ImageUrl);
                        await _userManager.AddClaimAsync(user, ImageUrlClaim);
                    }
                }
                #endregion

                if (String.IsNullOrEmpty(modelUser.ID))
                {
                    //user.SubjectId = Guid.NewGuid().ToString();
                    //// User is not present in ID and the call is made for creating User
                    //_marvinUserRepository.AddUser(user);
                }
                else
                {
                    // Update user object with claims
                    await _userManager.UpdateAsync(user);
                }
                return Ok(user.Id);

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
                throw ex;
            }

        }

        [HttpPost]
        public async Task<IActionResult> InviteUserMail(bool NewUser, string Email, string AdminName, string Role, string FirstName, string LastName)
        {
            var user = await _userManager.FindByNameAsync(Email);
            var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
            var callbackUrl = "";
            var confirmButton = "";

            if (NewUser)
            {
                callbackUrl = Url.Action(
                              "ConfirmEmail", "Account",
                              new { userId = user.Id, code = code, returnUrl = _configuration.GetSection("FrontendUrl").Value },
                              protocol: Request.Scheme);

                confirmButton = "<table width =\"350\" align=\"center\">" +
                "<tbody><tr><td style =\" text-align: center;\">" +
                "<img src=\"https://lfgcrmstorage.blob.core.windows.net/lfgcrmcontainer/Logo.png \" width=\"150\" height=\"68\"/></td></tr> " +
                   "<tr><td style=\"text-align: left;\">" +
                "<p style=\"padding-left: 30px;\"> Dear <strong> " + FirstName + " </strong></p> " +
                "<p style=\"padding-left: 30px;\">Welcome to Liberty Edge! We are happy to welcome you to our Liberty Edge. </p>" +
                "<p style=\"padding-left: 30px;\">Your access to Liberty Edge�s CRM has been created. Your login details are: </p>" +
                "<p style=\"padding-left: 30px;\">Your user id: <code>"+ Email +"</code></p>" +
                "<p style=\"padding-left: 30px;\">Your Password => <code>Edge@2020</code></p>" +
                "<p style=\"padding-left: 30px;\">Please confirm your email address by clicking on the button below to complete your LIBERTY EDGE CRM account registration.</p>" +
                "<br />" +
                "<table class=\"buttonwrapper\" style=\"background-color:#004284;\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\">" +
                "<tbody>" +
                "<tr>" +
                "<td class=\"button\" style=\"text-align: center; font-size: 18px; font-family: sans-serif; font-weight: bold; padding: 0 30px 0 30px;\" height=\"45\">" +
                "<a style=\"color: #fff; text-decoration: none;\" href ='" + callbackUrl + "'>Confirm My Email</a></td>" +
                "</tr>" +
                "</tbody>" +
                "</table>" +
                "<br />" +
                "<p style=\"padding-left: 30px;\">In case you need to speak with someone, please contact us at <span style=\"color: #004284;\"> 647 247 1148 or support@libertyfinancialgroup.ca </span> and we will be happy to assist you. </p>" +
                "<p style=\"padding-left: 30px;\">Thank you again,</p>" +
                "<p style=\"padding-left: 30px;\"><strong>Liberty Financial Group Team </strong></p> " +
                "</td>" +
                "</tr>" +
                "</tbody>" +
                "</table>";
            }
            else
            {
                callbackUrl =  _configuration.GetSection("FrontendUrl").Value;

                confirmButton = "<table width =\"350\" align=\"center\">" +
                "<tbody><tr><td style =\" text-align: center;\">" +
                "<img src=\"https://lfgcrmstorage.blob.core.windows.net/lfgcrmcontainer/Logo.png \" width=\"150\" height=\"68\"/></td></tr> " +
                   "<tr><td style=\"text-align: left;\">" +
                "<p style=\"padding-left: 30px;\"> Dear <strong> " + FirstName + " </strong></p> " +
                "<p style=\"padding-left: 30px;\">Welcome to Liberty Edge! We are happy to welcome you to our Liberty Edge. </p>" +
                "<p style=\"padding-left: 30px;\">Your access to Liberty Edge�s CRM has been created. Your login details are: </p>" +
                "<p style=\"padding-left: 30px;\">Your user id: <code>" + Email + "</code></p>" +
                "<p style=\"padding-left: 30px;\">Your Password => <code>Edge@2020</code></p>" +
                "<p style=\"padding-left: 30px;\">Please confirm your email address by clicking on the button below to complete your LIBERTY EDGE CRM account registration.</p>" +
                "<br />" +
                "<table class=\"buttonwrapper\" style=\"background-color:#004284;\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\">" +
                "<tbody>" +
                "<tr>" +
                "<td class=\"button\" style=\"text-align: center; font-size: 18px; font-family: sans-serif; font-weight: bold; padding: 0 30px 0 30px;\" height=\"45\">" +
                "<a style=\"color: #fff; text-decoration: none;\" href ='" + callbackUrl + "'>Confirm My Email</a></td>" +
                "</tr>" +
                "</tbody>" +
                "</table>" +
                "<br />" +
                "<p style=\"padding-left: 30px;\">In case you need to speak with someone, please contact us at <span style=\"color: #004284;\"> 647 247 1148 or support@libertyfinancialgroup.ca </span> and we will be happy to assist you. </p>" +
                "<p style=\"padding-left: 30px;\">Thank you again,</p>" +
                "<p style=\"padding-left: 30px;\"><strong>Liberty Financial Group Team </strong></p> " +
                "</td>" +
                "</tr>" +
                "</tbody>" +
                "</table>";
            }

            if (NewUser)
            {                
                    Claim firstNameClaim;
                    firstNameClaim = new Claim("given_name", FirstName);
                    await _userManager.AddClaimAsync(user, firstNameClaim);

                Claim lastNameClaim;
                lastNameClaim = new Claim("family_name", LastName);
                await _userManager.AddClaimAsync(user, lastNameClaim);
                
                Claim roleClaim;
                var roleList = Role.Split(",").ToList();
                foreach(var role in roleList)
                {
                    roleClaim = new Claim("role", role);
                    await _userManager.AddClaimAsync(user, roleClaim);
                }                             
            }

            var client = new SendGridClient(_configuration.GetSection("SendgridApi").Value);
            var msg = MailHelper.CreateSingleEmail(new EmailAddress("support@libertyfinancialgroup.ca"), new EmailAddress(user.UserName),
                "Confirm Email", callbackUrl, confirmButton);
            var response = client.SendEmailAsync(msg);

            return Ok(user.Id);
        }

        public async Task<ActionResult> ConfirmEmail(string userId, string code, string returnUrl)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var user = await _userManager.FindByIdAsync(userId);
            if (user != null)
            {
                var result = await _userManager.ConfirmEmailAsync(user, code);
                if (result.Succeeded)
                {
                    ConfirmEmailViewModel model = new ConfirmEmailViewModel();
                    model.RetutnUrl = returnUrl;
                    return View(model);
                }
                if (!user.EmailConfirmed)
                {
                    var code1 = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                    var callbackUrl = Url.Action(
                                  "ConfirmEmail", "Account",
                                  new { userId = user.Id, code = code1, returnUrl = _configuration.GetSection("FrontendUrl").Value },
                                  protocol: Request.Scheme);

                    var confirmButton = "<table width =\"350\" align=\"center\">" +
                "<tbody><tr><td style =\" text-align: center;\">" +
                "<img src=\"https://lfgcrmstorage.blob.core.windows.net/lfgcrmcontainer/Logo.png \" width=\"150\" height=\"68\"/></td></tr> " +
                   "<tr><td style=\"text-align: left;\">" +
                "<p style=\"padding-left: 30px;\"> Welcome to Liberty Financial Group </p> " +
                "<p style=\"padding-left: 30px;\">Thank you for choosing Liberty Financial Group. We're glad to have you on board.</p>" +
                "<p style=\"padding-left: 30px;\">your password => <code>Edge@2020</code></p>" +
                "<p style=\"padding-left: 30px;\">Please confirm your email address by clicking on the button below to complete your LFG CRM account registration.</p>" +
                "<br />" +
                "<table class=\"buttonwrapper\" style=\"background-color:#004284;\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\">" +
                "<tbody>" +
                "<tr>" +
                "<td class=\"button\" style=\"text-align: center; font-size: 18px; font-family: sans-serif; font-weight: bold; padding: 0 30px 0 30px;\" height=\"45\">" +
                "<a style=\"color: #fff; text-decoration: none;\" href ='" + callbackUrl + "'>Confirm My Email</a></td>" +
                "</tr>" +
                "</tbody>" +
                "</table>" +
                "<br />" +
                "<p style=\"padding-left: 30px;\">This is a notification-only email, please don't reply to this message. </p>" +
                "<p style=\"padding-left: 30px;\"> If you have any questions, please feel free to contact by clicking here <span style=\"color: #004284;\">help center</span>.</p>" +
                "<p style=\"padding-left: 30px;\">Sincerely,</p>" +
                "<p style=\"padding-left: 30px;\"><strong>- The LFG Team  </strong></p> " +
                "</td>" +
                "</tr>" +
                "</tbody>" +
                "</table>";
                    var client = new SendGridClient(_configuration.GetSection("SendgridApi").Value);
                    var msg = MailHelper.CreateSingleEmail(new EmailAddress("support@libertyfinancialgroup.ca"), new EmailAddress(user.UserName),
                        "Confirm Email", callbackUrl, confirmButton);
                    var response = client.SendEmailAsync(msg);
                    return View("EmailRedirect");
                }
                else
                {
                    return View("EmailAlreadyConfirmed");
                }
            }
            else
            {
                return View("Error");
            }
        }

        [HttpPost]
        public async Task<ActionResult> UpdateRole(string Email, string OldRole, string NewRole)
        {
            var user = await _userManager.FindByNameAsync(Email);

            if (!string.IsNullOrEmpty(OldRole))
            {
                Claim oldRoleClaim;
                var oldRoleList = OldRole.Split(",").ToList();
                foreach (var oldRole in oldRoleList)
                {
                    oldRoleClaim = new Claim("role", oldRole);
                    await _userManager.RemoveClaimAsync(user, oldRoleClaim);
                }
            }

            if (!string.IsNullOrEmpty(NewRole))
            {
                Claim newRoleClaim;
                var newRoleList = NewRole.Split(",").ToList();
                foreach (var newRole in newRoleList)
                {
                    newRoleClaim = new Claim("role", newRole);
                    await _userManager.AddClaimAsync(user, newRoleClaim);
                }
            }

            
            return Ok(user.Id);
        }


        [HttpGet]
        public async Task<ActionResult> GenerateSinToken(string userId) 
        {
            var user = await _userManager.FindByIdAsync(userId);
            var token = await _userManager.GenerateUserTokenAsync(user, "CustomTokenProvider", "Sin");
            var result = await _userManager.VerifyUserTokenAsync(user, "CustomTokenProvider", "Sin", token);
            return Ok(token);
        }

        [HttpGet]
        public async Task<ActionResult> VerifySinToken(string userId, string token)
        {
            var user = await _userManager.FindByIdAsync(userId);
            token = token.Replace("-", "+");
            token = token.Replace("_", "/");
            var result = await _userManager.VerifyUserTokenAsync(user, "CustomTokenProvider", "Sin", token);            
            return Ok(result);
        }

        [HttpGet]
        public IActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(model.Email);

                if (user != null)
                {
                    var token = await _userManager.GeneratePasswordResetTokenAsync(user);
                    var resetUrl = Url.Action("ResetPassword", "Account",
                        new { token = token, email = user.Email }, Request.Scheme);

                    //System.IO.File.WriteAllText("resetLink.txt", resetUrl);

                    var resetButton = "<table width =\"350\" align=\"center\">" +
                        "<tbody><tr><td style =\"text-align: center;\">" +
                        "<img src=\"https://lfgcrmstorage.blob.core.windows.net/lfgcrmcontainer/Logo.png \" width=\"150\" height=\"68\"/></td></tr> " +
                        "<tr><td style=\"text-align: left;\">" +
                        "<p style=\"padding-left: 30px;\">We received a request to reset your password.</p>" +
                        "<p style=\"padding-left: 30px;\">Use the link below to set up a new password for your account.</p>" +
                        "<p style=\"padding-left: 30px;\">If you did not request to reset your password, ignore this email and the link will expire on its own.</p>" +
                        "<br />" +
                        "<table class=\"buttonwrapper\" style=\"background-color:#004284;\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\">" +
                        "<tbody>" +
                        "<tr>" +
                        "<td class=\"button\" style=\"text-align: center; font-size: 18px; font-family: sans-serif; font-weight: bold; padding: 0 30px 0 30px;\" height=\"45\">" +
                        "<a style=\"color: #fff; text-decoration: none;\" href ='" + resetUrl + "'>Reset Password</a></td>" +
                        "</tr>" +
                        "</tbody>" +
                        "</table>" +
                        "<br />" +
                        "<p style=\"padding-left: 30px;\">We love hearing from you!</p>" +
                        "<p style=\"padding-left: 30px;\">Have any question? Please checkout our <span style=\"color: #004284;\">help center</span>.</p>" +
                        "<p style=\"padding-left: 30px;\">Thanks</p>" +
                        "<p style=\"padding-left: 30px;\"><strong>Liberty Financial Group </strong></p> " +
                        "</td>" +
                        "</tr>" +
                        "</tbody>" +
                        "</table>";

                    var client = new SendGridClient(_configuration.GetSection("SendgridApi").Value);
                    var msg = MailHelper.CreateSingleEmail(new EmailAddress("support@libertyfinancialgroup.ca"), new EmailAddress(model.Email),
                        "Reset Password", resetUrl, resetButton);
                    var response = client.SendEmailAsync(msg);

                }
                else
                {
                    // email user and inform them that they do not have an account

                }
                return View("ForgotPasswordConfirmation");
            }
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ResetPassword(string token, string email)
        {
            // If password reset token or email is null, most likely the
            // user tried to tamper the password reset link
            if (token == null || email == null)
            {
                ModelState.AddModelError("", "Invalid password reset token");
            }
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> ResetPassword(ResetPasswordModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(model.Email);

                if (user != null)
                {
                    var result = await _userManager.ResetPasswordAsync(user, model.Token, model.Password);

                    if (!result.Succeeded)
                    {
                        foreach (var error in result.Errors)
                        {
                            ModelState.AddModelError("", error.Description);
                        }
                        return View(model);
                    }

                    if (await _userManager.IsLockedOutAsync(user))
                    {
                        await _userManager.SetLockoutEndDateAsync(user, DateTimeOffset.UtcNow);
                    }
                    TempData["RedirectUrl"] = _configuration.GetSection("FrontendUrl").Value;
                    TempData["UserName"] = user.Email;
                    return View("ResetPasswordConfirmation");
                }
                ModelState.AddModelError("", "Invalid Request");
            }
            return View(model);
        }

        [HttpGet]
        public IActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> ChangePasswordFromApplication(string userId, string currentPassword, string newPassword)
        {
            var response = new ChangePasswordResponse
            {
                Status = false,
                Message = string.Empty
            };

            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByIdAsync(userId);

                // ChangePasswordAsync changes the user password
                var result = await _userManager.ChangePasswordAsync(user,
                    currentPassword, newPassword);    
                
                if (result.Errors != null && result.Errors.Count() > 0)
                {
                    response.Status = false;
                    response.Message = result.Errors.Select(x => x.Description).FirstOrDefault();
                    return Ok(response);
                } else
                {
                    response.Status = true;
                }

                // Upon successfully changing the password refresh sign-in cookie
                await _signInManager.RefreshSignInAsync(user);
                
                
            }
            return Ok(response);
        }


        [HttpPost]
        public async Task<IActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.GetUserAsync(User);
                if (user == null)
                {
                    return RedirectToAction("Login");
                }

                // ChangePasswordAsync changes the user password
                var result = await _userManager.ChangePasswordAsync(user,
                    model.CurrentPassword, model.NewPassword);

                // The new password did not meet the complexity rules or
                // the current password is incorrect. Add these errors to
                // the ModelState and rerender ChangePassword view
                if (!result.Succeeded)
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                    return View();
                }

                // Upon successfully changing the password refresh sign-in cookie
                await _signInManager.RefreshSignInAsync(user);
                var returnURL = _configuration.GetSection("FrontendUrl").Value;
                return Redirect(returnURL);
            }

            return View(model);
        }

        /// <summary>
        /// fetch user by given Subject ID
        /// </summary>
        [HttpGet]
        public async Task<IActionResult> getUserBySubjectID(string sID)
        {
            try
            {
                var user = await _userManager.FindByIdAsync(sID);
                var calims = await getClaims(user);
                dynamic MyDynamic = new System.Dynamic.ExpandoObject();
                MyDynamic.subjectId = user.Id;
                MyDynamic.username = user.UserName; // Change this with email
                MyDynamic.database = user.Database;
                MyDynamic.claims = new List<dynamic>();

                foreach (var claim in calims)
                {
                    dynamic MyDynamicClaim = new System.Dynamic.ExpandoObject();
                    MyDynamicClaim.claimType = claim.Type;
                    MyDynamicClaim.claimValue = claim.Value;

                    MyDynamic.claims.Add(MyDynamicClaim);
                }

                return Ok(MyDynamic);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Fetch all Users from Identity server
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Users(List<string> uID)
        {
            try
            {
                dynamic users;
                if (uID.Count == 0)
                {
                    users = _userManager.Users.ToList();// .GetUserWithClaims();                   
                }
                else
                {
                    users = _userManager.Users.Where(e => uID.Contains(e.Id)).ToList();
                }

                dynamic MyDynamicUserList = new List<System.Dynamic.ExpandoObject>();
                foreach (var user in users)
                {
                    dynamic MyDynamic = new System.Dynamic.ExpandoObject();
                    MyDynamic.subjectId = user.Id;
                    MyDynamic.Id = user.Id;
                    MyDynamic.username = user.UserName; // Change this with email
                    MyDynamic.database = user.Database;
                    var calims = await getClaims(user);
                    MyDynamic.claims = new List<dynamic>();

                    foreach (var claim in calims)
                    {
                        dynamic MyDynamicClaim = new System.Dynamic.ExpandoObject();
                        MyDynamicClaim.claimType = claim.Type;
                        MyDynamicClaim.claimValue = claim.Value;

                        MyDynamic.claims.Add(MyDynamicClaim);
                    }

                    MyDynamicUserList.Add(MyDynamic);
                }
                return Ok(MyDynamicUserList);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        /// <summary>
        /// Show logout page
        /// </summary>
        [HttpGet]
        public async Task<IActionResult> Logout(string logoutId)
        {
            // build a model so the logout page knows what to display
            var vm = await BuildLogoutViewModelAsync(logoutId);

            if (vm.ShowLogoutPrompt == false)
            {
                // if the request for logout was properly authenticated from IdentityServer, then
                // we don't need to show the prompt and can just log the user out directly.
                return await Logout(vm);
            }

            return View(vm);
        }

        /// <summary>
        /// Handle logout page postback
        /// </summary>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout(LogoutInputModel model)
        {
            // build a model so the logged out page knows what to display
            var vm = await BuildLoggedOutViewModelAsync(model.LogoutId);
            vm.RetutnUrl = _configuration.GetSection("FrontendUrl").Value;

            if (User?.Identity.IsAuthenticated == true)
            {
                // delete local authentication cookie
                await _signInManager.SignOutAsync();

                // raise the logout event
                await _events.RaiseAsync(new UserLogoutSuccessEvent(User.GetSubjectId(), User.GetDisplayName()));
            }

            // check if we need to trigger sign-out at an upstream identity provider
            if (vm.TriggerExternalSignout)
            {
                // build a return URL so the upstream provider will redirect back
                // to us after the user has logged out. this allows us to then
                // complete our single sign-out processing.
                string url = Url.Action("Logout", new { logoutId = vm.LogoutId });

                // this triggers a redirect to the external provider for sign-out
                return SignOut(new AuthenticationProperties { RedirectUri = url }, vm.ExternalAuthenticationScheme);
            }

            return View("LoggedOut", vm);
        }

        /// <summary>
        /// This method will delete all the order presitent grants for the user from the database. 
        /// </summary>
        /// <param name="subjectid"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult DeleteOldTokenBySubjectId(string subjectid)
        {
            try
            {
                var persitedgrants = _persistedGrantStore.GetAllAsync(subjectid).Result;

                persitedgrants = persitedgrants.OrderByDescending(x => x.Expiration);

                var grant = persitedgrants.FirstOrDefault();

                foreach (var grantRemove in persitedgrants)
                {
                    if (grant.Key == grantRemove.Key)
                    {
                        // Do nothing
                    }
                    else
                    {
                        _persistedGrantStore.RemoveAsync(grantRemove.Key);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Ok();
        }

        [HttpGet]
        public IActionResult AccessDenied()
        {
            return View();
        }


        //[HttpGet]
        //public IActionResult TwoFactor()
        //{
        //    return View();
        //}

        //[HttpPost]
        //public async Task<IActionResult> TwoFactor(TwoFactorModel model)
        //{
        //    var result = await HttpContext.AuthenticateAsync(IdentityConstants.TwoFactorUserIdScheme);
        //    if (!result.Succeeded)
        //    {
        //        ModelState.AddModelError("", "You login request has expired, please start over");
        //        return View();
        //    }

        //    if (ModelState.IsValid)
        //    {
        //        var user = await _userManager.FindByIdAsync(result.Principal.FindFirstValue("sub"));

        //        if (user != null)
        //        {
        //            var isValid = await _userManager.VerifyTwoFactorTokenAsync(user,
        //                result.Principal.FindFirstValue("amr"), model.Token);

        //            if (isValid)
        //            {
        //                await HttpContext.SignOutAsync(IdentityConstants.TwoFactorUserIdScheme);

        //                var claimsPrincipal = await _claimsFactory.CreateAsync(user);
        //                await HttpContext.SignInAsync(IdentityConstants.ApplicationScheme, claimsPrincipal);


        //                return Redirect(TempData["RedirectUrl"].ToString());
        //                // return RedirectToAction("Index");
        //            }

        //            ModelState.AddModelError("", "Invalid token");
        //            return View();
        //        }

        //        ModelState.AddModelError("", "Invalid Request");
        //    }

        //    return View();
        //}


        /*****************************************/
        /* helper APIs for the AccountController */
        /*****************************************/
        private async Task<LoginViewModel> BuildLoginViewModelAsync(string returnUrl)
        {
            var context = await _interaction.GetAuthorizationContextAsync(returnUrl);
            if (context?.IdP != null && await _schemeProvider.GetSchemeAsync(context.IdP) != null)
            {
                var local = context.IdP == IdentityServer4.IdentityServerConstants.LocalIdentityProvider;

                // this is meant to short circuit the UI and only trigger the one external IdP
                var vm = new LoginViewModel
                {
                    EnableLocalLogin = local,
                    ReturnUrl = returnUrl,
                    Username = context?.LoginHint,
                };

                if (!local)
                {
                    vm.ExternalProviders = new[] { new ExternalProvider { AuthenticationScheme = context.IdP } };
                }

                return vm;
            }

            var schemes = await _schemeProvider.GetAllSchemesAsync();

            var providers = schemes
                .Where(x => x.DisplayName != null ||
                            (x.Name.Equals(AccountOptions.WindowsAuthenticationSchemeName, StringComparison.OrdinalIgnoreCase))
                )
                .Select(x => new ExternalProvider
                {
                    DisplayName = x.DisplayName,
                    AuthenticationScheme = x.Name
                }).ToList();

            var allowLocal = true;
            if (context?.ClientId != null)
            {
                var client = await _clientStore.FindEnabledClientByIdAsync(context.ClientId);
                if (client != null)
                {
                    allowLocal = client.EnableLocalLogin;

                    if (client.IdentityProviderRestrictions != null && client.IdentityProviderRestrictions.Any())
                    {
                        providers = providers.Where(provider => client.IdentityProviderRestrictions.Contains(provider.AuthenticationScheme)).ToList();
                    }
                }
            }

            return new LoginViewModel
            {
                AllowRememberLogin = AccountOptions.AllowRememberLogin,
                EnableLocalLogin = allowLocal && AccountOptions.AllowLocalLogin,
                ReturnUrl = returnUrl,
                Username = context?.LoginHint,
                ExternalProviders = providers.ToArray()
            };
        }

        private async Task<LoginViewModel> BuildLoginViewModelAsync(LoginInputModel model)
        {
            var vm = await BuildLoginViewModelAsync(model.ReturnUrl);
            vm.Username = model.Username;
            vm.RememberLogin = model.RememberLogin;
            return vm;
        }

        private async Task<LogoutViewModel> BuildLogoutViewModelAsync(string logoutId)
        {
            var vm = new LogoutViewModel { LogoutId = logoutId, ShowLogoutPrompt = AccountOptions.ShowLogoutPrompt };

            if (User?.Identity.IsAuthenticated != true)
            {
                // if the user is not authenticated, then just show logged out page
                vm.ShowLogoutPrompt = false;
                return vm;
            }

            var context = await _interaction.GetLogoutContextAsync(logoutId);
            if (context?.ShowSignoutPrompt == false)
            {
                // it's safe to automatically sign-out
                vm.ShowLogoutPrompt = false;
                return vm;
            }

            // show the logout prompt. this prevents attacks where the user
            // is automatically signed out by another malicious web page.
            return vm;
        }

        private async Task<LoggedOutViewModel> BuildLoggedOutViewModelAsync(string logoutId)
        {
            // get context information (client name, post logout redirect URI and iframe for federated signout)
            var logout = await _interaction.GetLogoutContextAsync(logoutId);

            var vm = new LoggedOutViewModel
            {
                AutomaticRedirectAfterSignOut = AccountOptions.AutomaticRedirectAfterSignOut,
                PostLogoutRedirectUri = _configuration.GetSection("FrontendUrl").Value,
                ClientName = string.IsNullOrEmpty(logout?.ClientName) ? logout?.ClientId : logout?.ClientName,
                SignOutIframeUrl = logout?.SignOutIFrameUrl,
                LogoutId = logoutId
            };

            if (User?.Identity.IsAuthenticated == true)
            {
                var idp = User.FindFirst(JwtClaimTypes.IdentityProvider)?.Value;
                if (idp != null && idp != IdentityServer4.IdentityServerConstants.LocalIdentityProvider)
                {
                    var providerSupportsSignout = await HttpContext.GetSchemeSupportsSignOutAsync(idp);
                    if (providerSupportsSignout)
                    {
                        if (vm.LogoutId == null)
                        {
                            // if there's no current logout context, we need to create one
                            // this captures necessary info from the current logged in user
                            // before we signout and redirect away to the external IdP for signout
                            vm.LogoutId = await _interaction.CreateLogoutContextAsync();
                        }

                        vm.ExternalAuthenticationScheme = idp;
                    }
                }
            }

            return vm;
        }

        private async Task<List<Claim>> getClaims(User user)
        {
            var claims = new List<Claim>
            {
                new Claim(JwtClaimTypes.Subject, await _userManager.GetUserIdAsync(user)),
                new Claim(JwtClaimTypes.Name, await _userManager.GetUserNameAsync(user))
            };

            if (_userManager.SupportsUserEmail)
            {
                var email = await _userManager.GetEmailAsync(user);
                if (!string.IsNullOrWhiteSpace(email))
                {
                    claims.AddRange(new[]
                    {
                        new Claim(JwtClaimTypes.Email, email),
                        new Claim(JwtClaimTypes.EmailVerified,
                            await _userManager.IsEmailConfirmedAsync(user) ? "true" : "false", ClaimValueTypes.Boolean)
                    });
                }
            }

            if (_userManager.SupportsUserPhoneNumber)
            {
                var phoneNumber = await _userManager.GetPhoneNumberAsync(user);
                if (!string.IsNullOrWhiteSpace(phoneNumber))
                {
                    claims.AddRange(new[]
                    {
                        new Claim(JwtClaimTypes.PhoneNumber, phoneNumber),
                        new Claim(JwtClaimTypes.PhoneNumberVerified,
                            await _userManager.IsPhoneNumberConfirmedAsync(user) ? "true" : "false", ClaimValueTypes.Boolean)
                    });
                }
            }

            if (_userManager.SupportsUserClaim)
            {
                claims.AddRange(await _userManager.GetClaimsAsync(user));
            }

            if (_userManager.SupportsUserRole)
            {
                var roles = await _userManager.GetRolesAsync(user);
                claims.AddRange(roles.Select(role => new Claim(JwtClaimTypes.Role, role)));
            }

            // Add databases
            if (user.Database != null)
            {
                claims.Add(new Claim("Database", user.Database));
            }
            if (user.LivesIn != null)
            {
                claims.Add(new Claim("livesIn", user.LivesIn));
            }
            if (user.Occupation != null)
            {
                claims.Add(new Claim("phoneNumber", user.PhoneNumber));
            }
            if (user.Occupation != null)
            {
                claims.Add(new Claim("occupation", user.Occupation));
            }
            if (user.Gender != null)
            {
                claims.Add(new Claim("gender", user.Gender));
            }
            if (user.Birthplace != null)
            {
                claims.Add(new Claim("birthplace", user.Birthplace));
            }
            if (user.Birthday != null)
            {
                claims.Add(new Claim("birthday", user.Birthday.ToString("MM/dd/yyyy")));
            }

            claims.Add(new Claim("TwoFactorEnabled", await _userManager.GetTwoFactorEnabledAsync(user) ? "true" : "false", ClaimValueTypes.Boolean));
            return claims;
        }

        private class ChangePasswordResponse
        {
            public bool Status { get; set; }
            public string Message { get; set; }
        }
    }
}



