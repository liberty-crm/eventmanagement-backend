﻿using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Marvin.IDP.Security
{
    public class CustomTokenProvider<TUser>
     : DataProtectorTokenProvider<TUser> where TUser : class
    {
        public CustomTokenProvider(IDataProtectionProvider dataProtectionProvider,
                                        IOptions<CustomTokenProviderOptions> options)
            : base(dataProtectionProvider, options)
        { }
    }
}
