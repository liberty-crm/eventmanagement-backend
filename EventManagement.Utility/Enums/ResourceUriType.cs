using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EventManagement.Utility
{
    public enum ResourceUriType
    {
        PreviousPage,
        NextPage,
        Current
    }
}
