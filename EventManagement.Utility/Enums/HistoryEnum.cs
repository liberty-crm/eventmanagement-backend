﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EventManagement.Utility.Enums
{
    public enum HistoryEnum
    {
        LeadCreated = 1,
        LeadUpdated = 2,
        ServiceAdded = 3,
        ServiceUpdated = 4,
        ServiceRemoved = 5,
        ContactCreated = 6,
        ContactUpdated = 7,
        TaskCreated = 8,
        TaskUpdated = 9,
        TaskDeleted = 10,
        AttachmentCreated = 11,
        AttachmentDownloaded = 12,
        SensitiveInformationMailSent = 13,
        NotificationMailSent = 14,
        IntegrationTriggedSuccessfully = 15,
        IntegrationTriggerFailed = 16
    }
}
