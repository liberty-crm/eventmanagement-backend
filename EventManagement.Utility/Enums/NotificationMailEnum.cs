﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EventManagement.Utility.Enums
{
    public enum NotificationMailEnum
    {
        FtCrmUpdateStatus,        
        LeadCreatedToClient,
        LeadCreatedToEmployee,
        LeadAssigned,
        ConfidentialEmail,
        IntegrationTriggeredToClient,
        IntegrationTriggeredToEmployee,
        ServiceAssigned,
        ProcessCompleted,
        ProcessCompletedToReferenceId,
        InvoiceToClient,
        PaymentReceivedConfirmation,
        FeedBackMail, 
        GetSensitiveInfo,
        IntegrationFaliureToServiceTeam,
        InvoiceEmailToClient

    }
}
