using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace EventManagement.Utility
{
    public static class EventManagementUtils
    {
        public static bool TypeHasProperties<T>(string fields)
        {
            if (string.IsNullOrWhiteSpace(fields))
            {
                return true;
            }

            // the field are separated by ",", so we split it.
            var fieldsAfterSplit = fields.Split(',');

            // check if the requested fields exist on source
            foreach (var field in fieldsAfterSplit)
            {
                // trim each field, as it might contain leading 
                // or trailing spaces. Can't trim the var in foreach,
                // so use another var.
                var propertyName = field.Trim();

                // use reflection to check if the property can be
                // found on T. 
                var propertyInfo = typeof(T)
                    .GetProperty(propertyName, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);

                // it can't be found, return false
                if (propertyInfo == null)
                {
                    return false;
                }
            }

            // all checks out, return true
            return true;
        }

        public static bool IsValidFileExtension(string file)
        {
            var backListFileExtensions = new List<string> {".ADE", ".ADP", ".APK", ".BAT", ".CHM", ".CMD", ".COM", ".CPL",
                                         ".DLL", ".DMG", ".EXE", ".HTA", ".INS", ".ISP", ".JAR", ".JS", ".JSE", ".LIB", ".LNK",
                                         ".MDE", ".MSC", ".MSI", ".MSP", ".MST", ".NSH", ".PIF", ".SCR", ".SCT", ".SHB", ".SYS",
                                         ".VB", ".VBE", ".VBS", ".VXD", ".WSC", ".WSF", ".WSH", ".CAB", ".HTML", ".HTM" };

            var fileExtension = Path.GetExtension(file).ToUpper();

            if (backListFileExtensions.Contains(fileExtension))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }


}
