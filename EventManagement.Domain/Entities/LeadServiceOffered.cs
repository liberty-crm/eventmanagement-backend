using FinanaceManagement.API.Models;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EventManagement.Domain.Entities
{
    public class LeadServiceOffered : AuditableEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        public Guid LeadId { get; set; }

        public Guid ContactId { get; set; }

        public Guid ServiceOfferedId { get; set; }

        public string Description { get; set; }

        public string AssignedTo { get; set; }

        public string AssignedStatus { get; set; }

        [ForeignKey("LeadId")]
        public virtual Lead Lead { get; set; }

        [ForeignKey("ServiceOfferedId")]
        public virtual ServiceOffered ServiceOffered { get; set; }

        [ForeignKey("ContactId")]
        public virtual Contact Contact { get; set; }

        [ForeignKey("AssignedTo")]
        public virtual AspNetUsers User { get; set; }
    }
}
