using FinanaceManagement.API.Models;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EventManagement.Domain.Entities
{
    public class Task : AuditableEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [Required]
        [MaxLength(250)]
        public string Title { get; set; }

        public DateTime? Date { get; set; }

        public bool IsCompleted { get; set; }

        public string Description { get; set; }

        public bool IsImportant { get; set; }

        public bool IsMarked { get; set; }

        public Guid? LeadId { get; set; }

        public Guid? ServiceId { get; set; }

        public string UserId { get; set; }

        [ForeignKey("LeadId")]
        public virtual Lead Lead{get;set;}

        [ForeignKey("ServiceId")]
        public virtual LeadServiceOffered Service { get; set; }

        [ForeignKey("UserId")]
        public virtual AspNetUsers User { get; set; }

    }
}
