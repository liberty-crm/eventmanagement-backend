using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EventManagement.Domain.Entities
{
    public class Payment : AuditableEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [Required]
        public DateTime Date { get; set; }

        [Required]
        [MaxLength(250)]
        public string Mode { get; set; }

        [Required]
        [MaxLength(250)]
        public string Location { get; set; }

        [Required]
        public decimal Amount { get; set; }

        [Required]
        [MaxLength(250)]
        public string ReferenceDetails { get; set; }

        public Guid LeadServiceId { get; set; }

        [ForeignKey("LeadServiceId")]
        public virtual LeadServiceOffered LeadServiceOffered { get; set; }
    }
}
