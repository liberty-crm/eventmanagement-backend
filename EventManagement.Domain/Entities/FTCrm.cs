using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EventManagement.Domain.Entities
{
    public class FTCrm : AuditableEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        public string FtCrmUniqueId { get; set; }

        public Guid LeadServiceId { get; set; }

        [ForeignKey("LeadServiceId")]
        public virtual LeadServiceOffered LeadServiceOffered { get; set; }

        public string Status { get; set; }

        public float Amount { get; set; }

        public string Reason { get; set; }

    }
}
