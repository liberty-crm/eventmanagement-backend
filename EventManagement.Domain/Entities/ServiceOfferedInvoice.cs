using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EventManagement.Domain.Entities
{
    public class ServiceOfferedInvoice : AuditableEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int InvoiceNumber { get; set; }

        public Guid LeadServiceOfferedId { get; set; }

        public float Amount { get; set; }

        public DateTime? SentDate { get; set; }

        public string AttachmentPath { get; set; }

        [ForeignKey("LeadServiceOfferedId")]
        public virtual LeadServiceOffered LeadServiceOffered { get; set; }

    }
}
