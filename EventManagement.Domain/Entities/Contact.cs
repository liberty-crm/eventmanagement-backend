using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EventManagement.Domain.Entities
{
    public class Contact : AuditableEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [Required]
        [MaxLength(250)]
        public string FirstName { get; set; }

        [MaxLength(250)]
        public string MiddleName { get; set; }

        [Required]
        [MaxLength(250)]
        public string LastName { get; set; }

        public string Address { get; set; }

        public string City { get; set; }

        public string Province { get; set; }

        public string Zipcode { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string Phone { get; set; }

        public string Source { get; set; }

        public bool ExistingClient { get; set; }

        public string Employment { get; set; }

        public string Position { get; set; }

        public string CompanyName { get; set; }

        public string Website { get; set; }

        public DateTime? EmploymentDate { get; set; }

        public string SalaryFrequency { get; set; }

        public int NoOfChildren { get; set; }        

        public string RelationWithLead { get; set; }

        public Guid? RelatedLeadContactId { get; set; }

        public string Status { get; set; } //Lead or Customer

        public bool? AllowMarketingMessages { get; set; }

        public bool? AllowUseOfPersonalInfo { get; set; }

        public string MaritalStatus { get; set; }

        public string Sin { get; set; }

        public float? SalaryAmount { get; set; }

        [ForeignKey("RelatedLeadContactId")]
        public virtual Lead Lead { get; set; }

    }
}
