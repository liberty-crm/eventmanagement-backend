using FinanaceManagement.API.Models;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EventManagement.Domain.Entities
{
    public class ServiceOffered : AuditableEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [Required]
        [MaxLength(250)]
        public string Name { get; set; }

         [Required]
        [MaxLength(250)]
        public string Code { get; set; }

        [Required]
        [MaxLength(250)]
        public string Category { get; set; }
           
        public string ServiceManagerId { get; set; }

        [Required]
        [MaxLength(250)]
        public string InvoiceGeneration { get; set; }

        [MaxLength(250)]
        public string InvoiceAmountCalculation { get; set; }

        [MaxLength(250)]
        public string Value { get; set; }

        [ForeignKey("ServiceManagerId")]
        public virtual AspNetUsers User { get; set; }
    }
}
