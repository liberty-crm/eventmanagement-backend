using FinanaceManagement.API.Models;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EventManagement.Domain.Entities
{
    public class History : AuditableEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        public Guid? LeadId { get; set; }

        public Guid? ServiceId { get; set; }

        public string UserId { get; set; }

        public int? Activity { get; set; }

        public string TagData { get; set; }

        [ForeignKey("LeadId")]
        public virtual Lead Lead { get; set; }

        [ForeignKey("ServiceId")]
        public virtual LeadServiceOffered Service { get; set; }

        [ForeignKey("UserId")]
        public virtual AspNetUsers User { get; set; }
    }
}
