using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Serialization;
using FinanaceManagement.API.Models;
using Nancy.Json;
using Newtonsoft.Json;

namespace EventManagement.Domain.Entities
{
    public class Lead : AuditableEntity
    {
        [Key]       
        public Guid Id { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long LeadNumber { get; set; }

        public string Status { get; set; }

        [MaxLength(450)]
        public string AssignedTo { get; set; }

        public string AgentId { get; set; }

        public string CustomerInsight { get; set; }

        public Guid? AssignedStore { get; set; }

        public bool IsEmailSent { get; set; }

        [ScriptIgnore]
        [JsonIgnore]
        [XmlIgnore]
        public virtual ICollection<Contact> Contacts { get; set; }

        [ScriptIgnore]
        [JsonIgnore]
        [XmlIgnore]
        public virtual ICollection<LeadServiceOffered> LeadServiceOffereds { get; set; }

        [ForeignKey("AssignedTo")]
        public virtual AspNetUsers User { get; set; }

        [ForeignKey("AssignedStore")]
        public virtual Store Store { get; set; }       

    }
}
