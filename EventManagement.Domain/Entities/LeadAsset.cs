using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EventManagement.Domain.Entities
{
    public class LeadAsset : AuditableEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        public Guid LeadId { get; set; }

        public Guid AssetId { get; set; }

        public Guid ContactId { get; set; }

        public string Type { get; set; } // Assets/Liability

        public string Valuation { get; set; }

        public string Description { get; set; }

        [ForeignKey("LeadId")]
        public virtual Lead Lead { get; set; }

        [ForeignKey("AssetId")]
        public virtual Asset Asset { get; set; }

        [ForeignKey("ContactId")]
        public virtual Contact Contact { get; set; }
    }
}
