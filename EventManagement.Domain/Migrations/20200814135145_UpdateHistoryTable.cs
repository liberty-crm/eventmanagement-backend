﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EventManagement.Domain.Migrations
{
    public partial class UpdateHistoryTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Historys_ServiceOffereds_ServiceId",
                table: "Historys");

            migrationBuilder.AddForeignKey(
                name: "FK_Historys_LeadServiceOffereds_ServiceId",
                table: "Historys",
                column: "ServiceId",
                principalTable: "LeadServiceOffereds",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Historys_LeadServiceOffereds_ServiceId",
                table: "Historys");

            migrationBuilder.AddForeignKey(
                name: "FK_Historys_ServiceOffereds_ServiceId",
                table: "Historys",
                column: "ServiceId",
                principalTable: "ServiceOffereds",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
