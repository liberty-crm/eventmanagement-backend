﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EventManagement.Domain.Migrations
{
    public partial class UpdateAttachmentTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Attachments_ServiceOffereds_ServiceId",
                table: "Attachments");

            migrationBuilder.AddForeignKey(
                name: "FK_Attachments_LeadServiceOffereds_ServiceId",
                table: "Attachments",
                column: "ServiceId",
                principalTable: "LeadServiceOffereds",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Attachments_LeadServiceOffereds_ServiceId",
                table: "Attachments");

            migrationBuilder.AddForeignKey(
                name: "FK_Attachments_ServiceOffereds_ServiceId",
                table: "Attachments",
                column: "ServiceId",
                principalTable: "ServiceOffereds",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
