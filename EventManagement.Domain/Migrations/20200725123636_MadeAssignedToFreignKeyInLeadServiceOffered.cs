﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EventManagement.Domain.Migrations
{
    public partial class MadeAssignedToFreignKeyInLeadServiceOffered : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "AssignedTo",
                table: "LeadServiceOffereds",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_LeadServiceOffereds_AssignedTo",
                table: "LeadServiceOffereds",
                column: "AssignedTo");

            migrationBuilder.AddForeignKey(
                name: "FK_LeadServiceOffereds_AspNetUsers_AssignedTo",
                table: "LeadServiceOffereds",
                column: "AssignedTo",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LeadServiceOffereds_AspNetUsers_AssignedTo",
                table: "LeadServiceOffereds");

            migrationBuilder.DropIndex(
                name: "IX_LeadServiceOffereds_AssignedTo",
                table: "LeadServiceOffereds");

            migrationBuilder.AlterColumn<string>(
                name: "AssignedTo",
                table: "LeadServiceOffereds",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
