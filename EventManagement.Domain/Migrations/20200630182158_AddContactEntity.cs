﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EventManagement.Domain.Migrations
{
    public partial class AddContactEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Address",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "AllowMarketingMessages",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "City",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "CompanyName",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "Email",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "Employment",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "EmploymentDate",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "ExistingClient",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "FirstName",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "LastName",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "MiddleName",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "NoOfChildren",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "Phone",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "Position",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "Province",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "SalaryFrequency",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "SpouseOf",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "Email",
                table: "LeadContacts");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "LeadContacts");

            migrationBuilder.DropColumn(
                name: "Phone",
                table: "LeadContacts");

            migrationBuilder.DropColumn(
                name: "Relation",
                table: "LeadContacts");

            migrationBuilder.RenameColumn(
                name: "Zipcode",
                table: "Leads",
                newName: "Status");

            migrationBuilder.RenameColumn(
                name: "Website",
                table: "Leads",
                newName: "AssignedTo");

            migrationBuilder.RenameColumn(
                name: "Source",
                table: "Leads",
                newName: "AgentId");

            migrationBuilder.AddColumn<Guid>(
                name: "ContactId",
                table: "LeadServiceOffereds",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "ContactId",
                table: "LeadContacts",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "ContactId",
                table: "LeadAssets",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateTable(
                name: "Contacts",
                columns: table => new
                {
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: false),
                    UpdatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<string>(nullable: true),
                    Id = table.Column<Guid>(nullable: false),
                    FirstName = table.Column<string>(maxLength: 250, nullable: false),
                    MiddleName = table.Column<string>(maxLength: 250, nullable: true),
                    LastName = table.Column<string>(maxLength: 250, nullable: true),
                    Address = table.Column<string>(nullable: true),
                    City = table.Column<string>(nullable: true),
                    Province = table.Column<string>(nullable: true),
                    Zipcode = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    Source = table.Column<string>(nullable: true),
                    ExistingClient = table.Column<bool>(nullable: false),
                    Employment = table.Column<string>(nullable: true),
                    Position = table.Column<string>(nullable: true),
                    CompanyName = table.Column<string>(nullable: true),
                    Website = table.Column<string>(nullable: true),
                    EmploymentDate = table.Column<DateTime>(nullable: false),
                    SalaryFrequency = table.Column<string>(nullable: true),
                    NoOfChildren = table.Column<int>(nullable: false),
                    RelationWithLead = table.Column<string>(nullable: true),
                    RelatedLeadContactId = table.Column<Guid>(nullable: true),
                    Status = table.Column<string>(nullable: true),
                    AllowMarketingMessages = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contacts", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_LeadServiceOffereds_ContactId",
                table: "LeadServiceOffereds",
                column: "ContactId");

            migrationBuilder.CreateIndex(
                name: "IX_LeadContacts_ContactId",
                table: "LeadContacts",
                column: "ContactId");

            migrationBuilder.CreateIndex(
                name: "IX_LeadAssets_ContactId",
                table: "LeadAssets",
                column: "ContactId");

            migrationBuilder.AddForeignKey(
                name: "FK_LeadAssets_Contacts_ContactId",
                table: "LeadAssets",
                column: "ContactId",
                principalTable: "Contacts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_LeadContacts_Contacts_ContactId",
                table: "LeadContacts",
                column: "ContactId",
                principalTable: "Contacts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_LeadServiceOffereds_Contacts_ContactId",
                table: "LeadServiceOffereds",
                column: "ContactId",
                principalTable: "Contacts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LeadAssets_Contacts_ContactId",
                table: "LeadAssets");

            migrationBuilder.DropForeignKey(
                name: "FK_LeadContacts_Contacts_ContactId",
                table: "LeadContacts");

            migrationBuilder.DropForeignKey(
                name: "FK_LeadServiceOffereds_Contacts_ContactId",
                table: "LeadServiceOffereds");

            migrationBuilder.DropTable(
                name: "Contacts");

            migrationBuilder.DropIndex(
                name: "IX_LeadServiceOffereds_ContactId",
                table: "LeadServiceOffereds");

            migrationBuilder.DropIndex(
                name: "IX_LeadContacts_ContactId",
                table: "LeadContacts");

            migrationBuilder.DropIndex(
                name: "IX_LeadAssets_ContactId",
                table: "LeadAssets");

            migrationBuilder.DropColumn(
                name: "ContactId",
                table: "LeadServiceOffereds");

            migrationBuilder.DropColumn(
                name: "ContactId",
                table: "LeadContacts");

            migrationBuilder.DropColumn(
                name: "ContactId",
                table: "LeadAssets");

            migrationBuilder.RenameColumn(
                name: "Status",
                table: "Leads",
                newName: "Zipcode");

            migrationBuilder.RenameColumn(
                name: "AssignedTo",
                table: "Leads",
                newName: "Website");

            migrationBuilder.RenameColumn(
                name: "AgentId",
                table: "Leads",
                newName: "Source");

            migrationBuilder.AddColumn<string>(
                name: "Address",
                table: "Leads",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "AllowMarketingMessages",
                table: "Leads",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "City",
                table: "Leads",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CompanyName",
                table: "Leads",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "Leads",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Employment",
                table: "Leads",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "EmploymentDate",
                table: "Leads",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<bool>(
                name: "ExistingClient",
                table: "Leads",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "FirstName",
                table: "Leads",
                maxLength: 250,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "LastName",
                table: "Leads",
                maxLength: 250,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MiddleName",
                table: "Leads",
                maxLength: 250,
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "NoOfChildren",
                table: "Leads",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Phone",
                table: "Leads",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Position",
                table: "Leads",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Province",
                table: "Leads",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SalaryFrequency",
                table: "Leads",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "SpouseOf",
                table: "Leads",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "LeadContacts",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "LeadContacts",
                maxLength: 250,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Phone",
                table: "LeadContacts",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Relation",
                table: "LeadContacts",
                nullable: true);
        }
    }
}
