﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EventManagement.Domain.Migrations
{
    public partial class ChangeServiceToLeadServiceInTask : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Tasks_ServiceOffereds_ServiceId",
                table: "Tasks");

            migrationBuilder.AddForeignKey(
                name: "FK_Tasks_LeadServiceOffereds_ServiceId",
                table: "Tasks",
                column: "ServiceId",
                principalTable: "LeadServiceOffereds",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Tasks_LeadServiceOffereds_ServiceId",
                table: "Tasks");

            migrationBuilder.AddForeignKey(
                name: "FK_Tasks_ServiceOffereds_ServiceId",
                table: "Tasks",
                column: "ServiceId",
                principalTable: "ServiceOffereds",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
