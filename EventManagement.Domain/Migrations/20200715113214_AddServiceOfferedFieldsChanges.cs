﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EventManagement.Domain.Migrations
{
    public partial class AddServiceOfferedFieldsChanges : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "InvoiceAmountCalculation",
                table: "ServiceOffereds",
                maxLength: 250,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "InvoiceGeneration",
                table: "ServiceOffereds",
                maxLength: 250,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Value",
                table: "ServiceOffereds",
                maxLength: 250,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "InvoiceAmountCalculation",
                table: "ServiceOffereds");

            migrationBuilder.DropColumn(
                name: "InvoiceGeneration",
                table: "ServiceOffereds");

            migrationBuilder.DropColumn(
                name: "Value",
                table: "ServiceOffereds");
        }
    }
}
