﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EventManagement.Domain.Migrations
{
    public partial class UpdateLeadAssetAndServiceOffered : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "LeadServiceOffereds",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "LeadAssets",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Type",
                table: "LeadAssets",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Valuation",
                table: "LeadAssets",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Description",
                table: "LeadServiceOffereds");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "LeadAssets");

            migrationBuilder.DropColumn(
                name: "Type",
                table: "LeadAssets");

            migrationBuilder.DropColumn(
                name: "Valuation",
                table: "LeadAssets");
        }
    }
}
