﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EventManagement.Domain.Migrations
{
    public partial class AddLeadServiceOfferedChangesFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AssignedStatus",
                table: "LeadServiceOffereds",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AssignedTo",
                table: "LeadServiceOffereds",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AssignedStatus",
                table: "LeadServiceOffereds");

            migrationBuilder.DropColumn(
                name: "AssignedTo",
                table: "LeadServiceOffereds");
        }
    }
}
