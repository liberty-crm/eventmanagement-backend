﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EventManagement.Domain.Migrations
{
    public partial class AddServiceInvoiceTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ServiceOfferedInvoices",
                columns: table => new
                {
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: false),
                    UpdatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<string>(nullable: true),
                    Id = table.Column<Guid>(nullable: false),
                    InvoiceNumber = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    LeadServiceOfferedId = table.Column<Guid>(nullable: false),
                    Amount = table.Column<float>(nullable: false),
                    SentDate = table.Column<DateTime>(nullable: true),
                    AttachmentPath = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServiceOfferedInvoices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ServiceOfferedInvoices_LeadServiceOffereds_LeadServiceOfferedId",
                        column: x => x.LeadServiceOfferedId,
                        principalTable: "LeadServiceOffereds",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ServiceOfferedInvoices_LeadServiceOfferedId",
                table: "ServiceOfferedInvoices",
                column: "LeadServiceOfferedId");

            migrationBuilder.CreateIndex(
                name: "IX_InvoiceNumber",
                table: "ServiceOfferedInvoices",
                column: "InvoiceNumber");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ServiceOfferedInvoices");
        }
    }
}
