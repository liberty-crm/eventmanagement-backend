﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EventManagement.Domain.Migrations
{
    public partial class AddFTCrmTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "FTCrms",
                columns: table => new
                {
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: false),
                    UpdatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<string>(nullable: true),
                    Id = table.Column<Guid>(nullable: false),
                    FtCrmUniqueId = table.Column<string>(nullable: true),
                    LeadServiceId = table.Column<Guid>(nullable: false),
                    Status = table.Column<string>(nullable: true),
                    Amount = table.Column<float>(nullable: false),
                    Reason = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FTCrms", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FTCrms_LeadServiceOffereds_LeadServiceId",
                        column: x => x.LeadServiceId,
                        principalTable: "LeadServiceOffereds",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_FTCrms_LeadServiceId",
                table: "FTCrms",
                column: "LeadServiceId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "FTCrms");
        }
    }
}
