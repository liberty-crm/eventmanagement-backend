﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EventManagement.Domain.Migrations
{
    public partial class AddedServiceManagerIdAsForeignKeyInServiceOffered : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ServiceManager",
                table: "ServiceOffereds");

            migrationBuilder.AddColumn<string>(
                name: "ServiceManagerId",
                table: "ServiceOffereds",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ServiceOffereds_ServiceManagerId",
                table: "ServiceOffereds",
                column: "ServiceManagerId");

            migrationBuilder.AddForeignKey(
                name: "FK_ServiceOffereds_AspNetUsers_ServiceManagerId",
                table: "ServiceOffereds",
                column: "ServiceManagerId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ServiceOffereds_AspNetUsers_ServiceManagerId",
                table: "ServiceOffereds");

            migrationBuilder.DropIndex(
                name: "IX_ServiceOffereds_ServiceManagerId",
                table: "ServiceOffereds");

            migrationBuilder.DropColumn(
                name: "ServiceManagerId",
                table: "ServiceOffereds");

            migrationBuilder.AddColumn<string>(
                name: "ServiceManager",
                table: "ServiceOffereds",
                maxLength: 250,
                nullable: false,
                defaultValue: "");
        }
    }
}
