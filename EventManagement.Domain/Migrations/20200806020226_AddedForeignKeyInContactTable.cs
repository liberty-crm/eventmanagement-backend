﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EventManagement.Domain.Migrations
{
    public partial class AddedForeignKeyInContactTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Contacts_RelatedLeadContactId",
                table: "Contacts",
                column: "RelatedLeadContactId");

            migrationBuilder.AddForeignKey(
                name: "FK_Contacts_Leads_RelatedLeadContactId",
                table: "Contacts",
                column: "RelatedLeadContactId",
                principalTable: "Leads",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Contacts_Leads_RelatedLeadContactId",
                table: "Contacts");

            migrationBuilder.DropIndex(
                name: "IX_Contacts_RelatedLeadContactId",
                table: "Contacts");
        }
    }
}
