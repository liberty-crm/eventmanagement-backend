﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EventManagement.Domain.Migrations
{
    public partial class AddServiceOfferedServiceFieldChanges : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Tasks_Leads_LeadId",
                table: "Tasks");

            migrationBuilder.DropForeignKey(
                name: "FK_Tasks_ServiceOffereds_ServiceId",
                table: "Tasks");

            migrationBuilder.AlterColumn<Guid>(
                name: "ServiceId",
                table: "Tasks",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AlterColumn<Guid>(
                name: "LeadId",
                table: "Tasks",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AddColumn<string>(
                name: "ServiceManager",
                table: "ServiceOffereds",
                maxLength: 250,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddForeignKey(
                name: "FK_Tasks_Leads_LeadId",
                table: "Tasks",
                column: "LeadId",
                principalTable: "Leads",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Tasks_ServiceOffereds_ServiceId",
                table: "Tasks",
                column: "ServiceId",
                principalTable: "ServiceOffereds",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Tasks_Leads_LeadId",
                table: "Tasks");

            migrationBuilder.DropForeignKey(
                name: "FK_Tasks_ServiceOffereds_ServiceId",
                table: "Tasks");

            migrationBuilder.DropColumn(
                name: "ServiceManager",
                table: "ServiceOffereds");

            migrationBuilder.AlterColumn<Guid>(
                name: "ServiceId",
                table: "Tasks",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "LeadId",
                table: "Tasks",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Tasks_Leads_LeadId",
                table: "Tasks",
                column: "LeadId",
                principalTable: "Leads",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Tasks_ServiceOffereds_ServiceId",
                table: "Tasks",
                column: "ServiceId",
                principalTable: "ServiceOffereds",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
