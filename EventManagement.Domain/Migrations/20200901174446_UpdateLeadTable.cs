﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EventManagement.Domain.Migrations
{
    public partial class UpdateLeadTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.DropColumn(
                name: "AssignedTo",
                table: "Leads");

            migrationBuilder.AddColumn<string>(
                name: "AssignedTo",
                table: "Leads",
                maxLength: 450,
                nullable: true);

            migrationBuilder.DropColumn(
                name: "AssignedStore",
                table: "Leads");

            migrationBuilder.AddColumn<Guid>(
                name: "AssignedStore",
                table: "Leads",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Leads_AssignedStore",
                table: "Leads",
                column: "AssignedStore");

            migrationBuilder.CreateIndex(
                name: "IX_Leads_AssignedTo",
                table: "Leads",
                column: "AssignedTo");

            migrationBuilder.AddForeignKey(
                name: "FK_Leads_Stores_AssignedStore",
                table: "Leads",
                column: "AssignedStore",
                principalTable: "Stores",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Leads_AspNetUsers_AssignedTo",
                table: "Leads",
                column: "AssignedTo",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Leads_Stores_AssignedStore",
                table: "Leads");

            migrationBuilder.DropForeignKey(
                name: "FK_Leads_AspNetUsers_AssignedTo",
                table: "Leads");

            migrationBuilder.DropTable(
                name: "Reports");

            migrationBuilder.DropIndex(
                name: "IX_Leads_AssignedStore",
                table: "Leads");

            migrationBuilder.DropIndex(
                name: "IX_Leads_AssignedTo",
                table: "Leads");

            migrationBuilder.AlterColumn<string>(
                name: "AssignedTo",
                table: "Leads",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 450,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "AssignedStore",
                table: "Leads",
                nullable: true,
                oldClrType: typeof(Guid),
                oldNullable: true);
        }
    }
}
