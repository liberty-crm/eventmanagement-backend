﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EventManagement.Domain.Migrations
{
    public partial class CreateAttachmentTableFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Tasks_Leads_LeadId",
                table: "Tasks");

            migrationBuilder.DropForeignKey(
                name: "FK_Tasks_ServiceOffereds_ServiceId",
                table: "Tasks");

            migrationBuilder.AlterColumn<Guid>(
                name: "ServiceId",
                table: "Tasks",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AlterColumn<Guid>(
                name: "LeadId",
                table: "Tasks",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.CreateTable(
                name: "Attachments",
                columns: table => new
                {
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: false),
                    UpdatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<string>(nullable: true),
                    Id = table.Column<Guid>(nullable: false),
                    LeadId = table.Column<Guid>(nullable: false),
                    ServiceId = table.Column<Guid>(nullable: true),
                    UserId = table.Column<string>(nullable: true),
                    Type = table.Column<string>(nullable: true),
                    OriginalDocumentName = table.Column<string>(nullable: true),
                    UploadedDocumentName = table.Column<string>(nullable: true),
                    UploadPath = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Attachments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Attachments_Leads_LeadId",
                        column: x => x.LeadId,
                        principalTable: "Leads",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Attachments_ServiceOffereds_ServiceId",
                        column: x => x.ServiceId,
                        principalTable: "ServiceOffereds",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Attachments_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Attachments_LeadId",
                table: "Attachments",
                column: "LeadId");

            migrationBuilder.CreateIndex(
                name: "IX_Attachments_ServiceId",
                table: "Attachments",
                column: "ServiceId");

            migrationBuilder.CreateIndex(
                name: "IX_Attachments_UserId",
                table: "Attachments",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Tasks_Leads_LeadId",
                table: "Tasks",
                column: "LeadId",
                principalTable: "Leads",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Tasks_ServiceOffereds_ServiceId",
                table: "Tasks",
                column: "ServiceId",
                principalTable: "ServiceOffereds",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Tasks_Leads_LeadId",
                table: "Tasks");

            migrationBuilder.DropForeignKey(
                name: "FK_Tasks_ServiceOffereds_ServiceId",
                table: "Tasks");

            migrationBuilder.DropTable(
                name: "Attachments");

            migrationBuilder.AlterColumn<Guid>(
                name: "ServiceId",
                table: "Tasks",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "LeadId",
                table: "Tasks",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Tasks_Leads_LeadId",
                table: "Tasks",
                column: "LeadId",
                principalTable: "Leads",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Tasks_ServiceOffereds_ServiceId",
                table: "Tasks",
                column: "ServiceId",
                principalTable: "ServiceOffereds",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
