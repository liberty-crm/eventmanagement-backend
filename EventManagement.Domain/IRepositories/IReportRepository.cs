using System;
using EventManagement.Domain.Entities;

namespace EventManagement.Domain
{
    public interface IReportRepository : IRepository<Report, Guid>
    {

    }
}
