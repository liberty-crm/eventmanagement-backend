using System;
using EventManagement.Domain.Entities;

namespace EventManagement.Domain
{
    public interface IAttachmentRepository : IRepository<Attachment, Guid>
    {

    }
}
