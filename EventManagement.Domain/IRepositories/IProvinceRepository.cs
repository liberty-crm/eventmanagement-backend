using System;
using EventManagement.Domain.Entities;

namespace EventManagement.Domain
{
    public interface IProvinceRepository : IRepository<Province, Guid>
    {

    }
}
