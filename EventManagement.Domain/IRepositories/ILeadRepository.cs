using System;
using EventManagement.Domain.Entities;

namespace EventManagement.Domain
{
    public interface ILeadRepository : IRepository<Lead, Guid>
    {

    }
}
