using System;
using EventManagement.Domain.Entities;

namespace EventManagement.Domain
{
    public interface ITaskRepository : IRepository<Task, Guid>
    {

    }
}
