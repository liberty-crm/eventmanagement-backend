using System;
using EventManagement.Domain.Entities;

namespace EventManagement.Domain
{
    public interface IServiceOfferedInvoiceRepository : IRepository<ServiceOfferedInvoice, Guid>
    {

    }
}
