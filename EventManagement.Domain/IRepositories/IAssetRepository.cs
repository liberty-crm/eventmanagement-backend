using System;
using EventManagement.Domain.Entities;

namespace EventManagement.Domain
{
    public interface IAssetRepository : IRepository<Asset, Guid>
    {

    }
}
