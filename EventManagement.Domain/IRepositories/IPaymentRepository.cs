using System;
using EventManagement.Domain.Entities;

namespace EventManagement.Domain
{
    public interface IPaymentRepository : IRepository<Payment, Guid>
    {

    }
}
