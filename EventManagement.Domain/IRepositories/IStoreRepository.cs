using System;
using EventManagement.Domain.Entities;

namespace EventManagement.Domain
{
    public interface IStoreRepository : IRepository<Store, Guid>
    {

    }
}
