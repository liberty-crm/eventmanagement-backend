using System;
using EventManagement.Domain.Entities;

namespace EventManagement.Domain
{
    public interface IContactRepository : IRepository<Contact, Guid>
    {

    }
}
