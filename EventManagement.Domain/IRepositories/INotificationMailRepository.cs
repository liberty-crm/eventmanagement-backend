using System;
using EventManagement.Domain.Entities;

namespace EventManagement.Domain
{
    public interface INotificationMailRepository : IRepository<NotificationMail, Guid>
    {

    }
}
