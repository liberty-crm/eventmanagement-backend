using System;
using EventManagement.Domain.Entities;

namespace EventManagement.Domain
{
    public interface IHistoryRepository : IRepository<History, Guid>
    {

    }
}
