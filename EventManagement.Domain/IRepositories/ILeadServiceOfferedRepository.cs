using System;
using EventManagement.Domain.Entities;

namespace EventManagement.Domain
{
    public interface ILeadServiceOfferedRepository : IRepository<LeadServiceOffered, Guid>
    {

    }
}
