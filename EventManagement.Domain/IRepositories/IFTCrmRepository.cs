using System;
using EventManagement.Domain.Entities;

namespace EventManagement.Domain
{
    public interface IFTCrmRepository : IRepository<FTCrm, Guid>
    {

    }
}
