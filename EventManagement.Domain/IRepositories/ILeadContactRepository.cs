using System;
using EventManagement.Domain.Entities;

namespace EventManagement.Domain
{
    public interface ILeadContactRepository : IRepository<LeadContact, Guid>
    {

    }
}
