using System;
using EventManagement.Domain.Entities;

namespace EventManagement.Domain
{
    public interface IServiceOfferedRepository : IRepository<ServiceOffered, Guid>
    {

    }
}
