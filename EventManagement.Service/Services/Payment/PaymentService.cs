using System;
using System.Collections.Generic;
using EventManagement.Domain;
using EventManagement.Domain.Entities;
using EventManagement.Dto;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using System.Linq;
using Newtonsoft.Json;
namespace EventManagement.Service
{
    public class PaymentService : ServiceBase<Payment, Guid>, IPaymentService
    {

        #region PRIVATE MEMBERS

        private readonly IPaymentRepository _paymentRepository;
        private readonly IConfiguration _configuration;
        #endregion


        #region CONSTRUCTOR

        public PaymentService(IPaymentRepository paymentRepository, ILogger<PaymentService> logger, IConfiguration configuration) : base(paymentRepository, logger)
        {
            _paymentRepository = paymentRepository;
            _configuration = configuration;
        }

        #endregion


        #region PUBLIC MEMBERS   


        #endregion


        #region OVERRIDDEN IMPLEMENTATION

        public override Dictionary<string, PropertyMappingValue> GetPropertyMapping()
        {
            return new Dictionary<string, PropertyMappingValue>(StringComparer.OrdinalIgnoreCase)
                    {
                        { "Id", new PropertyMappingValue(new List<string>() { "Id" } ) },
                        { "Date", new PropertyMappingValue(new List<string>() { "Date" } )},
                        { "Mode", new PropertyMappingValue(new List<string>() { "Mode" } )},
                        { "Location", new PropertyMappingValue(new List<string>() { "Location" } )},
                        { "Amount", new PropertyMappingValue(new List<string>() { "Amount" } )},
                        { "ReferenceDetails", new PropertyMappingValue(new List<string>() { "ReferenceDetails" } )},
                    };
        }

        public override string GetDefaultOrderByColumn()
        {
            return "Id";
        }

        public override string GetDefaultFieldsToSelect()
        {
            return "Id,Date,Mode,Location,Amount,ReferenceDetails,LeadServiceId,LeadServiceOffered";
        }

        #endregion
    }
}
