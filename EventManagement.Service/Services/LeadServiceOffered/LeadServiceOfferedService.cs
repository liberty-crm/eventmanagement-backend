using System;
using System.Collections.Generic;
using EventManagement.Domain;
using EventManagement.Domain.Entities;
using EventManagement.Dto;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using System.Linq;
using Newtonsoft.Json;
using EventManagement.Dto.LeadServiceOffered;

namespace EventManagement.Service
{
    public class LeadServiceOfferedService : ServiceBase<LeadServiceOffered, Guid>, ILeadServiceOfferedService
    {

        #region PRIVATE MEMBERS

        private readonly ILeadServiceOfferedRepository _leadserviceofferedRepository;
        private readonly IConfiguration _configuration;
        private readonly IFTCrmService _ftCrmService;
        private readonly IContactService _contactService;
        private readonly IServiceOfferedService _serviceOfferedService;
        private readonly IProvinceService _provinceService;

        #endregion


        #region CONSTRUCTOR

        public LeadServiceOfferedService(ILeadServiceOfferedRepository leadserviceofferedRepository, 
            ILogger<LeadServiceOfferedService> logger, IConfiguration configuration,
            IFTCrmService ftCrmService,
            IContactService contactService,
            IServiceOfferedService serviceOfferedService,
            IProvinceService provinceService) : base(leadserviceofferedRepository, logger)
        {
            _leadserviceofferedRepository = leadserviceofferedRepository;
            _configuration = configuration;
            _ftCrmService = ftCrmService;
            _contactService = contactService;
            _serviceOfferedService = serviceOfferedService;
            _provinceService = provinceService;
        }

        #endregion


        #region PUBLIC MEMBERS   

        /// <summary>
        /// Get invoice for the offered service/product
        /// </summary>
        /// <param name="leadServiceOfferedId">leadServiceOfferedId</param>
        /// <returns>invocie</returns>
        public async Task<InvoiceDto> GetInvoice(string leadServiceOfferedId)
        {
            var filterOption = new FilterOptionsModel();
            filterOption.SearchQuery = "Id==\"" + leadServiceOfferedId + "\"";
            var serviceOfferedd = await GetFilteredEntities(filterOption);
            var clientDetailss = serviceOfferedd.Where(x => x.RelationWithLead.ToLower() == "self");

            var serviceOffered = GetAllEntities().Where(x => x.Id == Guid.Parse(leadServiceOfferedId)).FirstOrDefault();
            var clientDetails = _contactService.GetAllEntities().Where(x => x.RelatedLeadContactId == serviceOffered.LeadId && x.RelationWithLead.ToLower() == "self").FirstOrDefault();
            var ftCrm = _ftCrmService.GetAllEntities().Where(x => x.LeadServiceId == Guid.Parse(leadServiceOfferedId)).FirstOrDefault();
            var service = _serviceOfferedService.GetEntityById(serviceOffered.ServiceOfferedId);



            var random = new Random();
            var invoiceNumber = random.Next(0, 100000);
            var invoice = new InvoiceDto();
            invoice.InvoiceNumber = invoiceNumber.ToString();
            invoice.IssueDate = DateTime.UtcNow;
            invoice.DueDate = invoice.IssueDate.AddDays(10);

            invoice.From = new InvoiceFrom()
            {
                Title = "Liberty Financial Group",
                Address = "British Columbia, Canada",
                PhoneNumber = "+1 9987654564",
                Email = "hello@lfg.com",
                Website = "www.lfg.com"
            };

            invoice.Client = new InvoiceClient()
            {
                Title = clientDetails.FirstName + " " + clientDetails.LastName,
                Address = clientDetails.Address,
                Email = clientDetails.Email,
                PhoneNumber = clientDetails.Phone
            };

            invoice.Client.ProvinceDetails = _provinceService.GetEntityById(Guid.Parse(clientDetails.Province));

            invoice.InvoiceService = new InvoiceService()
            {
                Title = service.Name,
                Description = serviceOffered.Description,
                Qty = 1,
                Rate = ftCrm != null ? (float)(ftCrm.Amount * 0.33) : 0,
                Total = ftCrm != null ? (float)(ftCrm.Amount * 0.33) : 0
            };

            invoice.HSTAmount = ((ftCrm != null ? (float)(ftCrm.Amount * 0.33) : 0) * invoice.Client.ProvinceDetails.HSTPercent) / 100;
            invoice.TotalAmount = invoice.InvoiceService.Total + invoice.HSTAmount;
           
            return invoice;
        }

        #endregion


        #region OVERRIDDEN IMPLEMENTATION

        public override Dictionary<string, PropertyMappingValue> GetPropertyMapping()
        {
            return new Dictionary<string, PropertyMappingValue>(StringComparer.OrdinalIgnoreCase)
                    {
                        { "Id", new PropertyMappingValue(new List<string>() { "Id" } ) },
                        //{ "Name", new PropertyMappingValue(new List<string>() { "Name" } )},
                        { "ServiceOffered", new PropertyMappingValue(new List<string>() { "ServiceOffered" } )},
                        { "UpdatedOn", new PropertyMappingValue(new List<string>() { "UpdatedOn" } )},
                        { "AssignedTo", new PropertyMappingValue(new List<string>() { "AssignedTo" } )},
                        { "AssignedStatus", new PropertyMappingValue(new List<string>() { "AssignedStatus" } )},
                        { "User", new PropertyMappingValue(new List<string>() { "User" } )},
                    };
        }

        public override string GetDefaultOrderByColumn()
        {
            return "UpdatedOn";
        }

        public override string GetDefaultFieldsToSelect()
        {
            return "Id,Description,ServiceOffered,AssignedTo,AssignedStatus,Lead,Contact,User";
        }

        #endregion
    }
}
