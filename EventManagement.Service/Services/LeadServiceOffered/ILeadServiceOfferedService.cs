using System;
using System.Threading.Tasks;
using EventManagement.Dto;
using EventManagement.Domain.Entities;
using EventManagement.Dto.LeadServiceOffered;

namespace EventManagement.Service
{
    public interface ILeadServiceOfferedService : IService<LeadServiceOffered, Guid>
    {
        /// <summary>
        /// Get invoice for the offered service/product
        /// </summary>
        /// <param name="leadServiceOfferedId">leadServiceOfferedId</param>
        /// <returns>invocie</returns>
        Task<InvoiceDto> GetInvoice(string leadServiceOfferedId);
    }
}
