using System;
using System.Threading.Tasks;
using EventManagement.Dto;
using EventManagement.Domain.Entities;

namespace EventManagement.Service
{
    public interface IAttachmentService : IService<Attachment, Guid>
    {
        /// <summary>
        ///     check content type is multipart content type or not
        /// </summary>
        /// <param name="contentType">Content Type</param> 
        /// <returns>True or False</returns>
        bool IsMultipartContentType(string contentType);

        /// <summary>
        ///     Upload Attachment
        /// </summary>
        /// <param name="fileName">File Name</param>
        /// <param name="referenceId">Reference Id. i.g logodId,WebsiteBannerId</param>
        /// <param name="fileArray">Binary Array of File</param>
        /// <returns>AttachmentForUpdate Dto</returns>
        Task<AttachmentDto> UploadAttachment(string fileName, AttachmentForCreation attachment, byte[] fileArray);

        /// <summary>
        /// Get File URL By File Name
        /// </summary>
        /// <param name="fileName">File Name</param>
        /// <param name="destinationContainer">Container Name where file is uploaded</param>
        /// <returns>Remote File URL</returns>
        string GetFileUrlByFileName(string fileName, string destinationContainer);

    }
}
