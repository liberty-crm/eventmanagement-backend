using System;
using System.Collections.Generic;
using EventManagement.Domain;
using EventManagement.Domain.Entities;
using EventManagement.Dto;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using System.Linq;
using Newtonsoft.Json;
using Microsoft.Azure.Storage;
using Microsoft.Azure.Storage.Blob;
using System.IO;
using EventManagement.Utility;
using Microsoft.AspNetCore.StaticFiles;
using AutoMapper;

namespace EventManagement.Service
{
    public class AttachmentService : ServiceBase<Attachment, Guid>, IAttachmentService
    {

        #region PRIVATE MEMBERS

        private readonly IAttachmentRepository _attachmentRepository;
        private readonly IConfiguration _configuration;
        private readonly CloudBlobClient _cloudBlobClient;
        #endregion


        #region CONSTRUCTOR

        public AttachmentService(IAttachmentRepository attachmentRepository, ILogger<AttachmentService> logger, IConfiguration configuration) : base(attachmentRepository, logger)
        {
            _attachmentRepository = attachmentRepository;
            _configuration = configuration;           

            var storageConnectionString = _configuration.GetSection("SendgridConnectionString").Value;//webconfig or Azure config file
            CloudStorageAccount cloudStorageAccount = CloudStorageAccount.Parse(storageConnectionString);
            _cloudBlobClient = cloudStorageAccount.CreateCloudBlobClient();
        }

        #endregion


        #region PUBLIC MEMBERS   
        /// <summary>
        ///     check content type is multipart content type or not
        /// </summary>
        /// <param name="contentType">Content Type</param> 
        /// <returns>True or False</returns>

        public bool IsMultipartContentType(string contentType)
        {
            return !string.IsNullOrEmpty(contentType)
                   && contentType.IndexOf("multipart/", StringComparison.OrdinalIgnoreCase) >= 0;
        }


        /// <summary>
        ///     Upload Attachment
        /// </summary>
        /// <param name="fileName">File Name</param>
        /// <param name="referenceId">Reference Id. i.g logodId,WebsiteBannerId</param>
        /// <param name="fileArray">Binary Array of File</param>
        /// <returns>AttachmentForUpdate Dto</returns>
        public async Task<AttachmentDto> UploadAttachment(string fileName, AttachmentForCreation attachment, byte[] fileArray)
        {
            //attachmentIDsDTO for sending list of attachment id to client side to bind file with attachment id

            Guid refId = Guid.NewGuid();

           
            // Concatenate attachmentID with fileName to make fileName unique
            var uploadedFileNameBlob = refId + "/attachments/" + refId + "_" + fileName;

            //Upload file to azure blob
            
            UploadToAzureBlob(uploadedFileNameBlob, fileName, _configuration.GetSection("BlobContainer").Value, fileArray);


            var azureUploadedPath = GetFileUrlByFileName(uploadedFileNameBlob, _configuration.GetSection("BlobContainer").Value);

            attachment.Id = refId.ToString();
            attachment.OriginalDocumentName = fileName;
            attachment.UploadPath = azureUploadedPath;
            attachment.UploadedDocumentName = uploadedFileNameBlob;

            //update attachment
            var attachmentToReturn = await CreateEntityAsync<AttachmentDto, AttachmentForCreation>(attachment);

            return attachmentToReturn;
        }

        /// <summary>
        ///     Upload document to azure
        /// </summary>
        /// <param name="uploadedFileNameBlob">File name</param>
        /// <param name="attFile">File location</param>
        /// <param name="destinationContainer">destination folder on azure</param>
        /// <returns>Uri path of the uploaded content</returns>
        public string UploadToAzureBlob(string uploadedFileNameBlob, string attFile, string destinationContainer, byte[] fileArray)
        {
            if (EventManagementUtils.IsValidFileExtension(uploadedFileNameBlob))
            {
                //Upload to Blob storage Code
                var cloudBlobContainer = GetContainerFromAzureBlob(destinationContainer);
                var cloudBlockBlob = cloudBlobContainer.GetBlockBlobReference(uploadedFileNameBlob);

                string contentType = null;

                new FileExtensionContentTypeProvider().TryGetContentType(uploadedFileNameBlob, out contentType);                

                if (!string.IsNullOrEmpty(contentType))
                {
                    cloudBlockBlob.Properties.ContentType = contentType;
                }
                using (var stream = new MemoryStream(fileArray, writable: false))
                {
                    cloudBlockBlob.UploadFromStream(stream);
                }
                return cloudBlockBlob.StorageUri.PrimaryUri.AbsoluteUri;
            }

            return string.Empty;

        }

        /// <summary>
        ///     Method to get connected to Azure blob container
        /// </summary>
        /// <param name="destinationContainer">Container name to connect to</param>
        /// <returns>Cloud Blob Container object</returns>
        public CloudBlobContainer GetContainerFromAzureBlob(string destinationContainer)
        {
            var cloudBlobContainer = _cloudBlobClient.GetContainerReference(destinationContainer);
            cloudBlobContainer.CreateIfNotExists();
            return cloudBlobContainer;
        }

        /// <summary>
        /// Get File URL By File Name
        /// </summary>
        /// <param name="fileName">File Name</param>
        /// <param name="destinationContainer">Container Name where file is uploaded</param>
        /// <returns>Remote File URL</returns>
        public string GetFileUrlByFileName(string fileName, string destinationContainer)
        {
            var cloudBlobContainer = GetContainerFromAzureBlob(destinationContainer);
            var cloudBlob = cloudBlobContainer.GetBlobReference(fileName);
            var fileUrl = cloudBlob.StorageUri.PrimaryUri.AbsoluteUri;

            return fileUrl;
        }

        #endregion


        #region OVERRIDDEN IMPLEMENTATION

        public override Dictionary<string, PropertyMappingValue> GetPropertyMapping()
        {
            return new Dictionary<string, PropertyMappingValue>(StringComparer.OrdinalIgnoreCase)
                    {
                        { "Id", new PropertyMappingValue(new List<string>() { "Id" } ) },
                        { "UpdatedOn", new PropertyMappingValue(new List<string>() { "UpdatedOn" } )},
                    };
        }

        public override string GetDefaultOrderByColumn()
        {
            return "UpdatedOn";
        }

        public override string GetDefaultFieldsToSelect()
        {
            return "Id,Type,OriginalDocumentName,CreatedOn,UploadPath";
        }

        #endregion
    }
}
