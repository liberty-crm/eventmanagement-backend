using System;
using System.Collections.Generic;
using EventManagement.Domain;
using EventManagement.Domain.Entities;
using EventManagement.Dto;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using System.Linq;
using Newtonsoft.Json;
namespace EventManagement.Service
{
    public class ServiceOfferedService : ServiceBase<ServiceOffered, Guid>, IServiceOfferedService
    {

        #region PRIVATE MEMBERS

        private readonly IServiceOfferedRepository _serviceofferedRepository;
        private readonly IConfiguration _configuration;
        #endregion


        #region CONSTRUCTOR

        public ServiceOfferedService(IServiceOfferedRepository serviceofferedRepository, ILogger<ServiceOfferedService> logger, IConfiguration configuration) : base(serviceofferedRepository, logger)
        {
            _serviceofferedRepository = serviceofferedRepository;
            _configuration = configuration;
        }

        #endregion


        #region PUBLIC MEMBERS   


        #endregion


        #region OVERRIDDEN IMPLEMENTATION

        public override Dictionary<string, PropertyMappingValue> GetPropertyMapping()
        {
            return new Dictionary<string, PropertyMappingValue>(StringComparer.OrdinalIgnoreCase)
                    {
                        { "Id", new PropertyMappingValue(new List<string>() { "Id" } ) },
                        { "Name", new PropertyMappingValue(new List<string>() { "Name" } )},
                        //{ "UpdatedOn", new PropertyMappingValue(new List<string>() { "UpdatedOn" } )},
                        { "Code", new PropertyMappingValue(new List<string>() { "Code" } )},
                        { "Category", new PropertyMappingValue(new List<string>() { "Category" } )},
                        { "ServiceManagerId", new PropertyMappingValue(new List<string>() { "ServiceManagerId" } )},
                        { "InvoiceGeneration", new PropertyMappingValue(new List<string>() { "InvoiceGeneration" } )},
                        { "InvoiceAmountCalculation", new PropertyMappingValue(new List<string>() { "InvoiceAmountCalculation" } )},
                        { "Value", new PropertyMappingValue(new List<string>() { "Value" } )},
                        { "User", new PropertyMappingValue(new List<string>() { "User" } )},
                    };
        }

        public override string GetDefaultOrderByColumn()
        {
            return "Id";
        }

        public override string GetDefaultFieldsToSelect()
        {
            return "Id,Name,Code,Category,ServiceManagerId,InvoiceGeneration,InvoiceAmountCalculation,Value,User";
        }

        #endregion
    }
}
