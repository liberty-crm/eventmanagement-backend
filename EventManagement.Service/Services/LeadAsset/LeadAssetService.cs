using System;
using System.Collections.Generic;
using EventManagement.Domain;
using EventManagement.Domain.Entities;
using EventManagement.Dto;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using System.Linq;
using Newtonsoft.Json;
namespace EventManagement.Service
{
    public class LeadAssetService : ServiceBase<LeadAsset, Guid>, ILeadAssetService
    {

        #region PRIVATE MEMBERS

        private readonly ILeadAssetRepository _leadassetRepository;
        private readonly IConfiguration _configuration;
        #endregion


        #region CONSTRUCTOR

        public LeadAssetService(ILeadAssetRepository leadassetRepository, ILogger<LeadAssetService> logger, IConfiguration configuration) : base(leadassetRepository, logger)
        {
            _leadassetRepository = leadassetRepository;
            _configuration = configuration;
        }

        #endregion


        #region PUBLIC MEMBERS   
        

        #endregion


        #region OVERRIDDEN IMPLEMENTATION

        public override Dictionary<string, PropertyMappingValue> GetPropertyMapping()
        {
            return new Dictionary<string, PropertyMappingValue>(StringComparer.OrdinalIgnoreCase)
                    {
                        { "Id", new PropertyMappingValue(new List<string>() { "Id" } ) },
                        { "Name", new PropertyMappingValue(new List<string>() { "Name" } )},
                        { "UpdatedOn", new PropertyMappingValue(new List<string>() { "UpdatedOn" } )},
                        { "Asset", new PropertyMappingValue(new List<string>() { "Asset" } )},
                    };
        }

        public override string GetDefaultOrderByColumn()
        {
            return "UpdatedOn";
        }

        public override string GetDefaultFieldsToSelect()
        {
            return "Id,Name";
        }

        #endregion
    }
}
