using System;
using System.Collections.Generic;
using EventManagement.Domain;
using EventManagement.Domain.Entities;
using EventManagement.Dto;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using System.Linq;
using Newtonsoft.Json;
namespace EventManagement.Service
{
    public class HistoryService : ServiceBase<History, Guid>, IHistoryService
    {

        #region PRIVATE MEMBERS

        private readonly IHistoryRepository _historyRepository;
        private readonly IConfiguration _configuration;
        #endregion


        #region CONSTRUCTOR

        public HistoryService(IHistoryRepository historyRepository, ILogger<HistoryService> logger, IConfiguration configuration) : base(historyRepository, logger)
        {
            _historyRepository = historyRepository;
            _configuration = configuration;
        }

        #endregion


        #region PUBLIC MEMBERS   


        #endregion


        #region OVERRIDDEN IMPLEMENTATION

        public override Dictionary<string, PropertyMappingValue> GetPropertyMapping()
        {
            return new Dictionary<string, PropertyMappingValue>(StringComparer.OrdinalIgnoreCase)
                    {
                        { "Id", new PropertyMappingValue(new List<string>() { "Id" } ) },
                        { "UpdatedOn", new PropertyMappingValue(new List<string>() { "UpdatedOn" } )},
                        { "CreatedOn", new PropertyMappingValue(new List<string>() { "CreatedOn" } )},
                    };
        }

        public override string GetDefaultOrderByColumn()
        {
            return "UpdatedOn";
        }

        public override string GetDefaultFieldsToSelect()
        {
            return "Id,Activity,TagData,User,CreatedOn";
        }

        #endregion
    }
}
