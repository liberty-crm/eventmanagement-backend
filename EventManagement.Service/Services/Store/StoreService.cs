using System;
using System.Collections.Generic;
using EventManagement.Domain;
using EventManagement.Domain.Entities;
using EventManagement.Dto;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using System.Linq;
using Newtonsoft.Json;
namespace EventManagement.Service
{
    public class StoreService : ServiceBase<Store, Guid>, IStoreService
    {

        #region PRIVATE MEMBERS

        private readonly IStoreRepository _storeRepository;
        private readonly IConfiguration _configuration;
        #endregion


        #region CONSTRUCTOR

        public StoreService(IStoreRepository storeRepository, ILogger<StoreService> logger, IConfiguration configuration) : base(storeRepository, logger)
        {
            _storeRepository = storeRepository;
            _configuration = configuration;
        }

        #endregion


        #region PUBLIC MEMBERS   


        #endregion


        #region OVERRIDDEN IMPLEMENTATION

        public override Dictionary<string, PropertyMappingValue> GetPropertyMapping()
        {
            return new Dictionary<string, PropertyMappingValue>(StringComparer.OrdinalIgnoreCase)
                    {
                        { "Id", new PropertyMappingValue(new List<string>() { "Id" } ) },
                        { "Code", new PropertyMappingValue(new List<string>() { "Code" } )},
                        { "Name", new PropertyMappingValue(new List<string>() { "Name" } )},
                        { "Address", new PropertyMappingValue(new List<string>() { "Address" } )},
                        { "Zipcode", new PropertyMappingValue(new List<string>() { "Zipcode" } )},
                        { "City", new PropertyMappingValue(new List<string>() { "City" } )},
                        { "Province", new PropertyMappingValue(new List<string>() { "Province" } )},
                        { "Country", new PropertyMappingValue(new List<string>() { "Country" } )},
                        { "Parentdistrict", new PropertyMappingValue(new List<string>() { "Parentdistrict" } )},
                    };
        }

        public override string GetDefaultOrderByColumn()
        {
            return "Id";
        }

        public override string GetDefaultFieldsToSelect()
        {
            return "Id,Code,Name,Address,Zipcode,City,Province,Country,Parentdistrict";
        }

        #endregion
    }
}
