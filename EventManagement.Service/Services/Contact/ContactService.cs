using System;
using System.Collections.Generic;
using EventManagement.Domain;
using EventManagement.Domain.Entities;
using EventManagement.Dto;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using System.Linq;
using Newtonsoft.Json;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.JsonPatch.Operations;

namespace EventManagement.Service
{
    public class ContactService : ServiceBase<Contact, Guid>, IContactService
    {

        #region PRIVATE MEMBERS

        private readonly IContactRepository _contactRepository;
        private readonly IConfiguration _configuration;
        #endregion


        #region CONSTRUCTOR

        public ContactService(IContactRepository contactRepository, ILogger<ContactService> logger, IConfiguration configuration) : base(contactRepository, logger)
        {
            _contactRepository = contactRepository;
            _configuration = configuration;
        }

        #endregion


        #region PUBLIC MEMBERS   

        /// <summary>
        /// Update sin detail for lead contact
        /// </summary>
        /// <param name="leadId">Lead Id</param>
        /// <param name="sin">sin</param>
        /// <returns>status of operation</returns>
        public async Task<int> UpdateSinDetails(string leadId, string sin)
        {
            var contact = _contactRepository.GetAllEntities().Where(x => x.RelatedLeadContactId == Guid.Parse(leadId)
                                && x.RelationWithLead == "Self").FirstOrDefault();

            var patchContact = new JsonPatchDocument<Contact>();
            patchContact.Operations.Add(new Operation<Contact> { op = "replace", path = "/sin", value = sin });
            
            patchContact.ApplyTo(contact);            

            return await _contactRepository.UpdatePartialEntityAsync(contact, patchContact);
        }

        #endregion


        #region OVERRIDDEN IMPLEMENTATION

        public override Dictionary<string, PropertyMappingValue> GetPropertyMapping()
        {
            return new Dictionary<string, PropertyMappingValue>(StringComparer.OrdinalIgnoreCase)
                    {
                        { "Id", new PropertyMappingValue(new List<string>() { "Id" } ) },
                        { "FirstName", new PropertyMappingValue(new List<string>() { "FirstName" } )},
                        { "UpdatedOn", new PropertyMappingValue(new List<string>() { "UpdatedOn" } )},
                        { "MiddleName", new PropertyMappingValue(new List<string>() { "MiddleName" } )},
                        { "LastName", new PropertyMappingValue(new List<string>() { "LastName" } )},
                        { "Address", new PropertyMappingValue(new List<string>() { "Address" } )},
                        { "City", new PropertyMappingValue(new List<string>() { "City" } )},
                        { "Province", new PropertyMappingValue(new List<string>() { "Province" } )},
                        { "Zipcode", new PropertyMappingValue(new List<string>() { "Zipcode" } )},
                        { "Email", new PropertyMappingValue(new List<string>() { "Email" } )},
                        { "Phone", new PropertyMappingValue(new List<string>() { "Phone" } )},
                        { "ExistingClient", new PropertyMappingValue(new List<string>() { "ExistingClient" } )},
                        { "Employment", new PropertyMappingValue(new List<string>() { "Employment" } )},
                        { "Source", new PropertyMappingValue(new List<string>() { "Source" } )},
                        { "Position", new PropertyMappingValue(new List<string>() { "Position" } )},
                        { "CompanyName", new PropertyMappingValue(new List<string>() { "CompanyName" } )},
                        { "Website", new PropertyMappingValue(new List<string>() { "Website" } )},
                        { "EmploymentDate", new PropertyMappingValue(new List<string>() { "EmploymentDate" } )},
                        { "SalaryFrequency", new PropertyMappingValue(new List<string>() { "SalaryFrequency" } )},
                        { "NoOfChildren", new PropertyMappingValue(new List<string>() { "NoOfChildren" } )},
                        { "AllowMarketingMessages", new PropertyMappingValue(new List<string>() { "AllowMarketingMessages" } )},
                        {"AllowUseOfPersonalInfo", new PropertyMappingValue(new List<string>() { "AllowUseOfPersonalInfo" } )},
                        { "RelationWithLead", new PropertyMappingValue(new List<string>() { "RelationWithLead" } )},
                        { "RelatedLeadContactId", new PropertyMappingValue(new List<string>() { "RelatedLeadContactId" } )},
                        { "Status", new PropertyMappingValue(new List<string>() { "Status" } )},
                        { "MaritalStatus", new PropertyMappingValue(new List<string>() { "MaritalStatus" } )},
                        { "Sin", new PropertyMappingValue(new List<string>() { "Sin" } )},
                        { "SalaryAmount", new PropertyMappingValue(new List<string>() { "SalaryAmount" } )},
                    };
        }

        public override string GetDefaultOrderByColumn()
        {
            return "UpdatedOn";
        }

        public override string GetDefaultFieldsToSelect()
        {
            return "Id,FirstName,MiddleName,LastName,Address,City,Province,Zipcode,Email,Phone,ExistingClient,Employment,Source,Position,CompanyName,Website,EmploymentDate,SalaryFrequency,NoOfChildren,AllowMarketingMessages,RelatedLeadContactId,MaritalStatus,Sin,SalaryAmount,AllowUseOfPersonalInfo";
        }

        #endregion
    }
}
