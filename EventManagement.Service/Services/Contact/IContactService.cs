using System;
using System.Threading.Tasks;
using EventManagement.Dto;
using EventManagement.Domain.Entities;

namespace EventManagement.Service
{
    public interface IContactService : IService<Contact, Guid>
    {
        /// <summary>
        /// Update sin detail for lead contact
        /// </summary>
        /// <param name="leadId">Lead Id</param>
        /// <param name="sin">sin</param>
        /// <returns>status of operation</returns>
        Task<int> UpdateSinDetails(string leadId, string sin);
    }
}
