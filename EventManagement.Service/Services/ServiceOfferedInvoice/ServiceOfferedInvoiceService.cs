using System;
using System.Collections.Generic;
using EventManagement.Domain;
using EventManagement.Domain.Entities;
using EventManagement.Dto;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using System.Linq;
using Newtonsoft.Json;
namespace EventManagement.Service
{
    public class ServiceOfferedInvoiceService : ServiceBase<ServiceOfferedInvoice, Guid>, IServiceOfferedInvoiceService
    {

        #region PRIVATE MEMBERS

        private readonly IServiceOfferedInvoiceRepository _serviceofferedinvoiceRepository;
        private readonly IConfiguration _configuration;
        #endregion


        #region CONSTRUCTOR

        public ServiceOfferedInvoiceService(IServiceOfferedInvoiceRepository serviceofferedinvoiceRepository, ILogger<ServiceOfferedInvoiceService> logger, IConfiguration configuration) : base(serviceofferedinvoiceRepository, logger)
        {
            _serviceofferedinvoiceRepository = serviceofferedinvoiceRepository;
            _configuration = configuration;
        }

        #endregion


        #region PUBLIC MEMBERS   


        #endregion


        #region OVERRIDDEN IMPLEMENTATION

        public override Dictionary<string, PropertyMappingValue> GetPropertyMapping()
        {
            return new Dictionary<string, PropertyMappingValue>(StringComparer.OrdinalIgnoreCase)
                    {
                        { "Id", new PropertyMappingValue(new List<string>() { "Id" } ) },                        
                        { "UpdatedOn", new PropertyMappingValue(new List<string>() { "UpdatedOn" } )},
                        { "InvoiceNumber", new PropertyMappingValue(new List<string>() { "InvoiceNumber" } )},
                        { "AttachmentPath", new PropertyMappingValue(new List<string>() { "AttachmentPath" } )},
                        { "CreatedOn", new PropertyMappingValue(new List<string>() { "CreatedOn" } )},
                        { "Amount", new PropertyMappingValue(new List<string>() { "Amount" } )},
                    };
        }

        public override string GetDefaultOrderByColumn()
        {
            return "UpdatedOn";
        }

        public override string GetDefaultFieldsToSelect()
        {
            return "Id,UpdatedOn,InvoiceNumber,AttachmentPath,CreatedOn,Amount,LeadServiceOfferedId,LeadServiceOffered";
        }

        #endregion
    }
}
