using System;
using System.Collections.Generic;
using EventManagement.Domain;
using EventManagement.Dto;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using Task = EventManagement.Domain.Entities.Task;

namespace EventManagement.Service
{
    public class TaskService : ServiceBase<Task, Guid>, ITaskService
    {

        #region PRIVATE MEMBERS

        private readonly ITaskRepository _taskRepository;
        private readonly IConfiguration _configuration;
        #endregion


        #region CONSTRUCTOR

        public TaskService(ITaskRepository taskRepository, ILogger<TaskService> logger, IConfiguration configuration) : base(taskRepository, logger)
        {
            _taskRepository = taskRepository;
            _configuration = configuration;
        }

        #endregion


        #region PUBLIC MEMBERS   


        #endregion


        #region OVERRIDDEN IMPLEMENTATION

        public override Dictionary<string, PropertyMappingValue> GetPropertyMapping()
        {
            return new Dictionary<string, PropertyMappingValue>(StringComparer.OrdinalIgnoreCase)
                    {
                        { "Id", new PropertyMappingValue(new List<string>() { "Id" } ) },
                        { "UpdatedOn", new PropertyMappingValue(new List<string>() { "UpdatedOn" } )},
                    };
        }

        public override string GetDefaultOrderByColumn()
        {
            return "UpdatedOn";
        }

        public override string GetDefaultFieldsToSelect()
        {
            return "Id,Title,Date,IsCompleted,Description,IsImportant,IsMarked,LeadId,ServiceId,UserId";
        }

        #endregion
    }
}
