using System;
using Task = EventManagement.Domain.Entities.Task;

namespace EventManagement.Service
{
    public interface ITaskService : IService<Task, Guid>
    {

    }
}
