using System;
using System.Collections.Generic;
using EventManagement.Domain;
using EventManagement.Domain.Entities;
using EventManagement.Dto;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using System.Linq;
using Newtonsoft.Json;
using System.IO;
using SendGrid;
using SendGrid.Helpers.Mail;
using DinkToPdf;
using System.Runtime.Loader;
using System.Reflection;
using PuppeteerSharp;
using EventManagement.Utility;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.Azure.Storage.Blob;
using Microsoft.Azure.Storage;
using SelectPdf;
using System.Text.RegularExpressions;

namespace EventManagement.Service
{

    internal class CustomAssemblyLoadContext : AssemblyLoadContext
    {
        public IntPtr LoadUnmanagedLibrary(string absolutePath)
        {
            return LoadUnmanagedDll(absolutePath);
        }
        protected override IntPtr LoadUnmanagedDll(String unmanagedDllName)
        {
            return LoadUnmanagedDllFromPath(unmanagedDllName);
        }

        protected override Assembly Load(AssemblyName assemblyName)
        {
            throw new NotImplementedException();
        }
    }

    public class FTCrmService : ServiceBase<FTCrm, Guid>, IFTCrmService
    {

        #region PRIVATE MEMBERS

        private readonly IFTCrmRepository _ftcrmRepository;
        private readonly IConfiguration _configuration;
        private readonly CloudBlobClient _cloudBlobClient;
        private readonly IServiceOfferedInvoiceService _serviceOfferedInvoiceService;

        #endregion


        #region CONSTRUCTOR

        public FTCrmService(IFTCrmRepository ftcrmRepository, ILogger<FTCrmService> logger, IConfiguration configuration, IServiceOfferedInvoiceService serviceOfferedInvoiceService) : base(ftcrmRepository, logger)
        {
            _ftcrmRepository = ftcrmRepository;
            _configuration = configuration;
            _serviceOfferedInvoiceService = serviceOfferedInvoiceService;

            var storageConnectionString = _configuration.GetSection("SendgridConnectionString").Value;//webconfig or Azure config file
            CloudStorageAccount cloudStorageAccount = CloudStorageAccount.Parse(storageConnectionString);
            _cloudBlobClient = cloudStorageAccount.CreateCloudBlobClient();
        }

        #endregion


        #region PUBLIC MEMBERS   

        public async Task<bool> SendInvoiceToClientAsync(string invoiceHtml,string email, Guid leadServiceId,float invoiceAmount, string emailHtml, string subject)
        {

            var invoice = new ServiceOfferedInvoiceForCreation();
            invoice.Id = Guid.NewGuid();
            invoice.LeadServiceOfferedId = leadServiceId;
            invoice.SentDate = DateTime.Now;
            invoice.Amount = invoiceAmount;            

            var result = await _serviceOfferedInvoiceService.CreateEntityAsync<ServiceOfferedInvoiceDto, ServiceOfferedInvoiceForCreation>(invoice);

            invoiceHtml = Regex.Replace(invoiceHtml, "{{invoice.invoiceNumber}}", Convert.ToString(result.InvoiceNumber), RegexOptions.IgnoreCase);

            string htmlString = invoiceHtml;
            string baseUrl = invoiceHtml;                      

            // instantiate a html to pdf converter object
            HtmlToPdf converter = new HtmlToPdf();            

            // create a new pdf document converting an url
            PdfDocument doc = converter.ConvertHtmlString(htmlString, baseUrl);
            byte[] pdf = doc.Save();
           
          
            // Concatenate attachmentID with fileName to make fileName unique
            var uploadedFileNameBlob = leadServiceId + "/invoices/" + leadServiceId + "_Invoice.pdf";

            //Upload file to azure blob
            
            UploadToAzureBlob(uploadedFileNameBlob, "Invoice", _configuration.GetSection("BlobContainer").Value, pdf);

            var azureUploadedPath = GetFileUrlByFileName(uploadedFileNameBlob, _configuration.GetSection("BlobContainer").Value);

            await _serviceOfferedInvoiceService.UpdateEntityAsync(invoice.Id, x => new ServiceOfferedInvoice { AttachmentPath = azureUploadedPath });

             var base64File = Convert.ToBase64String(pdf);

            var client = new SendGridClient(_configuration.GetSection("SendgridApi").Value);
            var msg = MailHelper.CreateSingleEmail(new EmailAddress("support@libertyfinancialgroup.ca"), new EmailAddress(email),
                subject, "", emailHtml);
            msg.AddAttachment("Invoice.pdf", base64File);
            var response = client.SendEmailAsync(msg);

            return true;
        }

        public string UploadToAzureBlob(string uploadedFileNameBlob, string attFile, string destinationContainer, byte[] fileArray)
        {
            if (EventManagementUtils.IsValidFileExtension(uploadedFileNameBlob))
            {
                //Upload to Blob storage Code
                var cloudBlobContainer = GetContainerFromAzureBlob(destinationContainer);
                var cloudBlockBlob = cloudBlobContainer.GetBlockBlobReference(uploadedFileNameBlob);

                string contentType = null;

                new FileExtensionContentTypeProvider().TryGetContentType(uploadedFileNameBlob, out contentType);

                if (!string.IsNullOrEmpty(contentType))
                {
                    cloudBlockBlob.Properties.ContentType = contentType;
                }
                using (var stream = new MemoryStream(fileArray, writable: false))
                {
                    cloudBlockBlob.UploadFromStream(stream);
                }
                return cloudBlockBlob.StorageUri.PrimaryUri.AbsoluteUri;
            }

            return string.Empty;

        }

        public CloudBlobContainer GetContainerFromAzureBlob(string destinationContainer)
        {
            var cloudBlobContainer = _cloudBlobClient.GetContainerReference(destinationContainer);
            cloudBlobContainer.CreateIfNotExists();
            return cloudBlobContainer;
        }

        public string GetFileUrlByFileName(string fileName, string destinationContainer)
        {
            var cloudBlobContainer = GetContainerFromAzureBlob(destinationContainer);
            var cloudBlob = cloudBlobContainer.GetBlobReference(fileName);
            var fileUrl = cloudBlob.StorageUri.PrimaryUri.AbsoluteUri;

            return fileUrl;
        }

        #endregion


        #region OVERRIDDEN IMPLEMENTATION

        public override Dictionary<string, PropertyMappingValue> GetPropertyMapping()
        {
            return new Dictionary<string, PropertyMappingValue>(StringComparer.OrdinalIgnoreCase)
                    {
                        { "Id", new PropertyMappingValue(new List<string>() { "Id" } ) },
                        { "UpdatedOn", new PropertyMappingValue(new List<string>() { "UpdatedOn" } )},
                    };
        }

        public override string GetDefaultOrderByColumn()
        {
            return "UpdatedOn";
        }

        public override string GetDefaultFieldsToSelect()
        {
            return "Id,LeadServiceId,LeadServiceOffered,FtCrmUniqueId,Status,Amount,Reason";
        }

        #endregion
    }
}
