using System;
using System.Threading.Tasks;
using EventManagement.Dto;
using EventManagement.Domain.Entities;

namespace EventManagement.Service
{
    public interface IFTCrmService : IService<FTCrm, Guid>
    {
        Task<bool> SendInvoiceToClientAsync(string invoiceHtml,string email, Guid leadServiceId,float invoiceAmount, string emailHtml, string subject);
    }
}
