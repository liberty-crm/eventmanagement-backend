using System;
using System.Collections.Generic;
using EventManagement.Domain;
using EventManagement.Domain.Entities;
using EventManagement.Dto;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using System.Linq;
using Newtonsoft.Json;
namespace EventManagement.Service
{
    public class AssetService : ServiceBase<Asset, Guid>, IAssetService
    {

        #region PRIVATE MEMBERS

        private readonly IAssetRepository _assetRepository;
        private readonly IConfiguration _configuration;
        #endregion


        #region CONSTRUCTOR

        public AssetService(IAssetRepository assetRepository, ILogger<AssetService> logger, IConfiguration configuration) : base(assetRepository, logger)
        {
            _assetRepository = assetRepository;
            _configuration = configuration;
        }

        #endregion


        #region PUBLIC MEMBERS   


        #endregion


        #region OVERRIDDEN IMPLEMENTATION

        public override Dictionary<string, PropertyMappingValue> GetPropertyMapping()
        {
            return new Dictionary<string, PropertyMappingValue>(StringComparer.OrdinalIgnoreCase)
                    {
                        { "Id", new PropertyMappingValue(new List<string>() { "Id" } ) },
                        { "Name", new PropertyMappingValue(new List<string>() { "Name" } )},
                        { "Type", new PropertyMappingValue(new List<string>() { "Type" } )},
                        //{ "UpdatedOn", new PropertyMappingValue(new List<string>() { "UpdatedOn" } )},
                    };
        }

        public override string GetDefaultOrderByColumn()
        {
            return "Id";
        }

        public override string GetDefaultFieldsToSelect()
        {
            return "Id,Name,Type";
        }

        #endregion
    }
}
