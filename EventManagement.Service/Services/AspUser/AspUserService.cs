using System;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using System.Linq;
using Newtonsoft.Json;
using FinanaceManagement.API.Models;
using EventManagement.Service;
using EventManagement.Dto;
using EventManagement.Domain;

namespace EventManagement.Service
{
    public class AspUserService : ServiceBase<AspNetUsers, string>, IAspUserService
    {

        #region PRIVATE MEMBERS

        private readonly IAspUserRepository _aspuserRepository;
        private readonly IConfiguration _configuration;
        #endregion


        #region CONSTRUCTOR

        public AspUserService(IAspUserRepository aspuserRepository, ILogger<AspUserService> logger, IConfiguration configuration) : base(aspuserRepository, logger)
        {
            _aspuserRepository = aspuserRepository;
            _configuration = configuration;
        }

        #endregion


        #region PUBLIC MEMBERS   


        #endregion


        #region OVERRIDDEN IMPLEMENTATION

        public override Dictionary<string, PropertyMappingValue> GetPropertyMapping()
        {
            return new Dictionary<string, PropertyMappingValue>(StringComparer.OrdinalIgnoreCase)
                    {
                        { "Id", new PropertyMappingValue(new List<string>() { "Id" } ) },
                        { "Name", new PropertyMappingValue(new List<string>() { "Name" } )},
                        { "UpdatedOn", new PropertyMappingValue(new List<string>() { "UpdatedOn" } )},
                    };
        }

        public override string GetDefaultOrderByColumn()
        {
            return "UpdatedOn";
        }

        public override string GetDefaultFieldsToSelect()
        {
            return "Id,Email,UserName, FName, LName, ImageUrl, PhoneNumber,  Gender, Birthplace, Birthday, LivesIn, Occupation, EmailConfirmed, TwoFactorEnabled, EmployeeNumber, UpdatedOn, IsActive, MiddleName, Address, City, Province, PostalCode, ReferencePercentage";
        }

        #endregion
    }
}
