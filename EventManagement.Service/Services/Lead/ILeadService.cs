using System;
using System.Threading.Tasks;
using EventManagement.Dto;
using EventManagement.Domain.Entities;

namespace EventManagement.Service
{
    public interface ILeadService : IService<Lead, Guid>
    {
        Task<bool> SendEmailToGetLeadSensitiveInfo(string leadId, string token);

        /// <summary>
        /// Mark email sent True/False
        /// </summary>
        /// <param name="leadId">LeadId</param>
        /// <param name="isSent">True/False</param>
        /// <returns></returns>
        Task<int> MarkEmailSent(string leadId, bool isSent);

        /// <summary>
        /// Create lead (from widget)
        /// </summary>
        /// <param name="lead">LeadForCreation object</param>
        /// <returns>Newly created lead</returns>
        Task<LeadDto> CreateWidgetLead(LeadForCreation lead);

        Task<FTCrmDto> CreateFtCrmAndIntegration(FTCrmForCreation ftcrm);
    }
}
