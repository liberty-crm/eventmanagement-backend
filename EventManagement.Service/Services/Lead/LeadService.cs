using System;
using System.Collections.Generic;
using EventManagement.Domain;
using EventManagement.Domain.Entities;
using EventManagement.Dto;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using SendGrid;
using SendGrid.Helpers.Mail;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.JsonPatch.Operations;
using System.Text.RegularExpressions;
using EventManagement.Utility.Enums;
using System.Net.Http;
using Polly;
using Newtonsoft.Json;
using EventManagement.Dto.FTCrm;
using AutoMapper;

namespace EventManagement.Service
{
    public class LeadService : ServiceBase<Lead, Guid>, ILeadService
    {

        #region PRIVATE MEMBERS

        private readonly ILeadRepository _leadRepository;
        private readonly IConfiguration _configuration;
        private readonly IContactService _contactService;
        private readonly ILeadContactService _leadContactService;
        private readonly ILeadAssetService _leadAssetService;
        private readonly ILeadServiceOfferedService _leadServiceOfferedService;
        private readonly INotificationMailService _notificationMailService;
        private readonly IAspNetUserClaimService _aspNetUserClaimService;
        private readonly IAspUserService _aspUserService;
        private readonly IFTCrmService _fTCrmService;
        private readonly ITaskService _taskService;
        private readonly IHistoryService _historyService;
        private readonly IUserInfoService _userInfoService;

        #endregion


        #region CONSTRUCTOR

        public LeadService(ILeadRepository leadRepository, ILogger<LeadService> logger, 
            IConfiguration configuration,
            IContactService contactService,
            ILeadContactService leadContactService,
            ILeadAssetService leadAssetService,
            ILeadServiceOfferedService leadServiceOfferedService,
            INotificationMailService notificationMailService,
            IAspUserService aspUserService,
            IAspNetUserClaimService aspNetUserClaimService,
            IFTCrmService fTCrmService,
            IHistoryService historyService,
            ITaskService taskService,
            IUserInfoService userInfoService
            ) : base(leadRepository, logger)
        {
            _leadRepository = leadRepository;
            _configuration = configuration;
            _contactService = contactService;
            _leadContactService = leadContactService;
            _leadAssetService = leadAssetService;
            _leadServiceOfferedService = leadServiceOfferedService;
            _notificationMailService = notificationMailService;
            _aspNetUserClaimService = aspNetUserClaimService;
            _aspUserService = aspUserService;
            _fTCrmService = fTCrmService;
            _taskService = taskService;
            _historyService = historyService;
            _userInfoService = userInfoService;

        }

        #endregion


        #region PUBLIC MEMBERS   

        public async Task<bool> SendEmailToGetLeadSensitiveInfo(string leadId, string token)
        {
            token = token.Replace("/", "_");
            token = token.Replace("+", "-");
            var link = "<a href=" + _configuration.GetSection("Frontend").Value + "lead-sin-details/" + leadId + "/" + token + " target=\"_blank\">Click Here</a>";
            var contact = _contactService.GetAllEntities().Where(x => x.RelatedLeadContactId.Equals(Guid.Parse(leadId))).FirstOrDefault();
            var filterOption = new FilterOptionsModel();
            filterOption.SearchQuery = "LeadId==\"" + leadId + "\"";
            filterOption.Fields= "ServiceOffered";
            var service = _leadServiceOfferedService.GetFilteredEntities(filterOption).Result.FirstOrDefault();

            // No Lead Id
            var htmlContent = _notificationMailService.GetAllEntities().Where(x => x.Name.Equals(NotificationMailEnum.GetSensitiveInfo.ToString())).FirstOrDefault().MailContent;
            htmlContent = Regex.Replace(htmlContent, "{%contact.FirstName%}", contact.FirstName ?? "", RegexOptions.IgnoreCase);
            htmlContent = Regex.Replace(htmlContent, "{%service.Name%}", service.ServiceOffered.Name ?? "", RegexOptions.IgnoreCase);
            htmlContent = Regex.Replace(htmlContent, "{%contact.Phone%}", contact.Phone ?? "", RegexOptions.IgnoreCase);
            htmlContent = Regex.Replace(htmlContent, "{%contact.LastName%}", contact.LastName ?? "", RegexOptions.IgnoreCase);
            htmlContent = Regex.Replace(htmlContent, "{%contact.Email%}", contact.Email ?? "", RegexOptions.IgnoreCase);
            htmlContent = Regex.Replace(htmlContent, "{%info.Link%}", link, RegexOptions.IgnoreCase);            
            var subject = "Additional details required for your "+ service.ServiceOffered.Name + " request with Liberty Edge";
             var result = _notificationMailService.SendNotificationMail(contact.Email, htmlContent, subject);          

            await MarkEmailSent(leadId, true);

            return true;
        }


        /// <summary>
        /// Mark email sent True/False
        /// </summary>
        /// <param name="leadId">LeadId</param>
        /// <param name="isSent">True/False</param>
        /// <returns></returns>
        public async Task<int> MarkEmailSent(string leadId, bool isSent)
        {
            var retVal = 0;
            try
            {
                var lead = _leadRepository.GetAllEntities().Where(x => x.Id == Guid.Parse(leadId)).FirstOrDefault();
                var patchLead = new JsonPatchDocument<Lead>();
                patchLead.Operations.Add(new Operation<Lead> { op = "replace", path = "/IsEmailSent", value = isSent });

                patchLead.ApplyTo(lead);
                
                retVal = await _leadRepository.UpdatePartialEntityAsync(lead, patchLead);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return retVal;

        }

        /// <summary>
        /// Create lead (from widget)
        /// </summary>
        /// <param name="lead">LeadForCreation object</param>
        /// <returns>Newly created lead</returns>
        public async Task<LeadDto> CreateWidgetLead(LeadForCreation lead)
        {
            lead.Id = Guid.NewGuid().ToString();           
            lead.Status = "New";
            var agent = _aspUserService.GetEntityById(lead.AgentId);

            var leadAdmin = _aspNetUserClaimService.GetAllEntities().Where(x => x.ClaimValue == "Lead Admin").FirstOrDefault();

            lead.AssignedTo = leadAdmin.UserId;
            var leadToReturn = await CreateEntityAsync<LeadDto, LeadForCreation>(lead);
            var contactForCreation = new ContactForCreation
            {
                FirstName = lead.FirstName,
                MiddleName = lead.MiddleName,
                LastName = lead.LastName,
                Address = lead.Address,
                City = lead.City,
                Province = lead.Province,
                Zipcode = lead.Zipcode,
                Phone = lead.Phone,
                Email = lead.Email,
                Source = lead.Source,
                ExistingClient = lead.ExistingClient,
                Employment = lead.Employment,
                Position = lead.Position,
                EmploymentDate = lead.EmploymentDate,
                CompanyName = lead.CompanyName,
                Website = lead.Website,
                SalaryFrequency = lead.SalaryFrequency,
                NoOfChildren = lead.NoOfChildren,
                Status = "Lead",
                RelationWithLead = "Self",
                RelatedLeadContactId = Guid.Parse(lead.Id),
                AllowMarketingMessages = lead.AllowMarketingMessages,
                MaritalStatus = lead.MaritalStatus,
                AllowUseOfPersonalInfo = lead.AllowUseOfPersonalInfo,

            };

            var contactToReturn = _contactService.CreateEntityAsync<ContactDto, ContactForCreation>(contactForCreation);
            var leadContactForCreation = new LeadContactForCreation { LeadId = Guid.Parse(leadToReturn.Id), ContactId = Guid.Parse(contactToReturn.Result.Id) };
            var leadContact = await _leadContactService.CreateEntityAsync<LeadContactDto, LeadContactForCreation>(leadContactForCreation);
            var leadAssets = new List<LeadAsset>();
            var leadServicesSelected = new List<LeadServiceOffered>();
            foreach (var asset in lead.SelectedAssetId)
            {
                leadAssets.Add(new LeadAsset { LeadId = Guid.Parse(leadToReturn.Id), AssetId = Guid.Parse(asset.Id), Type = asset.Type, Valuation = asset.Valuation, Description = asset.Description, ContactId = Guid.Parse(contactToReturn.Result.Id) });
            }

            _leadAssetService.CreateBulkEntity(leadAssets);

            foreach (var service in lead.SelectedServiceId)
            {
                leadServicesSelected.Add(new LeadServiceOffered { LeadId = Guid.Parse(leadToReturn.Id), ServiceOfferedId = Guid.Parse(service.Id), Description = service.Description, AssignedStatus = "Interested", ContactId = Guid.Parse(contactToReturn.Result.Id) });
            }

            _leadServiceOfferedService.CreateBulkEntity(leadServicesSelected);

            await _leadServiceOfferedService.SaveChangesAsync();

            var serviceHistory = new HistoryForCreation();
            foreach (var selectedService in leadServicesSelected)
            {
                serviceHistory = new HistoryForCreation();
                serviceHistory.LeadId = leadToReturn.Id;
                serviceHistory.ServiceId = selectedService.Id.ToString();
                serviceHistory.Activity = (int)HistoryEnum.ServiceAdded;
                serviceHistory.UserId = _userInfoService.UserId;

                var historyToReturn = await _historyService.CreateEntityAsync<HistoryDto, HistoryForCreation>(serviceHistory);
            }

            var htmlContent = _notificationMailService.GetAllEntities().Where(x => x.Name.Equals(NotificationMailEnum.LeadCreatedToClient.ToString())).FirstOrDefault().MailContent;
            htmlContent = Regex.Replace(htmlContent, "{%contact.FirstName%}", lead.FirstName ?? "", RegexOptions.IgnoreCase);
            htmlContent = Regex.Replace(htmlContent, "{%lead.Id%}", "No: " + leadToReturn.LeadNumber.ToString() ?? "", RegexOptions.IgnoreCase);
            var subject = "Thank you for connecting with Liberty Edge!";
            _notificationMailService.SendNotificationMail(lead.Email, htmlContent, subject);

            if (agent != null)
            {
                // Send notification mail to agent
                var htmlContentAgent = _notificationMailService.GetAllEntities().Where(x => x.Name.Equals(NotificationMailEnum.LeadCreatedToEmployee.ToString())).FirstOrDefault().MailContent;
                htmlContentAgent = Regex.Replace(htmlContentAgent, "{%lead.agentId%}", agent.FName ?? "", RegexOptions.IgnoreCase);
                htmlContentAgent = Regex.Replace(htmlContentAgent, "{%contact.FirstName%}", lead.FirstName ?? "", RegexOptions.IgnoreCase);
                htmlContentAgent = Regex.Replace(htmlContentAgent, "{%contact.LastName%}", lead.LastName ?? "", RegexOptions.IgnoreCase);
                htmlContentAgent = Regex.Replace(htmlContentAgent, "{%lead.Id%}", leadToReturn.LeadNumber ?? "", RegexOptions.IgnoreCase);
                var subjectAgent = lead.FirstName + " " + lead.LastName + "- LeadId: " + leadToReturn.LeadNumber + " has submitted the lead form";
                _notificationMailService.SendNotificationMail(agent.Email, htmlContentAgent, subjectAgent);
            }
            // Create Lead created history
            var historyForCreation = new HistoryForCreation();
            historyForCreation.Activity = 1;
            historyForCreation.LeadId = leadToReturn.Id;
            historyForCreation.UserId = null;
            try
            {
                await _historyService.CreateEntityAsync<HistoryDto, HistoryForCreation>(historyForCreation);
            }
            catch (Exception ex)
            {
                var m = ex.Message;
            }

            return leadToReturn;
        }


        public async Task<FTCrmDto> CreateFtCrmAndIntegration(FTCrmForCreation ftcrm)
        {
            var existingFtCrm = _fTCrmService.GetAllEntities().Where(x => x.LeadServiceId == ftcrm.LeadServiceId).FirstOrDefault();
            FTCrmDto ftcrmToReturn;
            if (existingFtCrm == null)
            {
                ftcrmToReturn = await _fTCrmService.CreateEntityAsync<FTCrmDto, FTCrmForCreation>(ftcrm);
            }
            else 
            {
                var ftcrmlocal = await _fTCrmService.GetEntityByIdAsync(existingFtCrm.Id);
                ftcrmToReturn = new FTCrmDto() 
                { 
                    Id= ftcrmlocal.Id,
                    Amount = ftcrmlocal.Amount,
                    FtCrmUniqueId = ftcrmlocal.FtCrmUniqueId,
                    LeadServiceId = ftcrmlocal.LeadServiceId,
                    Reason = ftcrmlocal.Reason,
                    Status = ftcrmlocal.Status,
                    
                };
            }
            //create a show in db.            
                
            
            //Code to trigger Family Tax CRM
            var filterOption = new FilterOptionsModel();
            filterOption.PageSize = 10000;
            filterOption.SearchQuery = "Id==\"" + ftcrm.LeadServiceId + "\"";

            var leadServiceOffered = await _leadServiceOfferedService.GetFilteredEntities(filterOption);
            var leadService = leadServiceOffered.Where(x => x.Id.Equals(ftcrm.LeadServiceId)).FirstOrDefault();

            var relationShipManager = _aspUserService.GetEntityById(leadService.Lead.AssignedTo);

            var salesRep = _aspUserService.GetEntityById(leadService.Lead.AgentId);

            var contact = _contactService.GetAllEntities().Where(x => x.RelatedLeadContactId.Equals(leadService.Lead.Id) && x.RelationWithLead.Equals("Self")).FirstOrDefault();

            leadService.AssignedTo = leadService.ServiceOffered.ServiceManagerId;

            // _leadServiceOfferedService.UpdateEntity(leadService);

            var assignedToUser = _aspUserService.GetEntityById(leadService.AssignedTo);

            //FTCRM Api call with retry for 3 times
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Add("Auth-Key", "8D65A4647FED05E4C13DF30B5E8235DC");
            var formContent = new FormUrlEncodedContent(new[]
            {
                    new KeyValuePair<string, string>("first_name", contact.FirstName),
                    new KeyValuePair<string, string>("last_name", contact.LastName),
                    new KeyValuePair<string, string>("phone", contact.Phone),
                    new KeyValuePair<string, string>("email", contact.Email),
                    new KeyValuePair<string, string>("sin", contact.Sin),

                });
            var pauseBetweenFailures = TimeSpan.FromSeconds(2);

            var retryPolicy = Policy
                .Handle<HttpRequestException>()
                .WaitAndRetryAsync(3, retryAttempt =>
                    TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)));

            var ftcrmResponse = new FtCrmResponse();
            await retryPolicy.ExecuteAsync(async () =>
            {
                var uri = "https://fintaxcrm.com/fapi/onboard";
                var response = await httpClient
                  .PostAsync(uri, formContent);
                var data = await response.Content.ReadAsStringAsync();
                ftcrmResponse = JsonConvert.DeserializeObject<FtCrmResponse>(data);
                response.EnsureSuccessStatusCode();
            });

            var ftcrmToUpdate = new FTCrmForUpdate();
            if (ftcrmResponse.status == 201 && ftcrmResponse.success.Equals("A New Opp has been created") && Int32.Parse(ftcrmResponse.data.opp_id)>0)
            {
                ftcrmToUpdate.Id = ftcrmToReturn.Id;
                ftcrmToUpdate.FtCrmUniqueId = ftcrmResponse.data.opp_id;
                ftcrmToUpdate.LeadServiceId = ftcrmToReturn.LeadServiceId;
                ftcrmToUpdate.Status = "Successfully Intiated";
                ftcrmToUpdate.Amount = 0;
                ftcrmToUpdate.Reason = ftcrmResponse.success;

                //Update LeadServiceOffered
                var patchLeadServiceOffered = new JsonPatchDocument<LeadServiceOffered>();
                patchLeadServiceOffered.Operations.Add(new Operation<LeadServiceOffered> { op = "replace", path = "/AssignedTo", value = leadService.ServiceOffered.ServiceManagerId });
                patchLeadServiceOffered.Operations.Add(new Operation<LeadServiceOffered> { op = "replace", path = "/AssignedStatus", value = "Working" });
                LeadServiceOffered _leadServiceOffered = new LeadServiceOffered();
                patchLeadServiceOffered.ApplyTo(_leadServiceOffered);
                _leadServiceOffered.Id = leadService.Id;
                _leadServiceOfferedService.UpdatePartialEntityAsync(_leadServiceOffered, patchLeadServiceOffered);

                //Add task for service
                TaskForCreation serviceTask = CreateSericeTask(leadService);
                var taskToReturn = await _taskService.CreateEntityAsync<TaskDto, TaskForCreation>(serviceTask);

                // Save Ftcrm
                await _fTCrmService.UpdateEntityAsync(ftcrmToReturn.Id, ftcrmToUpdate);

                // Create Success History
                var historyForCreation = new HistoryForCreation();
                historyForCreation.Activity = 15;
                historyForCreation.LeadId = leadService.Lead.Id.ToString();
                historyForCreation.ServiceId = leadService.Id.ToString();
                historyForCreation.TagData = JsonConvert.SerializeObject(new[] { new { key = ftcrmToUpdate.Reason, isNotification = true } }.ToList());
                await _historyService.CreateEntityAsync<HistoryDto, HistoryForCreation>(historyForCreation);
               
                //Send Notification Mails
                SendEmailsOnSuccess(leadService, relationShipManager, assignedToUser, salesRep);

            }
            else
            {
                ftcrmToUpdate.Id = ftcrmToReturn.Id;
                ftcrmToUpdate.FtCrmUniqueId = null;
                ftcrmToUpdate.LeadServiceId = ftcrmToReturn.LeadServiceId;
                ftcrmToUpdate.Status = "Initialization Failed";
                ftcrmToUpdate.Amount = 0;
                ftcrmToUpdate.Reason = ftcrmResponse.error;

                // Save Ftcrm
                await _fTCrmService.UpdateEntityAsync(ftcrmToReturn.Id, ftcrmToUpdate);

                //Create Faliure History
                var historyForCreation = new HistoryForCreation();
                historyForCreation.Activity = 16;
                historyForCreation.LeadId = leadService.Lead.Id.ToString();
                historyForCreation.ServiceId = leadService.Id.ToString();
                historyForCreation.TagData = JsonConvert.SerializeObject(new[] { new { key = ftcrmToUpdate.Reason, isNotification = true } }.ToList());
                await _historyService.CreateEntityAsync<HistoryDto, HistoryForCreation>(historyForCreation);

                // Send faliure mail
                SendEmailsOnFaliure(leadService, relationShipManager, assignedToUser, ftcrmResponse.error);
            }
            
            return ftcrmToReturn;             
        }

        private static TaskForCreation CreateSericeTask(dynamic leadService)
        {
            return new TaskForCreation()
            {
                LeadId = leadService.Lead.Id.ToString(),
                Title = "Service " + leadService.ServiceOffered.Name + " of Lead " + leadService.Contact.FirstName + " " + leadService.Contact.LastName + " has been assigned to you.",
                Description = "Service has been assigned to you.",
                Date = DateTime.Now,
                UserId = leadService.ServiceOffered.ServiceManagerId,
                IsCompleted = false,
                IsImportant = true,
                IsMarked = false,
                ServiceId = leadService.Id.ToString()
            };
        }

        #endregion


        #region PRIVATE METHODS
        private void SendEmailsOnFaliure(dynamic leadService, dynamic relationShipManager, dynamic assignedToUser, string reason)
        {       
            var htmlContentServiceAssigned = _notificationMailService.GetAllEntities().Where(x => x.Name.Equals(NotificationMailEnum.IntegrationFaliureToServiceTeam.ToString())).FirstOrDefault().MailContent;
            htmlContentServiceAssigned = Regex.Replace(htmlContentServiceAssigned, "{%service.AssignedTo%}", relationShipManager.FName ?? "", RegexOptions.IgnoreCase);
            htmlContentServiceAssigned = Regex.Replace(htmlContentServiceAssigned, "{%contact.FirstName%}", leadService.Contact.FirstName ?? "", RegexOptions.IgnoreCase);
            htmlContentServiceAssigned = Regex.Replace(htmlContentServiceAssigned, "{%contact.LastName%}", leadService.Contact.LastName ?? "", RegexOptions.IgnoreCase);
            htmlContentServiceAssigned = Regex.Replace(htmlContentServiceAssigned, "{%contact.Email%}", leadService.Contact.Email ?? "", RegexOptions.IgnoreCase);
            htmlContentServiceAssigned = Regex.Replace(htmlContentServiceAssigned, "{%ftcrm.Reason%}", reason ?? "", RegexOptions.IgnoreCase);
            var subjectServiceAssigned = "10-year tax review update from Family Tax CRM failed for" + " - Lead Id: " + leadService.Lead.LeadNumber;
            _notificationMailService.SendNotificationMail(relationShipManager.Email, htmlContentServiceAssigned, subjectServiceAssigned,leadService.Lead.Id.ToString(),leadService.Id.ToString(),"10-year tax Integration Failed. Mail send to Relationship Manager");
        }


        private async Task<bool> SendEmailsOnSuccess(dynamic leadService, dynamic relationShipManager, dynamic assignedToUser, dynamic salesRep)
        {
           
            var res = false;
            try
            {
                {
                    // Mail to Customer
                    var htmlContent = _notificationMailService.GetAllEntities().Where(x => x.Name.Equals(NotificationMailEnum.IntegrationTriggeredToClient.ToString())).FirstOrDefault().MailContent;
                    htmlContent = Regex.Replace(htmlContent, "{%contact.FirstName%}", leadService.Contact.FirstName ?? "", RegexOptions.IgnoreCase);
                    htmlContent = Regex.Replace(htmlContent, "{%lead.assignedTo%}", relationShipManager.FName ?? "", RegexOptions.IgnoreCase);
                    var subject = "Processing of your " + leadService.ServiceOffered.Name + " request with Liberty Edge";
                    var res1 = _notificationMailService.SendNotificationMail(leadService.Contact.Email, htmlContent, subject, leadService.Lead.Id.ToString(), leadService.Id.ToString(), "10-year tax Integration Succeeded. Mail sent to Customer");
                   
                    // Mail to Product Manager of Assignment
                    var htmlContentServiceAssigned = (from mail in _notificationMailService.GetAllEntities()
                                                      where mail.Name.Equals(NotificationMailEnum.ServiceAssigned.ToString())
                                                      select mail.MailContent).FirstOrDefault();
                    htmlContentServiceAssigned = Regex.Replace(htmlContentServiceAssigned, "{%service.assignedTo%}", assignedToUser.FName ?? "", RegexOptions.IgnoreCase);
                    htmlContentServiceAssigned = Regex.Replace(htmlContentServiceAssigned, "{%service.Name%}", leadService.ServiceOffered.Name ?? "", RegexOptions.IgnoreCase);
                    htmlContentServiceAssigned = Regex.Replace(htmlContentServiceAssigned, "{%contact.FirstName%}", leadService.Contact.FirstName ?? "", RegexOptions.IgnoreCase);
                    htmlContentServiceAssigned = Regex.Replace(htmlContentServiceAssigned, "{%contact.LastName%}", leadService.Contact.LastName ?? "", RegexOptions.IgnoreCase);
                    htmlContentServiceAssigned = Regex.Replace(htmlContentServiceAssigned, "{%lead.Id%}", "Lead Id: " + Convert.ToString(leadService.Lead.LeadNumber) ?? "", RegexOptions.IgnoreCase);
                    htmlContentServiceAssigned = Regex.Replace(htmlContentServiceAssigned, "{%lead.Link%}", _configuration.GetSection("Frontend").Value + "lead/edit/" + leadService.Lead.Id.ToString() ?? "", RegexOptions.IgnoreCase);

                    var subjectServiceAssigned = "A new " + leadService.ServiceOffered.Name + " request for " + leadService.Contact.FirstName + " " + leadService.Contact.LastName + " - Lead Id: " + leadService.Lead.LeadNumber + " has been assigned to you";
                    var res2 = _notificationMailService.SendNotificationMail(assignedToUser.Email, htmlContentServiceAssigned, subjectServiceAssigned, leadService.Lead.Id.ToString(), leadService.Id.ToString(), "Service Assigned mail to Product Manager");


                    // Mail to Relationship Manager
                    var htmlContentIntegrationTriggeredToLeadEmployee = (from mail in _notificationMailService.GetAllEntities()
                                                                         where mail.Name.Equals(NotificationMailEnum.IntegrationTriggeredToEmployee.ToString())
                                                                         select mail.MailContent).FirstOrDefault();
                    var htmlContentIntegrationTriggeredToSalesRep = htmlContentIntegrationTriggeredToLeadEmployee;

                    htmlContentIntegrationTriggeredToLeadEmployee = Regex.Replace(htmlContentIntegrationTriggeredToLeadEmployee, "{%service.assignedTo%}", relationShipManager.FName ?? "", RegexOptions.IgnoreCase);
                    htmlContentIntegrationTriggeredToLeadEmployee = Regex.Replace(htmlContentIntegrationTriggeredToLeadEmployee, "{%service.Name%}", leadService.ServiceOffered.Name ?? "", RegexOptions.IgnoreCase);
                    htmlContentIntegrationTriggeredToLeadEmployee = Regex.Replace(htmlContentIntegrationTriggeredToLeadEmployee, "{%contact.FirstName%}", leadService.Contact.FirstName ?? "", RegexOptions.IgnoreCase);
                    htmlContentIntegrationTriggeredToLeadEmployee = Regex.Replace(htmlContentIntegrationTriggeredToLeadEmployee, "{%contact.LastName%}", leadService.Contact.LastName ?? "", RegexOptions.IgnoreCase);
                    htmlContentIntegrationTriggeredToLeadEmployee = Regex.Replace(htmlContentIntegrationTriggeredToLeadEmployee, "{%lead.Id%}", "Lead Id: " + Convert.ToString(leadService.Lead.LeadNumber) ?? "", RegexOptions.IgnoreCase);
                    var subjecthtmlContentIntegrationTriggeredToLeadEmployee = leadService.Contact.FirstName + " " + leadService.Contact.LastName + " - Lead Id: " + leadService.Lead.LeadNumber + " has submitted the additional details for their " + leadService.ServiceOffered.Name + " request";
                    var res3 = _notificationMailService.SendNotificationMail(relationShipManager.Email, htmlContentIntegrationTriggeredToLeadEmployee, subjecthtmlContentIntegrationTriggeredToLeadEmployee, leadService.Lead.Id.ToString(), leadService.Id.ToString(), "10-year tax Integration Succeeded. Mail to Relationship Manager");

                    if (salesRep != null)
                    {
                        // Mail to Ambassador
                        htmlContentIntegrationTriggeredToSalesRep = Regex.Replace(htmlContentIntegrationTriggeredToSalesRep, "{%service.assignedTo%}", salesRep.FName ?? "", RegexOptions.IgnoreCase);
                        htmlContentIntegrationTriggeredToSalesRep = Regex.Replace(htmlContentIntegrationTriggeredToSalesRep, "{%service.Name%}", leadService.ServiceOffered.Name ?? "", RegexOptions.IgnoreCase);
                        htmlContentIntegrationTriggeredToSalesRep = Regex.Replace(htmlContentIntegrationTriggeredToSalesRep, "{%contact.FirstName%}", leadService.Contact.FirstName ?? "", RegexOptions.IgnoreCase);
                        htmlContentIntegrationTriggeredToSalesRep = Regex.Replace(htmlContentIntegrationTriggeredToSalesRep, "{%contact.LastName%}", leadService.Contact.LastName ?? "", RegexOptions.IgnoreCase);
                        htmlContentIntegrationTriggeredToSalesRep = Regex.Replace(htmlContentIntegrationTriggeredToSalesRep, "{%lead.Id%}", "Lead Id: " + Convert.ToString(leadService.Lead.LeadNumber) ?? "", RegexOptions.IgnoreCase);
                        var subjecthtmlContentIntegrationTriggeredToSalesRep = leadService.Contact.FirstName + " " + leadService.Contact.LastName + " - Lead Id: " + leadService.Lead.LeadNumber + " has submitted the additional details for their " + leadService.ServiceOffered.Name + " request";
                        var res4 = _notificationMailService.SendNotificationMail(salesRep.Email, htmlContentIntegrationTriggeredToSalesRep, subjecthtmlContentIntegrationTriggeredToSalesRep, leadService.Lead.Id.ToString(), leadService.Id.ToString(), "10-year tax Integration Succeeded. Mail to Ambassador");
                    }
                    res = true;
                }
            }
            catch (Exception ex)
            {
                var me = ex.Message;
            }
            return res;
        }
        #endregion


        #region OVERRIDDEN IMPLEMENTATION

        public override Dictionary<string, PropertyMappingValue> GetPropertyMapping()
        {
            return new Dictionary<string, PropertyMappingValue>(StringComparer.OrdinalIgnoreCase)
                    {
                        { "Id", new PropertyMappingValue(new List<string>() { "Id" } ) },
                        { "Status", new PropertyMappingValue(new List<string>() { "Status" } ) },
                        { "AgentId", new PropertyMappingValue(new List<string>() { "AgentId" } ) },
                        { "AssignedTo", new PropertyMappingValue(new List<string>() { "AssignedTo" } ) },
                        { "CustomerInsight", new PropertyMappingValue(new List<string>() { "CustomerInsight" } ) },
                        { "AssignedStore", new PropertyMappingValue(new List<string>() { "AssignedStore" } ) },
                        { "UpdatedOn", new PropertyMappingValue(new List<string>() { "UpdatedOn" } ) },
                        { "Contacts", new PropertyMappingValue(new List<string>() { "Contacts" } ) },
            };
        }

        public override string GetDefaultOrderByColumn()
        {
            return "UpdatedOn";
        }

        public override string GetDefaultFieldsToSelect()
        {
            return "Id,Status,AgentId,AssignedTo,CustomerInsight,AssignedStore,UpdatedOn,Contacts,LeadNumber,User,Store";
        }

        #endregion
    }
}
