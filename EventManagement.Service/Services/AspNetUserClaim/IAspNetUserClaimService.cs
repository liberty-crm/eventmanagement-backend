using System;
using System.Threading.Tasks;
using EventManagement.Dto;
using EventManagement.Domain.Entities;
using FinanaceManagement.API.Models;
using System.Collections.Generic;

namespace EventManagement.Service
{
    public interface IAspNetUserClaimService : IService<AspNetUserClaim, int>
    {
        string GetRoleByUserId(string userId);

        AspNetUserClaim GetClaimByUserId(string userId);

        /// <summary>
        /// Get all roles of user by user id
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <returns>List of role</returns>
        List<string> GetAllRolesByUserId(string userId);
    }
}
