using System;
using System.Collections.Generic;
using EventManagement.Domain;
using EventManagement.Domain.Entities;
using EventManagement.Dto;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using System.Linq;
using Newtonsoft.Json;
using FinanaceManagement.API.Models;

namespace EventManagement.Service
{
    public class AspNetUserClaimService : ServiceBase<AspNetUserClaim, int>, IAspNetUserClaimService
    {

        #region PRIVATE MEMBERS

        private readonly IAspNetUserClaimRepository _aspnetuserclaimRepository;
        private readonly IConfiguration _configuration;
        #endregion


        #region CONSTRUCTOR

        public AspNetUserClaimService(IAspNetUserClaimRepository aspnetuserclaimRepository, ILogger<AspNetUserClaimService> logger, IConfiguration configuration) : base(aspnetuserclaimRepository, logger)
        {
            _aspnetuserclaimRepository = aspnetuserclaimRepository;
            _configuration = configuration;
        }

        #endregion


        #region PUBLIC MEMBERS   
        public string GetRoleByUserId(string userId)
        {
            try
            {
                return GetAllEntities().Where(x => x.UserId.Equals(userId) && x.ClaimType == "role").FirstOrDefault()?.ClaimValue;
            }
            catch (Exception ex)
            { return null; }
        }

        /// <summary>
        /// Get all roles of user by user id
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <returns>List of role</returns>
        public List<string> GetAllRolesByUserId(string userId)
        {
            try
            {
                return GetAllEntities().Where(x => x.UserId.Equals(userId) && x.ClaimType == "role")
                    .Select(x => x.ClaimValue).ToList();
            }
            catch (Exception ex)
            { return null; }
        }

        public AspNetUserClaim GetClaimByUserId(string userId)
        {
            return GetAllEntities().Where(x => x.UserId.Equals(userId) && x.ClaimType == "role").FirstOrDefault();
        }

        #endregion


        #region OVERRIDDEN IMPLEMENTATION

        public override Dictionary<string, PropertyMappingValue> GetPropertyMapping()
        {
            return new Dictionary<string, PropertyMappingValue>(StringComparer.OrdinalIgnoreCase)
                    {
                        { "Id", new PropertyMappingValue(new List<string>() { "Id" } ) },
                        { "ClaimType", new PropertyMappingValue(new List<string>() { "ClaimType" } )},
                        { "ClaimValue", new PropertyMappingValue(new List<string>() { "ClaimValue" } )},
                        { "UserId", new PropertyMappingValue(new List<string>() { "UserId" } )},
                    };
        }

        public override string GetDefaultOrderByColumn()
        {
            return "Id";
        }

        public override string GetDefaultFieldsToSelect()
        {
            return "Id,ClaimType,ClaimValue,UserId,User";
        }

        #endregion
    }
}
