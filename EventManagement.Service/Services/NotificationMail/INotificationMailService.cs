using System;
using System.Threading.Tasks;
using EventManagement.Dto;
using EventManagement.Domain.Entities;

namespace EventManagement.Service
{
    public interface INotificationMailService : IService<NotificationMail, Guid>
    {
        bool SendNotificationMail(string toEmail, string htmlContent, string subject,string leadId = null, string serviceId = null, string extratext = null);
    }
}
