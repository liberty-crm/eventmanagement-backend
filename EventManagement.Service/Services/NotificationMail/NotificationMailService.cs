using System;
using System.Collections.Generic;
using EventManagement.Domain;
using EventManagement.Domain.Entities;
using EventManagement.Dto;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using System.Linq;
using Newtonsoft.Json;
using SendGrid.Helpers.Mail;
using SendGrid;

namespace EventManagement.Service
{
    public class NotificationMailService : ServiceBase<NotificationMail, Guid>, INotificationMailService
    {

        #region PRIVATE MEMBERS

        private readonly INotificationMailRepository _notificationmailRepository;
        private readonly IConfiguration _configuration;        
        private readonly IHistoryService _historyService;
        #endregion


        #region CONSTRUCTOR

        public NotificationMailService(INotificationMailRepository notificationmailRepository, ILogger<NotificationMailService> logger, IConfiguration configuration, IHistoryService historyService) : base(notificationmailRepository, logger)
        {
            _notificationmailRepository = notificationmailRepository;
            _configuration = configuration;
            _historyService = historyService;
            
        }

        #endregion


        #region PUBLIC MEMBERS   
        public bool SendNotificationMail(string toEmail, string htmlContent, string subject,string leadId = null,string serviceId = null, string extratext=null)
        {
            var res = false;
            {
                var client = new SendGridClient(_configuration.GetSection("SendgridApi").Value);
                var msg = MailHelper.CreateSingleEmail(new EmailAddress("support@libertyfinancialgroup.ca"), new EmailAddress(toEmail),
                    subject, "", htmlContent);
                var response = client.SendEmailAsync(msg);

                if (leadId != null)
                {
                    var historyForCreation = new HistoryForCreation();
                    historyForCreation.Activity = 14;
                    historyForCreation.LeadId = leadId;
                    historyForCreation.ServiceId = serviceId;                   
                    historyForCreation.TagData = JsonConvert.SerializeObject(new[] { new { key = extratext, isNotification = true } }.ToList());                   
                    var historyId = _historyService.CreateEntityAsync<HistoryDto, HistoryForCreation>(historyForCreation).Result.Id;
                    res = true;
                    
                }
            }
            return res;
        }

        #endregion


        #region OVERRIDDEN IMPLEMENTATION

        public override Dictionary<string, PropertyMappingValue> GetPropertyMapping()
        {
            return new Dictionary<string, PropertyMappingValue>(StringComparer.OrdinalIgnoreCase)
                    {
                        { "Id", new PropertyMappingValue(new List<string>() { "Id" } ) },
                        { "Name", new PropertyMappingValue(new List<string>() { "Name" } )},
                    };
        }

        public override string GetDefaultOrderByColumn()
        {
            return "UpdatedOn";
        }

        public override string GetDefaultFieldsToSelect()
        {
            return "Id,Name";
        }

        #endregion
    }
}
